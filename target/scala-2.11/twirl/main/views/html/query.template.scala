
package views.html

import play.twirl.api._


     object query_Scope0 {
import java.util._

import controllers._
import models._
import views.html._

import scala.collection.JavaConversions._

class query extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template18[List[String],ArrayList[Taxonomy],List[Biolsource],List[List[String]],HashSet[String],HashSet[String],ArrayList[Proteins],ArrayList[Tissue],List[Tissue],List[GlycobaseSource],List[String],Integer,List[String],Integer,List[String],Integer,HashSet[String],ArrayList[Proteins],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(taxonomy: List[String], taxon: ArrayList[Taxonomy],  biolsource: List[Biolsource], groupTax: List[List[String]], sources: HashSet[String], proteins: HashSet[String], proteinList: ArrayList[Proteins], tissueList: ArrayList[Tissue], tissueTerms: List[Tissue], glycobasePerturbation: List[GlycobaseSource], output: List[String], count: Integer, outputtissue: List[String], counttissue: Integer, outputprotein: List[String], countprotein: Integer, proteinaccession: HashSet[String],  accsearch: ArrayList[Proteins]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.514*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""

"""),format.raw/*5.1*/("""<script>
  $(function() """),format.raw/*6.16*/("""{"""),format.raw/*6.17*/("""
  """),format.raw/*7.3*/("""$(".glycanType .btn").click(function() """),format.raw/*7.42*/("""{"""),format.raw/*7.43*/("""
    """),format.raw/*8.5*/("""$("#glycanType").val($(this).text());
  """),format.raw/*9.3*/("""}"""),format.raw/*9.4*/("""); """),format.raw/*9.7*/("""}"""),format.raw/*9.8*/("""); 
</script>

<script>
$(document).ready(function() """),format.raw/*13.30*/("""{"""),format.raw/*13.31*/("""
"""),format.raw/*14.1*/("""$('#myTabBar a[data-toggle="tab"]').on('shown', function (e) """),format.raw/*14.62*/("""{"""),format.raw/*14.63*/("""
  """),format.raw/*15.3*/("""e.target // activated tab
  var filterstring = e.target.toString();
  var filtervalue =filterstring.split('#');
  var name = filtervalue[1];

  if(name == "filter3")"""),format.raw/*20.24*/("""{"""),format.raw/*20.25*/("""
   """),format.raw/*21.4*/("""$('.glycobase').show();
  """),format.raw/*22.3*/("""}"""),format.raw/*22.4*/("""
  """),format.raw/*23.3*/("""if(name != "filter3")"""),format.raw/*23.24*/("""{"""),format.raw/*23.25*/("""
   """),format.raw/*24.4*/("""$('.glycobase').hide();
  """),format.raw/*25.3*/("""}"""),format.raw/*25.4*/("""

  """),format.raw/*27.3*/("""if(name == "filter1")"""),format.raw/*27.24*/("""{"""),format.raw/*27.25*/("""
   """),format.raw/*28.4*/("""$('.glycosuiteResult').show();
  """),format.raw/*29.3*/("""}"""),format.raw/*29.4*/("""

  """),format.raw/*31.3*/("""if(name != "filter1")"""),format.raw/*31.24*/("""{"""),format.raw/*31.25*/("""
   """),format.raw/*32.4*/("""$('.glycosuiteResult').hide();
  """),format.raw/*33.3*/("""}"""),format.raw/*33.4*/("""

  
"""),format.raw/*36.1*/("""}"""),format.raw/*36.2*/(""");
"""),format.raw/*37.1*/("""}"""),format.raw/*37.2*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*41.35*/("""{"""),format.raw/*41.36*/("""
        """),format.raw/*42.9*/("""$("#e12").select2("""),format.raw/*42.27*/("""{"""),format.raw/*42.28*/("""
          """),format.raw/*43.11*/("""placeholder: "Select SwissProt ID...",
          allowClear: true,
        minimumInputLength: 1,
        """),format.raw/*46.9*/("""}"""),format.raw/*46.10*/(""");
        """),format.raw/*47.9*/("""}"""),format.raw/*47.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*51.35*/("""{"""),format.raw/*51.36*/("""
        """),format.raw/*52.9*/("""$("#e13").select2("""),format.raw/*52.27*/("""{"""),format.raw/*52.28*/("""
          """),format.raw/*53.11*/("""placeholder: "Select SwissProt ID...",
          allowClear: true,
        minimumInputLength: 1,
        """),format.raw/*56.9*/("""}"""),format.raw/*56.10*/(""");
        """),format.raw/*57.9*/("""}"""),format.raw/*57.10*/(""");
</script>


<script>
      $(document).ready(function()"""),format.raw/*62.35*/("""{"""),format.raw/*62.36*/("""
        """),format.raw/*63.9*/("""$("#e1").select2("""),format.raw/*63.26*/("""{"""),format.raw/*63.27*/("""
          """),format.raw/*64.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	minimumInputLength: 1,
        """),format.raw/*67.9*/("""}"""),format.raw/*67.10*/(""");
	"""),format.raw/*68.2*/("""}"""),format.raw/*68.3*/(""");
</script>
<script>
      $(document).ready(function()"""),format.raw/*71.35*/("""{"""),format.raw/*71.36*/("""
        """),format.raw/*72.9*/("""$("#e2").select2("""),format.raw/*72.26*/("""{"""),format.raw/*72.27*/("""
          """),format.raw/*73.11*/("""placeholder: "Select multiple tissues... ",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*76.9*/("""}"""),format.raw/*76.10*/(""");
        """),format.raw/*77.9*/("""}"""),format.raw/*77.10*/(""");
</script>
<script>
      $(document).ready(function()"""),format.raw/*80.35*/("""{"""),format.raw/*80.36*/("""
        """),format.raw/*81.9*/("""$("#e3").select2("""),format.raw/*81.26*/("""{"""),format.raw/*81.27*/("""
          """),format.raw/*82.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*85.9*/("""}"""),format.raw/*85.10*/(""");
        """),format.raw/*86.9*/("""}"""),format.raw/*86.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*90.35*/("""{"""),format.raw/*90.36*/("""
        """),format.raw/*91.9*/("""$("#e4").select2("""),format.raw/*91.26*/("""{"""),format.raw/*91.27*/("""
          """),format.raw/*92.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*95.9*/("""}"""),format.raw/*95.10*/(""");
        """),format.raw/*96.9*/("""}"""),format.raw/*96.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*100.35*/("""{"""),format.raw/*100.36*/("""
        """),format.raw/*101.9*/("""$("#e5").select2("""),format.raw/*101.26*/("""{"""),format.raw/*101.27*/("""
          """),format.raw/*102.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*105.9*/("""}"""),format.raw/*105.10*/(""");
        """),format.raw/*106.9*/("""}"""),format.raw/*106.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*110.35*/("""{"""),format.raw/*110.36*/("""
        """),format.raw/*111.9*/("""$("#e6").select2("""),format.raw/*111.26*/("""{"""),format.raw/*111.27*/("""
          """),format.raw/*112.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*115.9*/("""}"""),format.raw/*115.10*/(""");
        """),format.raw/*116.9*/("""}"""),format.raw/*116.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*120.35*/("""{"""),format.raw/*120.36*/("""
        """),format.raw/*121.9*/("""$("#e7").select2("""),format.raw/*121.26*/("""{"""),format.raw/*121.27*/("""
          """),format.raw/*122.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*125.9*/("""}"""),format.raw/*125.10*/(""");
        """),format.raw/*126.9*/("""}"""),format.raw/*126.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*130.35*/("""{"""),format.raw/*130.36*/("""
        """),format.raw/*131.9*/("""$("#e8").select2("""),format.raw/*131.26*/("""{"""),format.raw/*131.27*/("""
          """),format.raw/*132.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*135.9*/("""}"""),format.raw/*135.10*/(""");
        """),format.raw/*136.9*/("""}"""),format.raw/*136.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*140.35*/("""{"""),format.raw/*140.36*/("""
        """),format.raw/*141.9*/("""$("#e9").select2("""),format.raw/*141.26*/("""{"""),format.raw/*141.27*/("""
          """),format.raw/*142.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*145.9*/("""}"""),format.raw/*145.10*/(""");
        """),format.raw/*146.9*/("""}"""),format.raw/*146.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*150.35*/("""{"""),format.raw/*150.36*/("""
        """),format.raw/*151.9*/("""$("#e10").select2("""),format.raw/*151.27*/("""{"""),format.raw/*151.28*/("""
          """),format.raw/*152.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
          minimumInputLength: 1,
        """),format.raw/*155.9*/("""}"""),format.raw/*155.10*/(""");
        """),format.raw/*156.9*/("""}"""),format.raw/*156.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*160.35*/("""{"""),format.raw/*160.36*/("""
        """),format.raw/*161.9*/("""$("#e11").select2("""),format.raw/*161.27*/("""{"""),format.raw/*161.28*/("""
          """),format.raw/*162.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*165.9*/("""}"""),format.raw/*165.10*/(""");
        """),format.raw/*166.9*/("""}"""),format.raw/*166.10*/(""");
</script>

<script>
        $(function() """),format.raw/*170.22*/("""{"""),format.raw/*170.23*/("""
        """),format.raw/*171.9*/("""var myArray = new Array();
        """),_display_(/*172.10*/for(a <- taxonomy) yield /*172.28*/{_display_(Seq[Any](format.raw/*172.29*/("""
                """),format.raw/*173.17*/("""myArray.push(""""),_display_(/*173.32*/a),format.raw/*173.33*/("""");
        """)))}),format.raw/*174.10*/("""
	"""),format.raw/*175.2*/("""myArray.sort();
        options = '';
        for (var i = 0; i < myArray.length; i++) """),format.raw/*177.50*/("""{"""),format.raw/*177.51*/("""
                """),format.raw/*178.17*/("""options += '<option value="' + myArray[i] + '">' + myArray[i] + '</option>';
        """),format.raw/*179.9*/("""}"""),format.raw/*179.10*/("""
        """),format.raw/*180.9*/("""$('#e1').html(options);
	$('#e5').html(options);
	$('#e7').html(options);
	$('#e9').html(options);
        """),format.raw/*184.9*/("""}"""),format.raw/*184.10*/(""");
</script>

<script>
        $(function() """),format.raw/*188.22*/("""{"""),format.raw/*188.23*/("""
        """),format.raw/*189.9*/("""var myAcc = new Array();
        """),_display_(/*190.10*/for(a <- proteinaccession) yield /*190.36*/{_display_(Seq[Any](format.raw/*190.37*/("""
                """),format.raw/*191.17*/("""myAcc.push(""""),_display_(/*191.30*/a),format.raw/*191.31*/("""");
        """)))}),format.raw/*192.10*/("""
        """),format.raw/*193.9*/("""myAcc.sort();
        options = '';
        for (var i = 0; i < myAcc.length; i++) """),format.raw/*195.48*/("""{"""),format.raw/*195.49*/("""
                """),format.raw/*196.17*/("""options += '<option value="' + myAcc[i] + '">' + myAcc[i] + '</option>';
        """),format.raw/*197.9*/("""}"""),format.raw/*197.10*/("""
	"""),format.raw/*198.2*/("""$('#e12').html(options);
	$('#e13').html(options);
	"""),format.raw/*200.2*/("""}"""),format.raw/*200.3*/(""");
</script>

<script>
	$(function() """),format.raw/*204.15*/("""{"""),format.raw/*204.16*/("""
        """),format.raw/*205.9*/("""var mySources = new Array();
	"""),_display_(/*206.3*/for(s <- sources) yield /*206.20*/{_display_(Seq[Any](format.raw/*206.21*/("""
		"""),format.raw/*207.3*/("""mySources.push(""""),_display_(/*207.20*/s),format.raw/*207.21*/("""");
	""")))}),format.raw/*208.3*/("""
	"""),format.raw/*209.2*/("""mySources.sort();
	options = '';
	for (var i = 0; i < mySources.length; i++) """),format.raw/*211.45*/("""{"""),format.raw/*211.46*/("""
		"""),format.raw/*212.3*/("""options += '<option value="' + mySources[i] + '">' + mySources[i] + '</option>';
        """),format.raw/*213.9*/("""}"""),format.raw/*213.10*/("""
	"""),format.raw/*214.2*/("""$('#e2').html(options);
	$('#e4').html(options);
	$('#e8').html(options);
	$('#e10').html(options);
	"""),format.raw/*218.2*/("""}"""),format.raw/*218.3*/(""");
</script>

<script>
        $(function() """),format.raw/*222.22*/("""{"""),format.raw/*222.23*/("""
        """),format.raw/*223.9*/("""var myProteins = new Array();
        """),_display_(/*224.10*/for(s <- proteins ) yield /*224.29*/{_display_(Seq[Any](format.raw/*224.30*/("""
                """),format.raw/*225.17*/("""myProteins.push(""""),_display_(/*225.35*/s),format.raw/*225.36*/("""");
        """)))}),format.raw/*226.10*/("""
        """),format.raw/*227.9*/("""myProteins.sort();
        options = '';
        for (var i = 0; i < myProteins.length; i++) """),format.raw/*229.53*/("""{"""),format.raw/*229.54*/("""
                """),format.raw/*230.17*/("""options += '<option value="' + myProteins[i] + '">' + myProteins[i] + '</option>';
        """),format.raw/*231.9*/("""}"""),format.raw/*231.10*/("""
        """),format.raw/*232.9*/("""$('#e3').html(options);
	$('#e6').html(options);
	$('#e11').html(options);
        """),format.raw/*235.9*/("""}"""),format.raw/*235.10*/(""");
</script>


<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
  <!--<li class="active" > You are here</li>-->
</ul>

<section id="layouts" class="browse">

  <div class="page-header row-fluid">
    <h1>Search UniCarbKB</h1>
    <h4 class="subheader">Retrieve records from GlycosuiteDB, EUROCarbDB and GlycoBase</h4>
  </div>

<div class="row-fluid">
  <div class="span3 search">
    <div class="row-fluid">
      <div class="filterbar tabbable clearfix">
        <ul class="nav nav-tabs" id="myTabBar">
          <li class="title">Select a Structure Database: </li>
          <li><a href="#filter1" data-toggle="tab">GlycoSuiteDB <span class="pull-right count">"""),_display_(/*258.97*/if(taxon.size() != 0)/*258.118*/ {_display_(_display_(/*258.121*/taxon/*258.126*/.size()))}),format.raw/*258.134*/(""" """),_display_(/*258.136*/if(proteinList.size() != 0)/*258.163*/ {_display_(_display_(/*258.166*/proteinList/*258.177*/.size()))}),format.raw/*258.185*/(""" """),_display_(/*258.187*/if(tissueList.size() != 0)/*258.213*/ {_display_(_display_(/*258.216*/tissueList/*258.226*/.size()))}),format.raw/*258.234*/(""" """),_display_(/*258.236*/if(!accsearch.isEmpty() )/*258.261*/ {_display_(_display_(/*258.264*/accsearch/*258.273*/.size()))}),format.raw/*258.281*/("""</span></a></li>
          <li class=""><a href="#filter2" data-toggle="tab">EUROCarbDB</a></li>
          <li><a href="#filter3" data-toggle="tab">GlycoBase <span class="pull-right count"> """),_display_(/*260.95*/if(count != 0)/*260.109*/ {_display_(_display_(/*260.112*/count))}),format.raw/*260.118*/(""" """),_display_(/*260.120*/if(counttissue != 0)/*260.140*/ {_display_(_display_(/*260.143*/counttissue))}),format.raw/*260.155*/("""</span></a></li> </ul>
      </div>

      <div class="info">
        <h4>Other Options:</h4>
        <p><b>Glycan Builder:</b> Build and search a glycan structure<a href="/search"> using the new interface</a>.</p>
        <p><b>Curated Publications:</b> Search the <a href="/references">growing list of publications, associated structures, and metadata</a>.</p>
      </div>
    </div>
  </div>

  <div class="span9 rightpanel">
    <div class="tabbable"> <!-- Only required for left/right tabs -->

      <ul class="nav nav-tabs row-fluid" id="myTab">
        <li class="active"><a href="#tab1" data-toggle="tab"><h2>Construct a Query:</h2></a></li>
        <li class="span12"><a class="returnedresults" href="#tab2" data-toggle="tab">
          <h2>Search returned """),_display_(/*277.32*/if(taxon.size() != 0)/*277.53*/ {_display_(_display_(/*277.56*/taxon/*277.61*/.size()))}),format.raw/*277.69*/(""" """),_display_(/*277.71*/if(proteinList.size() != 0)/*277.98*/ {_display_(_display_(/*277.101*/proteinList/*277.112*/.size()))}),format.raw/*277.120*/(""" """),_display_(/*277.122*/if(tissueList.size() != 0)/*277.148*/ {_display_(_display_(/*277.151*/tissueList/*277.161*/.size()))}),format.raw/*277.169*/(""" """),_display_(/*277.171*/if(!accsearch.isEmpty()  )/*277.197*/ {_display_(_display_(/*277.200*/accsearch/*277.209*/.size()))}),format.raw/*277.217*/(""" """),format.raw/*277.218*/("""results...
          </h2>
          <a href="/query"><input type="button" value="Search Again" href="/query" class="btn btn-small pull-right"></a>
        </a></li>
        <li><a href="#tab3" data-toggle="tab">Related Results</a></li>
      </ul>

      <div class="tab-content results">

        <div class="tab-pane active" id="tab1">

          <!--<h2><img src="../images/icon_search.png"> Search Features &amp; Results</h2>-->
          <div id="filter1" class="control-group row-fluid glycosuitedb">
            <div id="tab1" class="span12" >

              <div class="multi-option clearfix">
                <span class="pull-left label">Search by: </span>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Taxonomy" checked> <h4>Taxonomy</h4>
                </label>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Localisation"> <h4>Tissue</h4>
                </label>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="ProteinLocalisation"> <h4>Protein</h4>
                </label>
		<label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Accession"> <h4>Protein Accession</h4>
                </label>
                  <label class="radio">
                      <input type="radio" name="glycosuitedbSearch" value="Disease"> <h4>Disease</h4>
                  </label>
              </div>

              <div id="glycosuitedbTaxonomy" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Taxonomy</h4>-->
                    <div id="selection" class="input-append row-fluid">
                      <select  name=taxonomy MULTIPLE id="e1" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
              <div id="glycosuitedbLocalisation" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Localisation</h4>-->
                    <div id="selection" class="row-fluid">
                      <select  name=tissue MULTIPLE id="e2" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
              <div id="glycosuitedbAssociation" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Protein Association</h4>-->
                    <div id="selection" class="row-fluid">
                      <select  name=protein MULTIPLE id="e3" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
                <div id="glycosuitedbDisease" class="controls">
                    <div id="selection" class="row-fluid">
                        <div class="alert alert-info" role="alert">We are in the process of updating this section. Disease information can be found <a href="/diseases">here</a></div>
                    </div>
                </div>
	      <div id="glycosuitedbAcc" class="controls">
		<label class="checkbox">
		   <form class="form-search">
                    <!--<h4>Localisation</h4>-->
		     <div id="selection" class="row-fluid">
                      <select  name=swiss MULTIPLE id="e12" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
		</label>
	      </div>

            </div> """),format.raw/*361.45*/("""

          """),format.raw/*363.11*/("""</div> """),format.raw/*363.35*/("""

          """),format.raw/*365.11*/("""<div id="filter2" class="tab-pane control-group row-fluid eurocarb">

	    <p>Support for EUROCarbDB will be available in the next release</p>

	    """),format.raw/*418.8*/("""

          """),format.raw/*420.11*/("""</div> """),format.raw/*420.38*/("""

          """),format.raw/*422.11*/("""<div id="filter3" class="tab-pane control-group nextprot">
            <div class="row-fluid">

                <div class="multi-option clearfix">
                  <span class="pull-left label">Search by: </span>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="Taxonomy" checked> <h4>Taxonomy</h4>
                  </label>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="Localisation"> <h4>Localisation</h4>
                  </label>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="ProteinLocalisation"> <h4>Protein</h4>
                  </label>
                </div>

                <div id="glycobaseTaxonomy" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Taxonomy</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=taxonomy MULTIPLE id="e5" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>

                <div id="glycobaseLocalisation" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Localisation</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=tissue MULTIPLE id="e10" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>

                <div id="glycobaseAssociation" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Protein Association</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=protein MULTIPLE id="e11" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>



            </div> """),format.raw/*476.40*/("""

          """),format.raw/*478.11*/("""</div> """),format.raw/*478.35*/("""
        """),format.raw/*479.9*/("""</div> """),format.raw/*479.33*/("""

      """),format.raw/*481.7*/("""<div class="tab-pane" id="tab2">

        <hr />

    """),_display_(/*485.6*/for(out <- output) yield /*485.24*/ {_display_(Seq[Any](format.raw/*485.26*/("""
    """),format.raw/*486.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*487.8*/Html(out)),format.raw/*487.17*/("""
    """),format.raw/*488.5*/("""</div>
    """)))}),format.raw/*489.6*/("""

    """),_display_(/*491.6*/for(out <- outputtissue) yield /*491.30*/ {_display_(Seq[Any](format.raw/*491.32*/("""
    """),format.raw/*492.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*493.8*/Html(out)),format.raw/*493.17*/("""
    """),format.raw/*494.5*/("""</div>
    """)))}),format.raw/*495.6*/("""

    """),_display_(/*497.6*/for(out <- outputprotein) yield /*497.31*/ {_display_(Seq[Any](format.raw/*497.33*/("""
    """),format.raw/*498.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*499.8*/Html(out)),format.raw/*499.17*/("""
    """),format.raw/*500.5*/("""</div>
    """)))}),format.raw/*501.6*/("""


    """),_display_(/*504.6*/if(proteinList != null)/*504.29*/ {_display_(Seq[Any](format.raw/*504.31*/("""

    """),_display_(/*506.6*/if(proteinList.size() >=1 )/*506.33*/{_display_(Seq[Any](format.raw/*506.34*/("""
    """),format.raw/*507.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="controls">
        <label class="checkbox">
          <form class="form-search">
            <div id="selection">
              <select  name=protein MULTIPLE id="e6" style="width:350px"  id="listBox"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </form>
        </label>
      </div>
    </div>
    """)))}),format.raw/*519.6*/("""


    """),_display_(/*522.6*/for(p <- proteinList) yield /*522.27*/ {_display_(Seq[Any](format.raw/*522.29*/("""
    """),format.raw/*523.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="details span9">
        <h3>"""),_display_(/*525.14*/if(p.swissProt != null)/*525.37*/ {_display_(Seq[Any](format.raw/*525.39*/("""<a href=""""),_display_(/*525.49*/routes/*525.55*/.Application.proteinsummary(p.swissProt, "annotated" )),format.raw/*525.109*/("""">"""),_display_(/*525.112*/p/*525.113*/.name),format.raw/*525.118*/("""</a>""")))}),format.raw/*525.123*/("""
	    """),_display_(/*526.7*/if(p.swissProt == null)/*526.30*/{_display_(Seq[Any](format.raw/*526.31*/("""

	    """),_display_(/*528.7*/for(t <- p.proteinsTax) yield /*528.30*/{_display_(Seq[Any](format.raw/*528.31*/(""" """),format.raw/*528.32*/("""<a href="/proteinsummary/"""),_display_(/*528.58*/p/*528.59*/.name),format.raw/*528.64*/("""/"""),_display_(/*528.66*/t/*528.67*/.species),format.raw/*528.75*/("""">"""),_display_(/*528.78*/p/*528.79*/.name),format.raw/*528.84*/("""</a>
	    """)))}),format.raw/*529.7*/("""
	""")))}),format.raw/*530.3*/(""" 


	   """),format.raw/*534.19*/("""

	"""),format.raw/*536.2*/("""</h3>
        <p class="">"""),_display_(/*537.22*/p/*537.23*/.stproteins.size()),format.raw/*537.41*/(""" """),format.raw/*537.42*/("""structures are associated with this protein</p>
      </div>
      <div class="span3">
	  """),_display_(/*540.5*/for(t <- p.proteinsTax) yield /*540.28*/{_display_(Seq[Any](format.raw/*540.29*/(""" """),format.raw/*540.30*/("""<p class="pull-right structures"><a href="/proteinsummary/"""),_display_(/*540.89*/p/*540.90*/.name),format.raw/*540.95*/("""/"""),_display_(/*540.97*/t/*540.98*/.species),format.raw/*540.106*/("""">"""),_display_(/*540.109*/p/*540.110*/.stproteins.size()),format.raw/*540.128*/(""" """),format.raw/*540.129*/("""structures </a></p> """)))}),format.raw/*540.150*/("""


        """),format.raw/*544.6*/("""

      """),format.raw/*546.7*/("""</div>
    </div>
    """)))}),format.raw/*548.6*/("""
    """)))}),format.raw/*549.6*/("""



    """),_display_(/*553.6*/if(tissueList != null && tissueTerms != null)/*553.51*/ {_display_(Seq[Any](format.raw/*553.53*/("""
    """),_display_(/*554.6*/if(tissueList.size() >=1 )/*554.32*/{_display_(Seq[Any](format.raw/*554.33*/("""
    """),format.raw/*555.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="controls">
        <label class="checkbox">
          <form class="form-search">
            <div id="selection">
              <select  name=tissue MULTIPLE id="e4" style="width:350px"  id="listBox"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </form>
        </label>
      </div>
    </div>
    """)))}),format.raw/*567.6*/("""

    """),_display_(/*569.6*/for(p <- tissueList) yield /*569.26*/ {_display_(Seq[Any](format.raw/*569.28*/("""
    """),format.raw/*570.5*/("""<div class="row-fluid glycosuiteResult">
      <h3><a href="tissuesummary/"""),_display_(/*571.35*/p/*571.36*/.id),format.raw/*571.39*/("""">"""),_display_(/*571.42*/p/*571.43*/.div1),format.raw/*571.48*/(""", """),_display_(/*571.51*/p/*571.52*/.div2),format.raw/*571.57*/(""", """),_display_(/*571.60*/p/*571.61*/.div3),format.raw/*571.66*/(""", """),_display_(/*571.69*/p/*571.70*/.div4),format.raw/*571.75*/("""</a></h3>
      <div class="details span9">
        <p class="">"""),_display_(/*573.22*/p/*573.23*/.stsource.size()),format.raw/*573.39*/(""" """),format.raw/*573.40*/("""structures are associated with this tissue</p>
      </div>
      <div class="span3">
        <p class="pull-right structures"><a href="tissuesummary/"""),_display_(/*576.66*/p/*576.67*/.id),format.raw/*576.70*/("""">"""),_display_(/*576.73*/p/*576.74*/.stsource.size()),format.raw/*576.90*/(""" """),format.raw/*576.91*/("""structures </a></p>
      </div>
    </div>
    """)))}),format.raw/*579.6*/("""
    """)))}),format.raw/*580.6*/("""

    """),_display_(/*582.6*/if(accsearch.size() != 0 )/*582.32*/ {_display_(Seq[Any](format.raw/*582.34*/("""
    """),format.raw/*583.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="controls">
        <label class="checkbox">
          <form class="form-search">
            <div id="selection">
              <select  name=swiss MULTIPLE id="e13" style="width:350px"  id="listBox"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </form>
        </label>
      </div>
    </div>
    """),_display_(/*595.6*/for(a <- accsearch) yield /*595.25*/ {_display_(Seq[Any](format.raw/*595.27*/("""
    """),format.raw/*596.5*/("""<div class="row-fluid glycosuiteResult">
      <h3><a href="proteinsummary/"""),_display_(/*597.36*/a/*597.37*/.swissProt),format.raw/*597.47*/("""">"""),_display_(/*597.50*/a/*597.51*/.swissProt),format.raw/*597.61*/(""" """),format.raw/*597.62*/("""("""),_display_(/*597.64*/a/*597.65*/.name),format.raw/*597.70*/(""")</a></h3>
    </div>
    """)))}),format.raw/*599.6*/("""
    """)))}),format.raw/*600.6*/("""

    """),_display_(/*602.6*/for( (taxon, ii) <- taxon.zipWithIndex) yield /*602.45*/ {_display_(Seq[Any](format.raw/*602.47*/("""        """),format.raw/*602.55*/("""<div class="row-fluid glycosuiteResult">
      <div class="details span9">
        <h3><a href="/taxonomysearch?taxonomy="""),_display_(/*604.48*/taxon/*604.53*/.species),format.raw/*604.61*/("""">"""),_display_(/*604.64*/taxon/*604.69*/.species),format.raw/*604.77*/("""</a>"""),format.raw/*604.212*/("""</h3>
        <p class="">"""),_display_(/*605.22*/taxon/*605.27*/.taxprotein.size()),format.raw/*605.45*/(""" """),format.raw/*605.46*/("""glycoproteins are associated with this taxonomy including: """),_display_(/*605.106*/for( (tax, i) <- taxon.taxprotein.zipWithIndex; if (i <= 6)) yield /*605.166*/ {_display_(Seq[Any](format.raw/*605.168*/("""  """),_display_(/*605.171*/if(tax.swiss_prot != null)/*605.197*/ {_display_(Seq[Any](format.raw/*605.199*/(""" """),format.raw/*605.200*/("""<a href=""""),_display_(/*605.210*/routes/*605.216*/.Application.proteinsummary(tax.swiss_prot, "annotated" )),format.raw/*605.273*/("""">""")))}),format.raw/*605.276*/(""" """),_display_(/*605.278*/if(tax.swiss_prot == null)/*605.304*/ {_display_(Seq[Any](format.raw/*605.306*/("""<a href=""""),_display_(/*605.316*/routes/*605.322*/.Application.proteinsummary(tax.protein, taxon.species )),format.raw/*605.378*/("""">""")))}),format.raw/*605.381*/(""" """),_display_(/*605.383*/tax/*605.386*/.protein),format.raw/*605.394*/(""", </a> """)))}),format.raw/*605.402*/("""</p>
      </div>
      <div class="span3">
        <p class="pull-right structures"><a href="/taxonomysearch?taxonomy="""),_display_(/*608.77*/taxon/*608.82*/.species),format.raw/*608.90*/("""">"""),_display_(/*608.93*/taxon/*608.98*/.strtaxonomy.size()),format.raw/*608.117*/(""" """),format.raw/*608.118*/("""structures </a></p>
      </div>

    </div>
    """)))})))}),format.raw/*612.7*/("""
  """),format.raw/*613.3*/("""</div>
  """),format.raw/*614.17*/("""

  """),format.raw/*616.3*/("""<div class="tab-pane" id="tab3">
  </div>

</div>
    </div>
  </div><!-- /col -->
</div><!-- /row -->
"""),_display_(/*623.2*/views/*623.7*/.html.footerunicarb.footerunicarb()),format.raw/*623.42*/("""
"""),format.raw/*624.1*/("""</section>

"""),_display_(/*626.2*/if(taxon.size() != 0 )/*626.24*/ {_display_(Seq[Any](format.raw/*626.26*/("""
"""),format.raw/*627.1*/("""<script>
  $(function () """),format.raw/*628.17*/("""{"""),format.raw/*628.18*/("""
    """),format.raw/*629.5*/("""var test = 'first'; """),format.raw/*629.61*/("""
    """),format.raw/*630.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*632.3*/("""}"""),format.raw/*632.4*/(""")
</script>
""")))}),format.raw/*634.2*/("""

"""),_display_(/*636.2*/if(taxon.size() == 0  )/*636.25*/ {_display_(Seq[Any](format.raw/*636.27*/("""
"""),format.raw/*637.1*/("""<script>
  $(function () """),format.raw/*638.17*/("""{"""),format.raw/*638.18*/("""
    """),format.raw/*639.5*/("""var test = 'first'; """),format.raw/*639.61*/("""
    """),format.raw/*640.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab1"]').tab('show');
  """),format.raw/*642.3*/("""}"""),format.raw/*642.4*/(""")
</script>
""")))}),format.raw/*644.2*/("""

"""),_display_(/*646.2*/if(proteinList.size() != 0 )/*646.30*/ {_display_(Seq[Any](format.raw/*646.32*/("""
"""),format.raw/*647.1*/("""<script>
  $(function () """),format.raw/*648.17*/("""{"""),format.raw/*648.18*/("""
    """),format.raw/*649.5*/("""var test = 'first'; """),format.raw/*649.61*/("""
    """),format.raw/*650.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*652.3*/("""}"""),format.raw/*652.4*/(""")
</script>
""")))}),format.raw/*654.2*/("""

"""),_display_(/*656.2*/if(tissueList.size() != 0 )/*656.29*/ {_display_(Seq[Any](format.raw/*656.31*/("""
"""),format.raw/*657.1*/("""<script>
  $(function () """),format.raw/*658.17*/("""{"""),format.raw/*658.18*/("""
    """),format.raw/*659.5*/("""var test = 'first'; """),format.raw/*659.61*/("""
    """),format.raw/*660.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*662.3*/("""}"""),format.raw/*662.4*/(""")
</script>
""")))}),format.raw/*664.2*/("""

"""),_display_(/*666.2*/if(accsearch.size() != 0 )/*666.28*/ {_display_(Seq[Any](format.raw/*666.30*/("""
"""),format.raw/*667.1*/("""<script>
  $(function () """),format.raw/*668.17*/("""{"""),format.raw/*668.18*/("""
    """),format.raw/*669.5*/("""var test = 'first'; """),format.raw/*669.61*/("""
    """),format.raw/*670.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*672.3*/("""}"""),format.raw/*672.4*/(""")
</script>
""")))}),format.raw/*674.2*/("""


"""))
      }
    }
  }

  def render(taxonomy:List[String],taxon:ArrayList[Taxonomy],biolsource:List[Biolsource],groupTax:List[List[String]],sources:HashSet[String],proteins:HashSet[String],proteinList:ArrayList[Proteins],tissueList:ArrayList[Tissue],tissueTerms:List[Tissue],glycobasePerturbation:List[GlycobaseSource],output:List[String],count:Integer,outputtissue:List[String],counttissue:Integer,outputprotein:List[String],countprotein:Integer,proteinaccession:HashSet[String],accsearch:ArrayList[Proteins]): play.twirl.api.HtmlFormat.Appendable = apply(taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein,proteinaccession,accsearch)

  def f:((List[String],ArrayList[Taxonomy],List[Biolsource],List[List[String]],HashSet[String],HashSet[String],ArrayList[Proteins],ArrayList[Tissue],List[Tissue],List[GlycobaseSource],List[String],Integer,List[String],Integer,List[String],Integer,HashSet[String],ArrayList[Proteins]) => play.twirl.api.HtmlFormat.Appendable) = (taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein,proteinaccession,accsearch) => apply(taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein,proteinaccession,accsearch)

  def ref: this.type = this

}


}

/**/
object query extends query_Scope0.query
              /*
                  -- GENERATED --
                  DATE: Mon May 02 13:22:58 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/query.scala.html
                  HASH: 9154d51799b1eeec4d72b862f3947593ac645f13
                  MATRIX: 1012->1|1620->513|1648->516|1659->520|1696->521|1724->523|1775->547|1803->548|1832->551|1898->590|1926->591|1957->596|2023->636|2050->637|2079->640|2106->641|2187->694|2216->695|2244->696|2333->757|2362->758|2392->761|2585->926|2614->927|2645->931|2698->957|2726->958|2756->961|2805->982|2834->983|2865->987|2918->1013|2946->1014|2977->1018|3026->1039|3055->1040|3086->1044|3146->1077|3174->1078|3205->1082|3254->1103|3283->1104|3314->1108|3374->1141|3402->1142|3434->1147|3462->1148|3492->1151|3520->1152|3605->1209|3634->1210|3670->1219|3716->1237|3745->1238|3784->1249|3917->1355|3946->1356|3984->1367|4013->1368|4098->1425|4127->1426|4163->1435|4209->1453|4238->1454|4277->1465|4410->1571|4439->1572|4477->1583|4506->1584|4592->1642|4621->1643|4657->1652|4702->1669|4731->1670|4770->1681|4900->1784|4929->1785|4960->1789|4988->1790|5072->1846|5101->1847|5137->1856|5182->1873|5211->1874|5250->1885|5383->1991|5412->1992|5450->2003|5479->2004|5563->2060|5592->2061|5628->2070|5673->2087|5702->2088|5741->2099|5874->2205|5903->2206|5941->2217|5970->2218|6055->2275|6084->2276|6120->2285|6165->2302|6194->2303|6233->2314|6365->2419|6394->2420|6432->2431|6461->2432|6547->2489|6577->2490|6614->2499|6660->2516|6690->2517|6730->2528|6863->2633|6893->2634|6932->2645|6962->2646|7048->2703|7078->2704|7115->2713|7161->2730|7191->2731|7231->2742|7365->2848|7395->2849|7434->2860|7464->2861|7550->2918|7580->2919|7617->2928|7663->2945|7693->2946|7733->2957|7866->3062|7896->3063|7935->3074|7965->3075|8051->3132|8081->3133|8118->3142|8164->3159|8194->3160|8234->3171|8367->3276|8397->3277|8436->3288|8466->3289|8552->3346|8582->3347|8619->3356|8665->3373|8695->3374|8735->3385|8868->3490|8898->3491|8937->3502|8967->3503|9053->3560|9083->3561|9120->3570|9167->3588|9197->3589|9237->3600|9377->3712|9407->3713|9446->3724|9476->3725|9562->3782|9592->3783|9629->3792|9676->3810|9706->3811|9746->3822|9880->3928|9910->3929|9949->3940|9979->3941|10052->3985|10082->3986|10119->3995|10183->4031|10218->4049|10258->4050|10304->4067|10347->4082|10370->4083|10415->4096|10445->4098|10561->4185|10591->4186|10637->4203|10750->4288|10780->4289|10817->4298|10952->4405|10982->4406|11055->4450|11085->4451|11122->4460|11184->4494|11227->4520|11267->4521|11313->4538|11354->4551|11377->4552|11422->4565|11459->4574|11571->4657|11601->4658|11647->4675|11756->4756|11786->4757|11816->4759|11896->4811|11925->4812|11991->4849|12021->4850|12058->4859|12116->4890|12150->4907|12190->4908|12221->4911|12266->4928|12289->4929|12326->4935|12356->4937|12462->5014|12492->5015|12523->5018|12640->5107|12670->5108|12700->5110|12829->5211|12858->5212|12931->5256|12961->5257|12998->5266|13065->5305|13101->5324|13141->5325|13187->5342|13233->5360|13256->5361|13301->5374|13338->5383|13460->5476|13490->5477|13536->5494|13655->5585|13685->5586|13722->5595|13833->5678|13863->5679|14716->6504|14748->6525|14781->6528|14797->6533|14830->6541|14861->6543|14899->6570|14932->6573|14954->6584|14987->6592|15018->6594|15055->6620|15088->6623|15109->6633|15142->6641|15173->6643|15209->6668|15242->6671|15262->6680|15295->6688|15514->6879|15539->6893|15572->6896|15603->6902|15634->6904|15665->6924|15698->6927|15735->6939|16532->7708|16563->7729|16595->7732|16610->7737|16642->7745|16672->7747|16709->7774|16742->7777|16764->7788|16797->7796|16828->7798|16865->7824|16898->7827|16919->7837|16952->7845|16983->7847|17020->7873|17053->7876|17073->7885|17106->7893|17137->7894|21174->11927|21215->11939|21251->11963|21292->11975|21469->14324|21510->14336|21546->14363|21587->14375|24089->16868|24130->16880|24166->16904|24203->16913|24239->16937|24275->16945|24357->17000|24392->17018|24433->17020|24466->17025|24534->17066|24565->17075|24598->17080|24641->17092|24675->17099|24716->17123|24757->17125|24790->17130|24858->17171|24889->17180|24922->17185|24965->17197|24999->17204|25041->17229|25082->17231|25115->17236|25183->17277|25214->17286|25247->17291|25290->17303|25325->17311|25358->17334|25399->17336|25433->17343|25470->17370|25510->17371|25543->17376|26003->17805|26038->17813|26076->17834|26117->17836|26150->17841|26266->17929|26299->17952|26340->17954|26378->17964|26394->17970|26471->18024|26503->18027|26515->18028|26543->18033|26581->18038|26615->18045|26648->18068|26688->18069|26723->18077|26763->18100|26803->18101|26833->18102|26887->18128|26898->18129|26925->18134|26955->18136|26966->18137|26996->18145|27027->18148|27038->18149|27065->18154|27107->18165|27141->18168|27178->18261|27209->18264|27264->18291|27275->18292|27315->18310|27345->18311|27463->18402|27503->18425|27543->18426|27573->18427|27660->18486|27671->18487|27698->18492|27728->18494|27739->18495|27770->18503|27802->18506|27814->18507|27855->18525|27886->18526|27940->18547|27979->18710|28015->18718|28069->18741|28106->18747|28142->18756|28197->18801|28238->18803|28271->18809|28307->18835|28347->18836|28380->18841|28839->19269|28873->19276|28910->19296|28951->19298|28984->19303|29087->19378|29098->19379|29123->19382|29154->19385|29165->19386|29192->19391|29223->19394|29234->19395|29261->19400|29292->19403|29303->19404|29330->19409|29361->19412|29372->19413|29399->19418|29492->19483|29503->19484|29541->19500|29571->19501|29750->19652|29761->19653|29786->19656|29817->19659|29828->19660|29866->19676|29896->19677|29976->19726|30013->19732|30047->19739|30083->19765|30124->19767|30157->19772|30612->20200|30648->20219|30689->20221|30722->20226|30826->20302|30837->20303|30869->20313|30900->20316|30911->20317|30943->20327|30973->20328|31003->20330|31014->20331|31041->20336|31099->20363|31136->20369|31170->20376|31226->20415|31267->20417|31304->20425|31454->20547|31469->20552|31499->20560|31530->20563|31545->20568|31575->20576|31609->20711|31664->20738|31679->20743|31719->20761|31749->20762|31838->20822|31916->20882|31958->20884|31990->20887|32027->20913|32069->20915|32100->20916|32139->20926|32156->20932|32236->20989|32272->20992|32303->20994|32340->21020|32382->21022|32421->21032|32438->21038|32517->21094|32553->21097|32584->21099|32598->21102|32629->21110|32670->21118|32818->21238|32833->21243|32863->21251|32894->21254|32909->21259|32951->21278|32982->21279|33067->21330|33098->21333|33136->21356|33168->21360|33299->21464|33313->21469|33370->21504|33399->21505|33439->21518|33471->21540|33512->21542|33541->21543|33595->21568|33625->21569|33658->21574|33707->21630|33740->21635|33853->21720|33882->21721|33926->21734|33956->21737|33989->21760|34030->21762|34059->21763|34113->21788|34143->21789|34176->21794|34225->21850|34258->21855|34371->21940|34400->21941|34444->21954|34474->21957|34512->21985|34553->21987|34582->21988|34636->22013|34666->22014|34699->22019|34748->22075|34781->22080|34894->22165|34923->22166|34967->22179|34997->22182|35034->22209|35075->22211|35104->22212|35158->22237|35188->22238|35221->22243|35270->22299|35303->22304|35416->22389|35445->22390|35489->22403|35519->22406|35555->22432|35596->22434|35625->22435|35679->22460|35709->22461|35742->22466|35791->22522|35824->22527|35937->22612|35966->22613|36010->22626
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|37->6|37->6|38->7|38->7|38->7|39->8|40->9|40->9|40->9|40->9|44->13|44->13|45->14|45->14|45->14|46->15|51->20|51->20|52->21|53->22|53->22|54->23|54->23|54->23|55->24|56->25|56->25|58->27|58->27|58->27|59->28|60->29|60->29|62->31|62->31|62->31|63->32|64->33|64->33|67->36|67->36|68->37|68->37|72->41|72->41|73->42|73->42|73->42|74->43|77->46|77->46|78->47|78->47|82->51|82->51|83->52|83->52|83->52|84->53|87->56|87->56|88->57|88->57|93->62|93->62|94->63|94->63|94->63|95->64|98->67|98->67|99->68|99->68|102->71|102->71|103->72|103->72|103->72|104->73|107->76|107->76|108->77|108->77|111->80|111->80|112->81|112->81|112->81|113->82|116->85|116->85|117->86|117->86|121->90|121->90|122->91|122->91|122->91|123->92|126->95|126->95|127->96|127->96|131->100|131->100|132->101|132->101|132->101|133->102|136->105|136->105|137->106|137->106|141->110|141->110|142->111|142->111|142->111|143->112|146->115|146->115|147->116|147->116|151->120|151->120|152->121|152->121|152->121|153->122|156->125|156->125|157->126|157->126|161->130|161->130|162->131|162->131|162->131|163->132|166->135|166->135|167->136|167->136|171->140|171->140|172->141|172->141|172->141|173->142|176->145|176->145|177->146|177->146|181->150|181->150|182->151|182->151|182->151|183->152|186->155|186->155|187->156|187->156|191->160|191->160|192->161|192->161|192->161|193->162|196->165|196->165|197->166|197->166|201->170|201->170|202->171|203->172|203->172|203->172|204->173|204->173|204->173|205->174|206->175|208->177|208->177|209->178|210->179|210->179|211->180|215->184|215->184|219->188|219->188|220->189|221->190|221->190|221->190|222->191|222->191|222->191|223->192|224->193|226->195|226->195|227->196|228->197|228->197|229->198|231->200|231->200|235->204|235->204|236->205|237->206|237->206|237->206|238->207|238->207|238->207|239->208|240->209|242->211|242->211|243->212|244->213|244->213|245->214|249->218|249->218|253->222|253->222|254->223|255->224|255->224|255->224|256->225|256->225|256->225|257->226|258->227|260->229|260->229|261->230|262->231|262->231|263->232|266->235|266->235|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|289->258|291->260|291->260|291->260|291->260|291->260|291->260|291->260|291->260|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|308->277|392->361|394->363|394->363|396->365|400->418|402->420|402->420|404->422|458->476|460->478|460->478|461->479|461->479|463->481|467->485|467->485|467->485|468->486|469->487|469->487|470->488|471->489|473->491|473->491|473->491|474->492|475->493|475->493|476->494|477->495|479->497|479->497|479->497|480->498|481->499|481->499|482->500|483->501|486->504|486->504|486->504|488->506|488->506|488->506|489->507|501->519|504->522|504->522|504->522|505->523|507->525|507->525|507->525|507->525|507->525|507->525|507->525|507->525|507->525|507->525|508->526|508->526|508->526|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|510->528|511->529|512->530|515->534|517->536|518->537|518->537|518->537|518->537|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|521->540|524->544|526->546|528->548|529->549|533->553|533->553|533->553|534->554|534->554|534->554|535->555|547->567|549->569|549->569|549->569|550->570|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|551->571|553->573|553->573|553->573|553->573|556->576|556->576|556->576|556->576|556->576|556->576|556->576|559->579|560->580|562->582|562->582|562->582|563->583|575->595|575->595|575->595|576->596|577->597|577->597|577->597|577->597|577->597|577->597|577->597|577->597|577->597|577->597|579->599|580->600|582->602|582->602|582->602|582->602|584->604|584->604|584->604|584->604|584->604|584->604|584->604|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|585->605|588->608|588->608|588->608|588->608|588->608|588->608|588->608|592->612|593->613|594->614|596->616|603->623|603->623|603->623|604->624|606->626|606->626|606->626|607->627|608->628|608->628|609->629|609->629|610->630|612->632|612->632|614->634|616->636|616->636|616->636|617->637|618->638|618->638|619->639|619->639|620->640|622->642|622->642|624->644|626->646|626->646|626->646|627->647|628->648|628->648|629->649|629->649|630->650|632->652|632->652|634->654|636->656|636->656|636->656|637->657|638->658|638->658|639->659|639->659|640->660|642->662|642->662|644->664|646->666|646->666|646->666|647->667|648->668|648->668|649->669|649->669|650->670|652->672|652->672|654->674
                  -- GENERATED --
              */
          