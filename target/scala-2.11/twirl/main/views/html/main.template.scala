
package views.html

import play.twirl.api._


     object main_Scope0 {
import controllers._

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.17*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
    <head>
        <title>UniCarbKB</title>
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*7.70*/routes/*7.76*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*7.126*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*8.70*/routes/*8.76*/.Assets.versioned("stylesheets/customMarkdown.css")),format.raw/*8.127*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*9.70*/routes/*9.76*/.Assets.versioned("stylesheets/select2.css")),format.raw/*9.120*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*10.70*/routes/*10.76*/.Assets.versioned("stylesheets/unicarbkb.css")),format.raw/*10.122*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*11.70*/routes/*11.76*/.Assets.versioned("stylesheets/bootstrap-lightbox.css")),format.raw/*11.131*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*12.70*/routes/*12.76*/.Assets.versioned("stylesheets/bootstrap-image-gallery.min.css")),format.raw/*12.140*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*13.70*/routes/*13.76*/.Assets.versioned("stylesheets/docs.min.css")),format.raw/*13.121*/("""">
        <link rel="icon" href=""""),_display_(/*14.33*/routes/*14.39*/.Assets.versioned("favicon.ico")),format.raw/*14.71*/("""" type="image/x-icon">

        <script src=""""),_display_(/*16.23*/routes/*16.29*/.Assets.versioned("javascripts/jquery.js")),format.raw/*16.71*/(""""></script>
        <script src=""""),_display_(/*17.23*/routes/*17.29*/.Assets.versioned("javascripts/jquery.ae.image.resize.min.js")),format.raw/*17.91*/(""""></script>
        <script src=""""),_display_(/*18.23*/routes/*18.29*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*18.78*/(""""></script>
        <script src=""""),_display_(/*19.23*/routes/*19.29*/.Assets.versioned("javascripts/jquery.tablesorter.min.js")),format.raw/*19.87*/(""""></script>
        <script src=""""),_display_(/*20.23*/routes/*20.29*/.Assets.versioned("javascripts/bootstrap-lightbox.min.js")),format.raw/*20.87*/(""""></script>
        <script src=""""),_display_(/*21.23*/routes/*21.29*/.Assets.versioned("javascripts/showdown.js")),format.raw/*21.73*/(""""></script>
        <script src=""""),_display_(/*22.23*/routes/*22.29*/.Assets.versioned("javascripts/google-jsapi.js")),format.raw/*22.77*/(""""></script>
        """),format.raw/*23.87*/("""
        """),format.raw/*24.9*/("""<script src=""""),_display_(/*24.23*/routes/*24.29*/.Assets.versioned("javascripts/jPages.js")),format.raw/*24.71*/(""""></script>
        <script src=""""),_display_(/*25.23*/routes/*25.29*/.Assets.versioned("javascripts/jquery.lazyload.js")),format.raw/*25.80*/(""""></script>
        <script src=""""),_display_(/*26.23*/routes/*26.29*/.Assets.versioned("javascripts/load-image.js")),format.raw/*26.75*/(""""></script>
        <script src=""""),_display_(/*27.23*/routes/*27.29*/.Assets.versioned("javascripts/bootstrap-image-gallery.min.js")),format.raw/*27.92*/(""""></script>

        <script src=""""),_display_(/*29.23*/routes/*29.29*/.Assets.versioned("javascripts/select2.js")),format.raw/*29.72*/(""""></script>
        <script src=""""),_display_(/*30.23*/routes/*30.29*/.Assets.versioned("javascripts/application.js")),format.raw/*30.76*/(""""></script>

        <script src=""""),_display_(/*32.23*/routes/*32.29*/.Assets.versioned("javascripts/workflows.js")),format.raw/*32.74*/(""""></script>
        <script src="http://d3js.org/d3.v3.min.js"></script>

        <script src=""""),_display_(/*35.23*/routes/*35.29*/.Assets.versioned("javascripts/test/jira.js")),format.raw/*35.74*/(""""></script>
        """),format.raw/*37.11*/("""
        """),format.raw/*38.9*/("""<script>
        $(function() """),format.raw/*39.22*/("""{"""),format.raw/*39.23*/("""
            """),format.raw/*40.13*/("""$( ".resizeme" ).aeImageResize("""),format.raw/*40.44*/("""{"""),format.raw/*40.45*/(""" """),format.raw/*40.46*/("""height: 400, width: 280 """),format.raw/*40.70*/("""}"""),format.raw/*40.71*/(""");
            $( ".resizemeModal" ).aeImageResize("""),format.raw/*41.49*/("""{"""),format.raw/*41.50*/(""" """),format.raw/*41.51*/("""height: 200, width: 200 """),format.raw/*41.75*/("""}"""),format.raw/*41.76*/(""");
        """),format.raw/*42.9*/("""}"""),format.raw/*42.10*/(""");
        </script>
        <script>
        $(document).ready(function()"""),format.raw/*45.37*/("""{"""),format.raw/*45.38*/("""
            """),format.raw/*46.13*/("""$(".announceTaxStructures").click(function()"""),format.raw/*46.57*/("""{"""),format.raw/*46.58*/(""" """),format.raw/*46.59*/("""// Click to only happen on announce links
                $("#Id").val($(this).data('id'));
                $('#taxDescription').modal('show');
            """),format.raw/*49.13*/("""}"""),format.raw/*49.14*/(""");
        """),format.raw/*50.9*/("""}"""),format.raw/*50.10*/(""");
        function closeDialog () """),format.raw/*51.33*/("""{"""),format.raw/*51.34*/("""
            """),format.raw/*52.13*/("""$('#taxDescription').modal('hide');
        """),format.raw/*53.9*/("""}"""),format.raw/*53.10*/(""";
        function okClicked () """),format.raw/*54.31*/("""{"""),format.raw/*54.32*/("""
            """),format.raw/*55.13*/("""closeDialog ();
        """),format.raw/*56.9*/("""}"""),format.raw/*56.10*/(""";

        </script>

        <script>
        $(document).ready(function()"""),format.raw/*61.37*/("""{"""),format.raw/*61.38*/("""
            """),format.raw/*62.13*/("""$(".announce").click(function()"""),format.raw/*62.44*/("""{"""),format.raw/*62.45*/(""" """),format.raw/*62.46*/("""// Click to only happen on announce links
                $("#Id").val($(this).data('id'));
                $('#windowDescription').modal('show');

                var result = $(this).data('id');
                var replace = result.replace("[","");
                var replace = replace.replace("]","");
                var replace = replace.replace(",","");
                var replace = replace.replace("null","");
                var replace = replace.replace("> null ", "");
                var replace = replace.replace(" null", "" );
                var replace = replace.replace(" >  > ","");
                $('p').html(replace).appendTo(document.myDiv);
            """),format.raw/*75.13*/("""}"""),format.raw/*75.14*/(""");
        """),format.raw/*76.9*/("""}"""),format.raw/*76.10*/(""");

        function closeDialog () """),format.raw/*78.33*/("""{"""),format.raw/*78.34*/("""
            """),format.raw/*79.13*/("""$('#windowDescription').modal('hide');
        """),format.raw/*80.9*/("""}"""),format.raw/*80.10*/(""";
        function okClicked () """),format.raw/*81.31*/("""{"""),format.raw/*81.32*/("""
            """),format.raw/*82.13*/("""closeDialog ();
        """),format.raw/*83.9*/("""}"""),format.raw/*83.10*/(""";
    </script>


        <script>
      $(document).ready(function()"""),format.raw/*88.35*/("""{"""),format.raw/*88.36*/("""
          """),format.raw/*89.11*/("""$(".announce2").click(function()"""),format.raw/*89.43*/("""{"""),format.raw/*89.44*/(""" """),format.raw/*89.45*/("""// Click to only happen on announce links
              $("#Id").val($(this).data('id'));
              $('#windowDescription').modal('show');

              var resultdiv = $(this).data('id');
              var replace = resultdiv.replace("[","");
              var replace = replace.replace("]","");

              $('p').html(replace).appendTo(document.myDiv);


          """),format.raw/*100.11*/("""}"""),format.raw/*100.12*/(""");
      """),format.raw/*101.7*/("""}"""),format.raw/*101.8*/(""");

      function closeDialog () """),format.raw/*103.31*/("""{"""),format.raw/*103.32*/("""
          """),format.raw/*104.11*/("""$('#windowDescription').modal('hide');
      """),format.raw/*105.7*/("""}"""),format.raw/*105.8*/(""";
      function okClicked () """),format.raw/*106.29*/("""{"""),format.raw/*106.30*/("""
          """),format.raw/*107.11*/("""closeDialog ();
      """),format.raw/*108.7*/("""}"""),format.raw/*108.8*/(""";
    </script>

        <script>

      /* Download lazy load plugin and make sure you add it in the head of your page. */

      /* when document is ready */
      $(function() """),format.raw/*116.20*/("""{"""),format.raw/*116.21*/("""

          """),format.raw/*118.11*/("""/* initiate lazyload defining a custom event to trigger image loading  */
          $("ul li img").lazyload("""),format.raw/*119.35*/("""{"""),format.raw/*119.36*/("""
              """),format.raw/*120.15*/("""event : "turnPage",
              effect : "fadeIn"
          """),format.raw/*122.11*/("""}"""),format.raw/*122.12*/(""");

          /* initiate plugin */
          $("div.holder").jPages("""),format.raw/*125.34*/("""{"""),format.raw/*125.35*/("""
              """),format.raw/*126.15*/("""containerID : "itemContainer",
              animation   : "fadeInUp",
              callback    : function( pages, items )"""),format.raw/*128.53*/("""{"""),format.raw/*128.54*/("""
                  """),format.raw/*129.19*/("""/* lazy load current images */
                  items.showing.find("img").trigger("turnPage");
                  /* lazy load next page images */
                  items.oncoming.find("img").trigger("turnPage");
              """),format.raw/*133.15*/("""}"""),format.raw/*133.16*/("""
          """),format.raw/*134.11*/("""}"""),format.raw/*134.12*/(""");

      """),format.raw/*136.7*/("""}"""),format.raw/*136.8*/(""");
    </script>

        <script type="text/javascript">
		var _gaq = _gaq || [];
        var pluginUrl =
                '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', 'UA-28274077-1']);
        _gaq.push(['_trackPageview']);

        (function() """),format.raw/*147.21*/("""{"""),format.raw/*147.22*/("""
            """),format.raw/*148.13*/("""var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        """),format.raw/*151.9*/("""}"""),format.raw/*151.10*/(""")();

	</script>

        <script>
	    (function(i,s,o,g,r,a,m)"""),format.raw/*156.30*/("""{"""),format.raw/*156.31*/("""i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()"""),format.raw/*156.81*/("""{"""),format.raw/*156.82*/("""
                    """),format.raw/*157.21*/("""(i[r].q=i[r].q||[]).push(arguments)"""),format.raw/*157.56*/("""}"""),format.raw/*157.57*/(""",i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/(""")(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-28274077-2', 'unicarbkb.org');
        ga('send', 'pageview');
	</script>

    </head>
    <body>

        """),format.raw/*174.20*/("""
    """),format.raw/*175.5*/("""<header>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" href="/">UniCarbKB</a>
                    <ul class="nav">

                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Biological Source <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="/query" target="_blank">Query Options</a></li>
                                <li><a href="/proteins" target="_blank">Glycoproteins</a></li>
                                <li><a href="/celllines" target="_blank">Cell Lines</a></li>
                            </ul>
                        </li>

                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Structure <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="/structureQuery" target="_blank">Composition</a></li>
                                <li><a href="/builder">Glycan Builder</a></li>
                                <li><a href="/searchCT" target="_blank">Search by GlycoCT</a></li>
                            </ul>
                        </li>


                        <li><a href="/references?s=authors">References</a></li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Database <b class="caret"></b></a>
                            <ul class="dropdown-menu">

                                <li class="dropdown-submenu">
                                    <a href="#">GlycoMob</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/ionmobStructures" target="_blank">Structures</a></li>
                                        <li><a href="/ionmobilityStandards" target="_blank">
                                            Data Summary and Compositions</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown-submenu">
                                    <a href="#">Theoretical</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/milk" target="_blank">Milk</a></li>
                                        <li><a href="/unicorn" target="_blank">
                                            UniCorn (Human N-link)</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="divider"></li>

                                <li><a href="http://115.146.93.212/confluence/display/UK/Database+Cross-References" target="_blank">
                                        Cross-References (External)</a>
                                </li>

                            </ul>
                        </li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Tools <b class="caret"></b></a>
                            <ul class="dropdown-menu">

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        Substructure Search <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://glycoproteome.expasy.org/substructuresearch/" target="_blank">External Preview</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    GlycoDigest <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://www.glycodigest.org" target="_blank">Home (External)</a></li>

                                        <li class="dropdown-submenu">
                                            <a href="#">Get Started</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="https://github.com/alternativeTime/glycomics/wiki/GlycoDigest-Guide-(UniCarbKB)" target="_blank" >
                                                Guide/Wiki</a>
                                                </li>
                                                <li><a href="/builderDigest">UniCarbKB Integrated</a></li>
                                                <li><a href="https://bitbucket.org/sib-pig/glycodigest/downloads/GlycoDigest-StandAloneRunnableJar.jar">
                                                Download Standalone</a>
                                                </li>
                                                <li><a href="http://www.glycodigest.org/digestJava7.html" target="_blank">
                                                GlycoDigest Applet</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a href="#">Exoglycosidase Information</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="http://link.springer.com/protocol/10.1385%2F1-59745-167-3%3A125" target="_blank">
                                                Experimental Protocol</a>
                                                </li>
                                                <li><a href="http://www.glycodigest.org/exoglycosidase.pdf" target="_blank">
                                                Exoglycosidase Brief Guide</a>
                                                </li>
                                                <li><a href="http://www.glycodigest.org/lcms.pdf" target="_blank">
                                                Example IgG LC-MS</a>
                                                </li>
                                                <li><a href="http://link.springer.com/protocol/10.1385%2F1-59745-167-3:125/fulltext.html#Sec16" target="_blank">
                                                2-AB HPLC IgG Exoglycosidase Treatment</a>
                                                </li>
                                                <li><a href="http://www.mdpi.com/2218-1989/2/4/648" target="_blank">
                                                O-Links: Using Exoglycosidases and MS</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
                            </ul>
                        </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Help <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/about">About</a></li>
                                        <li><a href="http://115.146.93.212/confluence/display/UK/UniCarbKB+Summary" target="_blank">
                                        Documentation</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                </div>
            </div>
        </div>
    </header>
    <section id="main" class="container-fluid content">
    """),_display_(/*315.6*/content),format.raw/*315.13*/("""
    """),format.raw/*316.5*/("""</section>
    </body>
</html>
"""))
      }
    }
  }

  def render(content:Html): play.twirl.api.HtmlFormat.Appendable = apply(content)

  def f:((Html) => play.twirl.api.HtmlFormat.Appendable) = (content) => apply(content)

  def ref: this.type = this

}


}

/**/
object main extends main_Scope0.main
              /*
                  -- GENERATED --
                  DATE: Mon May 02 13:17:35 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/main.scala.html
                  HASH: c3749a245c03d34d0bc662c54a1c86638da2a08f
                  MATRIX: 741->1|851->16|879->18|1041->154|1055->160|1126->210|1224->282|1238->288|1310->339|1408->411|1422->417|1487->461|1586->533|1601->539|1669->585|1768->657|1783->663|1860->718|1959->790|1974->796|2060->860|2159->932|2174->938|2241->983|2303->1018|2318->1024|2371->1056|2444->1102|2459->1108|2522->1150|2583->1184|2598->1190|2681->1252|2742->1286|2757->1292|2827->1341|2888->1375|2903->1381|2982->1439|3043->1473|3058->1479|3137->1537|3198->1571|3213->1577|3278->1621|3339->1655|3354->1661|3423->1709|3471->1807|3507->1816|3548->1830|3563->1836|3626->1878|3687->1912|3702->1918|3774->1969|3835->2003|3850->2009|3917->2055|3978->2089|3993->2095|4077->2158|4139->2193|4154->2199|4218->2242|4279->2276|4294->2282|4362->2329|4424->2364|4439->2370|4505->2415|4628->2511|4643->2517|4709->2562|4757->2901|4793->2910|4851->2940|4880->2941|4921->2954|4980->2985|5009->2986|5038->2987|5090->3011|5119->3012|5198->3063|5227->3064|5256->3065|5308->3089|5337->3090|5375->3101|5404->3102|5506->3176|5535->3177|5576->3190|5648->3234|5677->3235|5706->3236|5890->3392|5919->3393|5957->3404|5986->3405|6049->3440|6078->3441|6119->3454|6190->3498|6219->3499|6279->3531|6308->3532|6349->3545|6400->3569|6429->3570|6532->3645|6561->3646|6602->3659|6661->3690|6690->3691|6719->3692|7424->4369|7453->4370|7491->4381|7520->4382|7584->4418|7613->4419|7654->4432|7728->4479|7757->4480|7817->4512|7846->4513|7887->4526|7938->4550|7967->4551|8064->4620|8093->4621|8132->4632|8192->4664|8221->4665|8250->4666|8655->5042|8685->5043|8722->5052|8751->5053|8814->5087|8844->5088|8884->5099|8957->5144|8986->5145|9045->5175|9075->5176|9115->5187|9165->5209|9194->5210|9402->5389|9432->5390|9473->5402|9610->5510|9640->5511|9684->5526|9775->5588|9805->5589|9903->5658|9933->5659|9977->5674|10129->5797|10159->5798|10207->5817|10463->6044|10493->6045|10533->6056|10563->6057|10601->6067|10630->6068|11014->6423|11044->6424|11086->6437|11435->6758|11465->6759|11558->6823|11588->6824|11667->6874|11697->6875|11747->6896|11811->6931|11841->6932|12016->7079|12046->7080|12284->7461|12317->7466|20679->15801|20708->15808|20741->15813
                  LINES: 27->1|32->1|34->3|38->7|38->7|38->7|39->8|39->8|39->8|40->9|40->9|40->9|41->10|41->10|41->10|42->11|42->11|42->11|43->12|43->12|43->12|44->13|44->13|44->13|45->14|45->14|45->14|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|55->24|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|58->27|58->27|58->27|60->29|60->29|60->29|61->30|61->30|61->30|63->32|63->32|63->32|66->35|66->35|66->35|67->37|68->38|69->39|69->39|70->40|70->40|70->40|70->40|70->40|70->40|71->41|71->41|71->41|71->41|71->41|72->42|72->42|75->45|75->45|76->46|76->46|76->46|76->46|79->49|79->49|80->50|80->50|81->51|81->51|82->52|83->53|83->53|84->54|84->54|85->55|86->56|86->56|91->61|91->61|92->62|92->62|92->62|92->62|105->75|105->75|106->76|106->76|108->78|108->78|109->79|110->80|110->80|111->81|111->81|112->82|113->83|113->83|118->88|118->88|119->89|119->89|119->89|119->89|130->100|130->100|131->101|131->101|133->103|133->103|134->104|135->105|135->105|136->106|136->106|137->107|138->108|138->108|146->116|146->116|148->118|149->119|149->119|150->120|152->122|152->122|155->125|155->125|156->126|158->128|158->128|159->129|163->133|163->133|164->134|164->134|166->136|166->136|177->147|177->147|178->148|181->151|181->151|186->156|186->156|186->156|186->156|187->157|187->157|187->157|189->159|189->159|198->174|199->175|339->315|339->315|340->316
                  -- GENERATED --
              */
          