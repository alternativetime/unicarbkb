
package views.html

import play.twirl.api._


     object about_Scope0 {
import controllers._

class about extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html class="about">
    <head>
        <title>UnICarbKB</title>
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*5.70*/routes/*5.76*/.Assets.versioned("assets/stylesheets/bootstrap.min.css")),format.raw/*5.133*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*6.70*/routes/*6.76*/.Assets.versioned("assets/stylesheets/unicarbkb.css")),format.raw/*6.129*/("""">
        <link rel="icon" href=""""),_display_(/*7.33*/routes/*7.39*/.Assets.versioned("assets/favicon.ico")),format.raw/*7.78*/("""" type="image/x-icon">
        <link rel="icon" href=""""),_display_(/*8.33*/routes/*8.39*/.Assets.versioned("assets/favicon.ico")),format.raw/*8.78*/("""" type="image/x-icon">
    </head>

    <header>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" href="/">UniCarbKB</a>
                    <ul class="nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/query">Search</a></li>
                        <li><a href="/references">References</a></li>
                        <li><a href="/builder">Glycan Builder</a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Help <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="/about">About</a></li>
                                <li><a href="http://115.146.93.212/confluence/display/UK/UniCarbKB+Summary" target="_blank">Documentation (Beta)</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div id="headersearch" class="pull-right">
                        <ul class='nav'>
                            <li class='active'><a href="/about">About</a></li>
                            <li class='active'><a href="/about">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid content">
        <div class="row-fluid info">
            <div class="span8 leftContent">
                <h3>About UniCarbKB</h3>
                <p>
                    The rapidly expanding glycomics field (structure and function of sugars) is being increasingly recognized as an important component of life science research, for application in diagnosis of disease, preventive medicines (vaccines) and therapeutic drugs. In contrast to the genomics and proteomics fields, and despite several international initiatives, glycomics still lacks maintained, accessible, curated, integrated and comprehensive data collections that summarize the structure, characteristics, biological origin and potential function of carbohydrates.
                </p>
                <p>
                    UniCarbKB is an initiative that aims to promote the creation of an online information storage and search platform for glycomics and glycobiology research. The knowledgebase will offer a freely accessible and information rich resource supported by querying interfaces, annotation technologies and the adoption of common standards to integrate structural, experimental and functional data. Through cross-referencing existing databases and information resources, the UniCarbKB framework endeavours to support the growth of glycobioinformatics and the dissemination of knowledge through the provision of an open and unified portal to encourage the sharing of data.
                </p>
                <p>
                    The initiative aims to make the technology available to all glycomics researchers and other ‘omics’ disciplines to explore and integrate cross-disciplinary boundaries to gain new insights. Australia has a unique opportunity to become a global focal point for building and enabling a capability area in glycomics that integrates and leverages the technology and data produced by previous international and Australian infrastructure initiatives.
                </p>
                <h3>Need help?</h3>
                <p>We have started to build a knowledge base / guide to using UniCarbKB. For more information go to our <a href="http://115.146.93.212/confluence/display/UK/UniCarbKB+Summary">confluence page</a></p>
                <h3>Acknowledgements</h3>
                <p>
	                The UniCarbKB team acknowledge the work and efforts of EUROCarbDB and GlycoSuiteDB including associated projects MonosaccharideDB, GlycoWorkBench, GlycanBuilder and GlycoBase. These actvities have enabled us to develop and provide the community with a growing resource for glycomics and glycoproteomic research. Furthermore, we welcome new collaborations with members from the JCCGDB, Rings and BCSDB databases.
                </p>
                <p>
            <h3>License</h3>
                <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width : 0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a> <br /> <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">UniCarbKB</span>
                is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>
                .
                </p>
            </div>

            <div class='span4 sidebar'>
                <h3>Contacts</h3>
                <p>From August 2015 all issues/bugs/errors will be processed using our Service Desk. Please send an email to <a href='mailto:info&#64;unicarbkb.org'>report a problem</a>
                    .</p>
                <p>One of the developers will contact you directly when the issue enters the processing stage.</p>

                <div class="info clearfix">
                    <h3>Social Connections</h3>
                    <img src='/assets/images/qr/homeqr.png' alt='' /> <br/>
                    <a href="../images/Macquarie_BiFoldBrochure_v1_HR.pdf">Check out our latest brochure</a> <br/> <br/>
                    <a href="https://twitter.com/unicarbkb" class="twitter-follow-button" data-show-count="false">Follow unicarbkb</a>
                    <script>!function(d,s,id)"""),format.raw/*77.46*/("""{"""),format.raw/*77.47*/("""var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id))"""),format.raw/*77.112*/("""{"""),format.raw/*77.113*/("""js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);"""),format.raw/*77.223*/("""}"""),format.raw/*77.224*/("""}"""),format.raw/*77.225*/("""(document,"script","twitter-wjs");</script>
                        <!-- Place this tag where you want the +1 button to render -->
                    <br> <g:plusone size="medium" annotation="none"></g:plusone>

                        <!-- Place this render call where appropriate -->
                    <script type="text/javascript">
                    (function() """),format.raw/*83.33*/("""{"""),format.raw/*83.34*/("""
                    """),format.raw/*84.21*/("""var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                    """),format.raw/*87.21*/("""}"""),format.raw/*87.22*/(""")();
          </script>
                </div>
            </div>
        </div>

        <div class="row-fluid logos">
            <div class='span3 nectar'><a href="http://www.nectar.org.au"><img src='/assets/img/NeCTAR_Logo.png' alt='' /></a></div>
            <div class='span3'><a href="http://www.ands.org.au"><img src='/assets/img/ANDS_Logo.png' alt='' /></a></div>
            <div class='span3'><a href="http://www.snf.ch/"><img src='/assets/img/SNF_Logo.png' alt='' /></a></div>
        </div>

        """),_display_(/*99.10*/views/*99.15*/.html.footerunicarb.footerunicarb()),format.raw/*99.50*/("""

    """),format.raw/*101.5*/("""</div>

</html>	
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object about extends about_Scope0.about
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:00 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/about.scala.html
                  HASH: c082d3aafca3444dc929efb342e676cbbf60a9a9
                  MATRIX: 827->0|1003->150|1017->156|1095->213|1193->285|1207->291|1281->344|1342->379|1356->385|1415->424|1496->479|1510->485|1569->524|7334->6261|7363->6262|7457->6327|7487->6328|7626->6438|7656->6439|7686->6440|8085->6811|8114->6812|8163->6833|8476->7118|8505->7119|9047->7634|9061->7639|9117->7674|9151->7680
                  LINES: 32->1|36->5|36->5|36->5|37->6|37->6|37->6|38->7|38->7|38->7|39->8|39->8|39->8|108->77|108->77|108->77|108->77|108->77|108->77|108->77|114->83|114->83|115->84|118->87|118->87|130->99|130->99|130->99|132->101
                  -- GENERATED --
              */
          