
package views.html

import play.twirl.api._


     object builderDigest_Scope0 {
import views.html._

class builderDigest extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""
    """),format.raw/*2.5*/("""<ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-wrench" ></i> Glycan Builder <span class="divider"></span></li>
    </ul>



    <div class="page-header row-fluid">
        <h1>Glycan Builder and GlycoDigest</h1>
        <h4 class="subheader">
            Welcome to the preview launch of UniCarbKBs integrated GlycoDigest tool. To successfully run GlycoDigest build N-link structures with embedded linkage information</h4>
    </div>

    <script type="text/javascript" src="/GlycanBuilder/VAADIN/vaadinBootstrap.js"></script>

    <iframe id="__gwt_historyFrame" style="width:0 ; height:0 ; border:0 ; overflow:hidden" src="javascript:false"></iframe>

    <div class="row-fluid">
        """),format.raw/*20.383*/("""
    """),format.raw/*21.5*/("""</div>

    <div style="height:450px ;" id="fb" class="v-app">
            <!-- Optional placeholder for the loading indicator -->
        <div class=" v-app-loading"></div>

            <!-- Alternative fallback text -->
        <noscript>You have to enable javascript in your browser to
            use an application built with Vaadin.</noscript>
    </div>
    <script type="text/javascript">//<![CDATA[
    if (!window.vaadin)
        alert("Failed to load the bootstrap JavaScript: VAADIN/vaadinBootstrap.js");

    /* The UI Configuration */
    vaadin.initApplication("fb", """),format.raw/*36.34*/("""{"""),format.raw/*36.35*/("""
        """),format.raw/*37.9*/(""""browserDetailsUrl": "GlycanBuilder/",
        "serviceUrl": "GlycanBuilder/",
        "widgetset": "ac.uk.icl.dell.vaadin.glycanbuilder.widgetset.GlycanbuilderWidgetset",
        "theme": "ucdb_2011theme",
        "versionInfo": """),format.raw/*41.24*/("""{"""),format.raw/*41.25*/(""""vaadinVersion": "7.0.0""""),format.raw/*41.49*/("""}"""),format.raw/*41.50*/(""",
        "vaadinDir": "/GlycanBuilder/VAADIN/",
        "heartbeatInterval": 300,
        "debug": false,
        "standalone": false,
        "authErrMsg": """),format.raw/*46.23*/("""{"""),format.raw/*46.24*/("""
            """),format.raw/*47.13*/(""""message": "Take note of any unsaved data, "+
            "and <u>click here<\/u> to continue.",
            "caption": "Authentication problem"
        """),format.raw/*50.9*/("""}"""),format.raw/*50.10*/(""",
        "comErrMsg": """),format.raw/*51.22*/("""{"""),format.raw/*51.23*/("""
            """),format.raw/*52.13*/(""""message": "Take note of any unsaved data, "+
            "and <u>click here<\/u> to continue.",
            "caption": "Communication problem"
        """),format.raw/*55.9*/("""}"""),format.raw/*55.10*/(""",
        "sessExpMsg": """),format.raw/*56.23*/("""{"""),format.raw/*56.24*/("""
            """),format.raw/*57.13*/(""""message": "Take note of any unsaved data, "+
            "and <u>click here<\/u> to continue.",
            "caption": "Session Expired"
        """),format.raw/*60.9*/("""}"""),format.raw/*60.10*/("""
    """),format.raw/*61.5*/("""}"""),format.raw/*61.6*/(""");//]]>
    </script>
    """),format.raw/*66.17*/("""
    """),format.raw/*67.5*/("""<script type="text/javascript">
            var callBack=[];
            callBack.run=function(response)"""),format.raw/*69.44*/("""{"""),format.raw/*69.45*/("""
                """),format.raw/*70.17*/("""document.write(response);
                var r = response;
                //document.write(r);
                //document.form.frm1.Search = 'test this';
                //document.form.frm1.submit();
                //document.forms["frm2"].submit();
                var url = "/builderDigestSearch/s?";
                var x = url + encodeURI(r);
                //var s = [ url, "" , r].join("");
                //document.write(x);
                window.location = x ;
            """),format.raw/*81.13*/("""}"""),format.raw/*81.14*/("""
    """),format.raw/*82.5*/("""</script>

    <form id="frm1" name="frm1" action="ms" method="POST" class="form-search">
        <input type="button" name="Digest" value="Digest" onclick='exportCanvas("glycoct_condensed","callBack");'/>
    </form>


    <div class="row-fluid">
        <div class="span6">
            <h3 class="">Find Structures</h3>
            <p class="builder">
                Build your glycan or epitope and search the UniCarbKB structure database. This new design is built using new technologies and discussed by the developer David Damerell.</p>
        </div>

        <div class="span6">
            <h3 class="">Publication</h3>
            <p class="builder">
                The GlycanBuilder: a fast, intuitive and flexible software tool for building and displaying glycan structures, Alessio Ceroni, Anne Dell, and Stuart M Haslam, Source Code Biol Med. 2007. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17683623">
                PubMed</a></p>
            <p class="builder">
                The GlycanBuilder and GlycoWorkbench glycoinformatics tools: updates and new developments, Damerell D, Ceroni A, Maass K <i>
                et al</i>, Biol Chem. 2012. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23109548">PubMed</a></p>
        </div>
    </div>


""")))}),format.raw/*108.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object builderDigest extends builderDigest_Scope0.builderDigest
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/builderDigest.scala.html
                  HASH: d61fd6f75d04eff0dd9b991122addabe6f4ea6ad
                  MATRIX: 843->1|854->5|892->7|923->12|1762->1196|1794->1201|2404->1783|2433->1784|2469->1793|2727->2023|2756->2024|2808->2048|2837->2049|3023->2207|3052->2208|3093->2221|3273->2374|3302->2375|3353->2398|3382->2399|3423->2412|3602->2564|3631->2565|3683->2589|3712->2590|3753->2603|3926->2749|3955->2750|3987->2755|4015->2756|4069->2908|4101->2913|4233->3017|4262->3018|4307->3035|4824->3524|4853->3525|4885->3530|6181->4795
                  LINES: 32->1|32->1|32->1|33->2|51->20|52->21|67->36|67->36|68->37|72->41|72->41|72->41|72->41|77->46|77->46|78->47|81->50|81->50|82->51|82->51|83->52|86->55|86->55|87->56|87->56|88->57|91->60|91->60|92->61|92->61|94->66|95->67|97->69|97->69|98->70|109->81|109->81|110->82|136->108
                  -- GENERATED --
              */
          