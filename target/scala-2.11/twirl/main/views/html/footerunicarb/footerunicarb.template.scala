
package views.html.footerunicarb

import play.twirl.api._


     object footerunicarb_Scope0 {

class footerunicarb extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<div class="footer row-fluid">
 <div class="span12">
        <p class="pull-left">UniCarbKB 
	<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png" /></a>
	</p>
        <p class="pull-right">Supported by
          <a href="http://www.nectar.org.au">NeCTAR</a> &nbsp;|&nbsp;
          <a href="http://www.ands.org.au">ANDS</a> &nbsp;|&nbsp;
          <a href="http://www.stint.se"> STINT</a> &nbsp;|&nbsp;
          <a href="http://www.snf.ch/"> SNF</a>
        </p>
 </div>
</div>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object footerunicarb extends footerunicarb_Scope0.footerunicarb
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/footerunicarb/footerunicarb.scala.html
                  HASH: 1e044e7100f8032000efabc72c9d503c9976f506
                  MATRIX: 857->0
                  LINES: 32->1
                  -- GENERATED --
              */
          