
package views.html.searchCT

import play.twirl.api._


     object searchCT_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class searchCT extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[models.GlycansCTCombined],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ct: List[models.GlycansCTCombined]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.38*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

    """),format.raw/*5.5*/("""<ul class="breadcrumb" xmlns="http://www.w3.org/1999/html">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>Structure Search - Text Input</h1>
        </div>

        <form class="form-vertical" action =""""),_display_(/*16.47*/(routes.Application.searchCTResults())),format.raw/*16.85*/("""">
            <fieldset>

                    <!-- Form Name -->
                <div class="row">
                    <div class="span12">
                        <div class="row-fluid">
                            <p>Search glycan structures that fully or partially match a defined GlycoCT entry:</p>
                            <div class="controls">
                                <textarea id="textinput" name="glycoct" type="text" placeholder="Paste Sequence" class="input-large span6"  cols="20" rows="10" ></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Search Database</button>
                        </div>
                    </div>
                </div>

            </fieldset>
        </form>

        """),_display_(/*35.10*/if(ct != null)/*35.24*/ {_display_(Seq[Any](format.raw/*35.26*/("""
            """),format.raw/*36.13*/("""<p>"""),_display_(/*36.17*/ct/*36.19*/.size),format.raw/*36.24*/(""" """),format.raw/*36.25*/("""Result</p>
            <table class="table table-condensed table-bordered table-striped volumes">
                <thead>
                    <tr>
                        <th>Accession Number</th>
                        <th>Structure</th>
                    </tr>
                </thead>
                """),_display_(/*44.18*/for(c <- ct) yield /*44.30*/ {_display_(Seq[Any](format.raw/*44.32*/("""
                    """),format.raw/*45.21*/("""<tr>
                        <td>"""),_display_(/*46.30*/c/*46.31*/.database.toUpperCase()),format.raw/*46.54*/(""" """),_display_(/*46.56*/c/*46.57*/.databaseId),format.raw/*46.68*/("""</td>
                        """),_display_(/*47.26*/if(c.database.matches("kb"))/*47.54*/ {_display_(Seq[Any](format.raw/*47.56*/("""
                            """),format.raw/*48.29*/("""<td><a href=""><img class="sugar_image" src="""),_display_(/*48.74*/{
                                routes.Image.showImage(c.databaseId, "cfgl", "extended")
                            }),format.raw/*50.30*/(""" """),format.raw/*50.31*/("""height="50%", width="50%" alt=""/></a></td>
                        """)))}),format.raw/*51.26*/("""
                        """),_display_(/*52.26*/if(c.database.matches("hmg"))/*52.55*/ {_display_(Seq[Any](format.raw/*52.57*/("""
                            """),format.raw/*53.29*/("""<td><a href=""><img class="sugar_image" src="""),_display_(/*53.74*/{
                                routes.Image.showImageHmg(c.databaseId, "cfgl", "extended")
                            }),format.raw/*55.30*/(""" """),format.raw/*55.31*/("""height="50%", width="50%" alt=""/></a></td>
                        """)))}),format.raw/*56.26*/("""
                        """),_display_(/*57.26*/if(c.database.matches("tcd"))/*57.55*/ {_display_(Seq[Any](format.raw/*57.57*/("""
                            """),format.raw/*58.29*/("""<td><a href=""><img class="sugar_image" src="""),_display_(/*58.74*/{
                                routes.Image.showImageTCD(c.databaseId, "cfgl", "extended")
                            }),format.raw/*60.30*/(""" """),format.raw/*60.31*/("""height="50%", width="50%" alt=""/></a></td>
                        """)))}),format.raw/*61.26*/("""
                    """),format.raw/*62.21*/("""</tr>
                """)))}),format.raw/*63.18*/("""
            """),format.raw/*64.13*/("""</table>
        """)))}),format.raw/*65.10*/("""
        """),_display_(/*66.10*/if(ct == null)/*66.24*/ {_display_(Seq[Any](format.raw/*66.26*/("""
            """),format.raw/*67.13*/("""<div class="bluecallout">
            <h2>What is GlycoCT?</h2>
            <p>The GlycoCT format is a common format for encoding carbohydrate sequences that is based on a connection table approach with a controlled vocabulary to name monosaccharides, adopting IUPAC rules to generate a consistent, machine-readable nomenclature.
                The condensed format allows for unique identification of glycan structures in a compact manner.</p>
            <h2>How to get a GlycoCT encoding</h2>
            <p>A number of databases and tools support the export of glycan structures into GlycoCT including GlycoWorkBench and the embedded <a href="/builder">GlycanBuilder</a>.
            Simply, build your structure then 'File'-->'Export'-->'GlycoCT Condensed'</p>
            </div>
        """)))}),format.raw/*75.10*/("""

        """),_display_(/*77.10*/views/*77.15*/.html.footerunicarb.footerunicarb()),format.raw/*77.50*/("""
"""),format.raw/*78.1*/("""</section>

""")))}),format.raw/*80.2*/("""
"""))
      }
    }
  }

  def render(ct:List[models.GlycansCTCombined]): play.twirl.api.HtmlFormat.Appendable = apply(ct)

  def f:((List[models.GlycansCTCombined]) => play.twirl.api.HtmlFormat.Appendable) = (ct) => apply(ct)

  def ref: this.type = this

}


}

/**/
object searchCT extends searchCT_Scope0.searchCT
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/searchCT/searchCT.scala.html
                  HASH: 862b7f3b51e423ce19cea77f3a1f5d1ab2250bf5
                  MATRIX: 784->1|915->37|943->40|954->44|992->46|1024->52|1577->578|1636->616|2457->1410|2480->1424|2520->1426|2561->1439|2592->1443|2603->1445|2629->1450|2658->1451|2993->1759|3021->1771|3061->1773|3110->1794|3171->1828|3181->1829|3225->1852|3254->1854|3264->1855|3296->1866|3354->1897|3391->1925|3431->1927|3488->1956|3560->2001|3701->2121|3730->2122|3830->2191|3883->2217|3921->2246|3961->2248|4018->2277|4090->2322|4234->2445|4263->2446|4363->2515|4416->2541|4454->2570|4494->2572|4551->2601|4623->2646|4767->2769|4796->2770|4896->2839|4945->2860|4999->2883|5040->2896|5089->2914|5126->2924|5149->2938|5189->2940|5230->2953|6056->3748|6094->3759|6108->3764|6164->3799|6192->3800|6235->3813
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|47->16|47->16|66->35|66->35|66->35|67->36|67->36|67->36|67->36|67->36|75->44|75->44|75->44|76->45|77->46|77->46|77->46|77->46|77->46|77->46|78->47|78->47|78->47|79->48|79->48|81->50|81->50|82->51|83->52|83->52|83->52|84->53|84->53|86->55|86->55|87->56|88->57|88->57|88->57|89->58|89->58|91->60|91->60|92->61|93->62|94->63|95->64|96->65|97->66|97->66|97->66|98->67|106->75|108->77|108->77|108->77|109->78|111->80
                  -- GENERATED --
              */
          