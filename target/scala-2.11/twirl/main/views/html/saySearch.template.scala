
package views.html

import play.twirl.api._


     object saySearch_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class saySearch extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[Translation],List[Structure],ArrayList[String],Structure,List[com.avaje.ebean.SqlRow],HashSet[Structure],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(result: List[Translation], structure: List[Structure], taxonomy: ArrayList[String], structureObject: Structure, listSub: List[com.avaje.ebean.SqlRow], subStr: HashSet[Structure]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

Seq[Any](format.raw/*1.181*/("""
"""),format.raw/*3.1*/("""

"""),_display_(/*5.2*/main/*5.6*/{_display_(Seq[Any](format.raw/*5.7*/("""
"""),format.raw/*6.1*/("""<script>
$(document).ready(function () """),format.raw/*7.31*/("""{"""),format.raw/*7.32*/("""

    """),format.raw/*9.5*/("""(function ($) """),format.raw/*9.19*/("""{"""),format.raw/*9.20*/("""

        """),format.raw/*11.9*/("""$('#filter').keyup(function () """),format.raw/*11.40*/("""{"""),format.raw/*11.41*/("""

            """),format.raw/*13.13*/("""var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () """),format.raw/*15.52*/("""{"""),format.raw/*15.53*/("""
                """),format.raw/*16.17*/("""return rex.test($(this).text());
            """),format.raw/*17.13*/("""}"""),format.raw/*17.14*/(""").show();

        """),format.raw/*19.9*/("""}"""),format.raw/*19.10*/(""")

    """),format.raw/*21.5*/("""}"""),format.raw/*21.6*/("""(jQuery));

"""),format.raw/*23.1*/("""}"""),format.raw/*23.2*/(""");
</script>
    <ul class="breadcrumb">
      <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
      <li><i class="icon-wrench" ></i><a href="/builder"> Glycan Builder</a> <span class="divider">></span></li>
      <li class='active'><i class='icon-search'></i> Glycan Search</span></li>
    </ul>

    <section id="layouts" class="browse">

    <div class="page-header row-fluid">
       <h1>Glycan Search</h1>
       <h4 class="subheader">Currently returning results from GlycosuiteDB, EuroCarb, and GlycoBase</h4>
    </div>

    <div class="row-fluid">
        <div class="span3 search">
          <div class="filterbar tabbable">
            <ul class="nav nav-tabs" id="myTabBar">
              <li class='title'>Database filter coming soon: <span class="pull-right count"></span></li>
              <li class='title'>Glycosuite <span class="pull-right count"></span></li>
              <li class='title'>EuroCarb</li>
              <li class='title'>GlycoBase</li>
            </ul>
          </div>
	  <div class="info">
            <h4>Other Options:</h4>
            <p><b>UniCarbKB Search:</b> Search UniCarbKB by Taxonomy, Tissue, Protein, or Composition with <a href="/query">our filtering system</a>.</p>
            <p><b>Curated Publications:</b> Search the <a href="/references">growing list of publications, associated structures, and metadata</a>.</p>
          </div>
        </div>
	<div class="span9">
    	    <ul class="nav nav-tabs" data-tabs="tabs">
      	      <li class="active"><a data-toggle="tab" href="#red">Structure Match</a></li>
              <li><a data-toggle="tab" href="#annotated">Substructure Match</a></li>
            </ul>

            <div class="tab-content">
              <div class="tab-pane active" id="red">
		<a class='btn pull-right' href="/builder"><i class="icon-refresh"></i> New Glycan</a>
                """),_display_(/*63.18*/for(result <- result) yield /*63.39*/{_display_(Seq[Any](format.raw/*63.40*/("""
                 """),format.raw/*64.18*/("""<a href="/structure/"""),_display_(/*64.39*/result/*64.45*/.structure.id),format.raw/*64.58*/(""""><img src="http://www.glycodigest.org:8080/eurocarb/get_sugar_image.action?download=true&scale=1.0&outputType=png&inputType=gws&tolerateUnknown=1&sequences="""),_display_(/*64.216*/{result.gws}),format.raw/*64.228*/("""" /></a>
                """)))}),format.raw/*65.18*/("""
		"""),_display_(/*66.4*/if(structureObject != null )/*66.32*/ {_display_(Seq[Any](format.raw/*66.34*/("""
      		"""),format.raw/*67.9*/("""<div class="bs-callout bs-callout-info">   
		   <h3>Search returned
        	      <small>
          		<span class='icon-book icon-white'></span> """),_display_(/*70.57*/structureObject/*70.72*/.references.size()),format.raw/*70.90*/(""" """),format.raw/*70.91*/("""References,
          		<span class='icon-leaf icon-white'></span> """),_display_(/*71.57*/structureObject/*71.72*/.strtaxonomy.size()),format.raw/*71.91*/(""" """),format.raw/*71.92*/("""Biological Associations,
          		<span class='icon-map-marker icon-white'></span> """),_display_(/*72.63*/structureObject/*72.78*/.stsource.size()),format.raw/*72.94*/(""" """),format.raw/*72.95*/("""Proteins
        	      </small>
      		   </h3>
    		</div>
    		""")))}/*76.9*/else/*76.14*/{_display_(Seq[Any](format.raw/*76.15*/("""
		   """),format.raw/*77.6*/("""<h3>Search returned</h3>
			<div class="bs-callout bs-callout-warning">
				<p>The structure submitted could not be found! <br/>Only structures completely matching the submitted glycan topology will be returned.</p>
				<p>Check any similar structure in the 'Substructure' tab above!</p>
			</div>	
		""")))}),format.raw/*82.4*/("""
		"""),_display_(/*83.4*/if(structure != null )/*83.26*/ {_display_(Seq[Any](format.raw/*83.28*/("""
    		   """),format.raw/*84.10*/("""<div class='row-fluid'>
      		   	<h3>Publications</h3>
      			<div class="details span9">
        		"""),_display_(/*87.12*/for(stref <- structure) yield /*87.35*/{_display_(Seq[Any](format.raw/*87.36*/("""
          		"""),_display_(/*88.14*/for( (reference,i) <- stref.references.zipWithIndex; if (i <=3)) yield /*88.78*/{_display_(Seq[Any](format.raw/*88.79*/("""
            		"""),format.raw/*89.15*/("""<p><a href="../references/"""),_display_(/*89.42*/reference/*89.51*/.reference.id),format.raw/*89.64*/("""" target="_blank">"""),_display_(/*89.83*/reference/*89.92*/.reference.title),format.raw/*89.108*/("""</a>, """),_display_(/*89.115*/reference/*89.124*/.reference.year),format.raw/*89.139*/(""", """),_display_(/*89.142*/reference/*89.151*/.reference.authors),format.raw/*89.169*/("""</p>
          		""")))}),format.raw/*90.14*/("""
        		""")))}),format.raw/*91.12*/("""
      			"""),format.raw/*92.10*/("""</div>
    		  </div>
    		""")))}),format.raw/*94.8*/("""

		"""),_display_(/*96.4*/if(structureObject != null )/*96.32*/ {_display_(Seq[Any](format.raw/*96.34*/("""
    		   """),format.raw/*97.10*/("""<div class="row-fluid">
      			<h3>Biological Associations</h3>
      			<div class="details span9">
        		"""),_display_(/*100.12*/for( (tax,j) <- taxonomy.zipWithIndex; if (j <=3)) yield /*100.62*/{_display_(Seq[Any](format.raw/*100.63*/("""
        		"""),format.raw/*101.11*/("""<a href="" style="font-size: 14px; font-weight: normal; line-height: 22px; margin-bottom: 9px; color: #4B5C66;">"""),_display_(/*101.124*/tax),format.raw/*101.127*/(""", </a>
                    	""")))}),format.raw/*102.23*/("""
		""")))}),format.raw/*103.4*/("""

            """),format.raw/*105.13*/("""</div>
            <div class="tab-pane" id="annotated">
		 """),_display_(/*107.5*/if(subStr.size() > 0)/*107.26*/ {_display_(Seq[Any](format.raw/*107.28*/("""
    		"""),format.raw/*108.7*/("""<h3>GlycoSuite Substructure Matches (<i>"""),_display_(/*108.48*/subStr/*108.54*/.size()),format.raw/*108.61*/(""" """),format.raw/*108.62*/("""hits</i>)</h3>	
		 <div class="input-group"> <span class="input-group-addon">Filter</span>
        	   <input id="filter" type="text" class="form-control" placeholder="Type species/glycoprotein/author...">
        	</div>
		<table id="demo" class="table table-striped table-bordered table-condensed">
                <thead><th>Structure</th><th>Taxonomy</th></thead>
                <tbody class="searchable">
                """),_display_(/*115.18*/for(ls <- subStr) yield /*115.35*/ {_display_(Seq[Any](format.raw/*115.37*/("""
                """),_display_(/*116.18*/for(l <- ls.translation) yield /*116.42*/ {_display_(Seq[Any](format.raw/*116.44*/("""
                   """),format.raw/*117.20*/("""<tr><td><a href="/structure/"""),_display_(/*117.49*/l/*117.50*/.structure.id),format.raw/*117.63*/(""""><img src="http://www.glycodigest.org:8080/eurocarb/get_sugar_image.action?download=true&scale=1.0&outputType=png&inputType=gws&tolerateUnknown=1&sequences="""),_display_(/*117.221*/{l.gws}),format.raw/*117.228*/(""".png" /></a>
                   </td><td>
		 
		   """),_display_(/*120.7*/l/*120.8*/.structure.strproteintaxbiolsource.groupBy(_.taxonomy).map/*120.66*/{ case (i, j) =>_display_(Seq[Any](format.raw/*120.82*/(""" """),format.raw/*120.83*/("""<p>"""),_display_(/*120.87*/i/*120.88*/.species),format.raw/*120.96*/("""   
		   """),format.raw/*121.118*/("""
		   """),_display_(/*122.7*/j/*122.8*/.groupBy(_.reference).map/*122.33*/{ case (y,z) =>_display_(Seq[Any](format.raw/*122.48*/(""" """),format.raw/*122.49*/("""(<a href="/references/"""),_display_(/*122.72*/y/*122.73*/.id),format.raw/*122.76*/("""" target="_blank">"""),_display_(/*122.95*/y/*122.96*/.first),format.raw/*122.102*/(""" """),format.raw/*122.103*/(""", """),_display_(/*122.106*/y/*122.107*/.year),format.raw/*122.112*/("""</a>) 
		   """),_display_(/*123.7*/z/*123.8*/.groupBy(_.proteins).map/*123.32*/{ case (d,e) =>_display_(Seq[Any](format.raw/*123.47*/("""  """),_display_(/*123.50*/if(d != null )/*123.64*/ {_display_(Seq[Any](format.raw/*123.66*/("""<span class="label label-default">"""),_display_(/*123.101*/d/*123.102*/.name),format.raw/*123.107*/("""</span>  """)))}),format.raw/*123.117*/("""
		   """),format.raw/*124.6*/("""</p>
		   """)))})))}),format.raw/*125.8*/("""
		   
		   
		   """),format.raw/*128.95*/("""
		   """)))}),format.raw/*129.7*/("""
		   

		   

                """),format.raw/*134.17*/("""</td></tr>
                """)))}),format.raw/*135.18*/("""
                """)))}),format.raw/*136.18*/("""
                """),format.raw/*137.17*/("""</tbody></table>
		""")))}),format.raw/*138.4*/("""		

            """),format.raw/*140.13*/("""</div>
        </div>
    </div>
""")))}),format.raw/*143.2*/("""
"""))
      }
    }
  }

  def render(result:List[Translation],structure:List[Structure],taxonomy:ArrayList[String],structureObject:Structure,listSub:List[com.avaje.ebean.SqlRow],subStr:HashSet[Structure]): play.twirl.api.HtmlFormat.Appendable = apply(result,structure,taxonomy,structureObject,listSub,subStr)

  def f:((List[Translation],List[Structure],ArrayList[String],Structure,List[com.avaje.ebean.SqlRow],HashSet[Structure]) => play.twirl.api.HtmlFormat.Appendable) = (result,structure,taxonomy,structureObject,listSub,subStr) => apply(result,structure,taxonomy,structureObject,listSub,subStr)

  def ref: this.type = this

}


}

/**/
object saySearch extends saySearch_Scope0.saySearch
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/saySearch.scala.html
                  HASH: 784883d51cdb7966be6670abc5a283bf4daf74c8
                  MATRIX: 856->1|1146->180|1173->198|1201->201|1212->205|1249->206|1276->207|1342->246|1370->247|1402->253|1443->267|1471->268|1508->278|1567->309|1596->310|1638->324|1799->457|1828->458|1873->475|1946->520|1975->521|2021->540|2050->541|2084->548|2112->549|2151->561|2179->562|4116->2472|4153->2493|4192->2494|4238->2512|4286->2533|4301->2539|4335->2552|4521->2710|4555->2722|4612->2748|4642->2752|4679->2780|4719->2782|4755->2791|4930->2939|4954->2954|4993->2972|5022->2973|5117->3041|5141->3056|5181->3075|5210->3076|5324->3163|5348->3178|5385->3194|5414->3195|5502->3266|5515->3271|5554->3272|5587->3278|5919->3580|5949->3584|5980->3606|6020->3608|6058->3618|6191->3724|6230->3747|6269->3748|6310->3762|6390->3826|6429->3827|6472->3842|6526->3869|6544->3878|6578->3891|6624->3910|6642->3919|6680->3935|6715->3942|6734->3951|6771->3966|6802->3969|6821->3978|6861->3996|6910->4014|6953->4026|6991->4036|7050->4065|7081->4070|7118->4098|7158->4100|7196->4110|7338->4224|7405->4274|7445->4275|7485->4286|7627->4399|7653->4402|7714->4431|7749->4435|7792->4449|7880->4510|7911->4531|7952->4533|7987->4540|8056->4581|8072->4587|8101->4594|8131->4595|8587->5023|8621->5040|8662->5042|8708->5060|8749->5084|8790->5086|8839->5106|8896->5135|8907->5136|8942->5149|9129->5307|9159->5314|9238->5366|9248->5367|9316->5425|9371->5441|9401->5442|9433->5446|9444->5447|9474->5455|9513->5576|9547->5583|9557->5584|9592->5609|9646->5624|9676->5625|9727->5648|9738->5649|9763->5652|9810->5671|9821->5672|9850->5678|9881->5679|9913->5682|9925->5683|9953->5688|9993->5701|10003->5702|10037->5726|10091->5741|10122->5744|10146->5758|10187->5760|10251->5795|10263->5796|10291->5801|10334->5811|10368->5817|10414->5829|10461->5936|10499->5943|10559->5974|10619->6002|10669->6020|10715->6037|10766->6057|10811->6073|10876->6107
                  LINES: 27->1|32->1|33->3|35->5|35->5|35->5|36->6|37->7|37->7|39->9|39->9|39->9|41->11|41->11|41->11|43->13|45->15|45->15|46->16|47->17|47->17|49->19|49->19|51->21|51->21|53->23|53->23|93->63|93->63|93->63|94->64|94->64|94->64|94->64|94->64|94->64|95->65|96->66|96->66|96->66|97->67|100->70|100->70|100->70|100->70|101->71|101->71|101->71|101->71|102->72|102->72|102->72|102->72|106->76|106->76|106->76|107->77|112->82|113->83|113->83|113->83|114->84|117->87|117->87|117->87|118->88|118->88|118->88|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|119->89|120->90|121->91|122->92|124->94|126->96|126->96|126->96|127->97|130->100|130->100|130->100|131->101|131->101|131->101|132->102|133->103|135->105|137->107|137->107|137->107|138->108|138->108|138->108|138->108|138->108|145->115|145->115|145->115|146->116|146->116|146->116|147->117|147->117|147->117|147->117|147->117|147->117|150->120|150->120|150->120|150->120|150->120|150->120|150->120|150->120|151->121|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|152->122|153->123|153->123|153->123|153->123|153->123|153->123|153->123|153->123|153->123|153->123|153->123|154->124|155->125|158->128|159->129|164->134|165->135|166->136|167->137|168->138|170->140|173->143
                  -- GENERATED --
              */
          