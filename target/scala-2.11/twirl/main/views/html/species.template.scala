
package views.html

import play.twirl.api._


     object species_Scope0 {
import java.util._

import play.api.i18n._
import play.core.j.PlayMagicForJava._
import views.html._

import scala.collection.JavaConversions._

class species extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,List[String],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, speciesDetails: List[String], jsSpecies: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
	"""),format.raw/*4.2*/("""<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js" type="text/javascript"></script>

<script>
	$(function() """),format.raw/*7.15*/("""{"""),format.raw/*7.16*/("""
	

	"""),format.raw/*10.2*/("""var myArray = new Array();
	"""),_display_(/*11.3*/for(a <- speciesDetails) yield /*11.27*/ {_display_(Seq[Any](format.raw/*11.29*/("""
	"""),format.raw/*12.2*/("""myArray.push(""""),_display_(/*12.17*/a),format.raw/*12.18*/("""");
	""")))}),format.raw/*13.3*/("""
	
		"""),format.raw/*15.3*/("""$( "#tags" ).autocomplete("""),format.raw/*15.29*/("""{"""),format.raw/*15.30*/("""
			"""),format.raw/*16.4*/("""source: myArray, 
			minLength: 3,
			select: function (event, ui) """),format.raw/*18.33*/("""{"""),format.raw/*18.34*/("""
			"""),format.raw/*19.4*/("""var selectobj = ui.item;
			$("#hidCidade").val(ui.item.label);
			
			"""),format.raw/*22.4*/("""}"""),format.raw/*22.5*/("""
			
		"""),format.raw/*24.3*/("""}"""),format.raw/*24.4*/(""");
	"""),format.raw/*25.2*/("""}"""),format.raw/*25.3*/(""");
	</script>


	<h1 id="homeTitle">"""),_display_(/*29.22*/Messages("EXample Taxonomy Search with typeahead")),format.raw/*29.72*/("""</h1>

	<form action="taxonomysearch" method="GET">
	<label for="">Taxonomy: </label>
	<input id="tags" class="ui-widget" name="taxonomy">
	<input id="hidCidade" type="hidden" />
	<input type="submit" value="go" />
	</form>
    
""")))}),format.raw/*38.2*/("""
"""))
      }
    }
  }

  def render(message:String,speciesDetails:List[String],jsSpecies:String): play.twirl.api.HtmlFormat.Appendable = apply(message,speciesDetails,jsSpecies)

  def f:((String,List[String],String) => play.twirl.api.HtmlFormat.Appendable) = (message,speciesDetails,jsSpecies) => apply(message,speciesDetails,jsSpecies)

  def ref: this.type = this

}


}

/**/
object species extends species_Scope0.species
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/species.scala.html
                  HASH: c59a24f64b7e1d3ec3ebfa7ca1f5bafc479d19ef
                  MATRIX: 769->1|930->67|958->70|969->74|1007->76|1035->78|1183->199|1211->200|1243->205|1298->234|1338->258|1378->260|1407->262|1449->277|1471->278|1507->284|1539->289|1593->315|1622->316|1653->320|1748->387|1777->388|1808->392|1906->463|1934->464|1968->471|1996->472|2027->476|2055->477|2119->514|2190->564|2450->794
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|38->7|38->7|41->10|42->11|42->11|42->11|43->12|43->12|43->12|44->13|46->15|46->15|46->15|47->16|49->18|49->18|50->19|53->22|53->22|55->24|55->24|56->25|56->25|60->29|60->29|69->38
                  -- GENERATED --
              */
          