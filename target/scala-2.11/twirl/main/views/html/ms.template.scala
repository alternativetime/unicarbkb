
package views.html

import play.twirl.api._


     object ms_Scope0 {
import controllers._

     object ms_Scope1 {

class ms extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**********************************
* Helper generating table headers *
***********************************/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.1*/("""<!DOCTYPE html>
<html>
    <head>
        <title>Reference</title>
         <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*11.71*/routes/*11.77*/.Assets.versioned("assets/stylesheets/bootstrap.min.css")),format.raw/*11.134*/(""""> 
         <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*12.71*/routes/*12.77*/.Assets.versioned("assets/stylesheets/style.css")),format.raw/*12.126*/(""""> 
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*13.70*/routes/*13.76*/.Assets.versioned("assets/stylesheets/customMarkdown.css")),format.raw/*13.134*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*14.70*/routes/*14.76*/.Assets.versioned("assets/stylesheets/bootstrap-lightbox.css")),format.raw/*14.138*/("""">
        <script src="http://static.scripting.com/github/bootstrap2/js/bootstrap-modal.js"></script>
	<script src=""""),_display_(/*16.16*/routes/*16.22*/.Assets.versioned("assets/javascripts/bootstrap-modal.js")),format.raw/*16.80*/(""""></script>
        <script src=""""),_display_(/*17.23*/routes/*17.29*/.Assets.versioned("assets/javascripts/jquery.ae.image.resize.min.js")),format.raw/*17.98*/(""""></script>
        <script src=""""),_display_(/*18.23*/routes/*18.29*/.Assets.versioned("assets/javascripts/bootstrap.min.js")),format.raw/*18.85*/(""""></script>
        <script src=""""),_display_(/*19.23*/routes/*19.29*/.Assets.versioned("assets/javascripts/jquery.tablesorter.min.js")),format.raw/*19.94*/(""""></script>
        <script src=""""),_display_(/*20.23*/routes/*20.29*/.Assets.versioned("assets/javascripts/bootstrap-lightbox.min.js")),format.raw/*20.94*/(""""></script>
        <script src=""""),_display_(/*21.23*/routes/*21.29*/.Assets.versioned("assets/javascripts/showdown.js")),format.raw/*21.80*/(""""></script>
        <script src=""""),_display_(/*22.23*/routes/*22.29*/.Assets.versioned("assets/javascripts/google-jsapi.js")),format.raw/*22.84*/(""""></script>
        <script src=""""),_display_(/*23.23*/routes/*23.29*/.Assets.versioned("assets/javascripts/jPages.js")),format.raw/*23.78*/(""""></script>
        <script src=""""),_display_(/*24.23*/routes/*24.29*/.Assets.versioned("assets/javascripts/jquery.lazyload.js")),format.raw/*24.87*/(""""></script>

	<script>

/* Download lazy load plugin and make sure you add it in the head of your page. */

/* when document is ready */
$(function() """),format.raw/*31.14*/("""{"""),format.raw/*31.15*/("""
    
    """),format.raw/*33.5*/("""/* initiate lazyload defining a custom event to trigger image loading  */
    $("ul li img").lazyload("""),format.raw/*34.29*/("""{"""),format.raw/*34.30*/("""
        """),format.raw/*35.9*/("""event : "turnPage",
        effect : "fadeIn"
    """),format.raw/*37.5*/("""}"""),format.raw/*37.6*/(""");

    /* initiate plugin */
    $("div.pagination").jPages("""),format.raw/*40.32*/("""{"""),format.raw/*40.33*/("""
        """),format.raw/*41.9*/("""containerID : "itemContainer",
        animation   : "fadeInUp",
        callback    : function( pages, items )"""),format.raw/*43.47*/("""{"""),format.raw/*43.48*/("""
            """),format.raw/*44.13*/("""/* lazy load current images */
            items.showing.find("img").trigger("turnPage");
            /* lazy load next page images */
            items.oncoming.find("img").trigger("turnPage");
        """),format.raw/*48.9*/("""}"""),format.raw/*48.10*/("""
    """),format.raw/*49.5*/("""}"""),format.raw/*49.6*/(""");

"""),format.raw/*51.1*/("""}"""),format.raw/*51.2*/(""");
</script>



<style type="text/css">

	.holder """),format.raw/*58.10*/("""{"""),format.raw/*58.11*/("""
    		"""),format.raw/*59.7*/("""margin: 15px 0;
    	"""),format.raw/*60.6*/("""}"""),format.raw/*60.7*/("""
    
    	"""),format.raw/*62.6*/(""".holder a """),format.raw/*62.16*/("""{"""),format.raw/*62.17*/("""
    		"""),format.raw/*63.7*/("""font-size: 14px;
    		cursor: pointer;
    		margin: 0 5px;
    		color: #0F466F; 
    	"""),format.raw/*67.6*/("""}"""),format.raw/*67.7*/("""
    
    	"""),format.raw/*69.6*/(""".holder a:hover """),format.raw/*69.22*/("""{"""),format.raw/*69.23*/("""
    		"""),format.raw/*70.7*/("""background-color: #222;
    		color: #fff;
    	"""),format.raw/*72.6*/("""}"""),format.raw/*72.7*/("""
    
    	"""),format.raw/*74.6*/(""".holder a.jp-previous """),format.raw/*74.28*/("""{"""),format.raw/*74.29*/(""" """),format.raw/*74.30*/("""margin-right: 15px; """),format.raw/*74.50*/("""}"""),format.raw/*74.51*/("""
    	"""),format.raw/*75.6*/(""".holder a.jp-next """),format.raw/*75.24*/("""{"""),format.raw/*75.25*/(""" """),format.raw/*75.26*/("""margin-left: 15px; """),format.raw/*75.45*/("""}"""),format.raw/*75.46*/("""
    
    	"""),format.raw/*77.6*/(""".holder a.jp-current, a.jp-current:hover """),format.raw/*77.47*/("""{"""),format.raw/*77.48*/(""" 
    		"""),format.raw/*78.7*/("""color: #FF4242;
    		font-weight: bold;
    	"""),format.raw/*80.6*/("""}"""),format.raw/*80.7*/("""
    
    	"""),format.raw/*82.6*/(""".holder a.jp-disabled, a.jp-disabled:hover """),format.raw/*82.49*/("""{"""),format.raw/*82.50*/("""
    		"""),format.raw/*83.7*/("""color: #bbb;
    	"""),format.raw/*84.6*/("""}"""),format.raw/*84.7*/("""
    
    	"""),format.raw/*86.6*/(""".holder a.jp-current, a.jp-current:hover,
    	.holder a.jp-disabled, a.jp-disabled:hover """),format.raw/*87.49*/("""{"""),format.raw/*87.50*/("""
    		"""),format.raw/*88.7*/("""cursor: default; 
    		background: none;
    	"""),format.raw/*90.6*/("""}"""),format.raw/*90.7*/("""
    
    	"""),format.raw/*92.6*/(""".holder span """),format.raw/*92.19*/("""{"""),format.raw/*92.20*/(""" """),format.raw/*92.21*/("""margin: 0 5px; """),format.raw/*92.36*/("""}"""),format.raw/*92.37*/("""
        
		"""),format.raw/*94.3*/("""/* content2 */

div#content2 """),format.raw/*96.14*/("""{"""),format.raw/*96.15*/(""" """),format.raw/*96.16*/("""float: left; width: 680px; """),format.raw/*96.43*/("""}"""),format.raw/*96.44*/("""
"""),format.raw/*97.1*/("""div#content2 h2 """),format.raw/*97.17*/("""{"""),format.raw/*97.18*/(""" """),format.raw/*97.19*/("""font-size: 21px; margin-bottom: 30px; """),format.raw/*97.57*/("""}"""),format.raw/*97.58*/("""
"""),format.raw/*98.1*/("""div#content2 h3 """),format.raw/*98.17*/("""{"""),format.raw/*98.18*/(""" """),format.raw/*98.19*/("""margin: 5px 0px 10px 0px; """),format.raw/*98.45*/("""}"""),format.raw/*98.46*/("""
"""),format.raw/*99.1*/("""div#content2 p """),format.raw/*99.16*/("""{"""),format.raw/*99.17*/(""" """),format.raw/*99.18*/("""line-height:1.5; """),format.raw/*99.35*/("""}"""),format.raw/*99.36*/("""
"""),format.raw/*100.1*/("""div#content2 p a """),format.raw/*100.18*/("""{"""),format.raw/*100.19*/(""" """),format.raw/*100.20*/("""color: white; background: #D4EE5E; font-weight: bold; """),format.raw/*100.74*/("""}"""),format.raw/*100.75*/("""
"""),format.raw/*101.1*/("""div#content2 p a:hover """),format.raw/*101.24*/("""{"""),format.raw/*101.25*/(""" """),format.raw/*101.26*/("""background: black; """),format.raw/*101.45*/("""}"""),format.raw/*101.46*/("""
"""),format.raw/*102.1*/("""div#content2 p + div """),format.raw/*102.22*/("""{"""),format.raw/*102.23*/(""" """),format.raw/*102.24*/("""margin-top: 50px; """),format.raw/*102.42*/("""}"""),format.raw/*102.43*/("""
"""),format.raw/*103.1*/("""div#content2 h3 span """),format.raw/*103.22*/("""{"""),format.raw/*103.23*/(""" """),format.raw/*103.24*/("""background-color: #F4FAD2; color: #5C826D; padding: 2px 3px; margin-left: 10px; font-weight: normal; """),format.raw/*103.125*/("""}"""),format.raw/*103.126*/("""

"""),format.raw/*105.1*/("""/* content2 -> Code */

div#content2 div.codeBlocks """),format.raw/*107.29*/("""{"""),format.raw/*107.30*/(""" """),format.raw/*107.31*/("""margin: 20px 0 35px; """),format.raw/*107.52*/("""}"""),format.raw/*107.53*/("""

"""),format.raw/*109.1*/("""div#content2 pre """),format.raw/*109.18*/("""{"""),format.raw/*109.19*/(""" """),format.raw/*109.20*/("""float: left; margin-right: 10px; cursor: pointer; """),format.raw/*109.70*/("""}"""),format.raw/*109.71*/("""
"""),format.raw/*110.1*/("""div#content2 pre:before """),format.raw/*110.25*/("""{"""),format.raw/*110.26*/(""" """),format.raw/*110.27*/("""content2: attr(class); padding: 2px 3px; color: white; """),format.raw/*110.82*/("""}"""),format.raw/*110.83*/("""

"""),format.raw/*112.1*/("""div#content2 pre.html """),format.raw/*112.23*/("""{"""),format.raw/*112.24*/(""" """),format.raw/*112.25*/("""background: #F0F2EB; border-bottom: 1px solid #D7D9D3; """),format.raw/*112.80*/("""}"""),format.raw/*112.81*/("""
"""),format.raw/*113.1*/("""div#content2 pre.html:before """),format.raw/*113.30*/("""{"""),format.raw/*113.31*/(""" """),format.raw/*113.32*/("""background: #D7D9D3; """),format.raw/*113.53*/("""}"""),format.raw/*113.54*/("""

"""),format.raw/*115.1*/("""div#content2 pre.javascript """),format.raw/*115.29*/("""{"""),format.raw/*115.30*/(""" """),format.raw/*115.31*/("""background: #F4FAD2; border-bottom: 1px solid #DBE0BC; """),format.raw/*115.86*/("""}"""),format.raw/*115.87*/("""
"""),format.raw/*116.1*/("""div#content2 pre.javascript:before """),format.raw/*116.36*/("""{"""),format.raw/*116.37*/(""" """),format.raw/*116.38*/("""background: #DBE0BC; """),format.raw/*116.59*/("""}"""),format.raw/*116.60*/("""

"""),format.raw/*118.1*/("""div#content2 pre.css """),format.raw/*118.22*/("""{"""),format.raw/*118.23*/(""" """),format.raw/*118.24*/("""background: #E1EDB9; border-bottom: 1px solid #CAD5A6; """),format.raw/*118.79*/("""}"""),format.raw/*118.80*/("""
"""),format.raw/*119.1*/("""div#content2 pre.css:before """),format.raw/*119.29*/("""{"""),format.raw/*119.30*/(""" """),format.raw/*119.31*/("""background: #CAD5A6; """),format.raw/*119.52*/("""}"""),format.raw/*119.53*/("""

"""),format.raw/*121.1*/("""div#content2 pre.collapse """),format.raw/*121.27*/("""{"""),format.raw/*121.28*/(""" """),format.raw/*121.29*/("""position: relative; padding:2% 3%; width:94%; """),format.raw/*121.75*/("""}"""),format.raw/*121.76*/("""
"""),format.raw/*122.1*/("""div#content2 pre.collapse:before """),format.raw/*122.34*/("""{"""),format.raw/*122.35*/(""" """),format.raw/*122.36*/("""position: absolute; right: 0; top: 0; """),format.raw/*122.74*/("""}"""),format.raw/*122.75*/("""

"""),format.raw/*124.1*/("""div#content2 pre code """),format.raw/*124.23*/("""{"""),format.raw/*124.24*/(""" """),format.raw/*124.25*/("""display: none; background: none; font: 11px/16px "Courier New", Courier, monospace; """),format.raw/*124.109*/("""}"""),format.raw/*124.110*/("""
"""),format.raw/*125.1*/("""div#content2 pre.collapse code """),format.raw/*125.32*/("""{"""),format.raw/*125.33*/(""" """),format.raw/*125.34*/("""display:block; """),format.raw/*125.49*/("""}"""),format.raw/*125.50*/("""

"""),format.raw/*127.1*/("""/* content2 -> Unordered Lists */

div#content2 ul#itemContainer """),format.raw/*129.31*/("""{"""),format.raw/*129.32*/(""" """),format.raw/*129.33*/("""list-style: none; padding:0; margin: 20px 0;  """),format.raw/*129.79*/("""}"""),format.raw/*129.80*/("""
"""),format.raw/*130.1*/("""div#content2 ul#itemContainer li """),format.raw/*130.34*/("""{"""),format.raw/*130.35*/(""" """),format.raw/*130.36*/("""display: inline-block; margin: 5px; zoom: 1; *display:inline; """),format.raw/*130.98*/("""}"""),format.raw/*130.99*/("""
"""),format.raw/*131.1*/("""div#content2 ul#itemContainer ll li img """),format.raw/*131.41*/("""{"""),format.raw/*131.42*/(""" """),format.raw/*131.43*/("""vertical-align: bottom; width: 125px; height: 125px; """),format.raw/*131.96*/("""}"""),format.raw/*131.97*/("""

"""),format.raw/*133.1*/("""/* content2 -> index */

div#content2.index pre """),format.raw/*135.24*/("""{"""),format.raw/*135.25*/(""" """),format.raw/*135.26*/("""float:none; cursor:default; """),format.raw/*135.54*/("""}"""),format.raw/*135.55*/("""
"""),format.raw/*136.1*/("""div#content2.index pre code """),format.raw/*136.29*/("""{"""),format.raw/*136.30*/(""" """),format.raw/*136.31*/("""display:block; """),format.raw/*136.46*/("""}"""),format.raw/*136.47*/("""
"""),format.raw/*137.1*/("""div#content2.index h2 """),format.raw/*137.23*/("""{"""),format.raw/*137.24*/(""" """),format.raw/*137.25*/("""margin-top: 55px; """),format.raw/*137.43*/("""}"""),format.raw/*137.44*/("""
"""),format.raw/*138.1*/("""div#content2.index dl dt,
div#content2.index dl dd """),format.raw/*139.26*/("""{"""),format.raw/*139.27*/(""" """),format.raw/*139.28*/("""float: left; margin-top: 10px; """),format.raw/*139.59*/("""}"""),format.raw/*139.60*/("""
"""),format.raw/*140.1*/("""div#content2.index dl dt """),format.raw/*140.26*/("""{"""),format.raw/*140.27*/(""" """),format.raw/*140.28*/("""clear: left; width: 70px; font-style: italic; """),format.raw/*140.74*/("""}"""),format.raw/*140.75*/("""
"""),format.raw/*141.1*/("""div#content2.index dl dd """),format.raw/*141.26*/("""{"""),format.raw/*141.27*/(""" """),format.raw/*141.28*/("""width: 450px; """),format.raw/*141.42*/("""}"""),format.raw/*141.43*/("""
"""),format.raw/*142.1*/("""div#content2.index i + p """),format.raw/*142.26*/("""{"""),format.raw/*142.27*/(""" """),format.raw/*142.28*/("""margin-top: 5px; """),format.raw/*142.45*/("""}"""),format.raw/*142.46*/("""

"""),format.raw/*144.1*/("""/* content2 -> documentation */

div#content2.documentation h2 """),format.raw/*146.31*/("""{"""),format.raw/*146.32*/(""" """),format.raw/*146.33*/("""margin-top: 55px; """),format.raw/*146.51*/("""}"""),format.raw/*146.52*/("""
"""),format.raw/*147.1*/("""div#content2.documentation dl dt,
div#content2.documentation dl dd """),format.raw/*148.34*/("""{"""),format.raw/*148.35*/(""" """),format.raw/*148.36*/("""float: left; margin-top: 10px; """),format.raw/*148.67*/("""}"""),format.raw/*148.68*/("""
"""),format.raw/*149.1*/("""div#content2.documentation dl dt """),format.raw/*149.34*/("""{"""),format.raw/*149.35*/(""" """),format.raw/*149.36*/("""clear: left; width: 70px; font-style: italic; """),format.raw/*149.82*/("""}"""),format.raw/*149.83*/("""
"""),format.raw/*150.1*/("""div#content2.documentation dl dd """),format.raw/*150.34*/("""{"""),format.raw/*150.35*/(""" """),format.raw/*150.36*/("""width: 450px; """),format.raw/*150.50*/("""}"""),format.raw/*150.51*/("""
"""),format.raw/*151.1*/("""div#content2.documentation i + p """),format.raw/*151.34*/("""{"""),format.raw/*151.35*/(""" """),format.raw/*151.36*/("""margin-top: 5px; """),format.raw/*151.53*/("""}"""),format.raw/*151.54*/("""

"""),format.raw/*153.1*/("""/* content2 -> author */

div#content2.author img """),format.raw/*155.25*/("""{"""),format.raw/*155.26*/(""" """),format.raw/*155.27*/("""margin-bottom: 50px; """),format.raw/*155.48*/("""}"""),format.raw/*155.49*/("""
"""),format.raw/*156.1*/("""div#content2.author img + p:before """),format.raw/*156.36*/("""{"""),format.raw/*156.37*/(""" """),format.raw/*156.38*/("""content2: '“'; font-size:65px; font-family: Georgia; position:absolute; top: -55px; color: #ccc; """),format.raw/*156.135*/("""}"""),format.raw/*156.136*/("""
"""),format.raw/*157.1*/("""div#content2.author p """),format.raw/*157.23*/("""{"""),format.raw/*157.24*/(""" """),format.raw/*157.25*/("""width: 590px; position: relative; """),format.raw/*157.59*/("""}"""),format.raw/*157.60*/("""

"""),format.raw/*159.1*/("""/* =============================================================================
   Non-semantic helper classes
   Please define your styles before this section.
   ========================================================================== */

/* For image replacement */
.ir """),format.raw/*165.5*/("""{"""),format.raw/*165.6*/(""" """),format.raw/*165.7*/("""display: block; text-indent: -999em; overflow: hidden; background-repeat: no-repeat; text-align: left; direction: ltr; """),format.raw/*165.126*/("""}"""),format.raw/*165.127*/("""
"""),format.raw/*166.1*/(""".ir br """),format.raw/*166.8*/("""{"""),format.raw/*166.9*/(""" """),format.raw/*166.10*/("""display: none; """),format.raw/*166.25*/("""}"""),format.raw/*166.26*/("""

"""),format.raw/*168.1*/("""/* Hide for both screenreaders and browsers:
   css-discuss.incutio.com/wiki/Screenreader_Visibility */
.hidden """),format.raw/*170.9*/("""{"""),format.raw/*170.10*/(""" """),format.raw/*170.11*/("""display: none; visibility: hidden; """),format.raw/*170.46*/("""}"""),format.raw/*170.47*/("""

"""),format.raw/*172.1*/("""/* Hide only visually, but have it available for screenreaders: by Jon Neal.
  www.webaim.org/techniques/css/invisiblecontent2/  &  j.mp/visuallyhidden */
.visuallyhidden """),format.raw/*174.17*/("""{"""),format.raw/*174.18*/(""" """),format.raw/*174.19*/("""border: 0; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px; """),format.raw/*174.140*/("""}"""),format.raw/*174.141*/("""

"""),format.raw/*176.1*/("""/* Extends the .visuallyhidden class to allow the element to be focusable when navigated to via the keyboard: drupal.org/node/897638 */
.visuallyhidden.focusable:active, .visuallyhidden.focusable:focus """),format.raw/*177.67*/("""{"""),format.raw/*177.68*/(""" """),format.raw/*177.69*/("""clip: auto; height: auto; margin: 0; overflow: visible; position: static; width: auto; """),format.raw/*177.156*/("""}"""),format.raw/*177.157*/("""

"""),format.raw/*179.1*/("""/* Hide visually and from screenreaders, but maintain layout */
.invisible """),format.raw/*180.12*/("""{"""),format.raw/*180.13*/(""" """),format.raw/*180.14*/("""visibility: hidden; """),format.raw/*180.34*/("""}"""),format.raw/*180.35*/("""

"""),format.raw/*182.1*/("""/* Contain floats: nicolasgallagher.com/micro-clearfix-hack/ */ 
.clearfix:before, .clearfix:after """),format.raw/*183.35*/("""{"""),format.raw/*183.36*/(""" """),format.raw/*183.37*/("""content2: ""; display: table; """),format.raw/*183.67*/("""}"""),format.raw/*183.68*/("""
"""),format.raw/*184.1*/(""".clearfix:after """),format.raw/*184.17*/("""{"""),format.raw/*184.18*/(""" """),format.raw/*184.19*/("""clear: both; """),format.raw/*184.32*/("""}"""),format.raw/*184.33*/("""
"""),format.raw/*185.1*/(""".clearfix """),format.raw/*185.11*/("""{"""),format.raw/*185.12*/(""" """),format.raw/*185.13*/("""zoom: 1; """),format.raw/*185.22*/("""}"""),format.raw/*185.23*/("""



"""),format.raw/*189.1*/("""/* =============================================================================
   PLACEHOLDER Media Queries for Responsive Design.
   These override the primary ('mobile first') styles
   Modify as content2 requires.
   ========================================================================== */


	</style>

    <body>

        """),format.raw/*206.20*/("""
"""),format.raw/*207.1*/("""<header>
<header>
        <div class="topbar">
      <div class="topbar-inner">
        <div class="container">
          <a class="brand" href="#">UniCarbKB</a>
          <ul class="nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
          """),format.raw/*218.79*/("""
        """),format.raw/*219.9*/("""</div>
      </div>
    </div>
       </header>



<section class="subHead" style="box-shadow: rgba(0, 0, 0, 0) 0px 0px 0px 0px; ">
	<section class="row-fluid subHeadInner borBox">
		<div class="span12">
			<h2 class="pull-left welcome"><img src="/images/dblogo.png" style="height:80px" />
 <small>an experimental LC-MS/MS database of glycan structures</small></h2>
			<figure class="menuButton btn sharp btn-success" style="display: none; "><i class="icon-white icon-th-list"></i> Menu</figure>
		</div>
	</section>
</section>
<section class="container-fluid" id="mainContainer">
	<div class="row-fluid">
		<div class="span2 sideBar" style="height: 273px; ">
			<ul>
				<li data-link="home" class=""><figure><i class="icon-home"></i> <div class="menuText">Home</div></figure></li>
				<li data-link="features"><figure><i class="icon-star"></i> <div class="menuText">Features</div></figure></li>
				<li data-link="dashboard"><figure><i class="icon-th-list"></i> <div class="menuText">Dashboard</div></figure></li>
				<hr>
				<li data-link="features"><figure><i class="icon-th-large"></i> <div class="menuText"><a href="http://unicarb-db.org/unicarbdb/show_mucin.action">303 Structures</a></div></figure></li>
				<li data-link="features"><figure><i class="icon-th-large"></i> <div class="menuText"><a href="http://unicarb-db.org/unicarbdb/show_mucin.action">269 MS/MS Spectra</a></div></figure></li>
				"""),format.raw/*251.14*/("""
			"""),format.raw/*252.4*/("""</ul>
		</div>
		<div class="span10 content" style="display: block">

<div class="row-fluid">
	<div class="span7">
	<p>A platform for presenting N- and O-linked oligosaccharide structures and fragment data characterized by LC-MS/MS strategies. The database is annotated with high-quality MS/MS datasets and is designed to extend and reinforce those standards and ontologies developed by existing glycomics databases.</p>
	<form class="well form-inline">
  <input type="text" class="input" placeholder="Search by mass">
  <label class="checkbox">
    <input type="checkbox"> -1 
  </label>
  <label class="checkbox">
    <input type="checkbox"> -2
  </label>
  <label class="checkbox">
    <input type="checkbox"> -3
  </label>
  <button type="submit" class="btn success">Filter</button>
  <a href="" class="btn btn-success">Glycan Builder Coming Soon</a>
</form>

		<!-- MEMEBER LIST ITEMS ARE FILLED HERE,
			YOU CAN DO IT BY EITHER METHOD, JAVASCRIPT BASED OR STATIC HTML -->
"""),format.raw/*286.13*/("""


"""),format.raw/*289.1*/("""<div id="content2"  style="min-height: 1000px">
       	<h3>Click on structure for information</h3> 
		<!-- navigation holder -->
        <div class="pagination" style="overflow-y:auto;overflow-x:hidden;height:100%;width:100%">></div>
        
        <!-- item container -->
        <ul id="itemContainer" >
		"""),format.raw/*300.5*/("""
		"""),format.raw/*301.3*/("""</ul>

	</div> <!--! end of #content -->

</div>



	<div class="span5">
	
		<div class="widget widget-nopad">
							
					<div class="widget-header">
						<i class="icon-list-alt"></i>
						<h3>Recent Published Data <small><a href="http://unicarb-db.org">(see all)</a></small></h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<ul class="news-items">
							<li>
								<div class="news-item-detail">										
									<a href="" class="news-item-title">Cross Validation of Liquid Chromatography-Mass Spectrometry and Lectin Array for Monitoring Glycosylation in Fed-Batch Glycoprotein Production</a>
									<p class="news-item-preview">Hayes CA, Doohan R, Kirkley D, Leister K, Harhen B, Savage AV, Karlsson NG.</p>
								</div>
								
								<div class="news-item-date">
									<img src="/images/charts.png">
								</div>
							</li>
							<li>
								<div class="news-item-detail">										
									<a href="javascript:;" class="news-item-title">GlycoSpectrumScan: fishing glycopeptides from MS spectra of protease digests of human colostrum sIgA.</a>
									<p class="news-item-preview">Deshpande N, Jensen PH, Packer NH, Kolarich D.</p>
								</div>
								
								<div class="news-item-date">
									<img src="/images/charts.png">
								</div>
							</li>
						</ul>
						
					</div> <!-- /widget-content -->
				
				</div>


	<div class="widget widget-nopad">

                                        <div class="widget-header">
                                                <i class="icon-eye-open"></i>
                                                <h3>Browse New Data Collections <small><a href="http://unicarb-db.org">(see all)</a></small></h3>
                                        </div> <!-- /widget-header -->

                                        <div class="widget-content">

                                                <ul class="news-items">
                                                        <li>
                                                                <div class="news-item-detail">
                                                                        <a href="" class="news-item-title">Saliva data ......</a>
                                                                </div>

                                                                <div class="news-item-date">
                                                                        <img src="/images/charts.png">
                                                                </div>
                                                        </li>
                                                        <li>
                                                                <div class="news-item-detail">
                                                                        <a href="javascript:;" class="news-item-title">Some agilent chip data</a>
                                                                </div>

                                                                <div class="news-item-date">
                                                                        <img src="/images/charts.png">
                                                                </div>
                                                        </li>
                                                </ul>

                                        </div> <!-- /widget-content -->

                                </div>

	<div class="page-header">
				<h1 class="hBig">Contributors <small>Curated and Annotated MS/MS Data</small></h1>
	</div>
				<p>Data collections have been contributed by researchers from:</p>
				<div class="news-item-detail">										
				<p class="news-item-preview">University of Gothenburg</p>
				<p class="news-item-preview">Macquarie University</p>
				<p class="news-item-preview">Max Planck</p>
				</div>

        </div>


</div></div>
	</div>
</section>
<footer style="box-shadow: rgba(0, 0, 0, 0.0585938) 0px -1px 5px 0px; ">
	<div class="container-fluid" id="footMain">
		<div class="row-fluid">
			<div class="span6">
				<div class="pull-left">
					<h6></h6>
				</div>
			</div>
			<div class="span6">
				<div class="pull-right">
					<h6>UniCarbKB</h6>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>


 <!-- ##### MODALS #####- -->
<div class="modal fade hide" id="thisModal">

</div>


<!-- ### BACKGROUND DIV -->
<div class="bg">
</div>

<div class="commandWindow hide">
	<div class="commandWindow-inner">
		<h2 class="hBig">CommandLine</h2><br>
		<div class="form sharp form-horizontal">
			<div class="input-append row-fluid">
				<input class="sharp span9" id="commandInput" placeholder="Enter Command" type="text"><button class="btn btn-info">Go</button>
			</div>
		</div>
		<br>
		<div class="alert fade in">
			<a class="close" data-dismiss="alert">Ã</a>
			<p>Type <code> goto("dashboard") or goto("portfolio") </code></p>

		</div>
	</div>
</div>


	
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}
}

/**********************************
* Helper generating table headers *
***********************************/
object ms extends ms_Scope0.ms_Scope1.ms
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ms.scala.html
                  HASH: ab12cf45e311c859fdd7deb1e88df576d3bc8c91
                  MATRIX: 966->128|1130->265|1145->271|1224->328|1325->402|1340->408|1411->457|1511->530|1526->536|1606->594|1705->666|1720->672|1804->734|1949->852|1964->858|2043->916|2104->950|2119->956|2209->1025|2270->1059|2285->1065|2362->1121|2423->1155|2438->1161|2524->1226|2585->1260|2600->1266|2686->1331|2747->1365|2762->1371|2834->1422|2895->1456|2910->1462|2986->1517|3047->1551|3062->1557|3132->1606|3193->1640|3208->1646|3287->1704|3465->1854|3494->1855|3531->1865|3661->1967|3690->1968|3726->1977|3803->2027|3831->2028|3920->2089|3949->2090|3985->2099|4124->2210|4153->2211|4194->2224|4424->2427|4453->2428|4485->2433|4513->2434|4544->2438|4572->2439|4650->2489|4679->2490|4713->2497|4761->2518|4789->2519|4827->2530|4865->2540|4894->2541|4928->2548|5044->2637|5072->2638|5110->2649|5154->2665|5183->2666|5217->2673|5292->2721|5320->2722|5358->2733|5408->2755|5437->2756|5466->2757|5514->2777|5543->2778|5576->2784|5622->2802|5651->2803|5680->2804|5727->2823|5756->2824|5794->2835|5863->2876|5892->2877|5927->2885|6000->2931|6028->2932|6066->2943|6137->2986|6166->2987|6200->2994|6245->3012|6273->3013|6311->3024|6429->3114|6458->3115|6492->3122|6566->3169|6594->3170|6632->3181|6673->3194|6702->3195|6731->3196|6774->3211|6803->3212|6842->3224|6899->3253|6928->3254|6957->3255|7012->3282|7041->3283|7069->3284|7113->3300|7142->3301|7171->3302|7237->3340|7266->3341|7294->3342|7338->3358|7367->3359|7396->3360|7450->3386|7479->3387|7507->3388|7550->3403|7579->3404|7608->3405|7653->3422|7682->3423|7711->3424|7757->3441|7787->3442|7817->3443|7900->3497|7930->3498|7959->3499|8011->3522|8041->3523|8071->3524|8119->3543|8149->3544|8178->3545|8228->3566|8258->3567|8288->3568|8335->3586|8365->3587|8394->3588|8444->3609|8474->3610|8504->3611|8635->3712|8666->3713|8696->3715|8777->3767|8807->3768|8837->3769|8887->3790|8917->3791|8947->3793|8993->3810|9023->3811|9053->3812|9132->3862|9162->3863|9191->3864|9244->3888|9274->3889|9304->3890|9388->3945|9418->3946|9448->3948|9499->3970|9529->3971|9559->3972|9643->4027|9673->4028|9702->4029|9760->4058|9790->4059|9820->4060|9870->4081|9900->4082|9930->4084|9987->4112|10017->4113|10047->4114|10131->4169|10161->4170|10190->4171|10254->4206|10284->4207|10314->4208|10364->4229|10394->4230|10424->4232|10474->4253|10504->4254|10534->4255|10618->4310|10648->4311|10677->4312|10734->4340|10764->4341|10794->4342|10844->4363|10874->4364|10904->4366|10959->4392|10989->4393|11019->4394|11094->4440|11124->4441|11153->4442|11215->4475|11245->4476|11275->4477|11342->4515|11372->4516|11402->4518|11453->4540|11483->4541|11513->4542|11627->4626|11658->4627|11687->4628|11747->4659|11777->4660|11807->4661|11851->4676|11881->4677|11911->4679|12005->4744|12035->4745|12065->4746|12140->4792|12170->4793|12199->4794|12261->4827|12291->4828|12321->4829|12412->4891|12442->4892|12471->4893|12540->4933|12570->4934|12600->4935|12682->4988|12712->4989|12742->4991|12819->5039|12849->5040|12879->5041|12936->5069|12966->5070|12995->5071|13052->5099|13082->5100|13112->5101|13156->5116|13186->5117|13215->5118|13266->5140|13296->5141|13326->5142|13373->5160|13403->5161|13432->5162|13512->5213|13542->5214|13572->5215|13632->5246|13662->5247|13691->5248|13745->5273|13775->5274|13805->5275|13880->5321|13910->5322|13939->5323|13993->5348|14023->5349|14053->5350|14096->5364|14126->5365|14155->5366|14209->5391|14239->5392|14269->5393|14315->5410|14345->5411|14375->5413|14467->5476|14497->5477|14527->5478|14574->5496|14604->5497|14633->5498|14729->5565|14759->5566|14789->5567|14849->5598|14879->5599|14908->5600|14970->5633|15000->5634|15030->5635|15105->5681|15135->5682|15164->5683|15226->5716|15256->5717|15286->5718|15329->5732|15359->5733|15388->5734|15450->5767|15480->5768|15510->5769|15556->5786|15586->5787|15616->5789|15695->5839|15725->5840|15755->5841|15805->5862|15835->5863|15864->5864|15928->5899|15958->5900|15988->5901|16115->5998|16146->5999|16175->6000|16226->6022|16256->6023|16286->6024|16349->6058|16379->6059|16409->6061|16713->6337|16742->6338|16771->6339|16920->6458|16951->6459|16980->6460|17015->6467|17044->6468|17074->6469|17118->6484|17148->6485|17178->6487|17318->6599|17348->6600|17378->6601|17442->6636|17472->6637|17502->6639|17702->6810|17732->6811|17762->6812|17913->6933|17944->6934|17974->6936|18205->7138|18235->7139|18265->7140|18382->7227|18413->7228|18443->7230|18547->7305|18577->7306|18607->7307|18656->7327|18686->7328|18716->7330|18844->7429|18874->7430|18904->7431|18963->7461|18993->7462|19022->7463|19067->7479|19097->7480|19127->7481|19169->7494|19199->7495|19228->7496|19267->7506|19297->7507|19327->7508|19365->7517|19395->7518|19427->7522|19789->8063|19818->8064|20219->8504|20256->8513|21693->10315|21725->10319|22732->11839|22763->11842|23102->12411|23133->12414
                  LINES: 37->7|41->11|41->11|41->11|42->12|42->12|42->12|43->13|43->13|43->13|44->14|44->14|44->14|46->16|46->16|46->16|47->17|47->17|47->17|48->18|48->18|48->18|49->19|49->19|49->19|50->20|50->20|50->20|51->21|51->21|51->21|52->22|52->22|52->22|53->23|53->23|53->23|54->24|54->24|54->24|61->31|61->31|63->33|64->34|64->34|65->35|67->37|67->37|70->40|70->40|71->41|73->43|73->43|74->44|78->48|78->48|79->49|79->49|81->51|81->51|88->58|88->58|89->59|90->60|90->60|92->62|92->62|92->62|93->63|97->67|97->67|99->69|99->69|99->69|100->70|102->72|102->72|104->74|104->74|104->74|104->74|104->74|104->74|105->75|105->75|105->75|105->75|105->75|105->75|107->77|107->77|107->77|108->78|110->80|110->80|112->82|112->82|112->82|113->83|114->84|114->84|116->86|117->87|117->87|118->88|120->90|120->90|122->92|122->92|122->92|122->92|122->92|122->92|124->94|126->96|126->96|126->96|126->96|126->96|127->97|127->97|127->97|127->97|127->97|127->97|128->98|128->98|128->98|128->98|128->98|128->98|129->99|129->99|129->99|129->99|129->99|129->99|130->100|130->100|130->100|130->100|130->100|130->100|131->101|131->101|131->101|131->101|131->101|131->101|132->102|132->102|132->102|132->102|132->102|132->102|133->103|133->103|133->103|133->103|133->103|133->103|135->105|137->107|137->107|137->107|137->107|137->107|139->109|139->109|139->109|139->109|139->109|139->109|140->110|140->110|140->110|140->110|140->110|140->110|142->112|142->112|142->112|142->112|142->112|142->112|143->113|143->113|143->113|143->113|143->113|143->113|145->115|145->115|145->115|145->115|145->115|145->115|146->116|146->116|146->116|146->116|146->116|146->116|148->118|148->118|148->118|148->118|148->118|148->118|149->119|149->119|149->119|149->119|149->119|149->119|151->121|151->121|151->121|151->121|151->121|151->121|152->122|152->122|152->122|152->122|152->122|152->122|154->124|154->124|154->124|154->124|154->124|154->124|155->125|155->125|155->125|155->125|155->125|155->125|157->127|159->129|159->129|159->129|159->129|159->129|160->130|160->130|160->130|160->130|160->130|160->130|161->131|161->131|161->131|161->131|161->131|161->131|163->133|165->135|165->135|165->135|165->135|165->135|166->136|166->136|166->136|166->136|166->136|166->136|167->137|167->137|167->137|167->137|167->137|167->137|168->138|169->139|169->139|169->139|169->139|169->139|170->140|170->140|170->140|170->140|170->140|170->140|171->141|171->141|171->141|171->141|171->141|171->141|172->142|172->142|172->142|172->142|172->142|172->142|174->144|176->146|176->146|176->146|176->146|176->146|177->147|178->148|178->148|178->148|178->148|178->148|179->149|179->149|179->149|179->149|179->149|179->149|180->150|180->150|180->150|180->150|180->150|180->150|181->151|181->151|181->151|181->151|181->151|181->151|183->153|185->155|185->155|185->155|185->155|185->155|186->156|186->156|186->156|186->156|186->156|186->156|187->157|187->157|187->157|187->157|187->157|187->157|189->159|195->165|195->165|195->165|195->165|195->165|196->166|196->166|196->166|196->166|196->166|196->166|198->168|200->170|200->170|200->170|200->170|200->170|202->172|204->174|204->174|204->174|204->174|204->174|206->176|207->177|207->177|207->177|207->177|207->177|209->179|210->180|210->180|210->180|210->180|210->180|212->182|213->183|213->183|213->183|213->183|213->183|214->184|214->184|214->184|214->184|214->184|214->184|215->185|215->185|215->185|215->185|215->185|215->185|219->189|230->206|231->207|242->218|243->219|269->251|270->252|294->286|297->289|304->300|305->301
                  -- GENERATED --
              */
          