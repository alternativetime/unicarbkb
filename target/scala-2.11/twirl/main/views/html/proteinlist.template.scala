
package views.html

import play.twirl.api._


     object proteinlist_Scope0 {
import java.util._

import controllers._
import models._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc.Http.Context.Implicit._
import views.html._

import scala.collection.JavaConversions._

class proteinlist extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[com.avaje.ebean.PagedList[Proteins],String,String,String,List[Proteins],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.PagedList[Proteins], currentSortBy: String, currentOrder: String, currentFilter: String, proteins: List[Proteins], filterYesNo: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*35.2*/header/*35.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*35.38*/("""
    """),format.raw/*36.5*/("""<th class=""""),_display_(/*36.17*/key/*36.20*/.replace(".","_")),format.raw/*36.37*/(""" """),format.raw/*36.38*/("""header """),_display_(/*36.46*/if(currentSortBy == key){/*36.72*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*36.136*/("""">
        <a href=""""),_display_(/*37.19*/link(0, key)),format.raw/*37.31*/("""">"""),_display_(/*37.34*/title),format.raw/*37.39*/("""</a>
    </th>
""")))};def /*8.2*/link/*8.6*/(newPage:Int, newSortBy:String) = {{
    
    var sortBy = currentSortBy
    var order = currentOrder
    
    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
                order = "asc"
            }
        } else {
            order = "asc"
        }
    }
    
    // Generate the link
    routes.ProteinDetails.proteinlist(newPage, sortBy, order, currentFilter)
    
}};
Seq[Any](format.raw/*1.167*/("""
"""),format.raw/*3.1*/("""

"""),format.raw/*7.42*/("""
"""),format.raw/*29.2*/("""


"""),format.raw/*34.37*/("""
"""),format.raw/*39.2*/("""

"""),_display_(/*41.2*/main/*41.6*/ {_display_(Seq[Any](format.raw/*41.8*/("""

"""),format.raw/*43.1*/("""<script>
      $(document).ready(function()"""),format.raw/*44.35*/("""{"""),format.raw/*44.36*/("""
        """),format.raw/*45.9*/("""$("#e3").select2("""),format.raw/*45.26*/("""{"""),format.raw/*45.27*/("""
          """),format.raw/*46.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
          minimumInputLength: 1,
        """),format.raw/*49.9*/("""}"""),format.raw/*49.10*/(""");
        """),format.raw/*50.9*/("""}"""),format.raw/*50.10*/(""");
</script>
<script>
        $(function() """),format.raw/*53.22*/("""{"""),format.raw/*53.23*/("""
        """),format.raw/*54.9*/("""var myProteins = new Array();
        """),_display_(/*55.10*/for(s <- proteins ) yield /*55.29*/{_display_(Seq[Any](format.raw/*55.30*/("""
                """),format.raw/*56.17*/("""myProteins.push(""""),_display_(/*56.35*/s/*56.36*/.name),format.raw/*56.41*/("""");
        """)))}),format.raw/*57.10*/("""
        """),format.raw/*58.9*/("""myProteins.sort();
        options = '';
        for (var i = 0; i < myProteins.length; i++) """),format.raw/*60.53*/("""{"""),format.raw/*60.54*/("""
                """),format.raw/*61.17*/("""options += '<option value="' + myProteins[i] + '">' + myProteins[i] + '</option>';
        """),format.raw/*62.9*/("""}"""),format.raw/*62.10*/("""
        """),format.raw/*63.9*/("""$('#e3').html(options);
        """),format.raw/*64.9*/("""}"""),format.raw/*64.10*/(""");
</script>


  <ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-map-marker" ></i> Proteins<span class="divider"></span></li>
  </ul>
   
    <div class="page-header row-fluid"> 
      <h1 id="homeTitle">"""),_display_(/*74.27*/Messages(" Protein Search ", currentPage.getTotalRowCount)),format.raw/*74.85*/("""</h1>
      <h4 class="subheader">Search the curated collection of Protein data by name.</h4>
    </div>
    """),_display_(/*77.6*/if(flash.containsKey("success"))/*77.38*/ {_display_(Seq[Any](format.raw/*77.40*/("""
        """),format.raw/*78.9*/("""<div class="alert-message warning">
            <strong>Done!</strong> """),_display_(/*79.37*/flash/*79.42*/.get("success")),format.raw/*79.57*/("""
        """),format.raw/*80.9*/("""</div>
    """)))}),format.raw/*81.6*/("""

    """),format.raw/*83.5*/("""<div id="actions">
 
        """),format.raw/*89.19*/("""
    
        """),format.raw/*91.9*/("""<div id='test'  class="input-append controls">
          <form class='form-search' action=""""),_display_(/*92.46*/link(0, "name")),format.raw/*92.61*/("""" method="GET">
            <select  name="m" MULTIPLE id="e3" id="listBox" class="input-xxlarge"></select>
            <input type="submit" id="searchsubmit" value="Filter" class="btn btn-primary">
            <a class="btn success" id="add" href="/proteins">Show All Glycoproteins</a>
          </form>
        </div>

    
    </div>
    
    <div class="row-fluid">
    """),_display_(/*103.6*/if(currentPage.getTotalRowCount == 0)/*103.43*/ {_display_(Seq[Any](format.raw/*103.45*/("""
        
        """),format.raw/*105.9*/("""<div class="well">
            <em>Nothing to display</em>
        </div>
        
    """)))}/*109.7*/else/*109.12*/{_display_(Seq[Any](format.raw/*109.13*/("""
        
        """),format.raw/*111.9*/("""<table class="computers table table-striped">
            <thead>
                <tr>
		    """),_display_(/*114.8*/header("name", "Name")),format.raw/*114.30*/("""
   		    """),_display_(/*115.11*/header("", "Accession")),format.raw/*115.34*/("""
                    """),_display_(/*116.22*/header("", "Taxonomy")),format.raw/*116.44*/(""" 
		    """),_display_(/*117.8*/header("stproteins", "Glycan Structures")),format.raw/*117.49*/("""
		    """),_display_(/*118.8*/header("proteinGeneralSites", "General")),format.raw/*118.48*/("""
                """),format.raw/*119.17*/("""</tr>
            </thead>
            <tbody>

                """),_display_(/*123.18*/for(protein <- currentPage.getList) yield /*123.53*/ {_display_(Seq[Any](format.raw/*123.55*/("""
"""),format.raw/*124.44*/("""
                    """),format.raw/*125.21*/("""<tr>
				<td>"""),_display_(/*126.10*/protein/*126.17*/.name),format.raw/*126.22*/("""</td>	    
				<td>"""),_display_(/*127.10*/if(protein.swissProt != null)/*127.39*/ {_display_(Seq[Any](format.raw/*127.41*/(""" """),format.raw/*127.42*/("""<a href="proteinsummary/"""),_display_(/*127.67*/protein/*127.74*/.swissProt),format.raw/*127.84*/("""/annotated">"""),_display_(/*127.97*/protein/*127.104*/.swissProt),format.raw/*127.114*/("""</a>""")))}),format.raw/*127.119*/("""</td>
			<td>			
			"""),_display_(/*129.5*/for(tax <- protein.proteinsTax) yield /*129.36*/ {_display_(Seq[Any](format.raw/*129.38*/("""
			"""),_display_(/*130.5*/if(protein.swissProt != null)/*130.34*/ {_display_(Seq[Any](format.raw/*130.36*/("""<a href="proteinsummary/"""),_display_(/*130.61*/protein/*130.68*/.swissProt),format.raw/*130.78*/("""/annotated">"""),_display_(/*130.91*/tax/*130.94*/.species),format.raw/*130.102*/(""" """),format.raw/*130.103*/("""</a>""")))}),format.raw/*130.108*/("""
			"""),_display_(/*131.5*/if(protein.swissProt == null)/*131.34*/ {_display_(Seq[Any](format.raw/*131.36*/("""<a href="proteinsummary/"""),_display_(/*131.61*/protein/*131.68*/.name),format.raw/*131.73*/("""/"""),_display_(/*131.75*/helper/*131.81*/.urlEncode(tax.species)),format.raw/*131.104*/("""">"""),_display_(/*131.107*/tax/*131.110*/.species),format.raw/*131.118*/(""" """),format.raw/*131.119*/("""</a>""")))}),format.raw/*131.124*/("""
        		""")))}),format.raw/*132.12*/("""
			"""),format.raw/*133.4*/("""</td>
			<td>
			"""),_display_(/*135.5*/for(tax <- protein.proteinsTax.take(1)) yield /*135.44*/{_display_(Seq[Any](format.raw/*135.45*/("""
				"""),_display_(/*136.6*/if(protein.swissProt != null)/*136.35*/ {_display_(Seq[Any](format.raw/*136.37*/("""
				"""),format.raw/*137.5*/("""<a href="proteinsummary/"""),_display_(/*137.30*/protein/*137.37*/.swissProt),format.raw/*137.47*/("""/annotated" style="text-decoration : none, font-color: #404040; ">"""),_display_(/*137.114*/protein/*137.121*/.stproteins.size()),format.raw/*137.139*/("""</a>""")))}),format.raw/*137.144*/("""	
				"""),_display_(/*138.6*/if(protein.swissProt == null)/*138.35*/ {_display_(Seq[Any](format.raw/*138.37*/(""" """),format.raw/*138.38*/("""<a href="proteinsummary/"""),_display_(/*138.63*/protein/*138.70*/.name),format.raw/*138.75*/("""/"""),_display_(/*138.77*/tax/*138.80*/.species),format.raw/*138.88*/("""">"""),_display_(/*138.91*/protein/*138.98*/.stproteins.size()),format.raw/*138.116*/("""</a> """)))}),format.raw/*138.122*/("""

			""")))}),format.raw/*140.5*/("""
			"""),format.raw/*141.4*/("""</td>
            <td><a href="proteinsummary/"""),_display_(/*142.42*/protein/*142.49*/.name),format.raw/*142.54*/("""" style="text-decoration : none, font-color: #404040; ">"""),_display_(/*142.111*/if(protein.proteinDefinedSites.size() > 0 )/*142.154*/ {_display_(Seq[Any](format.raw/*142.156*/(""" """),format.raw/*142.157*/("""<span class="label">Site-Specific</span>""")))}),format.raw/*142.198*/("""</a></td>
                    </tr>
                """)))}),format.raw/*144.18*/("""
"""),format.raw/*145.8*/("""

            """),format.raw/*147.13*/("""</tbody>
        </table>

	"""),_display_(/*150.3*/if(filterYesNo.matches("no"))/*150.32*/ {_display_(Seq[Any](format.raw/*150.34*/("""
        """),format.raw/*151.9*/("""<div id="pagination" class="pagination">
            <ul class="clearfix">
              """),_display_(/*153.16*/if(currentPage.hasPrev)/*153.39*/ {_display_(Seq[Any](format.raw/*153.41*/("""
                """),format.raw/*154.17*/("""<li class="prev"><a href=""""),_display_(/*154.44*/link(currentPage.getPageIndex - 1, null)),format.raw/*154.84*/("""">&larr; Previous</a></li>
              """)))}/*155.17*/else/*155.22*/{_display_(Seq[Any](format.raw/*155.23*/("""
                """),format.raw/*156.17*/("""<li class="prev disabled"><a>&larr; Previous</a></li>
              """)))}),format.raw/*157.16*/("""
                """),format.raw/*158.111*/("""
		"""),format.raw/*159.3*/("""<li class="current"><a>Displaying """),_display_(/*159.38*/(currentPage.getPageIndex()+1)),format.raw/*159.68*/(""" """),format.raw/*159.69*/("""of """),_display_(/*159.73*/currentPage/*159.84*/.getTotalPageCount()),format.raw/*159.104*/(""" """),format.raw/*159.105*/("""Pages</a></li>
              """),_display_(/*160.16*/if(currentPage.hasNext)/*160.39*/ {_display_(Seq[Any](format.raw/*160.41*/("""
                """),format.raw/*161.17*/("""<li class="next"><a href=""""),_display_(/*161.44*/link(currentPage.getPageIndex + 1, null)),format.raw/*161.84*/("""">Next &rarr;</a></li>
              """)))}/*162.17*/else/*162.22*/{_display_(Seq[Any](format.raw/*162.23*/("""
                """),format.raw/*163.17*/("""<li class="next disabled"><a>Next &rarr;</a></li>
              """)))}),format.raw/*164.16*/("""
            """),format.raw/*165.13*/("""</ul>
        </div>
	""")))}),format.raw/*167.3*/("""
	"""),_display_(/*168.3*/if(filterYesNo.matches("yes"))/*168.33*/{_display_(Seq[Any](format.raw/*168.34*/("""
	"""),format.raw/*169.2*/("""<div id="pagination" class="pagination">
            <ul class="clearfix">
		<li class="disabled"><a href="">Displaying All Results</a></li>
	    </ul>
        </div>
	""")))}),format.raw/*174.3*/("""

      """),format.raw/*176.7*/("""</div>
    
      """),_display_(/*178.8*/views/*178.13*/.html.footerunicarb.footerunicarb()),format.raw/*178.48*/("""    
    """)))}),format.raw/*179.6*/("""
        
""")))}),format.raw/*181.2*/("""

            
"""))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[Proteins],currentSortBy:String,currentOrder:String,currentFilter:String,proteins:List[Proteins],filterYesNo:String): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter,proteins,filterYesNo)

  def f:((com.avaje.ebean.PagedList[Proteins],String,String,String,List[Proteins],String) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter,proteins,filterYesNo) => apply(currentPage,currentSortBy,currentOrder,currentFilter,proteins,filterYesNo)

  def ref: this.type = this

}


}

/**/
object proteinlist extends proteinlist_Scope0.proteinlist
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/proteinlist.scala.html
                  HASH: 09c9625d30fce3dbe414177a2a1f7f9f20411d67
                  MATRIX: 829->1|1088->941|1102->947|1209->977|1241->982|1280->994|1292->997|1330->1014|1359->1015|1394->1023|1428->1049|1515->1113|1563->1134|1596->1146|1626->1149|1652->1154|1690->313|1701->317|2243->166|2270->184|2299->311|2327->828|2358->939|2386->1170|2415->1173|2427->1177|2466->1179|2495->1181|2566->1224|2595->1225|2631->1234|2676->1251|2705->1252|2744->1263|2884->1376|2913->1377|2951->1388|2980->1389|3051->1432|3080->1433|3116->1442|3182->1481|3217->1500|3256->1501|3301->1518|3346->1536|3356->1537|3382->1542|3426->1555|3462->1564|3583->1657|3612->1658|3657->1675|3775->1766|3804->1767|3840->1776|3899->1808|3928->1809|4271->2125|4350->2183|4486->2293|4527->2325|4567->2327|4603->2336|4702->2408|4716->2413|4752->2428|4788->2437|4830->2449|4863->2455|4920->2895|4961->2909|5080->3001|5116->3016|5518->3391|5565->3428|5606->3430|5652->3448|5759->3537|5773->3542|5813->3543|5859->3561|5980->3655|6024->3677|6063->3688|6108->3711|6158->3733|6202->3755|6238->3764|6301->3805|6336->3813|6398->3853|6444->3870|6537->3935|6589->3970|6630->3972|6660->4016|6710->4037|6752->4051|6769->4058|6796->4063|6844->4083|6883->4112|6924->4114|6954->4115|7007->4140|7024->4147|7056->4157|7097->4170|7115->4177|7148->4187|7186->4192|7234->4213|7282->4244|7323->4246|7355->4251|7394->4280|7435->4282|7488->4307|7505->4314|7537->4324|7578->4337|7591->4340|7622->4348|7653->4349|7691->4354|7723->4359|7762->4388|7803->4390|7856->4415|7873->4422|7900->4427|7930->4429|7946->4435|7992->4458|8024->4461|8038->4464|8069->4472|8100->4473|8138->4478|8182->4490|8214->4494|8259->4512|8315->4551|8355->4552|8388->4558|8427->4587|8468->4589|8501->4594|8554->4619|8571->4626|8603->4636|8699->4703|8717->4710|8758->4728|8796->4733|8830->4740|8869->4769|8910->4771|8940->4772|8993->4797|9010->4804|9037->4809|9067->4811|9080->4814|9110->4822|9141->4825|9158->4832|9199->4850|9238->4856|9275->4862|9307->4866|9382->4913|9399->4920|9426->4925|9512->4982|9566->5025|9608->5027|9639->5028|9713->5069|9798->5122|9827->5130|9870->5144|9926->5173|9965->5202|10006->5204|10043->5213|10161->5303|10194->5326|10235->5328|10281->5345|10336->5372|10398->5412|10460->5455|10474->5460|10514->5461|10560->5478|10661->5547|10708->5658|10739->5661|10802->5696|10854->5726|10884->5727|10916->5731|10937->5742|10980->5762|11011->5763|11069->5793|11102->5816|11143->5818|11189->5835|11244->5862|11306->5902|11364->5941|11378->5946|11418->5947|11464->5964|11561->6029|11603->6042|11657->6065|11687->6068|11727->6098|11767->6099|11797->6101|11997->6270|12033->6278|12079->6297|12094->6302|12151->6337|12192->6347|12234->6358
                  LINES: 27->1|31->35|31->35|33->35|34->36|34->36|34->36|34->36|34->36|34->36|34->36|34->36|35->37|35->37|35->37|35->37|37->8|37->8|59->1|60->3|62->7|63->29|66->34|67->39|69->41|69->41|69->41|71->43|72->44|72->44|73->45|73->45|73->45|74->46|77->49|77->49|78->50|78->50|81->53|81->53|82->54|83->55|83->55|83->55|84->56|84->56|84->56|84->56|85->57|86->58|88->60|88->60|89->61|90->62|90->62|91->63|92->64|92->64|102->74|102->74|105->77|105->77|105->77|106->78|107->79|107->79|107->79|108->80|109->81|111->83|113->89|115->91|116->92|116->92|127->103|127->103|127->103|129->105|133->109|133->109|133->109|135->111|138->114|138->114|139->115|139->115|140->116|140->116|141->117|141->117|142->118|142->118|143->119|147->123|147->123|147->123|148->124|149->125|150->126|150->126|150->126|151->127|151->127|151->127|151->127|151->127|151->127|151->127|151->127|151->127|151->127|151->127|153->129|153->129|153->129|154->130|154->130|154->130|154->130|154->130|154->130|154->130|154->130|154->130|154->130|154->130|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|156->132|157->133|159->135|159->135|159->135|160->136|160->136|160->136|161->137|161->137|161->137|161->137|161->137|161->137|161->137|161->137|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|162->138|164->140|165->141|166->142|166->142|166->142|166->142|166->142|166->142|166->142|166->142|168->144|169->145|171->147|174->150|174->150|174->150|175->151|177->153|177->153|177->153|178->154|178->154|178->154|179->155|179->155|179->155|180->156|181->157|182->158|183->159|183->159|183->159|183->159|183->159|183->159|183->159|183->159|184->160|184->160|184->160|185->161|185->161|185->161|186->162|186->162|186->162|187->163|188->164|189->165|191->167|192->168|192->168|192->168|193->169|198->174|200->176|202->178|202->178|202->178|203->179|205->181
                  -- GENERATED --
              */
          