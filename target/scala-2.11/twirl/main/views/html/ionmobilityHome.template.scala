
package views.html

import play.twirl.api._


     object ionmobilityHome_Scope0 {
import java.util._

import controllers._
import models._
import play.mvc.Http.Context.Implicit._
import views.html._

import scala.collection.JavaConversions._

class ionmobilityHome extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[List[ionmob.GlycanMob],List[ionmob.NitrogenNegative],List[ionmob.NitrogenPositive],List[ionmob.HeNegative],List[ionmob.HePositive],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(gm: List[ionmob.GlycanMob], ne: List[ionmob.NitrogenNegative], np: List[ionmob.NitrogenPositive], he: List[ionmob.HeNegative], hp: List[ionmob.HePositive]  ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.160*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),format.raw/*4.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-book" ></i><a href=""> Ion Mobility</a> <span class="divider">></span></li>
</ul>
<section id="layouts">

	<div class="page-header row-fluid">
	<h2>GlycoMob - Ion Mobility Database</h2>
   	</div>
	
	<div class="row-fluid">
        <div class="span8">
	<p></p>
	"""),_display_(/*17.3*/for(g <- gm) yield /*17.15*/ {_display_(Seq[Any](format.raw/*17.17*/("""

        """),format.raw/*19.9*/("""<img class="sugar_image" src="""),_display_(/*19.39*/{routes.Image.showImageIM(g.structure.id, session.get("notation"), "extended" )}),format.raw/*19.119*/(""" """),format.raw/*19.120*/("""alt="">
            <table class="computers table table-striped">
            <thead>
	    <tr><th align="middle" colspan="2">Ion Mobility Gas: Nitrogen</th></tr>
	    <tr><th align="middle" colspan="3">Negative</th><th align="middle" colspan="3">Positive</th></tr>
            <tr>
            <th>M+H</th>
            <th>M+Cl</th>
            <th>M+H2PO4</th>
	    <th>M+H</th>
	    <th>M+Na</th>
	    <th>M+K</th>
	    <th>
            </tr>
            </thead>
            <tbody>
	    """),_display_(/*35.7*/for(ne <- g.nitrogenNegativeList) yield /*35.40*/{_display_(Seq[Any](format.raw/*35.41*/("""
            	"""),format.raw/*36.14*/("""<tr><td>"""),_display_(/*36.23*/{"%.2f".format(ne.h)}),format.raw/*36.44*/("""</td><td>"""),_display_(/*36.54*/{"%.2f".format(ne.cl)}),format.raw/*36.76*/("""</td><td>"""),_display_(/*36.86*/{"%.2f".format(ne.p)}),format.raw/*36.107*/("""</td>
	    """)))}),format.raw/*37.7*/("""
            """),_display_(/*38.14*/for(ne <- g.nitrogenPositiveList) yield /*38.47*/{_display_(Seq[Any](format.raw/*38.48*/("""
		"""),format.raw/*39.3*/("""<td>"""),_display_(/*39.8*/{"%.2f".format(ne.h)}),format.raw/*39.29*/("""</td><td>"""),_display_(/*39.39*/{"%.2f".format(ne.na)}),format.raw/*39.61*/("""</td><td>"""),_display_(/*39.71*/{"%.2f".format(ne.k)}),format.raw/*39.92*/("""</td></tr>
	    """)))}),format.raw/*40.7*/("""
            """),format.raw/*41.13*/("""</tbody>
            </table>

            <table class="computers table table-striped">
            <thead>
	    <tr><th align="middle" colspan="2">Ion Mobility Gas: Helium</th></tr>
            <tr><th align="middle" colspan="3">Negative</th><th align="middle" colspan="3">Positive</th></tr>
            <tr>
            <th>M+H</th>
            <th>M+Cl</th>
            <th>M+H2PO4</th>
            <th>M+H</th>
            <th>M+Na</th>
            <th>M+K</th>
            <th>
            </tr>
            </thead>
            <tbody>
            """),_display_(/*59.14*/for(ne <- g.heNegativeList) yield /*59.41*/{_display_(Seq[Any](format.raw/*59.42*/("""
                """),format.raw/*60.17*/("""<tr><td>"""),_display_(/*60.26*/{"%.2f".format(ne.h)}),format.raw/*60.47*/("""</td><td>"""),_display_(/*60.57*/{"%.2f".format(ne.cl)}),format.raw/*60.79*/("""</td><td>"""),_display_(/*60.89*/{"%.2f".format(ne.p)}),format.raw/*60.110*/("""</td>
            """)))}),format.raw/*61.14*/("""
            """),_display_(/*62.14*/for(ne <- g.hePositiveList) yield /*62.41*/{_display_(Seq[Any](format.raw/*62.42*/("""
                """),format.raw/*63.17*/("""<td>"""),_display_(/*63.22*/{"%.2f".format(ne.h)}),format.raw/*63.43*/("""</td><td>"""),_display_(/*63.53*/{"%.2f".format(ne.na)}),format.raw/*63.75*/("""</td><td>"""),_display_(/*63.85*/{"%.2f".format(ne.k)}),format.raw/*63.106*/("""</td></tr>
            """)))}),format.raw/*64.14*/("""
            """),format.raw/*65.13*/("""</tbody>
            </table>

        """)))}),format.raw/*68.10*/("""
    """),format.raw/*69.5*/("""</div>

    <div class="span4 sidebar">
        """),_display_(/*72.10*/views/*72.15*/.html.format.format()),format.raw/*72.36*/("""
    """),format.raw/*73.5*/("""</div><!-- /col -->
</div>
  """),_display_(/*75.4*/views/*75.9*/.html.footerunicarb.footerunicarb()),format.raw/*75.44*/("""
"""),format.raw/*76.1*/("""</section>

""")))}),format.raw/*78.2*/("""
"""))
      }
    }
  }

  def render(gm:List[ionmob.GlycanMob],ne:List[ionmob.NitrogenNegative],np:List[ionmob.NitrogenPositive],he:List[ionmob.HeNegative],hp:List[ionmob.HePositive]): play.twirl.api.HtmlFormat.Appendable = apply(gm,ne,np,he,hp)

  def f:((List[ionmob.GlycanMob],List[ionmob.NitrogenNegative],List[ionmob.NitrogenPositive],List[ionmob.HeNegative],List[ionmob.HePositive]) => play.twirl.api.HtmlFormat.Appendable) = (gm,ne,np,he,hp) => apply(gm,ne,np,he,hp)

  def ref: this.type = this

}


}

/**/
object ionmobilityHome extends ionmobilityHome_Scope0.ionmobilityHome
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobilityHome.scala.html
                  HASH: 060adba9543c18da9a1ff9e409ff01852e0e590e
                  MATRIX: 889->1|1143->159|1171->162|1182->166|1220->168|1247->169|1676->572|1704->584|1744->586|1781->596|1838->626|1940->706|1970->707|2489->1200|2538->1233|2577->1234|2619->1248|2655->1257|2697->1278|2734->1288|2777->1310|2814->1320|2857->1341|2899->1353|2940->1367|2989->1400|3028->1401|3058->1404|3089->1409|3131->1430|3168->1440|3211->1462|3248->1472|3290->1493|3337->1510|3378->1523|3961->2079|4004->2106|4043->2107|4088->2124|4124->2133|4166->2154|4203->2164|4246->2186|4283->2196|4326->2217|4376->2236|4417->2250|4460->2277|4499->2278|4544->2295|4576->2300|4618->2321|4655->2331|4698->2353|4735->2363|4778->2384|4833->2408|4874->2421|4945->2461|4977->2466|5053->2515|5067->2520|5109->2541|5141->2546|5197->2576|5210->2581|5266->2616|5294->2617|5337->2630
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|48->17|48->17|48->17|50->19|50->19|50->19|50->19|66->35|66->35|66->35|67->36|67->36|67->36|67->36|67->36|67->36|67->36|68->37|69->38|69->38|69->38|70->39|70->39|70->39|70->39|70->39|70->39|70->39|71->40|72->41|90->59|90->59|90->59|91->60|91->60|91->60|91->60|91->60|91->60|91->60|92->61|93->62|93->62|93->62|94->63|94->63|94->63|94->63|94->63|94->63|94->63|95->64|96->65|99->68|100->69|103->72|103->72|103->72|104->73|106->75|106->75|106->75|107->76|109->78
                  -- GENERATED --
              */
          