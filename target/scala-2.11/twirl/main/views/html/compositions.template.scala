
package views.html

import play.twirl.api._


     object compositions_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class compositions extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[Structurecomp],Array[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structure: List[Structurecomp], query: Array[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.56*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""
"""),format.raw/*4.1*/("""<script>
$(document).ready(function () """),format.raw/*5.31*/("""{"""),format.raw/*5.32*/("""

    """),format.raw/*7.5*/("""(function ($) """),format.raw/*7.19*/("""{"""),format.raw/*7.20*/("""

        """),format.raw/*9.9*/("""$('#filter').keyup(function () """),format.raw/*9.40*/("""{"""),format.raw/*9.41*/("""

            """),format.raw/*11.13*/("""var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () """),format.raw/*13.52*/("""{"""),format.raw/*13.53*/("""
                """),format.raw/*14.17*/("""return rex.test($(this).text());
            """),format.raw/*15.13*/("""}"""),format.raw/*15.14*/(""").show();

        """),format.raw/*17.9*/("""}"""),format.raw/*17.10*/(""")

    """),format.raw/*19.5*/("""}"""),format.raw/*19.6*/("""(jQuery));

"""),format.raw/*21.1*/("""}"""),format.raw/*21.2*/(""");
</script>

<ul class="breadcrumb">
  <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">&gt;</span></li>
  <li><i class="icon-search"></i><a href="/query"> Search</a> <span class="divider">&gt;</span></li>
  <li class="active"> Composition Search</li>
</ul>

<section id="structureLayout">
  <section id="layouts">

  <div class="page-header row-fluid">
	<h1>Composition Search</h1>
	<h4 class="subheader span8">GlycosuiteDB records</h4>
  </div>
	<div class="row-fluid">

	<div class="span3 search">
    	<div class="row-fluid">
      	  <div class="filterbar tabbable clearfix">
           <ul class="nav nav-tabs" id="myTabBar">
           <li class="active"><a href="#filter1" data-toggle="tab">GlycoSuiteDB <span class="pull-right count">  </span></a></li>
           """),format.raw/*45.121*/("""
          """),format.raw/*46.11*/("""</div>

          <div class="info">
            <h4>Other Options:</h4>
            <p><b>Glycan Builder:</b> Build and search a glycan structure<a href="/builder"> using the new interface</a>.</p>
            <p><b>Curated Publications:</b> Search the <a href="/references">growing list of publications, associated structures, and metadata</a>.</p>
          </div>
        </div>
  	</div>

	<div class="span9 rightpanel">
	  <h3>Query</h3>
	  <p>"""),_display_(/*58.8*/for(q <- query) yield /*58.23*/ {_display_(_display_(/*58.26*/q))}),format.raw/*58.28*/("""</p>

	  """),_display_(/*60.5*/views/*60.10*/.html.format.format()),format.raw/*60.31*/("""
	  """),_display_(/*61.5*/if(structure.size() >1 )/*61.29*/ {_display_(Seq[Any](format.raw/*61.31*/("""
	  """),format.raw/*62.4*/("""<div class="input-group"> <span class="input-group-addon">Filter</span>
          <input id="filter" type="text" class="form-control" placeholder="Type here...">
      </div>


	  <table class="table table-striped table-bordered table-condensed">
          <thead><th>Structure</th><th>Information</th></thead>
          <tbody class="searchable">
          """),_display_(/*70.12*/for(structure <- structure) yield /*70.39*/ {_display_(Seq[Any](format.raw/*70.41*/("""
              """),format.raw/*71.15*/("""<tr>
                  <td>"""),_display_(/*72.24*/views/*72.29*/.html.format.structureSide(structure.structure.id)),format.raw/*72.79*/("""</td>
                  <td>"""),_display_(/*73.24*/if(structure.structure.strproteintax != null )/*73.70*/{_display_(Seq[Any](format.raw/*73.71*/("""
                        """),_display_(/*74.26*/for(s <- structure.structure.strproteintax) yield /*74.69*/{_display_(Seq[Any](format.raw/*74.70*/(""" """),_display_(/*74.72*/if(s.proteins != null )/*74.95*/ {_display_(Seq[Any](format.raw/*74.97*/("""
                            """),format.raw/*75.29*/("""<p>"""),_display_(/*75.33*/{ s.proteins.name }),format.raw/*75.52*/(""" """),format.raw/*75.53*/("""("""),_display_(/*75.55*/{s.taxonomy.species}),format.raw/*75.75*/(""") </p>
                        """)))}),format.raw/*76.26*/("""
                      """)))}),format.raw/*77.24*/("""

                  """)))}),format.raw/*79.20*/("""
                  """),format.raw/*80.19*/("""</td>
              </tr>
          """)))}),format.raw/*82.12*/("""

	  """),format.raw/*84.4*/("""</tbody></table>
	  """)))}),format.raw/*85.5*/("""


	"""),format.raw/*88.2*/("""</div>
	</div>


</section>


""")))}))
      }
    }
  }

  def render(structure:List[Structurecomp],query:Array[String]): play.twirl.api.HtmlFormat.Appendable = apply(structure,query)

  def f:((List[Structurecomp],Array[String]) => play.twirl.api.HtmlFormat.Appendable) = (structure,query) => apply(structure,query)

  def ref: this.type = this

}


}

/**/
object compositions extends compositions_Scope0.compositions
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/compositions.scala.html
                  HASH: 70c4204655c24d2bc5ed4c965718059f5b162841
                  MATRIX: 786->1|935->55|963->58|974->62|1011->63|1038->64|1104->103|1132->104|1164->110|1205->124|1233->125|1269->135|1327->166|1355->167|1397->181|1558->314|1587->315|1632->332|1705->377|1734->378|1780->397|1809->398|1843->405|1871->406|1910->418|1938->419|2770->1414|2809->1425|3286->1876|3317->1891|3348->1894|3373->1896|3409->1906|3423->1911|3465->1932|3496->1937|3529->1961|3569->1963|3600->1967|3986->2326|4029->2353|4069->2355|4112->2370|4167->2398|4181->2403|4252->2453|4308->2482|4363->2528|4402->2529|4455->2555|4514->2598|4553->2599|4582->2601|4614->2624|4654->2626|4711->2655|4742->2659|4782->2678|4811->2679|4840->2681|4881->2701|4944->2733|4999->2757|5051->2778|5098->2797|5166->2834|5198->2839|5249->2860|5280->2864
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|36->5|36->5|38->7|38->7|38->7|40->9|40->9|40->9|42->11|44->13|44->13|45->14|46->15|46->15|48->17|48->17|50->19|50->19|52->21|52->21|75->45|76->46|88->58|88->58|88->58|88->58|90->60|90->60|90->60|91->61|91->61|91->61|92->62|100->70|100->70|100->70|101->71|102->72|102->72|102->72|103->73|103->73|103->73|104->74|104->74|104->74|104->74|104->74|104->74|105->75|105->75|105->75|105->75|105->75|105->75|106->76|107->77|109->79|110->80|112->82|114->84|115->85|118->88
                  -- GENERATED --
              */
          