
package views.html.format

import play.twirl.api._


     object structureLarge_Scope0 {
import java.lang._

import controllers._
import play.mvc.Http.Context.Implicit._

class structureLarge extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Long,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structureId: Long):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.21*/("""


        """),_display_(/*4.10*/if(session.get("notation") == null )/*4.46*/{_display_(Seq[Any](format.raw/*4.47*/("""
                """),format.raw/*5.17*/("""<img class="sugar_image" src="""),_display_(/*5.47*/{routes.Image.showImage(structureId, "cfgl", "normal" )}),format.raw/*5.103*/(""" """),format.raw/*5.104*/("""alt="">
        """)))}),format.raw/*6.10*/("""

        """),_display_(/*8.10*/if({ session.get("notation") != null && session.get("notation").matches("gs")})/*8.89*/ {_display_(Seq[Any](format.raw/*8.91*/("""
                """),format.raw/*9.84*/("""
                """),format.raw/*10.17*/("""<img class="sugar_image" src="""),_display_(/*10.47*/{routes.Image.showImage(structureId, session.get("notation"), "extended" )}),format.raw/*10.122*/(""" """),format.raw/*10.123*/("""alt="">

        """)))}),format.raw/*12.10*/("""
        """),_display_(/*13.10*/if({ session.get("notation") != null && session.get("notation").matches("cfgl")} )/*13.92*/ {_display_(Seq[Any](format.raw/*13.94*/("""
                """),format.raw/*14.100*/("""
                """),format.raw/*15.17*/("""<img class="sugar_image" src="""),_display_(/*15.47*/{routes.Image.showImage(structureId, session.get("notation"), "extended" )}),format.raw/*15.122*/(""" """),format.raw/*15.123*/("""alt="">
        """)))}),format.raw/*16.10*/("""

        """),_display_(/*18.10*/if({ session.get("notation") != null && session.get("notation").matches("uoxf")} )/*18.92*/ {_display_(Seq[Any](format.raw/*18.94*/("""
                """),format.raw/*19.100*/("""
                """),format.raw/*20.17*/("""<img class="sugar_image" src="""),_display_(/*20.47*/{routes.Image.showImage(structureId, session.get("notation"), "extended" )}),format.raw/*20.122*/(""" """),format.raw/*20.123*/("""alt="">
                """)))}),format.raw/*21.18*/("""

"""))
      }
    }
  }

  def render(structureId:Long): play.twirl.api.HtmlFormat.Appendable = apply(structureId)

  def f:((Long) => play.twirl.api.HtmlFormat.Appendable) = (structureId) => apply(structureId)

  def ref: this.type = this

}


}

/**/
object structureLarge extends structureLarge_Scope0.structureLarge
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/format/structureLarge.scala.html
                  HASH: b2c0c344801a52a1e6dcfb50dc62e1c1c88d3ea5
                  MATRIX: 768->1|882->20|920->32|964->68|1002->69|1046->86|1102->116|1179->172|1208->173|1255->190|1292->201|1379->280|1418->282|1462->366|1507->383|1564->413|1661->488|1691->489|1740->507|1777->517|1868->599|1908->601|1954->701|1999->718|2056->748|2153->823|2183->824|2231->841|2269->852|2360->934|2400->936|2446->1036|2491->1053|2548->1083|2645->1158|2675->1159|2731->1184
                  LINES: 27->1|32->1|35->4|35->4|35->4|36->5|36->5|36->5|36->5|37->6|39->8|39->8|39->8|40->9|41->10|41->10|41->10|41->10|43->12|44->13|44->13|44->13|45->14|46->15|46->15|46->15|46->15|47->16|49->18|49->18|49->18|50->19|51->20|51->20|51->20|51->20|52->21
                  -- GENERATED --
              */
          