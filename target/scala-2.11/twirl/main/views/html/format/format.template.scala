
package views.html.format

import play.twirl.api._


     object format_Scope0 {

class format extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<div class="info">
  <h3>Structure Format</h3>
  <a href="/format/cfgl"><span class='label label-dark'><span class='icon-adjust icon-white'></span> CFG/Essentials</span></a>
  <a href="/format/gs"><span class='label label-dark'><span class='icon-font icon-white'></span> Text</span></a>
  <a href="/format/uoxf"><span class='label label-dark'><span class='icon-stop icon-white'></span> Oxford</span></a>

</div>

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object format extends format_Scope0.format
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/format/format.scala.html
                  HASH: b0c81295e74b6d73e24ea5cc5ac77ff21b956a38
                  MATRIX: 836->0
                  LINES: 32->1
                  -- GENERATED --
              */
          