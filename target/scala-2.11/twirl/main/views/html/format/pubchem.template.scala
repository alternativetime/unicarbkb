
package views.html.format

import play.twirl.api._


     object pubchem_Scope0 {

class pubchem extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(pubchem : String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.20*/("""
"""),format.raw/*2.1*/("""<div class="info">
  <h3>External Links</h3>
  <a href="http://www.ncbi.nlm.nih.gov/pcsubstance?term="""),_display_(/*4.58*/pubchem),format.raw/*4.65*/(""""><span class='label label-dark'>  PubChem Entry</span></a>
</div>

"""))
      }
    }
  }

  def render(pubchem:String): play.twirl.api.HtmlFormat.Appendable = apply(pubchem)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (pubchem) => apply(pubchem)

  def ref: this.type = this

}


}

/**/
object pubchem extends pubchem_Scope0.pubchem
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/format/pubchem.scala.html
                  HASH: cea686372cc55226ff1e6c7ad920543d4c70562e
                  MATRIX: 756->1|869->19|896->20|1024->122|1051->129
                  LINES: 27->1|32->1|33->2|35->4|35->4
                  -- GENERATED --
              */
          