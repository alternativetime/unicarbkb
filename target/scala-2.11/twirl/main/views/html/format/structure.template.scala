
package views.html.format

import play.twirl.api._


     object structure_Scope0 {
import java.lang._

import controllers._
import play.mvc.Http.Context.Implicit._

class structure extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Long,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structureId: Long):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.21*/("""

"""),format.raw/*3.1*/("""<a href=""""),_display_(/*3.11*/routes/*3.17*/.Application.structureDetails(structureId)),format.raw/*3.59*/("""">
"""),_display_(/*4.2*/if(session.get("notation") == null )/*4.38*/{_display_(Seq[Any](format.raw/*4.39*/("""
        """),format.raw/*5.9*/("""<img class="sugar_image" src="""),_display_(/*5.39*/{routes.Image.showImage(structureId, "cfgl", "normal" )}),format.raw/*5.95*/(""" """),format.raw/*5.96*/("""height="75%", width="75%" alt="">
""")))}),format.raw/*6.2*/("""
"""),format.raw/*7.1*/("""</a>

<a href=""""),_display_(/*9.11*/routes/*9.17*/.Application.structureDetails(structureId)),format.raw/*9.59*/("""">
"""),_display_(/*10.2*/if({ session.get("notation") != null && session.get("notation").matches("gs")})/*10.81*/ {_display_(Seq[Any](format.raw/*10.83*/("""
        """),format.raw/*11.76*/("""
        """),format.raw/*12.9*/("""<img class="sugar_image" src="""),_display_(/*12.39*/{routes.Image.showImage(structureId, session.get("notation"), "normal" )}),format.raw/*12.112*/(""" """),format.raw/*12.113*/("""height="75%", width="75%" alt="" >

""")))}),format.raw/*14.2*/("""
"""),_display_(/*15.2*/if({ session.get("notation") != null && session.get("notation").matches("cfgl")} )/*15.84*/ {_display_(Seq[Any](format.raw/*15.86*/("""
	"""),format.raw/*16.85*/("""
        """),format.raw/*17.9*/("""<img class="sugar_image" src="""),_display_(/*17.39*/{routes.Image.showImage(structureId, session.get("notation"), "extended" )}),format.raw/*17.114*/(""" """),format.raw/*17.115*/("""height="75%", width="75%" alt="">
        """)))}),format.raw/*18.10*/(""" 

"""),_display_(/*20.2*/if({ session.get("notation") != null && session.get("notation").matches("uoxf")} )/*20.84*/ {_display_(Seq[Any](format.raw/*20.86*/("""
        """),format.raw/*21.92*/("""
        """),format.raw/*22.9*/("""<img class="sugar_image" src="""),_display_(/*22.39*/{routes.Image.showImage(structureId, session.get("notation"), "normal" )}),format.raw/*22.112*/(""" """),format.raw/*22.113*/("""height="75%", width="75%" alt="">
        """)))}),format.raw/*23.10*/("""
"""),format.raw/*24.1*/("""</a>


"""))
      }
    }
  }

  def render(structureId:Long): play.twirl.api.HtmlFormat.Appendable = apply(structureId)

  def f:((Long) => play.twirl.api.HtmlFormat.Appendable) = (structureId) => apply(structureId)

  def ref: this.type = this

}


}

/**/
object structure extends structure_Scope0.structure
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/format/structure.scala.html
                  HASH: 55e87941ca49cf7c6b0dace01a52315dc52e5efd
                  MATRIX: 758->1|872->20|900->22|936->32|950->38|1012->80|1041->84|1085->120|1123->121|1158->130|1214->160|1290->216|1318->217|1382->252|1409->253|1451->269|1465->275|1527->317|1557->321|1645->400|1685->402|1722->478|1758->487|1815->517|1910->590|1940->591|2007->628|2035->630|2126->712|2166->714|2196->799|2232->808|2289->838|2386->913|2416->914|2490->957|2520->961|2611->1043|2651->1045|2688->1137|2724->1146|2781->1176|2876->1249|2906->1250|2980->1293|3008->1294
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|35->4|35->4|36->5|36->5|36->5|36->5|37->6|38->7|40->9|40->9|40->9|41->10|41->10|41->10|42->11|43->12|43->12|43->12|43->12|45->14|46->15|46->15|46->15|47->16|48->17|48->17|48->17|48->17|49->18|51->20|51->20|51->20|52->21|53->22|53->22|53->22|53->22|54->23|55->24
                  -- GENERATED --
              */
          