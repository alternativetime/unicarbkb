
package views.html.format

import play.twirl.api._


     object ionmob_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class ionmob extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(proteinList: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.70*/("""

"""),format.raw/*3.1*/("""<div class='info'>
    <h3>Ion Mobility</h3>

    <div class='taxonomy'>
        <a href="/ionmobilityMannose"><span class='label label-ionmobblue'>High-Mannose Structures</span></a>
        <a id='toggle-taxonomy'><span class='label label-notice'><span class='icon-tags icon-white'></span> Glycoprotein Standards  <span class="caret"></span></span></a>

        <a href="/dextranIonMobility"><span class='label label-warning'>Dextran Ladder</span></a>
    </div>
    <div>
        <ul id='more-taxonomy'>
            <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Glycoproteins</h3>
	    <li><a href='/ionmobilityStandards'>Summary</a></li>
	    """),_display_(/*16.7*/for(p <- proteinList) yield /*16.28*/ {_display_(Seq[Any](format.raw/*16.30*/("""
            """),_display_(/*17.14*/if(!p.name.equals("Dextran"))/*17.43*/ {_display_(Seq[Any](format.raw/*17.45*/("""
	    """),format.raw/*18.6*/("""<li><span class='icon-tag icon-white'></span> """),_display_(/*18.53*/p/*18.54*/.name),format.raw/*18.59*/(""" """),format.raw/*18.60*/("""( <a href='/ionmobSodiatedGlycoproteinData/"""),_display_(/*18.104*/p/*18.105*/.id),format.raw/*18.108*/("""'>pos</a>,  <a href='/ionmobCompleteProtein/"""),_display_(/*18.153*/p/*18.154*/.id),format.raw/*18.157*/("""'>neg</a> )</li>
            """)))}),format.raw/*19.14*/("""
            """)))}),format.raw/*20.14*/("""
        """),format.raw/*21.9*/("""</ul>
    </div>
</div>


<div class="info">
    <h3>Method Summary</h3>
    <p style="text-align: justify">Measurements of absolute CCS values were performed using a Synapt G1 HDMS quadrupole/IMS/oa-ToF instrument (Waters Co., Manchester, U.K.) modified for drift tube operation (<a href="http://pubs.acs.org/doi/abs/10.1021/ac1022953">Bush, et al., 2010</a>).
        Briefly, N-linked glycans were released with hydrazine from the well-characterized glycoproteins ribonuclease B, porcine thyroglobulin, chicken ovalbumin, and bovine fetuin
        obtained from Sigma Chemical Co., Ltd. (Poole, Dorset, U.K.) and reacetylated. Sialic acids were removed from the thyroglobulin and fetuin samples by heating with 1% acetic acid for 1 h at 70 °C.
        For electrospray analysis, samples were dissolved in water:methanol (1:1, v:v) at ∼1 mg/mL. More information refer to the supplementary section <a href="http://pubs.acs.org/doi/abs/10.1021/ac400403d">Pagel K and Harvey DJ, Analytical Chemistry 2013</a>.</p>
</div>

<div class="info">
    <h3>GlycoMob Composition Search</h3>

    """),_display_(/*37.6*/helper/*37.12*/.form(action = routes.IonMobility.ionmobCompositionSearch, 'method->"GET", 'enctype -> "multipart/form-data", 'class->"form-ionmob")/*37.144*/ {_display_(Seq[Any](format.raw/*37.146*/("""

        """),format.raw/*39.9*/("""<div class="control-group ionmobcomp">
            <label class="labelionmob ionmobcomp">Hex</label>
            <input id="ionmobcomp" type="text" class="input-block-level" name="hex" value="0">
        </div>
        <div class="control-group ionmobcomp ionmobcomp-align">
            <label class="labelionmob ionmobcomp">HexNAc</label>
            <input id="ionmobcomp" type="text" class="input-block-level" name="hexnac" value="0">
        </div>
        <div class="control-group ionmobcomp ionmobcomp-align">
            <label class="labelionmob ionmobcomp">dHex</label>
            <input id="ionmobcomp" type="text" class="input-block-level" name="dhex" value="0">
        </div>
        <div class="control-group ionmobcomp ionmobcomp-align">
            <label class="labelionmob ionmobcomp">NeuNAc</label>
            <input id="ionmobcomp" type="text" class="input-block-level" name="neunac" value="0">
        </div>
        <div class="form-ionmobfooter">
            <button class="btn btn-primary btn btn-block" type="submit">Submit Query</button>
        </div>
    """)))}),format.raw/*58.6*/("""
"""),format.raw/*59.1*/("""</div>

<div class="info">
    <h3>CSS Search</h3>
    """),_display_(/*63.6*/helper/*63.12*/.form(action = routes.IonMobility.cssSearch, 'method->"GET", 'enctype -> "multipart/form-data", 'class->"form-ionmob")/*63.130*/ {_display_(Seq[Any](format.raw/*63.132*/("""
        """),format.raw/*64.9*/("""<div class="control-group ionmobcomp">
            <label class="labelionmob ionmobcomp">CSS Value:</label>
            <input id="ionmobcomp" type="text" class="input-block-level" name="css" value="0">
        </div>
        <div class="form-ionmobfooter">
            <button class="btn btn-primary btn btn-block" type="submit">Submit Query</button>
        </div>
    """)))}),format.raw/*71.6*/("""


    """),format.raw/*105.14*/("""


"""),format.raw/*108.1*/("""</div>

<script>
    //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e)"""),format.raw/*113.35*/("""{"""),format.raw/*113.36*/("""
    """),format.raw/*114.5*/("""e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) """),format.raw/*120.29*/("""{"""),format.raw/*120.30*/("""
        """),format.raw/*121.9*/("""if(type == 'minus') """),format.raw/*121.29*/("""{"""),format.raw/*121.30*/("""

            """),format.raw/*123.13*/("""if(currentVal > input.attr('min')) """),format.raw/*123.48*/("""{"""),format.raw/*123.49*/("""
                """),format.raw/*124.17*/("""input.val(currentVal - 1).change();
            """),format.raw/*125.13*/("""}"""),format.raw/*125.14*/("""
            """),format.raw/*126.13*/("""if(parseInt(input.val()) == input.attr('min')) """),format.raw/*126.60*/("""{"""),format.raw/*126.61*/("""
                """),format.raw/*127.17*/("""$(this).attr('disabled', true);
            """),format.raw/*128.13*/("""}"""),format.raw/*128.14*/("""

        """),format.raw/*130.9*/("""}"""),format.raw/*130.10*/(""" """),format.raw/*130.11*/("""else if(type == 'plus') """),format.raw/*130.35*/("""{"""),format.raw/*130.36*/("""

            """),format.raw/*132.13*/("""if(currentVal < input.attr('max')) """),format.raw/*132.48*/("""{"""),format.raw/*132.49*/("""
                """),format.raw/*133.17*/("""input.val(currentVal + 1).change();
            """),format.raw/*134.13*/("""}"""),format.raw/*134.14*/("""
            """),format.raw/*135.13*/("""if(parseInt(input.val()) == input.attr('max')) """),format.raw/*135.60*/("""{"""),format.raw/*135.61*/("""
                """),format.raw/*136.17*/("""$(this).attr('disabled', true);
            """),format.raw/*137.13*/("""}"""),format.raw/*137.14*/("""

        """),format.raw/*139.9*/("""}"""),format.raw/*139.10*/("""
    """),format.raw/*140.5*/("""}"""),format.raw/*140.6*/(""" """),format.raw/*140.7*/("""else """),format.raw/*140.12*/("""{"""),format.raw/*140.13*/("""
        """),format.raw/*141.9*/("""input.val(0);
    """),format.raw/*142.5*/("""}"""),format.raw/*142.6*/("""
"""),format.raw/*143.1*/("""}"""),format.raw/*143.2*/(""");
$('.input-number').focusin(function()"""),format.raw/*144.38*/("""{"""),format.raw/*144.39*/("""
   """),format.raw/*145.4*/("""$(this).data('oldValue', $(this).val());
"""),format.raw/*146.1*/("""}"""),format.raw/*146.2*/(""");
$('.input-number').change(function() """),format.raw/*147.38*/("""{"""),format.raw/*147.39*/("""

    """),format.raw/*149.5*/("""minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) """),format.raw/*154.34*/("""{"""),format.raw/*154.35*/("""
        """),format.raw/*155.9*/("""$(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    """),format.raw/*156.5*/("""}"""),format.raw/*156.6*/(""" """),format.raw/*156.7*/("""else """),format.raw/*156.12*/("""{"""),format.raw/*156.13*/("""
        """),format.raw/*157.9*/("""alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    """),format.raw/*159.5*/("""}"""),format.raw/*159.6*/("""
    """),format.raw/*160.5*/("""if(valueCurrent <= maxValue) """),format.raw/*160.34*/("""{"""),format.raw/*160.35*/("""
        """),format.raw/*161.9*/("""$(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    """),format.raw/*162.5*/("""}"""),format.raw/*162.6*/(""" """),format.raw/*162.7*/("""else """),format.raw/*162.12*/("""{"""),format.raw/*162.13*/("""
        """),format.raw/*163.9*/("""alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    """),format.raw/*165.5*/("""}"""),format.raw/*165.6*/("""


"""),format.raw/*168.1*/("""}"""),format.raw/*168.2*/(""");
$(".input-number").keydown(function (e) """),format.raw/*169.41*/("""{"""),format.raw/*169.42*/("""
        """),format.raw/*170.9*/("""// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) """),format.raw/*175.51*/("""{"""),format.raw/*175.52*/("""
                 """),format.raw/*176.18*/("""// let it happen, don't do anything
                 return;
        """),format.raw/*178.9*/("""}"""),format.raw/*178.10*/("""
        """),format.raw/*179.9*/("""// Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) """),format.raw/*180.104*/("""{"""),format.raw/*180.105*/("""
            """),format.raw/*181.13*/("""e.preventDefault();
        """),format.raw/*182.9*/("""}"""),format.raw/*182.10*/("""
    """),format.raw/*183.5*/("""}"""),format.raw/*183.6*/(""");


</script>
"""))
      }
    }
  }

  def render(proteinList:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(proteinList)

  def f:((List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (proteinList) => apply(proteinList)

  def ref: this.type = this

}


}

/**/
object ionmob extends ionmob_Scope0.ionmob
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/format/ionmob.scala.html
                  HASH: a634bfbec732d074d47b4eddfd61446dc9fb378b
                  MATRIX: 801->1|964->69|992->71|1685->738|1722->759|1762->761|1803->775|1841->804|1881->806|1914->812|1988->859|1998->860|2024->865|2053->866|2125->910|2136->911|2161->914|2234->959|2245->960|2270->963|2331->993|2376->1007|2412->1016|3525->2103|3540->2109|3682->2241|3723->2243|3760->2253|4877->3340|4905->3341|4987->3397|5002->3403|5130->3521|5171->3523|5207->3532|5609->3904|5645->5384|5676->5387|5838->5520|5868->5521|5901->5526|6160->5756|6190->5757|6227->5766|6276->5786|6306->5787|6349->5801|6413->5836|6443->5837|6489->5854|6566->5902|6596->5903|6638->5916|6714->5963|6744->5964|6790->5981|6863->6025|6893->6026|6931->6036|6961->6037|6991->6038|7044->6062|7074->6063|7117->6077|7181->6112|7211->6113|7257->6130|7334->6178|7364->6179|7406->6192|7482->6239|7512->6240|7558->6257|7631->6301|7661->6302|7699->6312|7729->6313|7762->6318|7791->6319|7820->6320|7854->6325|7884->6326|7921->6335|7967->6353|7996->6354|8025->6355|8054->6356|8123->6396|8153->6397|8185->6401|8254->6442|8283->6443|8352->6483|8382->6484|8416->6490|8646->6691|8676->6692|8713->6701|8827->6787|8856->6788|8885->6789|8919->6794|8949->6795|8986->6804|9112->6902|9141->6903|9174->6908|9232->6937|9262->6938|9299->6947|9412->7032|9441->7033|9470->7034|9504->7039|9534->7040|9571->7049|9697->7147|9726->7148|9757->7151|9786->7152|9858->7195|9888->7196|9925->7205|10257->7508|10287->7509|10334->7527|10431->7596|10461->7597|10498->7606|10683->7761|10714->7762|10756->7775|10812->7803|10842->7804|10875->7809|10904->7810
                  LINES: 27->1|32->1|34->3|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|50->19|51->20|52->21|68->37|68->37|68->37|68->37|70->39|89->58|90->59|94->63|94->63|94->63|94->63|95->64|102->71|105->105|108->108|113->113|113->113|114->114|120->120|120->120|121->121|121->121|121->121|123->123|123->123|123->123|124->124|125->125|125->125|126->126|126->126|126->126|127->127|128->128|128->128|130->130|130->130|130->130|130->130|130->130|132->132|132->132|132->132|133->133|134->134|134->134|135->135|135->135|135->135|136->136|137->137|137->137|139->139|139->139|140->140|140->140|140->140|140->140|140->140|141->141|142->142|142->142|143->143|143->143|144->144|144->144|145->145|146->146|146->146|147->147|147->147|149->149|154->154|154->154|155->155|156->156|156->156|156->156|156->156|156->156|157->157|159->159|159->159|160->160|160->160|160->160|161->161|162->162|162->162|162->162|162->162|162->162|163->163|165->165|165->165|168->168|168->168|169->169|169->169|170->170|175->175|175->175|176->176|178->178|178->178|179->179|180->180|180->180|181->181|182->182|182->182|183->183|183->183
                  -- GENERATED --
              */
          