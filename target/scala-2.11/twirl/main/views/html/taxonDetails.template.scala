
package views.html

import play.twirl.api._


     object taxonDetails_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class taxonDetails extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,Taxonomy,List[Biolsource],List[com.avaje.ebean.SqlRow],List[composition_protein.CompTax],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, taxon: Taxonomy, biolsource: List[Biolsource], groupTax: List[com.avaje.ebean.SqlRow], compTax: List[composition_protein.CompTax] ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*8.2*/header/*8.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*8.38*/("""
"""),format.raw/*9.1*/("""<th class="">
  <a href="">"""),_display_(/*10.15*/title),format.raw/*10.20*/("""</a>
</th>
""")))};
Seq[Any](format.raw/*1.151*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*6.37*/("""

"""),format.raw/*12.2*/("""

"""),_display_(/*14.2*/main/*14.6*/ {_display_(Seq[Any](format.raw/*14.8*/("""

"""),format.raw/*16.1*/("""<div class="modal hide fade" id="taxDescription">
  <div class="modal-header">
    <a href="#" class="close" data-dismiss="modal">&times;</a>
    <h3>Taxonomy Structures</h3>
  </div>
  <div class="modal-body">
    <ul class="thumbnails">
      """),_display_(/*23.8*/for(s <- taxon.strtaxonomy ) yield /*23.36*/{_display_(Seq[Any](format.raw/*23.37*/("""

      """),format.raw/*25.7*/("""<li class="span4">
	  """),_display_(/*26.5*/views/*26.10*/.html.format.structure(s.structure.id)),format.raw/*26.48*/("""
      """),format.raw/*27.7*/("""</li>
      """)))}),format.raw/*28.8*/("""
    """),format.raw/*29.5*/("""</ul>
  </div>
  <div class="modal-footer">
    help<a href="#" class="btn primary" onclick="okClicked ();">OK</a>
  </div>

</div>	

<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-tags" ></i><a href="/references"> Taxonomy</a> <span class="divider">></span></li>
  <li><i class="icon-tag" ></i> """),_display_(/*40.34*/taxon/*40.39*/.species),format.raw/*40.47*/("""</li>
</ul>

<section id="layouts">

<div class="page-header row-fluid">
  <h1>"""),_display_(/*46.8*/taxon/*46.13*/.species),format.raw/*46.21*/("""</h1> 
  <h4 class="subheader span8">"""),_display_(/*47.32*/message),format.raw/*47.39*/(""" """),format.raw/*47.40*/("""<span class="pull-right"><span class="label label-light">Curated Entry</span> <span class="label label-light">GlycoSuiteDB</span></span></h4>
</div>

<div class="row-fluid">
  <div class="span8">

    <p>The GlycoSuite database is an annotated and curated relational database of glycan structures. For the taxonomy entry <b>"""),_display_(/*53.129*/taxon/*53.134*/.species),format.raw/*53.142*/("""</b> a number of glycan structures, glycoproteins and tissue source's have been curated.</p>

    """),_display_(/*55.6*/if(compTax != null)/*55.25*/ {_display_(Seq[Any](format.raw/*55.27*/("""
      """),format.raw/*56.7*/("""<div class='glycosylation-sites'>
          <div class="alert alert-info"><p>Compositional data available: """),_display_(/*57.75*/compTax/*57.82*/.size()),format.raw/*57.89*/(""" """),format.raw/*57.90*/("""compositions reported</p>
              <p>"""),_display_(/*58.19*/for(c <- compTax) yield /*58.36*/ {_display_(Seq[Any](format.raw/*58.38*/("""
                  """),_display_(/*59.20*/c/*59.21*/.composition_structure.composition),format.raw/*59.55*/("""
                  """)))}),format.raw/*60.20*/(""" """),format.raw/*60.21*/("""</p>
          </div>
      </div>
    """)))}),format.raw/*63.6*/("""

    """),_display_(/*65.6*/if(taxon.strtaxonomy.size() > 1 )/*65.39*/{_display_(Seq[Any](format.raw/*65.40*/("""
    """),format.raw/*66.5*/("""<ul class="structures clearfix">
      """),_display_(/*67.8*/for((s, i) <- taxon.strtaxonomy.zipWithIndex; if (i <=5) ) yield /*67.66*/{_display_(Seq[Any](format.raw/*67.67*/("""
        """),format.raw/*68.9*/("""<li class="span5">
          """),_display_(/*69.12*/views/*69.17*/.html.format.structure(s.structure.id)),format.raw/*69.55*/("""
        """),format.raw/*70.9*/("""</li>
      """)))}),format.raw/*71.8*/("""
    """),format.raw/*72.5*/("""</ul>
    <ul id='more-thumbnails' class='structures clearfix'>
      """),_display_(/*74.8*/for((s, i) <- taxon.strtaxonomy.zipWithIndex; if (i >=6) ) yield /*74.66*/{_display_(Seq[Any](format.raw/*74.67*/("""
        """),format.raw/*75.9*/("""<li class="span5">
 	    """),_display_(/*76.8*/views/*76.13*/.html.format.structure(s.structure.id)),format.raw/*76.51*/("""
        """),format.raw/*77.9*/("""</li>
      """)))}),format.raw/*78.8*/("""
    """),format.raw/*79.5*/("""</ul>
    """),_display_(/*80.6*/if(taxon.strtaxonomy.size() >=5)/*80.38*/ {_display_(Seq[Any](format.raw/*80.40*/("""
    """),format.raw/*81.5*/("""<div id='show-structures' class='more-structures' href='#'>
      <span class="linktext">See more structures</span>
      <span class="linktext" style="display:none">See less structures</span>
      <br />
      <span>&#9679; &#9679; &#9679;</span>
    </div>
    """)))}),format.raw/*87.6*/("""
    """)))}),format.raw/*88.6*/("""
  """),format.raw/*89.3*/("""</div>

  <div class="span4 sidebar">

    """),_display_(/*93.6*/views/*93.11*/.html.format.format()),format.raw/*93.32*/("""

    """),format.raw/*95.5*/("""<div class="info">
      <h3>Glycan Structures</h3>
      <p>
        <span id='more-structures' class='label label-notice'>
          <i class="icon-th icon-white"></i> Structures <b>("""),_display_(/*99.62*/taxon/*99.67*/.strtaxonomy.size()),format.raw/*99.86*/(""")</b> 
          <span class='caret'></span>
        </span>
      </p>

    </div>

      <div class='info'>
          <h3>Biological Associations</h3>
              <div class='source'>
                  <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein """),_display_(/*109.134*/groupTax/*109.142*/.size()),format.raw/*109.149*/("""  """),format.raw/*109.151*/("""<span class="caret"></span></span></a>
              </div>
          <div>
              <ul id='more-protein'>
                  <h3 id='less-protein'><span class='icon-map-marker icon-white'></span> Protein</h3>
                    """),_display_(/*114.22*/for(t <- groupTax) yield /*114.40*/{_display_(Seq[Any](format.raw/*114.41*/("""
                    """),_display_(/*115.22*/if(t.get("swiss_prot") != null)/*115.53*/ {_display_(Seq[Any](format.raw/*115.55*/("""
                    """),format.raw/*116.21*/("""<li><span class='icon-map-marker icon-white'></span> <a href='/proteinsummary/"""),_display_(/*116.100*/t/*116.101*/.get("swiss_prot")),format.raw/*116.119*/("""/annotated'>"""),_display_(/*116.132*/t/*116.133*/.get("protein")),format.raw/*116.148*/(""" """),format.raw/*116.149*/("""("""),_display_(/*116.151*/t/*116.152*/.get("swiss_prot")),format.raw/*116.170*/(""")</a></li>
                    """)))}),format.raw/*117.22*/("""
                    """),_display_(/*118.22*/if(t.get("swiss_prot") == null)/*118.53*/ {_display_(Seq[Any](format.raw/*118.55*/("""
                    """),format.raw/*119.21*/("""<li><span class='icon-map-marker icon-white'></span> <a href='/proteinsummary/"""),_display_(/*119.100*/t/*119.101*/.get("protein")),format.raw/*119.116*/("""/"""),_display_(/*119.118*/t/*119.119*/.get("taxonomy")),format.raw/*119.135*/("""'>"""),_display_(/*119.138*/t/*119.139*/.get("protein")),format.raw/*119.154*/(""" """),format.raw/*119.155*/("""</a></li>
                    """)))}),format.raw/*120.22*/("""
                    """)))}),format.raw/*121.22*/("""

              """),format.raw/*123.15*/("""</ul>
          </div>
       </div>

  </div>
</div>

"""),_display_(/*130.2*/views/*130.7*/.html.footerunicarb.footerunicarb()),format.raw/*130.42*/("""

"""),format.raw/*132.1*/("""</section>


""")))}),format.raw/*135.2*/("""
"""))
      }
    }
  }

  def render(message:String,taxon:Taxonomy,biolsource:List[Biolsource],groupTax:List[com.avaje.ebean.SqlRow],compTax:List[composition_protein.CompTax]): play.twirl.api.HtmlFormat.Appendable = apply(message,taxon,biolsource,groupTax,compTax)

  def f:((String,Taxonomy,List[Biolsource],List[com.avaje.ebean.SqlRow],List[composition_protein.CompTax]) => play.twirl.api.HtmlFormat.Appendable) = (message,taxon,biolsource,groupTax,compTax) => apply(message,taxon,biolsource,groupTax,compTax)

  def ref: this.type = this

}


}

/**/
object taxonDetails extends taxonDetails_Scope0.taxonDetails
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/taxonDetails.scala.html
                  HASH: 18f67654c3e8d92a4c7a2de32fb08de33e29c9d2
                  MATRIX: 848->1|1090->280|1103->286|1209->316|1236->317|1291->345|1317->350|1369->150|1396->168|1424->277|1453->362|1482->365|1494->369|1533->371|1562->373|1834->619|1878->647|1917->648|1952->656|2001->679|2015->684|2074->722|2108->729|2151->742|2183->747|2600->1137|2614->1142|2643->1150|2749->1230|2763->1235|2792->1243|2857->1281|2885->1288|2914->1289|3267->1614|3282->1619|3312->1627|3437->1726|3465->1745|3505->1747|3539->1754|3674->1862|3690->1869|3718->1876|3747->1877|3818->1921|3851->1938|3891->1940|3938->1960|3948->1961|4003->1995|4054->2015|4083->2016|4153->2056|4186->2063|4228->2096|4267->2097|4299->2102|4365->2142|4439->2200|4478->2201|4514->2210|4571->2240|4585->2245|4644->2283|4680->2292|4723->2305|4755->2310|4852->2381|4926->2439|4965->2440|5001->2449|5053->2475|5067->2480|5126->2518|5162->2527|5205->2540|5237->2545|5274->2556|5315->2588|5355->2590|5387->2595|5682->2860|5718->2866|5748->2869|5818->2913|5832->2918|5874->2939|5907->2945|6120->3131|6134->3136|6174->3155|6524->3476|6543->3484|6573->3491|6605->3493|6869->3729|6904->3747|6944->3748|6994->3770|7035->3801|7076->3803|7126->3824|7234->3903|7246->3904|7287->3922|7329->3935|7341->3936|7379->3951|7410->3952|7441->3954|7453->3955|7494->3973|7558->4005|7608->4027|7649->4058|7690->4060|7740->4081|7848->4160|7860->4161|7898->4176|7929->4178|7941->4179|7980->4195|8012->4198|8024->4199|8062->4214|8093->4215|8156->4246|8210->4268|8255->4284|8338->4340|8352->4345|8409->4380|8439->4382|8484->4396
                  LINES: 27->1|31->8|31->8|33->8|34->9|35->10|35->10|38->1|39->3|40->6|42->12|44->14|44->14|44->14|46->16|53->23|53->23|53->23|55->25|56->26|56->26|56->26|57->27|58->28|59->29|70->40|70->40|70->40|76->46|76->46|76->46|77->47|77->47|77->47|83->53|83->53|83->53|85->55|85->55|85->55|86->56|87->57|87->57|87->57|87->57|88->58|88->58|88->58|89->59|89->59|89->59|90->60|90->60|93->63|95->65|95->65|95->65|96->66|97->67|97->67|97->67|98->68|99->69|99->69|99->69|100->70|101->71|102->72|104->74|104->74|104->74|105->75|106->76|106->76|106->76|107->77|108->78|109->79|110->80|110->80|110->80|111->81|117->87|118->88|119->89|123->93|123->93|123->93|125->95|129->99|129->99|129->99|139->109|139->109|139->109|139->109|144->114|144->114|144->114|145->115|145->115|145->115|146->116|146->116|146->116|146->116|146->116|146->116|146->116|146->116|146->116|146->116|146->116|147->117|148->118|148->118|148->118|149->119|149->119|149->119|149->119|149->119|149->119|149->119|149->119|149->119|149->119|149->119|150->120|151->121|153->123|160->130|160->130|160->130|162->132|165->135
                  -- GENERATED --
              */
          