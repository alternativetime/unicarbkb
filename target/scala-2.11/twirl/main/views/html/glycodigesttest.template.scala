
package views.html

import play.twirl.api._


     object glycodigesttest_Scope0 {
import java.lang._
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class glycodigesttest extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Map[String, String],Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(map: Map[String,String], id: Long, ct: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

Seq[Any](format.raw/*1.49*/("""
"""),format.raw/*3.1*/("""

"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

"""),format.raw/*7.1*/("""<style type="text/css">
#wrapperpin """),format.raw/*8.13*/("""{"""),format.raw/*8.14*/("""
	"""),format.raw/*9.2*/("""width: 90%;
	max-width: 1100px;
	min-width: 800px;
	margin: 50px auto;
"""),format.raw/*13.1*/("""}"""),format.raw/*13.2*/("""

"""),format.raw/*15.1*/("""#columnspin """),format.raw/*15.13*/("""{"""),format.raw/*15.14*/("""
	"""),format.raw/*16.2*/("""-webkit-column-count: 3;
	-webkit-column-gap: 10px;
	-webkit-column-fill: auto;
	-moz-column-count: 3;
	-moz-column-gap: 10px;
	-moz-column-fill: auto;
	column-count: 3;
	column-gap: 15px;
	column-fill: auto;
"""),format.raw/*25.1*/("""}"""),format.raw/*25.2*/("""
"""),format.raw/*26.1*/(""".pin """),format.raw/*26.6*/("""{"""),format.raw/*26.7*/("""
	"""),format.raw/*27.2*/("""display: inline-block;
	background: #FEFEFE;
	border: 2px solid #FAFAFA;
	box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
	margin: 0 2px 15px;
	-webkit-column-break-inside: avoid;
	-moz-column-break-inside: avoid;
	column-break-inside: avoid;
	padding: 15px;
	padding-bottom: 5px;
	background: -webkit-linear-gradient(45deg, #FFF, #F9F9F9);
	opacity: 1;
	-webkit-transition: all .2s ease;
	-moz-transition: all .2s ease;
	-o-transition: all .2s ease;
	transition: all .2s ease;
"""),format.raw/*43.1*/("""}"""),format.raw/*43.2*/("""

"""),format.raw/*45.1*/(""".pin img """),format.raw/*45.10*/("""{"""),format.raw/*45.11*/("""
	"""),format.raw/*46.2*/("""width: 100%;
	border-bottom: 1px solid #ccc;
	padding-bottom: 15px;
	margin-bottom: 5px;
"""),format.raw/*50.1*/("""}"""),format.raw/*50.2*/("""

"""),format.raw/*52.1*/(""".pin2 p """),format.raw/*52.9*/("""{"""),format.raw/*52.10*/("""
	"""),format.raw/*53.2*/("""font: 12px/18px Arial, sans-serif;
	color: #333;
	margin: 0;
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/("""



"""),format.raw/*60.1*/("""#columns:hover .pin:not(:hover) """),format.raw/*60.33*/("""{"""),format.raw/*60.34*/("""
	"""),format.raw/*61.2*/("""opacity: 0.4;
"""),format.raw/*62.1*/("""}"""),format.raw/*62.2*/("""

"""),format.raw/*64.1*/("""</style>

<script>
	$(document).ready(function() """),format.raw/*67.31*/("""{"""),format.raw/*67.32*/("""
	"""),format.raw/*68.2*/("""$('input:button').click(function() """),format.raw/*68.37*/("""{"""),format.raw/*68.38*/("""
		    """),format.raw/*69.7*/("""var x = $(this).attr('id');
	   	    ajax(x);
   	"""),format.raw/*71.5*/("""}"""),format.raw/*71.6*/(""");

	function ajax(x)"""),format.raw/*73.18*/("""{"""),format.raw/*73.19*/("""
		"""),format.raw/*74.3*/("""$.ajax("""),format.raw/*74.10*/("""{"""),format.raw/*74.11*/("""
	    	"""),format.raw/*75.7*/("""type: 'GET',
	        url: 'http://localhost:9000/ajax/' + x,
	    	dataType: 'json',
        	success: function(json) """),format.raw/*78.34*/("""{"""),format.raw/*78.35*/("""
		"""),format.raw/*79.3*/("""$('#badge').append( "<a href=\"/structure/" + json.message + "\"><span class=\"label label-dark\"><span class=\"icon-adjust icon-white\"></span> Link to UniCarbKB</span></a>"  );
     		"""),format.raw/*80.8*/("""}"""),format.raw/*80.9*/(""",
		error: function(json) """),format.raw/*81.25*/("""{"""),format.raw/*81.26*/("""
			"""),format.raw/*82.4*/("""$('#badge').append("<p>no result</p>");
		"""),format.raw/*83.3*/("""}"""),format.raw/*83.4*/("""
        """),format.raw/*84.9*/("""}"""),format.raw/*84.10*/(""");
	"""),format.raw/*85.2*/("""}"""),format.raw/*85.3*/("""
	"""),format.raw/*86.2*/("""}"""),format.raw/*86.3*/(""");
	</script>

<script>
        $(document).ready(function() """),format.raw/*90.38*/("""{"""),format.raw/*90.39*/("""  
        
        """),format.raw/*92.9*/("""$("#e20").select2("""),format.raw/*92.27*/("""{"""),format.raw/*92.28*/("""
            """),format.raw/*93.13*/("""tags:["ABS", "AMF", "BKF", "BTG", "GUH", "JBM", "NAN1", "SPG" ],
            tokenSeparators: [",", " "]"""),format.raw/*94.40*/("""}"""),format.raw/*94.41*/(""");
        """),format.raw/*95.9*/("""}"""),format.raw/*95.10*/(""");
</script>

<ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider"></span></li>
    <li class="active"><i class="icon-map-marker" ></i> GlycoDigest<span class="divider"></span></li>
</ul>
   
<div class="page-header row-fluid"> 
    <h1 id="homeTitle">GlycoDigest</h1>
    <h4 class="subheader">A tool to predict exoglycosidase digestions</h4>
</div>

<div class="bs-callout bs-callout-warning" >
        <h4>Using Exoglycosidases <span class="glyphicon glyphicon-search"></span></h4>
    <p>Exoglycosidase enzyme array digestions, in combination with U/HPLC and LC-MS, can deliver semi-quantitative glycan analysis of sugars released from glycoproteins. A detailed description and example experimental protocols for using and applying exoglycosidases is published by <a href="http://link.springer.com/protocol/10.1385%2F1-59745-167-3%3A125" target="_blank">Royle et al., Methods Mol Biol. 2006;347:125-43</a>.</p>
    <p>A summary of the mode of action of exoglycosidases and examples can be found <a href="http://www.glycodigest.org/exoglycosidase.pdf" target="_blank">here</a>. Click the 'Information on Glycosidases' button in the 'Build Exoglycosidases' box below.</p>
</div>

<div id="actions">
	<div>
		<a href="/structure/1249"><img src="http://115.146.94.196:8123/eurocarb/get_sugar_image.action?download=false&amp;scale=1.0&amp;opaque=false&amp;outputType=png&notation=cfg&inputType=glycoct_condensed&sequences="""),_display_(/*116.215*/helper/*116.221*/.urlEncode(ct)),format.raw/*116.235*/("""" ></a>
	</div>
	
	"""),format.raw/*122.71*/("""

   """),format.raw/*124.4*/("""<div class="bs-callout bs-callout-info" >
        <h4>Build Exoglycosidase Array</h4>
        <p>Use the search box below to select the panel of exoglycosidase to digest the structure shown:</p>    
       <form class="form-search" action=""""),_display_(/*127.43*/routes/*127.49*/.Glycodigest.glycodigesttest(1249)),format.raw/*127.83*/("""" method="GET">  	
       <div id="selection" class="row-fluid">
       <input name=digest  id="e20" id="listBox" class="span4"></input>
       <button type="submit" class="btn btn-primary">Search</button>
       </div>
    </form>
    <div class='more-exoglycosidases'>
    <a id='toggle-exoglycosidases'><span class='label'><span class='icon-tags icon-white'></span> Information on Exoglycosidases <span class="caret"></span></span></a>
   </div>
   </div>
</div>

<div>
                  <ul id='more-exoglycosidases'>
<div class="table-responsive" id="exoglycosidases">
        <table class="table table-striped">
                <thead>
                <tr>
                <th>Short Name</th>
                <th>Full Name</th>
                <th>Source</th>
                <th>Specificity</th>
                </tr>
                </thead>
                <tr><td>
                        ABS</td>
                    <td>α(2-3,6,8,9)-Sialidase</td>
                    <td>Recombinant Arthrobacter ureafaciens gene, expressed in E. coli
                            </td><td>α(2-3,6,8,9)-specific, cleaves all non-reducing terminal branched and unbranched sialic acids</td></tr>
                <tr><td>NAN1
                    </td>
                    <td>α(2-3)-Sialidase
                    </td>
                    <td>Recombinant Streptococcus pneumoniae gene, expressed in E. coli
                    </td><td>Releases α(2-3)-linked sialic acid
                    </td>
    </td>
                </tr>
                <tr><td>BKF
                    </td><td>α(1-2,3,4,6)-Fucosidase
                    </td><td>Bovine Kidney
                    </td><td>iReleases non-reducing terminal α(1-6) core-linked fucose more efficiently than other α-fucose linkages. Frequently used for release of core fucose residues
                    </td>
                </tr>
                <tr><td>XMF
                    </td>
                    <td>α(1-2)-Fucosidase
                    </td>
                    <td>Xanthomonas manihotis
                    </td>
                    <td>Releases non-reducing terminal α(1-2)-linked fucose
                    </td>
                </tr>
                <tr>
                    <td>AMF
                    </td>
                    <td>α(1-3,4)-Fucosidase
                    </td>
                    <td>Almond Meal
                    </td>
                    <td>Releases non-reducing terminal α(1-3,4)-linked fucose. Does not release core linked fucose in α(1-3,6) configuration
                    </td>
                </tr>
                <tr>
                   <td>BTG
                   </td>
 <td> β(1-3,4)-Galactosidase
                   </td>
                   <td>Bovine testis
                   </td>
                   <td>Releases non-reducing terminal β(1-3,4)-linked galactose residues
                   </td>
                </tr>
                <tr>
                   <td>SPG
                   </td>
                   <td>β(1-4)-Galactosidase
                   </td>
                   <td>Streptococcus pneumoniae
                   </td>
                   <td>β(1-4) specific galactosidase removes galactose residues from non- reducing terminal
                   </td>
                </tr>
                <tr><td>CBG</td>
                <td>α(1-3,4,6)-Galactosidase</td>
                <td>Coffee Bean</td>
                        <td>Hydrolyses α(1-3,4,6)-linked terminal galactose residues</td>
                </tr>
                <tr>
                        <td>JBM</td>
                        <td>α(1-2,3,6)-Mannosidase</td>
                        <td>Jack Bean</td>
                        <td>Releases non-reducing terminal α(1-2,6)-linked mannose residues more efficiently than α(1-3)</td>
                </tr>
                <tr>
                        <td>GUH</td>
<td>β-N-Acetylhexosaminidase</td>
                        <td>Recombinant Streptococcus pneumoniae gene, expressed in E. coli</td>
                        <td>Releases all non-reducing terminal β-linked N-acetylglucosamine but not bisecting GlcNAc β(1-4)Man residues</td>
                </tr>
                <tr>
                        <td>JBH</td>
                        <td>β-N-Acetylhexosaminidase</td>
                        <td>Jack Bean</td>
                        <td>Specific to all non-reducing terminal β(1-2,3,4,6)-linked N- acetylglucosamine and N-acetylgalactosamine residues</td>
                </tr>



        </table>
</div>
</ul></div>

<h2>Results</h2>
<div>
    <ul class="thumbnails">
"""),_display_(/*243.2*/for((key, value) <- map) yield /*243.26*/{_display_(Seq[Any](format.raw/*243.27*/("""
    """),_display_(/*244.6*/if(!key.contains("no change"))/*244.36*/ {_display_(Seq[Any](format.raw/*244.38*/("""
    """),format.raw/*245.5*/("""<li class="span4">
    <div class="thumbnail">
      <img src="http://115.146.94.196:8123/eurocarb/get_sugar_image.action?download=false&amp;scale=0.5&amp;opaque=false&amp;outputType=png&notation=cfglink&inputType=glycoct_condensed&sequences="""),_display_(/*247.197*/value),format.raw/*247.202*/("""" />
      <p>"""),_display_(/*248.11*/key/*248.14*/.replace("ABS", "ABS - Sialidase").replace("BTG", " BTG β(1-3,4)-Galactosidase").replace("NAN1", "NAN1 - α(2-3)-Sialidase").replace("BKF", "BKF α(1-2,3,4,6)-Fucosidase").replace("XMF", "XMF α(1-2)-Fucosidase").replace("AMF", "AMF α(1-3,4)-Fucosidase").replace("SPG", "SPG β(1-4)-Galactosidase").replace("CBG", "CBG α(1-3,4,6)-Galactosidase").replace("JBM", "JBM α(1-2,3,6)-Mannosidase").replace("GUH", "GUH β-N-Acetylhexosaminidase").replace("JBH", "JBH β-N-Acetylhexosaminidase").replace("ABS - Sialidase + ABS - Sialidase", "ABS - Sialidase")),format.raw/*248.558*/("""</p>
    </div>
    </li>
    """)))}),format.raw/*251.6*/("""
""")))}),format.raw/*252.2*/("""
    """),format.raw/*253.5*/("""</ul>
</div>
      """),_display_(/*255.8*/views/*255.13*/.html.footerunicarb.footerunicarb()),format.raw/*255.48*/("""    
        
""")))}),format.raw/*257.2*/("""
"""))
      }
    }
  }

  def render(map:Map[String, String],id:Long,ct:String): play.twirl.api.HtmlFormat.Appendable = apply(map,id,ct)

  def f:((Map[String, String],Long,String) => play.twirl.api.HtmlFormat.Appendable) = (map,id,ct) => apply(map,id,ct)

  def ref: this.type = this

}


}

/**/
object glycodigesttest extends glycodigesttest_Scope0.glycodigesttest
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/glycodigesttest.scala.html
                  HASH: 1012099f3b876ebbe161d2cde6acc58818036b30
                  MATRIX: 790->1|947->48|974->66|1002->69|1013->73|1051->75|1079->77|1142->113|1170->114|1198->116|1296->187|1324->188|1353->190|1393->202|1422->203|1451->205|1687->414|1715->415|1743->416|1775->421|1803->422|1832->424|2334->899|2362->900|2391->902|2428->911|2457->912|2486->914|2602->1003|2630->1004|2659->1006|2694->1014|2723->1015|2752->1017|2840->1078|2868->1079|2899->1083|2959->1115|2988->1116|3017->1118|3058->1132|3086->1133|3115->1135|3192->1184|3221->1185|3250->1187|3313->1222|3342->1223|3376->1230|3453->1280|3481->1281|3530->1302|3559->1303|3589->1306|3624->1313|3653->1314|3687->1321|3834->1440|3863->1441|3893->1444|4106->1630|4134->1631|4188->1657|4217->1658|4248->1662|4317->1704|4345->1705|4381->1714|4410->1715|4441->1719|4469->1720|4498->1722|4526->1723|4615->1784|4644->1785|4691->1805|4737->1823|4766->1824|4807->1837|4939->1941|4968->1942|5006->1953|5035->1954|6540->3430|6557->3436|6594->3450|6642->3705|6675->3710|6944->3951|6960->3957|7016->3991|11640->8588|11681->8612|11721->8613|11754->8619|11794->8649|11835->8651|11868->8656|12140->8899|12168->8904|12211->8919|12224->8922|12791->9466|12853->9497|12886->9499|12919->9504|12966->9524|12981->9529|13038->9564|13084->9579
                  LINES: 27->1|32->1|33->3|35->5|35->5|35->5|37->7|38->8|38->8|39->9|43->13|43->13|45->15|45->15|45->15|46->16|55->25|55->25|56->26|56->26|56->26|57->27|73->43|73->43|75->45|75->45|75->45|76->46|80->50|80->50|82->52|82->52|82->52|83->53|86->56|86->56|90->60|90->60|90->60|91->61|92->62|92->62|94->64|97->67|97->67|98->68|98->68|98->68|99->69|101->71|101->71|103->73|103->73|104->74|104->74|104->74|105->75|108->78|108->78|109->79|110->80|110->80|111->81|111->81|112->82|113->83|113->83|114->84|114->84|115->85|115->85|116->86|116->86|120->90|120->90|122->92|122->92|122->92|123->93|124->94|124->94|125->95|125->95|146->116|146->116|146->116|149->122|151->124|154->127|154->127|154->127|270->243|270->243|270->243|271->244|271->244|271->244|272->245|274->247|274->247|275->248|275->248|275->248|278->251|279->252|280->253|282->255|282->255|282->255|284->257
                  -- GENERATED --
              */
          