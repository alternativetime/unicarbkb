
package views.html

import play.twirl.api._


     object builder_Scope0 {
import controllers._

class builder extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="width:100%;height:100%;border:0;margin:0;">
<head> 
<title>UniCarbKB - GlycanBuilder</title> 
	<link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*5.63*/routes/*5.69*/.Assets.versioned("assets/stylesheets/bootstrap.min.css")),format.raw/*5.126*/("""">
	    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*6.67*/routes/*6.73*/.Assets.versioned("assets/stylesheets/unicarbkbbuilder.css")),format.raw/*6.133*/("""">

	        <link rel="icon" href=""""),_display_(/*8.34*/routes/*8.40*/.Assets.versioned("assets/favicon.ico")),format.raw/*8.79*/("""" type="image/x-icon">

		    <script src=""""),_display_(/*10.21*/routes/*10.27*/.Assets.versioned("assets/javascripts/jquery.js")),format.raw/*10.76*/(""""></script>
		        <script src=""""),_display_(/*11.25*/routes/*11.31*/.Assets.versioned("assets/javascripts/jquery.ae.image.resize.min.js")),format.raw/*11.100*/(""""></script>
			    <script src=""""),_display_(/*12.22*/routes/*12.28*/.Assets.versioned("assets/javascripts/bootstrap.min.js")),format.raw/*12.84*/(""""></script>
			        <script src=""""),_display_(/*13.26*/routes/*13.32*/.Assets.versioned("assets/javascripts/jquery.tablesorter.min.js")),format.raw/*13.97*/(""""></script>
				    <script src=""""),_display_(/*14.23*/routes/*14.29*/.Assets.versioned("assets/javascripts/bootstrap-lightbox.min.js")),format.raw/*14.94*/(""""></script>
				        <script src=""""),_display_(/*15.27*/routes/*15.33*/.Assets.versioned("assets/javascripts/showdown.js")),format.raw/*15.84*/(""""></script>
					    <script src=""""),_display_(/*16.24*/routes/*16.30*/.Assets.versioned("assets/javascripts/google-jsapi.js")),format.raw/*16.85*/(""""></script>
					        """),format.raw/*17.100*/("""
						    """),format.raw/*18.11*/("""<script src=""""),_display_(/*18.25*/routes/*18.31*/.Assets.versioned("assets/javascripts/jPages.js")),format.raw/*18.80*/(""""></script>
						        <script src=""""),_display_(/*19.29*/routes/*19.35*/.Assets.versioned("assets/javascripts/jquery.lazyload.js")),format.raw/*19.93*/(""""></script>
							    <script src=""""),_display_(/*20.26*/routes/*20.32*/.Assets.versioned("assets/javascripts/load-image.js")),format.raw/*20.85*/(""""></script>
							        <script src=""""),_display_(/*21.30*/routes/*21.36*/.Assets.versioned("assets/javascripts/bootstrap-image-gallery.min.js")),format.raw/*21.106*/(""""></script>

								    <script src=""""),_display_(/*23.27*/routes/*23.33*/.Assets.versioned("assets/javascripts/select2.js")),format.raw/*23.83*/(""""></script>
								        <script src=""""),_display_(/*24.31*/routes/*24.37*/.Assets.versioned("assets/javascripts/application.js")),format.raw/*24.91*/(""""></script>

									    <script src=""""),_display_(/*26.28*/routes/*26.34*/.Assets.versioned("assets/javascripts/workflows.js")),format.raw/*26.86*/(""""></script>
									        <script src="http://d3js.org/d3.v3.min.js"></script>
</head>
<section id="main" class="container-fluid content">
<body>
<header>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="/">UniCarbKB</a>
          <ul class="nav">
            <li class=""><a href="/">Home</a></li>
            <li><a href="/query">Query</a></li>
            <li><a href="/references?s=authors">References</a></li>
            <li><a href="/proteins">Glycoproteins</a></li>
            <li><a href="/builder">Glycan Builder</a></li>
          </ul>
          <div id="headersearch" class="pull-right">
            <ul class='nav'>
            <li><a href="/about">About</a></li>
            <li><a href="/about"> Contact</a></li>
            """),format.raw/*48.78*/("""
            """),format.raw/*49.13*/("""</ul>
            </li>
            </ul>
          </div>


        </div>
      </div>
    </div>
</header>
<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-wrench" ></i> Glycan Builder <span class="divider"></span></li>
</ul>

<div class="page-header row-fluid">
      <h1>Glycan Builder</h1>
      <h4 class="subheader">An intuitive interface for building and displaying glycan structures</h4>
</div>

        <script type="text/javascript" src="/GlycanBuilder/VAADIN/vaadinBootstrap.js"></script>

        <iframe id="__gwt_historyFrame" style="width:0;height:0;border:0;overflow:hidden" src="javascript:false"></iframe>



<div style="height:450px;" id="fb" class="v-app">
   <!-- Optional placeholder for the loading indicator -->
    <div class=" v-app-loading"></div>

    <!-- Alternative fallback text -->
    <noscript>You have to enable javascript in your browser to
              use an application built with Vaadin.</noscript>
  </div>      
<script type="text/javascript">//<![CDATA[
    if (!window.vaadin)
        alert("Failed to load the bootstrap JavaScript: VAADIN/vaadinBootstrap.js");

    /* The UI Configuration */
    vaadin.initApplication("fb", """),format.raw/*88.34*/("""{"""),format.raw/*88.35*/("""
        """),format.raw/*89.9*/(""""browserDetailsUrl": "GlycanBuilder/",
        "serviceUrl": "GlycanBuilder/",
        "widgetset": "ac.uk.icl.dell.vaadin.glycanbuilder.widgetset.GlycanbuilderWidgetset",
        "theme": "ucdb_2011theme",
        "versionInfo": """),format.raw/*93.24*/("""{"""),format.raw/*93.25*/(""""vaadinVersion": "7.0.0""""),format.raw/*93.49*/("""}"""),format.raw/*93.50*/(""",
        "vaadinDir": "/GlycanBuilder/VAADIN/",
        "heartbeatInterval": 300,
        "debug": false,
        "standalone": false,
        "authErrMsg": """),format.raw/*98.23*/("""{"""),format.raw/*98.24*/("""
            """),format.raw/*99.13*/(""""message": "Take note of any unsaved data, "+
                       "and <u>click here<\/u> to continue.",
            "caption": "Authentication problem"
        """),format.raw/*102.9*/("""}"""),format.raw/*102.10*/(""",
        "comErrMsg": """),format.raw/*103.22*/("""{"""),format.raw/*103.23*/("""
            """),format.raw/*104.13*/(""""message": "Take note of any unsaved data, "+
                       "and <u>click here<\/u> to continue.",
            "caption": "Communication problem"
        """),format.raw/*107.9*/("""}"""),format.raw/*107.10*/(""",
        "sessExpMsg": """),format.raw/*108.23*/("""{"""),format.raw/*108.24*/("""
            """),format.raw/*109.13*/(""""message": "Take note of any unsaved data, "+
                       "and <u>click here<\/u> to continue.",
            "caption": "Session Expired"
        """),format.raw/*112.9*/("""}"""),format.raw/*112.10*/("""
    """),format.raw/*113.5*/("""}"""),format.raw/*113.6*/(""");//]]>
  </script>
        """),format.raw/*118.21*/("""

	"""),format.raw/*120.2*/("""<script type="text/javascript">
        var callBack=[];
        callBack.run=function(response)"""),format.raw/*122.40*/("""{"""),format.raw/*122.41*/("""
          """),format.raw/*123.11*/("""document.write(response);
         var r = response;
          //document.write(r);
          //document.form.frm1.Search = 'test this';
          //document.form.frm1.submit();
        //document.forms["frm2"].submit();
        var url = "../saySearch/s?";
        var x = url + encodeURI(r);
        //var s = [ url, "" , r].join("");
        //document.write(x);
window.location = x ;
        """),format.raw/*134.9*/("""}"""),format.raw/*134.10*/("""
       """),format.raw/*135.8*/("""</script>

        <form id="frm1" name="frm1" action="ms" method="POST" class="form-search">
        <input type="button" name="Search" value="Search" onclick='exportCanvas("glycoct_condensed","callBack");'/>
        </form>


<div class="row-fluid">
        <div class="span6">
        <h3 class="">Find Structures</h3>
        <p class="builder">Build your glycan or epitope and search the UniCarbKB structure database. This new design is built using new technologies and discussed by the developer David Damerell.</p>
        </div>

        <div class="span6">
        <h3 class="">Publication</h3>
        <p class="builder">The GlycanBuilder: a fast, intuitive and flexible software tool for building and displaying glycan structures, Alessio Ceroni, Anne Dell, and Stuart M Haslam, Source Code Biol Med. 2007. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17683623">PubMed</a></p>
        <p class="builder">The GlycanBuilder and GlycoWorkbench glycoinformatics tools: updates and new developments, Damerell D, Ceroni A, Maass K <i>et al</i>, Biol Chem. 2012. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23109548">PubMed</a></p>
        </div>
</div>


        </body>
</section>
</html>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object builder extends builder_Scope0.builder
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/builder.scala.html
                  HASH: 996bd4d371c11795a3baa6bd13b49b7caedfd19b
                  MATRIX: 831->0|1185->328|1199->334|1277->391|1372->460|1386->466|1467->526|1530->563|1544->569|1603->608|1674->652|1689->658|1759->707|1822->743|1837->749|1928->818|1988->851|2003->857|2080->913|2144->950|2159->956|2245->1021|2306->1055|2321->1061|2407->1126|2472->1164|2487->1170|2559->1221|2621->1256|2636->1262|2712->1317|2766->1428|2805->1439|2846->1453|2861->1459|2931->1508|2998->1548|3013->1554|3092->1612|3156->1649|3171->1655|3245->1708|3313->1749|3328->1755|3420->1825|3486->1864|3501->1870|3572->1920|3641->1962|3656->1968|3731->2022|3798->2062|3813->2068|3886->2120|4739->3077|4780->3090|6089->4371|6118->4372|6154->4381|6412->4611|6441->4612|6493->4636|6522->4637|6708->4795|6737->4796|6778->4809|6970->4973|7000->4974|7052->4997|7082->4998|7124->5011|7315->5174|7345->5175|7398->5199|7428->5200|7470->5213|7655->5370|7685->5371|7718->5376|7747->5377|7804->5544|7835->5547|7960->5643|7990->5644|8030->5655|8454->6051|8484->6052|8520->6060
                  LINES: 32->1|36->5|36->5|36->5|37->6|37->6|37->6|39->8|39->8|39->8|41->10|41->10|41->10|42->11|42->11|42->11|43->12|43->12|43->12|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|49->18|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|54->23|54->23|54->23|55->24|55->24|55->24|57->26|57->26|57->26|78->48|79->49|118->88|118->88|119->89|123->93|123->93|123->93|123->93|128->98|128->98|129->99|132->102|132->102|133->103|133->103|134->104|137->107|137->107|138->108|138->108|139->109|142->112|142->112|143->113|143->113|145->118|147->120|149->122|149->122|150->123|161->134|161->134|162->135
                  -- GENERATED --
              */
          