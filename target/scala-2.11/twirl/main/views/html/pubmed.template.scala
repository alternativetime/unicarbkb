
package views.html

import play.twirl.api._


     object pubmed_Scope0 {
import java.util._

import controllers._
import models._
import views.html._

import scala.collection.JavaConversions._

class pubmed extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,List[Method],List[TissueTaxonomy],List[Disease],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(p: String, method: List[Method], tissue: List[TissueTaxonomy], disease: List[Disease]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.89*/("""
"""),_display_(/*2.2*/main/*2.6*/{_display_(Seq[Any](format.raw/*2.7*/("""

"""),format.raw/*4.1*/("""<script src=""""),_display_(/*4.15*/routes/*4.21*/.Assets.versioned("assets/javascripts/Biojs.js")),format.raw/*4.69*/(""""></script>
<script src=""""),_display_(/*5.15*/routes/*5.21*/.Assets.versioned("assets/javascripts/Biojs.Sequence.js")),format.raw/*5.78*/(""""></script>
<script src=""""),_display_(/*6.15*/routes/*6.21*/.Assets.versioned("assets/javascripts/Biojs.EbiGlobalSearch.js")),format.raw/*6.85*/(""""></script>

"""),format.raw/*11.3*/("""

"""),format.raw/*13.1*/("""<script>
      $(document).ready(function()"""),format.raw/*14.35*/("""{"""),format.raw/*14.36*/("""
        """),format.raw/*15.9*/("""$("#e1").select2("""),format.raw/*15.26*/("""{"""),format.raw/*15.27*/("""
          """),format.raw/*16.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
        minimumInputLength: 1,
        """),format.raw/*19.9*/("""}"""),format.raw/*19.10*/(""");
        """),format.raw/*20.9*/("""}"""),format.raw/*20.10*/(""");
</script>
<script>
      $(document).ready(function()"""),format.raw/*23.35*/("""{"""),format.raw/*23.36*/("""
        """),format.raw/*24.9*/("""$("#e2").select2("""),format.raw/*24.26*/("""{"""),format.raw/*24.27*/("""
          """),format.raw/*25.11*/("""placeholder: "Select multiple disease states... ",
          allowClear: true,
          minimumInputLength: 1,
        """),format.raw/*28.9*/("""}"""),format.raw/*28.10*/(""");
        """),format.raw/*29.9*/("""}"""),format.raw/*29.10*/(""");
</script>

<script>
        $(function() """),format.raw/*33.22*/("""{"""),format.raw/*33.23*/("""
        """),format.raw/*34.9*/("""var myArray = new Array();
        """),_display_(/*35.10*/for(a <- tissue) yield /*35.26*/{_display_(Seq[Any](format.raw/*35.27*/("""
                """),format.raw/*36.17*/("""myArray.push(""""),_display_(/*36.32*/a/*36.33*/.tissue_taxon),format.raw/*36.46*/("""");
        """)))}),format.raw/*37.10*/("""
        """),format.raw/*38.9*/("""myArray.sort();
        options = '';
        for (var i = 0; i < myArray.length; i++) """),format.raw/*40.50*/("""{"""),format.raw/*40.51*/("""
                """),format.raw/*41.17*/("""options += '<option value="' + myArray[i] + '">' + myArray[i] + '</option>';
        """),format.raw/*42.9*/("""}"""),format.raw/*42.10*/("""
        """),format.raw/*43.9*/("""$('#e1').html(options);
        """),format.raw/*44.9*/("""}"""),format.raw/*44.10*/(""");
</script>

<script>
        $(function() """),format.raw/*48.22*/("""{"""),format.raw/*48.23*/("""
        """),format.raw/*49.9*/("""var myArray = new Array();
        """),_display_(/*50.10*/for(a <- disease) yield /*50.27*/{_display_(Seq[Any](format.raw/*50.28*/("""
                """),format.raw/*51.17*/("""myArray.push(""""),_display_(/*51.32*/a/*51.33*/.disease_name),format.raw/*51.46*/("""");
        """)))}),format.raw/*52.10*/("""
        """),format.raw/*53.9*/("""myArray.sort();
        options = '';
        for (var i = 0; i < myArray.length; i++) """),format.raw/*55.50*/("""{"""),format.raw/*55.51*/("""
                """),format.raw/*56.17*/("""options += '<option value="' + myArray[i] + '">' + myArray[i] + '</option>';
        """),format.raw/*57.9*/("""}"""),format.raw/*57.10*/("""
        """),format.raw/*58.9*/("""$('#e2').html(options);
        """),format.raw/*59.9*/("""}"""),format.raw/*59.10*/(""");
</script>


<script>
	var textfront = "front-";

    var textend = "-back";
    var pubmedid = "";

	

    $(document).ready(function() """),format.raw/*71.34*/("""{"""),format.raw/*71.35*/("""

        """),format.raw/*73.9*/("""$('.update').click(
            function ()"""),format.raw/*74.24*/("""{"""),format.raw/*74.25*/("""pubmed(); """),format.raw/*74.35*/("""}"""),format.raw/*74.36*/("""
        """),format.raw/*75.9*/(""");
        $('.clear').click(
              function ()"""),format.raw/*77.26*/("""{"""),format.raw/*77.27*/("""clear(); """),format.raw/*77.36*/("""}"""),format.raw/*77.37*/("""
        """),format.raw/*78.9*/(""");
        
    """),format.raw/*80.5*/("""}"""),format.raw/*80.6*/(""");

    function update()
    """),format.raw/*83.5*/("""{"""),format.raw/*83.6*/("""
        """),format.raw/*84.9*/("""//$('div.update-here').html(textfront+$('.update-it input').val()+textend);
	console.log("hello" + $('.update-it input').val()  );
	var pubmedid = $('.update-it input').val();
	var xx = String($('.update-it input').val());
    """),format.raw/*88.5*/("""}"""),format.raw/*88.6*/("""
    """),format.raw/*89.5*/("""function clear()
    """),format.raw/*90.5*/("""{"""),format.raw/*90.6*/("""
        """),format.raw/*91.9*/("""$('div.update-here').html($(''));
    """),format.raw/*92.5*/("""}"""),format.raw/*92.6*/("""


	"""),format.raw/*95.2*/("""function pubmed() """),format.raw/*95.20*/("""{"""),format.raw/*95.21*/("""
		"""),format.raw/*96.3*/("""console.log("check id entered only: " + $('.update-it input').val()  );
		var pubmedid = $('.update-it input').val();
		document.getElementById('pubmedresult').value=pubmedid;
		
	        args = """),format.raw/*100.17*/("""{"""),format.raw/*100.18*/("""'apikey'  : '191d24f81e61c107bca103f7d6a9ca10',
	                'dbfrom'  : 'pubmed',
	                'id'      : pubmedid,
	                'db'      : 'pubmed',
	                'mindate' : '1990',
	                'datetype': 'pdat',
	                'max'     : '1'"""),format.raw/*106.33*/("""}"""),format.raw/*106.34*/("""

	        """),format.raw/*108.10*/("""$.getJSON('http://entrezajax.appspot.com/elink+esummary?callback=?', args, function(data) """),format.raw/*108.100*/("""{"""),format.raw/*108.101*/("""                """),format.raw/*108.117*/("""$('#result').html('');
	                $.each(data.result, function(i, item) """),format.raw/*109.56*/("""{"""),format.raw/*109.57*/("""
	                        """),format.raw/*110.26*/("""var author_list = '';
	                        for(var i = 0; i < item.AuthorList.length; i ++) """),format.raw/*111.75*/("""{"""),format.raw/*111.76*/("""
	                                """),format.raw/*112.34*/("""if(i != 0) """),format.raw/*112.45*/("""{"""),format.raw/*112.46*/("""
	                                        """),format.raw/*113.42*/("""author_list += ', ';
	                                """),format.raw/*114.34*/("""}"""),format.raw/*114.35*/("""
	                                """),format.raw/*115.34*/("""author_list += item.AuthorList[i];
	                        """),format.raw/*116.26*/("""}"""),format.raw/*116.27*/("""
	                        """),format.raw/*117.26*/("""var html = '<p><a href=\'http://www.ncbi.nlm.nih.gov/pubmed/' + item.ArticleIds.pubmed + '\'>' + item.Title + '</a><br/>' + author_list + '<br/>' + item.FullJournalName + ' ' + item.PubDate + '</p>';
	                        $("<div/>").html(html).appendTo('#result');
	                """),format.raw/*119.18*/("""}"""),format.raw/*119.19*/(""");
	        """),format.raw/*120.10*/("""}"""),format.raw/*120.11*/(""");
	"""),format.raw/*121.2*/("""}"""),format.raw/*121.3*/(""";

</script>


"""),format.raw/*128.3*/("""


"""),format.raw/*131.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-briefcase" ></i> Data Entry<span class="divider"></span></li>
</ul>

<section id="layouts" class="browse">
  <div class="page-header row-fluid">
    <h1>Data Submission</h1>
    <h4 class="subheader">UnicarbKB Mass Spec details.</h4>
  </div>

  <div class="row-fluid">
    <div class="span4 search">
      <div class="row-fluid">
        <div class="filterbar tabbable clearfix">
          <ul class="nav nav-tabs" id="myTabBar">
            <li id='workflow1-toggle' class='active'><a href='#workflow1' data-toggle="tab">Pubmed ID<span class="pull-right count"></span></a></li>
            <li id='workflow2-toggle'><a href='#workflow2' data-toggle="tab">Method</a></li>
            <li id='workflow3-toggle'><a href='#workflow3' data-toggle="tab">UniProt<span class="pull-right count"></span></a></li>
          </ul>
        </div>
        <div class='info'>
          <h3>Mass Spec Details</h3>
        </div>

      </div>
    </div>

    <div class="span8 rightpanel">






      <div class='tab-content span12'>

        <div id="workflow1" class="tab-pane active control-group glycosuitedb update-it">

          <h3>Find details associated with a Pubmed unique identifier:</h3> 
          <div class='input-append'>
            <input class='input-xlarge' type="text" name="update" />
            <button type="button" class="update btn btn-primary" data-loading-text="Loading..." value="update">Load Pubmed Id</button>
          </div>

          """),format.raw/*176.68*/("""

          """),format.raw/*178.11*/("""<div class="update-here">
          </div>

          <div id="result">
            Contacting NCBI Entrez ...
          </div>

        </div> """),format.raw/*185.33*/("""

        """),format.raw/*187.9*/("""<div id="workflow2" class="tab-pane control-group row-fluid eurocarb">
          <h3>Selected (multiple) reported method(s)</h3>
          <form>
            <select name="method" multiple size="10">
              """),_display_(/*191.16*/for(m <- method) yield /*191.32*/ {_display_(Seq[Any](format.raw/*191.34*/(""" """),format.raw/*191.35*/("""<option value=""""),_display_(/*191.51*/m/*191.52*/.id),format.raw/*191.55*/("""">"""),_display_(/*191.58*/m/*191.59*/.description),format.raw/*191.71*/("""</option> """)))}),format.raw/*191.82*/("""
            """),format.raw/*192.13*/("""</select>
            <p><--- Add New ---></p>

            UniProt ID: <input type="text" id="uniprotid" name="uniprot">
            <input type="hidden" id="pubmedresult" value=""/>

            <div id="selection" class="row-fluid">
              <select  name=tissue MULTIPLE id="e1" id="listBox" class="span10"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>

            <div id="selection" class="row-fluid">
              <select  name=tissue MULTIPLE id="e2" id="listBox" class="span10"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>

          </form>
        </div> """),format.raw/*209.36*/("""

        """),format.raw/*211.9*/("""<div id="workflow3" class="tab-pane control-group nextprot">


        </div> """),format.raw/*214.36*/("""

      """),format.raw/*216.7*/("""</div>

    </div>
  </div>
</div><!-- /col -->
</div><!-- /row -->
<div class="footer row-fluid">
  <div class="span12">
    <p class="pull-left">UniCarbKB</p>
    <p class="pull-right">Supported by 
      <a href="http://www.nectar.org.au">NeCTAR</a> &nbsp;|&nbsp; 
      <a href="http://www.ands.org.au">ANDS</a> &nbsp;|&nbsp;
      <a href="http://www.stint.se"> STINT</a> &nbsp;|&nbsp;
      <a href="http://expasy.org/"> SIB ExPASy</a>
    </p>
  </div>
</div>
</section>

""")))}),format.raw/*235.2*/("""
"""))
      }
    }
  }

  def render(p:String,method:List[Method],tissue:List[TissueTaxonomy],disease:List[Disease]): play.twirl.api.HtmlFormat.Appendable = apply(p,method,tissue,disease)

  def f:((String,List[Method],List[TissueTaxonomy],List[Disease]) => play.twirl.api.HtmlFormat.Appendable) = (p,method,tissue,disease) => apply(p,method,tissue,disease)

  def ref: this.type = this

}


}

/**/
object pubmed extends pubmed_Scope0.pubmed
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/pubmed.scala.html
                  HASH: 2bae8ad6db23d9640be5232bd21676b4b94530e8
                  MATRIX: 795->1|977->88|1004->90|1015->94|1052->95|1080->97|1120->111|1134->117|1202->165|1254->191|1268->197|1345->254|1397->280|1411->286|1495->350|1535->596|1564->598|1635->641|1664->642|1700->651|1745->668|1774->669|1813->680|1950->790|1979->791|2017->802|2046->803|2130->859|2159->860|2195->869|2240->886|2269->887|2308->898|2455->1018|2484->1019|2522->1030|2551->1031|2623->1075|2652->1076|2688->1085|2751->1121|2783->1137|2822->1138|2867->1155|2909->1170|2919->1171|2953->1184|2997->1197|3033->1206|3148->1293|3177->1294|3222->1311|3334->1396|3363->1397|3399->1406|3458->1438|3487->1439|3559->1483|3588->1484|3624->1493|3687->1529|3720->1546|3759->1547|3804->1564|3846->1579|3856->1580|3890->1593|3934->1606|3970->1615|4085->1702|4114->1703|4159->1720|4271->1805|4300->1806|4336->1815|4395->1847|4424->1848|4591->1987|4620->1988|4657->1998|4728->2041|4757->2042|4795->2052|4824->2053|4860->2062|4943->2117|4972->2118|5009->2127|5038->2128|5074->2137|5117->2153|5145->2154|5202->2184|5230->2185|5266->2194|5520->2421|5548->2422|5580->2427|5628->2448|5656->2449|5692->2458|5757->2496|5785->2497|5816->2501|5862->2519|5891->2520|5921->2523|6145->2718|6175->2719|6475->2990|6505->2991|6545->3002|6665->3092|6696->3093|6742->3109|6849->3187|6879->3188|6934->3214|7059->3310|7089->3311|7152->3345|7192->3356|7222->3357|7293->3399|7376->3453|7406->3454|7469->3488|7558->3548|7588->3549|7643->3575|7958->3861|7988->3862|8029->3874|8059->3875|8091->3879|8120->3880|8163->3943|8194->3946|9837->5617|9878->5629|10051->5790|10089->5800|10332->6015|10365->6031|10406->6033|10436->6034|10480->6050|10491->6051|10516->6054|10547->6057|10558->6058|10592->6070|10635->6081|10677->6094|11398->6806|11436->6816|11543->6914|11579->6922|12090->7402
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|35->4|36->5|36->5|36->5|37->6|37->6|37->6|39->11|41->13|42->14|42->14|43->15|43->15|43->15|44->16|47->19|47->19|48->20|48->20|51->23|51->23|52->24|52->24|52->24|53->25|56->28|56->28|57->29|57->29|61->33|61->33|62->34|63->35|63->35|63->35|64->36|64->36|64->36|64->36|65->37|66->38|68->40|68->40|69->41|70->42|70->42|71->43|72->44|72->44|76->48|76->48|77->49|78->50|78->50|78->50|79->51|79->51|79->51|79->51|80->52|81->53|83->55|83->55|84->56|85->57|85->57|86->58|87->59|87->59|99->71|99->71|101->73|102->74|102->74|102->74|102->74|103->75|105->77|105->77|105->77|105->77|106->78|108->80|108->80|111->83|111->83|112->84|116->88|116->88|117->89|118->90|118->90|119->91|120->92|120->92|123->95|123->95|123->95|124->96|128->100|128->100|134->106|134->106|136->108|136->108|136->108|136->108|137->109|137->109|138->110|139->111|139->111|140->112|140->112|140->112|141->113|142->114|142->114|143->115|144->116|144->116|145->117|147->119|147->119|148->120|148->120|149->121|149->121|154->128|157->131|202->176|204->178|211->185|213->187|217->191|217->191|217->191|217->191|217->191|217->191|217->191|217->191|217->191|217->191|217->191|218->192|235->209|237->211|240->214|242->216|261->235
                  -- GENERATED --
              */
          