
package views.html.ionmobility

import play.twirl.api._


     object cssSearch_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class cssSearch extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssDataGeneral],List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(cssData: List[models.glycomobcomposition.CssData], cssDataGeneral: List[models.glycomobcomposition.CssDataGeneral],  proteinList: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.187*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""

"""),format.raw/*5.1*/("""<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>CCS Search Results </h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <ul class="nav nav-tabs" id="product-table">
                    <li class="active"><a href="#1" data-toggle="tab">Matching Sodiated CCS</a>
                    </li>
                    <li><a href="#2" data-toggle="tab">Other Matching CSS</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="1">

                        """),_display_(/*28.26*/if(!cssData.isEmpty())/*28.48*/{_display_(Seq[Any](format.raw/*28.49*/("""

                        """),format.raw/*30.25*/("""<table class="table table-condensed table-bordered table-striped volumes order-table">
                            <thead>
                            <tr>
                                <th>CSS</th>
                                <th>Mass (Na)</th>
                                <th>Mass</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Gas</th>
                                <th>Precursor Structure</th>
                                <th>Glycoprotein</th>

                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*47.30*/for(c <- cssData) yield /*47.47*/{_display_(Seq[Any](format.raw/*47.48*/("""
                            """),format.raw/*48.29*/("""<tr>
                                <td>"""),_display_(/*49.38*/c/*49.39*/.css),format.raw/*49.43*/("""</td>
                                <td>"""),_display_(/*50.38*/c/*50.39*/.sodiatedGlycomobCompositionList.massCharge),format.raw/*50.82*/("""</td>
                                <td>"""),_display_(/*51.38*/c/*51.39*/.sodiatedGlycomobCompositionList.mass),format.raw/*51.76*/("""</td>
                                <td>"""),_display_(/*52.38*/c/*52.39*/.sodiatedGlycomobCompositionList.hex),format.raw/*52.75*/("""</td>
                                <td>"""),_display_(/*53.38*/c/*53.39*/.sodiatedGlycomobCompositionList.hexnac),format.raw/*53.78*/("""</td>
                                <td>"""),_display_(/*54.38*/c/*54.39*/.sodiatedGlycomobCompositionList.dhex),format.raw/*54.76*/("""</td>
                                <td>"""),_display_(/*55.38*/c/*55.39*/.sodiatedGlycomobCompositionList.neunac),format.raw/*55.78*/("""</td>
                                <td>"""),_display_(/*56.38*/c/*56.39*/.ionmobGasList.name),format.raw/*56.58*/("""</td>
                                <td>"""),_display_(/*57.38*/c/*57.39*/.nativeStructure),format.raw/*57.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*58.70*/c/*58.71*/.glycoproteinStandardList.id),format.raw/*58.99*/("""">"""),_display_(/*58.102*/c/*58.103*/.glycoproteinStandardList.name),format.raw/*58.133*/("""</a>
                                </td>
                            </tr>
                            """)))}),format.raw/*61.30*/("""
                            """),format.raw/*62.29*/("""</tbody>
                        </table>

                        """)))}),format.raw/*65.26*/("""
                    """),format.raw/*66.21*/("""</div>


                    <div class="tab-pane" id="2">
                        """),_display_(/*70.26*/if(!cssDataGeneral.isEmpty())/*70.55*/{_display_(Seq[Any](format.raw/*70.56*/("""

                        """),format.raw/*72.25*/("""<table class="table table-condensed table-bordered table-striped volumes order-table">
                            <thead>
                            <tr>
                                <th>CSS</th>
                                <th>Mass (m/z)</th>
                                <th>Ion</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Gas</th>
                                <th>Native Structure</th>
                                <th>Glycoprotein</th>

                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*89.30*/for(g <- cssDataGeneral) yield /*89.54*/{_display_(Seq[Any](format.raw/*89.55*/("""
                            """),format.raw/*90.29*/("""<tr>
                                <td>"""),_display_(/*91.38*/g/*91.39*/.css),format.raw/*91.43*/("""</td>
                                <td>"""),_display_(/*92.38*/{"%.2f".format(g.ionMass)}),format.raw/*92.64*/("""</td>
                                <td>"""),_display_(/*93.38*/g/*93.39*/.ion),format.raw/*93.43*/("""</td>
                                <td>"""),_display_(/*94.38*/g/*94.39*/.glycomobcompositionList.hex),format.raw/*94.67*/("""</td>
                                <td>"""),_display_(/*95.38*/g/*95.39*/.glycomobcompositionList.hexnac),format.raw/*95.70*/("""</td>
                                <td>"""),_display_(/*96.38*/g/*96.39*/.glycomobcompositionList.dhex),format.raw/*96.68*/("""</td>
                                <td>"""),_display_(/*97.38*/g/*97.39*/.glycomobcompositionList.neunac),format.raw/*97.70*/("""</td>
                                <td>"""),_display_(/*98.38*/g/*98.39*/.ionmobGasList.name),format.raw/*98.58*/("""</td>
                                <td>"""),_display_(/*99.38*/g/*99.39*/.nativeStructure),format.raw/*99.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*100.70*/g/*100.71*/.glycoproteinStandardList.id),format.raw/*100.99*/("""">"""),_display_(/*100.102*/g/*100.103*/.glycoproteinStandardList.name),format.raw/*100.133*/("""</a>
                                </td>
                            </tr>
                            """)))}),format.raw/*103.30*/("""
                            """),format.raw/*104.29*/("""</tbody>
                        </table>

                        """)))}),format.raw/*107.26*/("""
                    """),format.raw/*108.21*/("""</div>
                </div>
            </div>

            <div class="span4">
                """),_display_(/*113.18*/views/*113.23*/.html.format.ionmob(proteinList)),format.raw/*113.55*/("""
            """),format.raw/*114.13*/("""</div>
        </div>
    </section>
</section>


""")))}))
      }
    }
  }

  def render(cssData:List[models.glycomobcomposition.CssData],cssDataGeneral:List[models.glycomobcomposition.CssDataGeneral],proteinList:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(cssData,cssDataGeneral,proteinList)

  def f:((List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssDataGeneral],List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (cssData,cssDataGeneral,proteinList) => apply(cssData,cssDataGeneral,proteinList)

  def ref: this.type = this

}


}

/**/
object cssSearch extends cssSearch_Scope0.cssSearch
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/cssSearch.scala.html
                  HASH: 0a967c0be88e6459cffc89ae8e7a79ed0e7e7de8
                  MATRIX: 901->1|1182->186|1210->189|1221->193|1258->194|1286->196|2188->1071|2219->1093|2258->1094|2312->1120|3121->1902|3154->1919|3193->1920|3250->1949|3319->1991|3329->1992|3354->1996|3424->2039|3434->2040|3498->2083|3568->2126|3578->2127|3636->2164|3706->2207|3716->2208|3773->2244|3843->2287|3853->2288|3913->2327|3983->2370|3993->2371|4051->2408|4121->2451|4131->2452|4191->2491|4261->2534|4271->2535|4311->2554|4381->2597|4391->2598|4428->2614|4530->2689|4540->2690|4589->2718|4620->2721|4631->2722|4683->2752|4820->2858|4877->2887|4976->2955|5025->2976|5136->3060|5174->3089|5213->3090|5267->3116|6073->3895|6113->3919|6152->3920|6209->3949|6278->3991|6288->3992|6313->3996|6383->4039|6430->4065|6500->4108|6510->4109|6535->4113|6605->4156|6615->4157|6664->4185|6734->4228|6744->4229|6796->4260|6866->4303|6876->4304|6926->4333|6996->4376|7006->4377|7058->4408|7128->4451|7138->4452|7178->4471|7248->4514|7258->4515|7295->4531|7398->4606|7409->4607|7459->4635|7491->4638|7503->4639|7556->4669|7694->4775|7752->4804|7852->4872|7902->4893|8029->4992|8044->4997|8098->5029|8140->5042
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|59->28|59->28|59->28|61->30|78->47|78->47|78->47|79->48|80->49|80->49|80->49|81->50|81->50|81->50|82->51|82->51|82->51|83->52|83->52|83->52|84->53|84->53|84->53|85->54|85->54|85->54|86->55|86->55|86->55|87->56|87->56|87->56|88->57|88->57|88->57|89->58|89->58|89->58|89->58|89->58|89->58|92->61|93->62|96->65|97->66|101->70|101->70|101->70|103->72|120->89|120->89|120->89|121->90|122->91|122->91|122->91|123->92|123->92|124->93|124->93|124->93|125->94|125->94|125->94|126->95|126->95|126->95|127->96|127->96|127->96|128->97|128->97|128->97|129->98|129->98|129->98|130->99|130->99|130->99|131->100|131->100|131->100|131->100|131->100|131->100|134->103|135->104|138->107|139->108|144->113|144->113|144->113|145->114
                  -- GENERATED --
              */
          