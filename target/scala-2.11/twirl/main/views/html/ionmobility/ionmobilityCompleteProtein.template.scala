
package views.html.ionmobility

import play.twirl.api._


     object ionmobilityCompleteProtein_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class ionmobilityCompleteProtein extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[models.glycomobcomposition.CssDataGeneral],List[models.glycomobcomposition.CssDataGeneral],models.glycomobcomposition.GlycoproteinStandard,List[models.glycomobcomposition.GlycoproteinStandard],List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(summaryNegativeHe: List[models.glycomobcomposition.CssDataGeneral], summaryNegativeN: List[models.glycomobcomposition.CssDataGeneral], protein: models.glycomobcomposition.GlycoproteinStandard, proteinList: List[models.glycomobcomposition.GlycoproteinStandard], summaryPositiveHe: List[models.glycomobcomposition.CssData], summaryPositiveN: List[models.glycomobcomposition.CssData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.384*/("""


"""),_display_(/*4.2*/main/*4.6*/{_display_(Seq[Any](format.raw/*4.7*/("""

"""),format.raw/*6.1*/("""<script src="http://wenzhixin.net.cn/p/bootstrap-table/src/extensions/filter/bootstrap-table-filter.js"></script>

<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>"""),_display_(/*17.18*/protein/*17.25*/.name),format.raw/*17.30*/(""" """),format.raw/*17.31*/("""CCS Values</h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filter by CCS" />
                <ul class="nav nav-tabs" id="product-table">
                    <li class="active"><a href="#1" data-toggle="tab">Negative Mode - Helium ("""),_display_(/*24.96*/summaryNegativeHe/*24.113*/.size),format.raw/*24.118*/(""")</a></li>
                    <li><a href="#2" data-toggle="tab">Negative Mode - Nitrogen ("""),_display_(/*25.83*/summaryNegativeN/*25.99*/.size),format.raw/*25.104*/(""")</a></li>
                    <li><a href="#3" data-toggle="tab">Positive Mode - Helium ("""),_display_(/*26.81*/summaryPositiveHe/*26.98*/.size),format.raw/*26.103*/(""")</a></li>
                    <li><a href="#4" data-toggle="tab">Positive Mode - Helium ("""),_display_(/*27.81*/summaryPositiveN/*27.97*/.size),format.raw/*27.102*/(""")</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="1">

                        <h3>Negative Mode (Helium)</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Ion</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Native Structure</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*49.30*/for(negative <-  summaryNegativeHe) yield /*49.65*/{_display_(Seq[Any](format.raw/*49.66*/("""
                            """),format.raw/*50.29*/("""<tr>
                                <td>"""),_display_(/*51.38*/negative/*51.46*/.css),format.raw/*51.50*/("""</td>
                                <td>"""),_display_(/*52.38*/negative/*52.46*/.ionMass),format.raw/*52.54*/("""</td>
                                <td>"""),_display_(/*53.38*/negative/*53.46*/.ion),format.raw/*53.50*/("""</td>
                                <td>"""),_display_(/*54.38*/negative/*54.46*/.glycomobcompositionList.hex.replace("0", "")),format.raw/*54.91*/("""</td>
                                <td>"""),_display_(/*55.38*/negative/*55.46*/.glycomobcompositionList.hexnac.replace("0", "")),format.raw/*55.94*/("""</td>
                                <td>"""),_display_(/*56.38*/negative/*56.46*/.glycomobcompositionList.dhex.replace("0", "")),format.raw/*56.92*/("""</td>
                                <td>"""),_display_(/*57.38*/negative/*57.46*/.glycomobcompositionList.neunac.replace("0", "")),format.raw/*57.94*/("""</td>
                                <td></td>
                                <td>"""),_display_(/*59.38*/negative/*59.46*/.nativeStructure),format.raw/*59.62*/("""</td>
                            </tr>
                            """)))}),format.raw/*61.30*/("""
                            """),format.raw/*62.29*/("""</tbody>
                        </table>
                    </div>


                    <div class="tab-pane" id="2">
                        <h3>Negative Mode (Nitrogen)</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Ion</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Native Structure</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*85.30*/for(negativeN <-  summaryNegativeN) yield /*85.65*/{_display_(Seq[Any](format.raw/*85.66*/("""
                            """),format.raw/*86.29*/("""<tr>
                                <td>"""),_display_(/*87.38*/negativeN/*87.47*/.css),format.raw/*87.51*/("""</td>
                                <td>"""),_display_(/*88.38*/negativeN/*88.47*/.ionMass),format.raw/*88.55*/("""</td>
                                <td>"""),_display_(/*89.38*/negativeN/*89.47*/.ion),format.raw/*89.51*/("""</td>
                                <td>"""),_display_(/*90.38*/negativeN/*90.47*/.glycomobcompositionList.hex.replace("0", "")),format.raw/*90.92*/("""</td>
                                <td>"""),_display_(/*91.38*/negativeN/*91.47*/.glycomobcompositionList.hexnac.replace("0", "")),format.raw/*91.95*/("""</td>
                                <td>"""),_display_(/*92.38*/negativeN/*92.47*/.glycomobcompositionList.dhex.replace("0", "")),format.raw/*92.93*/("""</td>
                                <td>"""),_display_(/*93.38*/negativeN/*93.47*/.glycomobcompositionList.neunac.replace("0", "")),format.raw/*93.95*/("""</td>
                                <td></td>
                                <td>"""),_display_(/*95.38*/negativeN/*95.47*/.nativeStructure),format.raw/*95.63*/("""</td>
                            </tr>
                            """)))}),format.raw/*97.30*/("""
                            """),format.raw/*98.29*/("""</tbody>
                        </table>
                    </div>


                    <div class="tab-pane" id="3">

                        <h3>Positve Mode (Helium)</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Ion</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*122.30*/for(positiveHe <-  summaryPositiveHe) yield /*122.67*/{_display_(Seq[Any](format.raw/*122.68*/("""
                            """),format.raw/*123.29*/("""<tr>
                                <td>"""),_display_(/*124.38*/positiveHe/*124.48*/.css),format.raw/*124.52*/("""</td>
                                <td>"""),_display_(/*125.38*/positiveHe/*125.48*/.sodiatedGlycomobCompositionList.mass),format.raw/*125.85*/("""</td>
                                <td>"""),_display_(/*126.38*/positiveHe/*126.48*/.sodiatedGlycomobCompositionList.massCharge),format.raw/*126.91*/("""</td>
                                <td>"""),_display_(/*127.38*/positiveHe/*127.48*/.sodiatedGlycomobCompositionList.hex.replace("0", "")),format.raw/*127.101*/("""</td>
                                <td>"""),_display_(/*128.38*/positiveHe/*128.48*/.sodiatedGlycomobCompositionList.hexnac.replace("0", "")),format.raw/*128.104*/("""</td>
                                <td>"""),_display_(/*129.38*/positiveHe/*129.48*/.sodiatedGlycomobCompositionList.dhex.replace("0", "")),format.raw/*129.102*/("""</td>
                                <td>"""),_display_(/*130.38*/positiveHe/*130.48*/.sodiatedGlycomobCompositionList.neunac.replace("0", "")),format.raw/*130.104*/("""</td>
                                <td></td>
                                <td>"""),_display_(/*132.38*/positiveHe/*132.48*/.nativeStructure),format.raw/*132.64*/("""</td>
                            </tr>
                            """)))}),format.raw/*134.30*/("""
                            """),format.raw/*135.29*/("""</tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="4">
                        <h3>Positive Mode (Nitrogen)</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Ion</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Native Structure</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*157.30*/for(positiveN <-  summaryPositiveN) yield /*157.65*/{_display_(Seq[Any](format.raw/*157.66*/("""
                            """),format.raw/*158.29*/("""<tr>
                                <td>"""),_display_(/*159.38*/positiveN/*159.47*/.css),format.raw/*159.51*/("""</td>
                                <td>"""),_display_(/*160.38*/positiveN/*160.47*/.sodiatedGlycomobCompositionList.mass),format.raw/*160.84*/("""</td>
                                <td>"""),_display_(/*161.38*/positiveN/*161.47*/.sodiatedGlycomobCompositionList.massCharge),format.raw/*161.90*/("""</td>
                                <td>"""),_display_(/*162.38*/positiveN/*162.47*/.sodiatedGlycomobCompositionList.hex.replace("0", "")),format.raw/*162.100*/("""</td>
                                <td>"""),_display_(/*163.38*/positiveN/*163.47*/.sodiatedGlycomobCompositionList.hexnac.replace("0", "")),format.raw/*163.103*/("""</td>
                                <td>"""),_display_(/*164.38*/positiveN/*164.47*/.sodiatedGlycomobCompositionList.dhex.replace("0", "")),format.raw/*164.101*/("""</td>
                                <td>"""),_display_(/*165.38*/positiveN/*165.47*/.sodiatedGlycomobCompositionList.neunac.replace("0", "")),format.raw/*165.103*/("""</td>
                                <td></td>
                                <td>"""),_display_(/*167.38*/positiveN/*167.47*/.nativeStructure),format.raw/*167.63*/("""</td>
                            </tr>
                            """)))}),format.raw/*169.30*/("""
                            """),format.raw/*170.29*/("""</tbody>
                        </table>
                    </div>




                </div>
            </div>

            <div class="span4">
                """),_display_(/*181.18*/views/*181.23*/.html.format.ionmob(proteinList)),format.raw/*181.55*/("""
            """),format.raw/*182.13*/("""</div>
        </div>
    </section>
</section>

<script>
    (function(document) """),format.raw/*188.25*/("""{"""),format.raw/*188.26*/("""
	"""),format.raw/*189.2*/("""'use strict';

	var LightTableFilter = (function(Arr) """),format.raw/*191.40*/("""{"""),format.raw/*191.41*/("""

		"""),format.raw/*193.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*195.29*/("""{"""),format.raw/*195.30*/("""
			"""),format.raw/*196.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) """),format.raw/*198.45*/("""{"""),format.raw/*198.46*/("""
				"""),format.raw/*199.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*199.53*/("""{"""),format.raw/*199.54*/("""
					"""),format.raw/*200.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*201.5*/("""}"""),format.raw/*201.6*/(""");
			"""),format.raw/*202.4*/("""}"""),format.raw/*202.5*/(""");
		"""),format.raw/*203.3*/("""}"""),format.raw/*203.4*/("""

		"""),format.raw/*205.3*/("""function _filter(row) """),format.raw/*205.25*/("""{"""),format.raw/*205.26*/("""
			"""),format.raw/*206.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*208.3*/("""}"""),format.raw/*208.4*/("""

		"""),format.raw/*210.3*/("""return """),format.raw/*210.10*/("""{"""),format.raw/*210.11*/("""
			"""),format.raw/*211.4*/("""init: function() """),format.raw/*211.21*/("""{"""),format.raw/*211.22*/("""
				"""),format.raw/*212.5*/("""var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) """),format.raw/*213.46*/("""{"""),format.raw/*213.47*/("""
					"""),format.raw/*214.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*215.5*/("""}"""),format.raw/*215.6*/(""");
			"""),format.raw/*216.4*/("""}"""),format.raw/*216.5*/("""
		"""),format.raw/*217.3*/("""}"""),format.raw/*217.4*/(""";
	"""),format.raw/*218.2*/("""}"""),format.raw/*218.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*220.59*/("""{"""),format.raw/*220.60*/("""
		"""),format.raw/*221.3*/("""if (document.readyState === 'complete') """),format.raw/*221.43*/("""{"""),format.raw/*221.44*/("""
			"""),format.raw/*222.4*/("""LightTableFilter.init();
		"""),format.raw/*223.3*/("""}"""),format.raw/*223.4*/("""
	"""),format.raw/*224.2*/("""}"""),format.raw/*224.3*/(""");

"""),format.raw/*226.1*/("""}"""),format.raw/*226.2*/(""")(document);

</script>

""")))}))
      }
    }
  }

  def render(summaryNegativeHe:List[models.glycomobcomposition.CssDataGeneral],summaryNegativeN:List[models.glycomobcomposition.CssDataGeneral],protein:models.glycomobcomposition.GlycoproteinStandard,proteinList:List[models.glycomobcomposition.GlycoproteinStandard],summaryPositiveHe:List[models.glycomobcomposition.CssData],summaryPositiveN:List[models.glycomobcomposition.CssData]): play.twirl.api.HtmlFormat.Appendable = apply(summaryNegativeHe,summaryNegativeN,protein,proteinList,summaryPositiveHe,summaryPositiveN)

  def f:((List[models.glycomobcomposition.CssDataGeneral],List[models.glycomobcomposition.CssDataGeneral],models.glycomobcomposition.GlycoproteinStandard,List[models.glycomobcomposition.GlycoproteinStandard],List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData]) => play.twirl.api.HtmlFormat.Appendable) = (summaryNegativeHe,summaryNegativeN,protein,proteinList,summaryPositiveHe,summaryPositiveN) => apply(summaryNegativeHe,summaryNegativeN,protein,proteinList,summaryPositiveHe,summaryPositiveN)

  def ref: this.type = this

}


}

/**/
object ionmobilityCompleteProtein extends ionmobilityCompleteProtein_Scope0.ionmobilityCompleteProtein
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/ionmobilityCompleteProtein.scala.html
                  HASH: 9410668722ceab188f7129ecc4779ae22383a894
                  MATRIX: 1072->1|1550->383|1579->387|1590->391|1627->392|1655->394|2134->846|2150->853|2176->858|2205->859|2604->1231|2631->1248|2658->1253|2778->1346|2803->1362|2830->1367|2948->1458|2974->1475|3001->1480|3119->1571|3144->1587|3171->1592|4211->2605|4262->2640|4301->2641|4358->2670|4427->2712|4444->2720|4469->2724|4539->2767|4556->2775|4585->2783|4655->2826|4672->2834|4697->2838|4767->2881|4784->2889|4850->2934|4920->2977|4937->2985|5006->3033|5076->3076|5093->3084|5160->3130|5230->3173|5247->3181|5316->3229|5428->3314|5445->3322|5482->3338|5582->3407|5639->3436|6669->4439|6720->4474|6759->4475|6816->4504|6885->4546|6903->4555|6928->4559|6998->4602|7016->4611|7045->4619|7115->4662|7133->4671|7158->4675|7228->4718|7246->4727|7312->4772|7382->4815|7400->4824|7469->4872|7539->4915|7557->4924|7624->4970|7694->5013|7712->5022|7781->5070|7893->5155|7911->5164|7948->5180|8048->5249|8105->5278|9137->6282|9191->6319|9231->6320|9289->6349|9359->6391|9379->6401|9405->6405|9476->6448|9496->6458|9555->6495|9626->6538|9646->6548|9711->6591|9782->6634|9802->6644|9878->6697|9949->6740|9969->6750|10048->6806|10119->6849|10139->6859|10216->6913|10287->6956|10307->6966|10386->7022|10499->7107|10519->7117|10557->7133|10658->7202|10716->7231|11746->8233|11798->8268|11838->8269|11896->8298|11966->8340|11985->8349|12011->8353|12082->8396|12101->8405|12160->8442|12231->8485|12250->8494|12315->8537|12386->8580|12405->8589|12481->8642|12552->8685|12571->8694|12650->8750|12721->8793|12740->8802|12817->8856|12888->8899|12907->8908|12986->8964|13099->9049|13118->9058|13156->9074|13257->9143|13315->9172|13508->9337|13523->9342|13577->9374|13619->9387|13730->9469|13760->9470|13790->9472|13873->9526|13903->9527|13935->9531|14005->9572|14035->9573|14067->9577|14243->9724|14273->9725|14306->9730|14383->9778|14413->9779|14447->9785|14518->9828|14547->9829|14581->9835|14610->9836|14643->9841|14672->9842|14704->9846|14755->9868|14785->9869|14817->9873|14995->10023|15024->10024|15056->10028|15092->10035|15122->10036|15154->10040|15200->10057|15230->10058|15263->10063|15405->10176|15435->10177|15469->10183|15532->10218|15561->10219|15595->10225|15624->10226|15655->10229|15684->10230|15715->10233|15744->10234|15852->10313|15882->10314|15913->10317|15982->10357|16012->10358|16044->10362|16099->10389|16128->10390|16158->10392|16187->10393|16219->10397|16248->10398
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|48->17|48->17|48->17|48->17|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|58->27|58->27|58->27|80->49|80->49|80->49|81->50|82->51|82->51|82->51|83->52|83->52|83->52|84->53|84->53|84->53|85->54|85->54|85->54|86->55|86->55|86->55|87->56|87->56|87->56|88->57|88->57|88->57|90->59|90->59|90->59|92->61|93->62|116->85|116->85|116->85|117->86|118->87|118->87|118->87|119->88|119->88|119->88|120->89|120->89|120->89|121->90|121->90|121->90|122->91|122->91|122->91|123->92|123->92|123->92|124->93|124->93|124->93|126->95|126->95|126->95|128->97|129->98|153->122|153->122|153->122|154->123|155->124|155->124|155->124|156->125|156->125|156->125|157->126|157->126|157->126|158->127|158->127|158->127|159->128|159->128|159->128|160->129|160->129|160->129|161->130|161->130|161->130|163->132|163->132|163->132|165->134|166->135|188->157|188->157|188->157|189->158|190->159|190->159|190->159|191->160|191->160|191->160|192->161|192->161|192->161|193->162|193->162|193->162|194->163|194->163|194->163|195->164|195->164|195->164|196->165|196->165|196->165|198->167|198->167|198->167|200->169|201->170|212->181|212->181|212->181|213->182|219->188|219->188|220->189|222->191|222->191|224->193|226->195|226->195|227->196|229->198|229->198|230->199|230->199|230->199|231->200|232->201|232->201|233->202|233->202|234->203|234->203|236->205|236->205|236->205|237->206|239->208|239->208|241->210|241->210|241->210|242->211|242->211|242->211|243->212|244->213|244->213|245->214|246->215|246->215|247->216|247->216|248->217|248->217|249->218|249->218|251->220|251->220|252->221|252->221|252->221|253->222|254->223|254->223|255->224|255->224|257->226|257->226
                  -- GENERATED --
              */
          