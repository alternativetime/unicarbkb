
package views.html.ionmobility

import play.twirl.api._


     object ionmobilityMannose_Scope0 {
import java.util._

import controllers._
import models._
import play.mvc.Http.Context.Implicit._
import views.html._

import scala.collection.JavaConversions._

class ionmobilityMannose extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[ionmob.GlycanMob],List[ionmob.NitrogenNegative],List[ionmob.NitrogenPositive],List[ionmob.HeNegative],List[ionmob.HePositive],List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(gm: List[ionmob.GlycanMob], ne: List[ionmob.NitrogenNegative], np: List[ionmob.NitrogenPositive], he: List[ionmob.HeNegative], hp: List[ionmob.HePositive], proteinList: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.226*/("""

    """),_display_(/*3.6*/main/*3.10*/ {_display_(Seq[Any](format.raw/*3.12*/("""
        """),format.raw/*4.9*/("""<ul class="breadcrumb">
            <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
            <li><i class="icon-book" ></i><a href=""> Ion Mobility</a> <span class="divider">></span></li>
        </ul>
        <section id="layouts">

            <div class="page-header row-fluid">
                <h2>GlycoMob - Ion Mobility Database</h2>
            </div>

            <div class="row-fluid">
                <div class="span8">
                    <p></p>
                    """),_display_(/*17.22*/for(g <- gm) yield /*17.34*/ {_display_(Seq[Any](format.raw/*17.36*/("""
                        """),format.raw/*18.25*/("""<h4><a name=""""),_display_(/*18.39*/g/*18.40*/.structureName),format.raw/*18.54*/("""">"""),_display_(/*18.57*/g/*18.58*/.structureName),format.raw/*18.72*/("""</a></h4>
                        <img class="sugar_image" src="""),_display_(/*19.55*/{
                            routes.Image.showImageIM(g.structure.id, session.get("notation"), "extended")
                        }),format.raw/*21.26*/(""" """),format.raw/*21.27*/("""alt="" width="50%" length="50%">

                        <br/>
                        """),_display_(/*24.26*/if(g.unicarbdb > 1)/*24.45*/ {_display_(Seq[Any](format.raw/*24.47*/("""
                            """),format.raw/*25.29*/("""<a href="http://unicarb-db.biomedicine.gu.se//unicarbdb/show_glycan.action?glycanSequenceId="""),_display_(/*25.122*/g/*25.123*/.unicarbdb),format.raw/*25.133*/(""""><span class="label label-light">
                                See UniCarb-DB MS entry</span></a>
                        """)))}),format.raw/*27.26*/("""
                        """),_display_(/*28.26*/if(g.structureName.matches("Man3"))/*28.61*/ {_display_(Seq[Any](format.raw/*28.63*/("""
                            """),format.raw/*29.29*/("""<a href="http://www.unicarbkb.org/structure/1053"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*31.26*/("""
                        """),_display_(/*32.26*/if(g.structureName.matches("Man5"))/*32.61*/ {_display_(Seq[Any](format.raw/*32.63*/("""
                            """),format.raw/*33.29*/("""<a href="http://www.unicarbkb.org/structure/1061"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*35.26*/("""
                        """),_display_(/*36.26*/if(g.structureName.matches("Man6"))/*36.61*/ {_display_(Seq[Any](format.raw/*36.63*/("""
                            """),format.raw/*37.29*/("""<a href="http://www.unicarbkb.org/structure/1064"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*39.26*/("""
                        """),_display_(/*40.26*/if(g.structureName.matches("Man7"))/*40.61*/ {_display_(Seq[Any](format.raw/*40.63*/("""
                            """),format.raw/*41.29*/("""<a href="http://www.unicarbkb.org/structure/1066"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*43.26*/("""
                        """),_display_(/*44.26*/if(g.structureName.matches("Man8"))/*44.61*/ {_display_(Seq[Any](format.raw/*44.63*/("""
                            """),format.raw/*45.29*/("""<a href="http://www.unicarbkb.org/structure/1083"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*47.26*/("""
                        """),_display_(/*48.26*/if(g.structureName.matches("Man9"))/*48.61*/ {_display_(Seq[Any](format.raw/*48.63*/("""
                            """),format.raw/*49.29*/("""<a href="http://www.unicarbkb.org/structure/1081"><span class="label label-light">
                                UniCarbKB Search</span></a>
                        """)))}),format.raw/*51.26*/("""
                        """),_display_(/*52.26*/if(g.structureName.matches("Man9Glc1"))/*52.65*/ {}),format.raw/*52.68*/("""

                        """),_display_(/*54.26*/if(g.nitrogenNegativeList.size() > 0 )/*54.64*/{_display_(Seq[Any](format.raw/*54.65*/("""
                        """),format.raw/*55.25*/("""<table class="computers table table-striped">
                            <col style="width:20%" span="5" />
                            <thead>
                                <tr><th align="middle" colspan="3">Gas: Nitrogen</th></tr>
                                <tr><th align="middle" colspan="3">Negative</th></tr>
                                <tr>
                                    <th>M+H</th>
                                    <th>M+Cl</th>
                                    <th>M+H2PO4</th>

                                </tr>
                            </thead>
                            <tbody>
                                """),_display_(/*68.34*/for(ne <- g.nitrogenNegativeList) yield /*68.67*/ {_display_(Seq[Any](format.raw/*68.69*/("""
                                    """),format.raw/*69.37*/("""<tr><td>"""),_display_(/*69.46*/{if(ne.h > 1.0){
                                        "%.2f".format(ne.h) } else {"-"}
                                    }),format.raw/*71.38*/("""</td> <td>"""),_display_(/*71.49*/{if(ne.cl > 1.0){
                                        "%.2f".format(ne.cl) } else {"-"}
                                    }),format.raw/*73.38*/("""</td> <td>"""),_display_(/*73.49*/{if(ne.p > 1.0){
                                        "%.2f".format(ne.p) } else {"-"}
                                    }),format.raw/*75.38*/("""</td></tr>
                                        """)))}),format.raw/*76.42*/("""

                            """),format.raw/*78.29*/("""</tbody>
                        </table>
                    """)))}),format.raw/*80.22*/("""

                    """),_display_(/*82.22*/if(g.nitrogenPositiveList.size() > 0)/*82.59*/ {_display_(Seq[Any](format.raw/*82.61*/("""
                        """),format.raw/*83.25*/("""<table class="computers table table-striped">
                            <col style="width:20%" span="5" />
                            <thead>
                                <tr><th align="middle" colspan="3">Positive</th></tr>
                                <tr>

                                    <th>M+H</th>
                                    <th>M+Na</th>
                                    <th>M+K</th>
                                </tr>
                            </thead>
                            <tbody>

                                """),_display_(/*96.34*/for(ne <- g.nitrogenPositiveList) yield /*96.67*/ {_display_(Seq[Any](format.raw/*96.69*/("""
                                    """),format.raw/*97.37*/("""<td>"""),_display_(/*97.42*/{if(ne.h > 1.0){
                                        "%.2f".format(ne.h) } else {"-"}
                                    }),format.raw/*99.38*/("""</td> <td>"""),_display_(/*99.49*/{if(ne.na > 1.0){
                                        "%.2f".format(ne.na) } else {"-"}
                                    }),format.raw/*101.38*/("""</td> <td>"""),_display_(/*101.49*/{if(ne.k > 1.0){
                                        "%.2f".format(ne.k) } else {"-"}
                                    }),format.raw/*103.38*/("""</td>
                                </tr>
                                """)))}),format.raw/*105.34*/("""
                            """),format.raw/*106.29*/("""</tbody>
                        </table>
                    """)))}),format.raw/*108.22*/("""

                        """),_display_(/*110.26*/if(g.heNegativeList.size() > 0)/*110.57*/ {_display_(Seq[Any](format.raw/*110.59*/("""
                        """),format.raw/*111.25*/("""<table class="computers table table-striped">
                            <col style="width:20%" span="5" />
                            <thead>
                                <tr><th align="middle" colspan="3">Gas: Helium</th></tr>
                                <tr><th align="middle" colspan="3">Negative</th></tr>
                                <tr>
                                    <th>M+H</th>
                                    <th>M+Cl</th>
                                    <th>M+H2PO4</th>

                                </tr>
                            </thead>
                            <tbody>
                                """),_display_(/*124.34*/for(ne <- g.heNegativeList) yield /*124.61*/ {_display_(Seq[Any](format.raw/*124.63*/("""
                                    """),format.raw/*125.37*/("""<tr><td>"""),_display_(/*125.46*/{if(ne.h > 1.0){
                                        "%.2f".format(ne.h) } else {"-"}
                                    }),format.raw/*127.38*/("""</td> <td>"""),_display_(/*127.49*/{if(ne.cl > 1.0){
                                        "%.2f".format(ne.cl) } else {"-"}
                                    }),format.raw/*129.38*/("""</td> <td>"""),_display_(/*129.49*/{if(ne.p > 1.0){
                                        "%.2f".format(ne.p) } else {"-"}
                                    }),format.raw/*131.38*/("""</td></tr>
                                        """)))}),format.raw/*132.42*/("""


                            """),format.raw/*135.29*/("""</tbody>
                        </table>
                    """)))}),format.raw/*137.22*/("""

                        """),_display_(/*139.26*/if(g.hePositiveList.size() > 0 )/*139.58*/ {_display_(Seq[Any](format.raw/*139.60*/("""
                            """),format.raw/*140.29*/("""<table class="computers table table-striped">
                                <col style="width:20%" span="5" />
                                <thead>
                                    <tr><th align="middle" colspan="3">Positive</th></tr>
                                    <tr>
                                        <th>M+H</th>
                                        <th>M+Na</th>
                                        <th>M+K</th>
                                    </tr>
                                </thead>
                                <tbody>

                                """),_display_(/*152.34*/for(ne <- g.hePositiveList) yield /*152.61*/ {_display_(Seq[Any](format.raw/*152.63*/("""
                                    """),format.raw/*153.37*/("""<tr> <td>"""),_display_(/*153.47*/{
                                        if(ne.h > 1.0) {
                                            "%.2f".format(ne.h)
                                        } else {
                                            "-"
                                        }
                                    }),format.raw/*159.38*/("""</td> <td>"""),_display_(/*159.49*/{
                                        if(ne.na > 1.0) {
                                            "%.2f".format(ne.na)
                                        } else {
                                            "-"
                                        }
                                    }),format.raw/*165.38*/("""</td> <td>"""),_display_(/*165.49*/{
                                        if(ne.k > 1.0) {
                                            "%.2f".format(ne.k)
                                        } else {
                                            "-"
                                        }
                                    }),format.raw/*171.38*/("""</td>
                                    </tr>
                                """)))}),format.raw/*173.34*/("""
                                """),format.raw/*174.33*/("""</tbody>
                            </table>
                        """)))}),format.raw/*176.26*/("""

                    """)))}),format.raw/*178.22*/("""
                """),format.raw/*179.17*/("""</div>
                <div class="span4">
                    """),_display_(/*181.22*/views/*181.27*/.html.format.format()),format.raw/*181.48*/("""
                    """),_display_(/*182.22*/views/*182.27*/.html.format.ionmob(proteinList)),format.raw/*182.59*/("""
                """),format.raw/*183.17*/("""</div>
            </div>
            """),_display_(/*185.14*/views/*185.19*/.html.footerunicarb.footerunicarb()),format.raw/*185.54*/("""
        """),format.raw/*186.9*/("""</section>

    """)))}),format.raw/*188.6*/("""
"""))
      }
    }
  }

  def render(gm:List[ionmob.GlycanMob],ne:List[ionmob.NitrogenNegative],np:List[ionmob.NitrogenPositive],he:List[ionmob.HeNegative],hp:List[ionmob.HePositive],proteinList:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(gm,ne,np,he,hp,proteinList)

  def f:((List[ionmob.GlycanMob],List[ionmob.NitrogenNegative],List[ionmob.NitrogenPositive],List[ionmob.HeNegative],List[ionmob.HePositive],List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (gm,ne,np,he,hp,proteinList) => apply(gm,ne,np,he,hp,proteinList)

  def ref: this.type = this

}


}

/**/
object ionmobilityMannose extends ionmobilityMannose_Scope0.ionmobilityMannose
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/ionmobilityMannose.scala.html
                  HASH: 18324abca51b10dea411ee336ff69f94cf9d9ae1
                  MATRIX: 961->1|1281->225|1313->232|1325->236|1364->238|1399->247|1955->776|1983->788|2023->790|2076->815|2117->829|2127->830|2162->844|2192->847|2202->848|2237->862|2328->926|2482->1059|2511->1060|2627->1149|2655->1168|2695->1170|2752->1199|2873->1292|2884->1293|2916->1303|3074->1430|3127->1456|3171->1491|3211->1493|3268->1522|3467->1690|3520->1716|3564->1751|3604->1753|3661->1782|3860->1950|3913->1976|3957->2011|3997->2013|4054->2042|4253->2210|4306->2236|4350->2271|4390->2273|4447->2302|4646->2470|4699->2496|4743->2531|4783->2533|4840->2562|5039->2730|5092->2756|5136->2791|5176->2793|5233->2822|5432->2990|5485->3016|5533->3055|5557->3058|5611->3085|5658->3123|5697->3124|5750->3149|6433->3805|6482->3838|6522->3840|6587->3877|6623->3886|6771->4013|6809->4024|6959->4153|6997->4164|7145->4291|7228->4343|7286->4373|7380->4436|7430->4459|7476->4496|7516->4498|7569->4523|8158->5085|8207->5118|8247->5120|8312->5157|8344->5162|8492->5289|8530->5300|8681->5429|8720->5440|8869->5567|8978->5644|9036->5673|9131->5736|9186->5763|9227->5794|9268->5796|9322->5821|10004->6475|10048->6502|10089->6504|10155->6541|10192->6550|10341->6677|10380->6688|10531->6817|10570->6828|10719->6955|10803->7007|10863->7038|10958->7101|11013->7128|11055->7160|11096->7162|11154->7191|11783->7792|11827->7819|11868->7821|11934->7858|11972->7868|12293->8167|12332->8178|12655->8479|12694->8490|13015->8789|13128->8870|13190->8903|13293->8974|13348->8997|13394->9014|13486->9078|13501->9083|13544->9104|13594->9126|13609->9131|13663->9163|13709->9180|13776->9219|13791->9224|13848->9259|13885->9268|13933->9285
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|48->17|48->17|48->17|49->18|49->18|49->18|49->18|49->18|49->18|49->18|50->19|52->21|52->21|55->24|55->24|55->24|56->25|56->25|56->25|56->25|58->27|59->28|59->28|59->28|60->29|62->31|63->32|63->32|63->32|64->33|66->35|67->36|67->36|67->36|68->37|70->39|71->40|71->40|71->40|72->41|74->43|75->44|75->44|75->44|76->45|78->47|79->48|79->48|79->48|80->49|82->51|83->52|83->52|83->52|85->54|85->54|85->54|86->55|99->68|99->68|99->68|100->69|100->69|102->71|102->71|104->73|104->73|106->75|107->76|109->78|111->80|113->82|113->82|113->82|114->83|127->96|127->96|127->96|128->97|128->97|130->99|130->99|132->101|132->101|134->103|136->105|137->106|139->108|141->110|141->110|141->110|142->111|155->124|155->124|155->124|156->125|156->125|158->127|158->127|160->129|160->129|162->131|163->132|166->135|168->137|170->139|170->139|170->139|171->140|183->152|183->152|183->152|184->153|184->153|190->159|190->159|196->165|196->165|202->171|204->173|205->174|207->176|209->178|210->179|212->181|212->181|212->181|213->182|213->182|213->182|214->183|216->185|216->185|216->185|217->186|219->188
                  -- GENERATED --
              */
          