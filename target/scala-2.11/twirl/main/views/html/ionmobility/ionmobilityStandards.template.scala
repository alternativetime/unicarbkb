
package views.html.ionmobility

import play.twirl.api._


     object ionmobilityStandards_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class ionmobilityStandards extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(standards: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.68*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""

"""),format.raw/*5.1*/("""<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>Glycoprotein CCS Values</h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
		<p>To support data interpretation of IM and MS/MS data we will populate a new CCS database, GlycoMob, as well as supplement the existing glycan fragmentation database UniCarb-DB with measurements of glycan standards. Glycan analysis using IM-MS is a relatively new approach and applications for CCS measurements have principally employed home-built instruments. As a consequence the wide-spread use of IM-MS for glycomics analysis on commercially available instruments has been hindered.</p>
                <br/>
                <h3>Glycoprotein Standards Summary</h3>
                <table class="table table-condensed table-bordered table-striped volumes">
                    <col style="width:20%" span="5" />
                    <thead>
                    <tr>
                        <th>Glycoprotein</th>
                        """),format.raw/*27.61*/("""
                        """),format.raw/*28.25*/("""<th>Positive Mode using Helium</th>
                        <th>Positive Mode using Nitrogen</th>
                        <th>Negative Mode using Helium</th>
                        <th>Negative Mode using Nitrogen</th>
                    </thead>
                    <tbody>
                    """),_display_(/*34.22*/for(s <- standards) yield /*34.41*/{_display_(Seq[Any](format.raw/*34.42*/("""
                    """),_display_(/*35.22*/if(!s.name.matches("Dextran"))/*35.52*/{_display_(Seq[Any](format.raw/*35.53*/("""
                    """),format.raw/*36.21*/("""<tr>
                        <td style="text-align:center">"""),_display_(/*37.56*/s/*37.57*/.name),format.raw/*37.62*/("""</td>
                        """),format.raw/*38.58*/("""
                        """),format.raw/*39.25*/("""<td style="text-align:center"><a href="/ionmobSodiatedGlycoproteinData/"""),_display_(/*39.97*/s/*39.98*/.id),format.raw/*39.101*/("""">"""),_display_(/*39.104*/s/*39.105*/.cssPosHeNative),format.raw/*39.120*/(""" """),format.raw/*39.121*/("""("""),_display_(/*39.123*/s/*39.124*/.totalCssPosHe),format.raw/*39.138*/(""")</a></td>
                        <td style="text-align:center"><a href="/ionmobSodiatedGlycoproteinData/"""),_display_(/*40.97*/s/*40.98*/.id),format.raw/*40.101*/("""">"""),_display_(/*40.104*/s/*40.105*/.cssPosNitrogenNative),format.raw/*40.126*/(""" """),format.raw/*40.127*/("""("""),_display_(/*40.129*/s/*40.130*/.totalCssPosN),format.raw/*40.143*/(""")</a></td>
                        <td style="text-align:center"><a href="/ionmobCompleteProtein/"""),_display_(/*41.88*/s/*41.89*/.id),format.raw/*41.92*/("""">"""),_display_(/*41.95*/if(s.totalCssNegHe > 0)/*41.118*/ {_display_(Seq[Any](_display_(/*41.121*/s/*41.122*/.cssNegHeNative),format.raw/*41.137*/(""" """),format.raw/*41.138*/("""("""),_display_(/*41.140*/s/*41.141*/.totalCssNegHe),format.raw/*41.155*/(""")""")))}/*41.158*/else/*41.163*/{_display_(Seq[Any](format.raw/*41.164*/(""" """),format.raw/*41.165*/("""- """)))}),format.raw/*41.168*/("""</a></td>
                        <td style="text-align:center"><a href="/ionmobCompleteProtein/"""),_display_(/*42.88*/s/*42.89*/.id),format.raw/*42.92*/("""">"""),_display_(/*42.95*/if(s.totalCssNegN > 0 )/*42.118*/ {_display_(Seq[Any](_display_(/*42.121*/s/*42.122*/.cssNegNitrogenNative),format.raw/*42.143*/(""" """),format.raw/*42.144*/("""("""),_display_(/*42.146*/s/*42.147*/.totalCssNegN),format.raw/*42.160*/(""")""")))}/*42.163*/else/*42.168*/{_display_(Seq[Any](format.raw/*42.169*/(""" """),format.raw/*42.170*/("""- """)))}),format.raw/*42.173*/("""</a></td>
                    </tr>
                    """)))}),format.raw/*44.22*/("""
                    """)))}),format.raw/*45.22*/("""
                    """),format.raw/*46.21*/("""</tbody>
                </table>

                <p>&#42;Content in parenthesis refers to the total number of native and fragment entries. Values outside parenthesis correspond to the number of native structures</p>

            </div>

            <div class="span4">
                """),_display_(/*54.18*/views/*54.23*/.html.format.ionmob(standards)),format.raw/*54.53*/("""
            """),format.raw/*55.13*/("""</div>
        </div>
    </section>
</section>


""")))}),format.raw/*61.2*/("""
"""))
      }
    }
  }

  def render(standards:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(standards)

  def f:((List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (standards) => apply(standards)

  def ref: this.type = this

}


}

/**/
object ionmobilityStandards extends ionmobilityStandards_Scope0.ionmobilityStandards
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/ionmobilityStandards.scala.html
                  HASH: f5559df74ccac2ef34f13b6d57a60f5952387996
                  MATRIX: 834->1|995->67|1023->70|1034->74|1071->75|1099->77|2413->1399|2466->1424|2791->1722|2826->1741|2865->1742|2914->1764|2953->1794|2992->1795|3041->1816|3128->1876|3138->1877|3164->1882|3222->1945|3275->1970|3374->2042|3384->2043|3409->2046|3440->2049|3451->2050|3488->2065|3518->2066|3548->2068|3559->2069|3595->2083|3729->2190|3739->2191|3764->2194|3795->2197|3806->2198|3849->2219|3879->2220|3909->2222|3920->2223|3955->2236|4080->2334|4090->2335|4114->2338|4144->2341|4177->2364|4218->2367|4229->2368|4266->2383|4296->2384|4326->2386|4337->2387|4373->2401|4395->2404|4409->2409|4449->2410|4479->2411|4514->2414|4638->2511|4648->2512|4672->2515|4702->2518|4735->2541|4776->2544|4787->2545|4830->2566|4860->2567|4890->2569|4901->2570|4936->2583|4958->2586|4972->2591|5012->2592|5042->2593|5077->2596|5165->2653|5218->2675|5267->2696|5582->2984|5596->2989|5647->3019|5688->3032|5769->3083
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|58->27|59->28|65->34|65->34|65->34|66->35|66->35|66->35|67->36|68->37|68->37|68->37|69->38|70->39|70->39|70->39|70->39|70->39|70->39|70->39|70->39|70->39|70->39|70->39|71->40|71->40|71->40|71->40|71->40|71->40|71->40|71->40|71->40|71->40|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|72->41|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|73->42|75->44|76->45|77->46|85->54|85->54|85->54|86->55|92->61
                  -- GENERATED --
              */
          