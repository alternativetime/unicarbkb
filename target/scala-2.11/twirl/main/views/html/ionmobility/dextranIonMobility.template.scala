
package views.html.ionmobility

import play.twirl.api._


     object dextranIonMobility_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class dextranIonMobility extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[models.ionmob.DextranNegative],List[models.ionmob.DextranPositive],List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(negative: List[models.ionmob.DextranNegative], positive: List[models.ionmob.DextranPositive], proteinList: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.164*/("""


"""),_display_(/*4.2*/main/*4.6*/{_display_(Seq[Any](format.raw/*4.7*/("""

"""),format.raw/*6.1*/("""<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>Dextran CCS Values</h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <ul class="nav nav-tabs" id="product-table">
                    <li class="active"><a href="#1" data-toggle="tab">Negative Mode</a>
                    </li>
                    <li><a href="#2" data-toggle="tab">Positive Mode</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="1">
                        <h3>Negative Mode</h3>
                        <table class="table table-condensed table-bordered table-striped volumes">
                            <thead>
                            <tr>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Glucose Unit</th>
                                <th>Ion Type</th>
                                <th>CCS He &Aring; <sup>2</sup></th>
                                <th>CCS N &Aring; <sup>2</sup></th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*41.30*/for(negative <- negative) yield /*41.55*/{_display_(Seq[Any](format.raw/*41.56*/("""
                            """),format.raw/*42.29*/("""<tr>
                                <td>"""),_display_(/*43.38*/("%.2f".format(negative.mass))),format.raw/*43.68*/("""</td>
                                <td>"""),_display_(/*44.38*/negative/*44.46*/.charge),format.raw/*44.53*/("""</td>
                                <td>"""),_display_(/*45.38*/negative/*45.46*/.glucose),format.raw/*45.54*/("""</td>
                                <td>"""),_display_(/*46.38*/negative/*46.46*/.ion.toUpperCase),format.raw/*46.62*/("""</td>
                                <td>"""),_display_(/*47.38*/("%.2f".format(negative.cssHe))),format.raw/*47.69*/("""</td>
                                <td>"""),_display_(/*48.38*/("%.2f".format(negative.cssN))),format.raw/*48.68*/("""</td>
                            </tr>
                            """)))}),format.raw/*50.30*/("""
                            """),format.raw/*51.29*/("""</tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="2">
                            <h3>Positive Mode</h3>
                            <table class="table table-condensed table-bordered table-striped volumes">
                                <thead>
                                <tr>
                                    <th>Mass (m/z)</th>
                                    <th>Charge</th>
                                    <th>Glucose Unit</th>
                                    <th>Ion Type</th>
                                    <th>CCS He &Aring; <sup>2</sup></th>
                                    <th>CCS N &Aring; <sup>2</sup></th>
                                </tr>
                                </thead>
                                <tbody>
                                """),_display_(/*68.34*/for(positive <- positive) yield /*68.59*/{_display_(Seq[Any](format.raw/*68.60*/("""
                                """),format.raw/*69.33*/("""<tr>
                                    <td>"""),_display_(/*70.42*/("%.2f".format(positive.mass))),format.raw/*70.72*/("""</td>
                                    <td>"""),_display_(/*71.42*/positive/*71.50*/.charge),format.raw/*71.57*/("""</td>
                                    <td>"""),_display_(/*72.42*/positive/*72.50*/.glucose),format.raw/*72.58*/("""</td>
                                    <td>"""),_display_(/*73.42*/positive/*73.50*/.ion.toUpperCase.replace("NA", "Na")),format.raw/*73.86*/("""</td>
                                    <td>"""),_display_(/*74.42*/("%.2f".format(positive.cssHe))),format.raw/*74.73*/("""</td>
                                    <td>"""),_display_(/*75.42*/("%.2f".format(positive.cssN))),format.raw/*75.72*/("""</td>
                                </tr>
                                """)))}),format.raw/*77.34*/("""
                                """),format.raw/*78.33*/("""</tbody>
                            </table>
                    </div>
                </div>
                </div>

                <div class="span4">
                    """),_display_(/*85.22*/views/*85.27*/.html.format.ionmob(proteinList)),format.raw/*85.59*/("""
                """),format.raw/*86.17*/("""</div>
            </div>
    </section>
</section>

""")))}))
      }
    }
  }

  def render(negative:List[models.ionmob.DextranNegative],positive:List[models.ionmob.DextranPositive],proteinList:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(negative,positive,proteinList)

  def f:((List[models.ionmob.DextranNegative],List[models.ionmob.DextranPositive],List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (negative,positive,proteinList) => apply(negative,positive,proteinList)

  def ref: this.type = this

}


}

/**/
object dextranIonMobility extends dextranIonMobility_Scope0.dextranIonMobility
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/dextranIonMobility.scala.html
                  HASH: c0a4e0f68fa6748f16ebb4823862a132842b409c
                  MATRIX: 902->1|1160->163|1189->167|1200->171|1237->172|1265->174|2819->1701|2860->1726|2899->1727|2956->1756|3025->1798|3076->1828|3146->1871|3163->1879|3191->1886|3261->1929|3278->1937|3307->1945|3377->1988|3394->1996|3431->2012|3501->2055|3553->2086|3623->2129|3674->2159|3774->2228|3831->2257|4725->3124|4766->3149|4805->3150|4866->3183|4939->3229|4990->3259|5064->3306|5081->3314|5109->3321|5183->3368|5200->3376|5229->3384|5303->3431|5320->3439|5377->3475|5451->3522|5503->3553|5577->3600|5628->3630|5736->3707|5797->3740|6001->3917|6015->3922|6068->3954|6113->3971
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|72->41|72->41|72->41|73->42|74->43|74->43|75->44|75->44|75->44|76->45|76->45|76->45|77->46|77->46|77->46|78->47|78->47|79->48|79->48|81->50|82->51|99->68|99->68|99->68|100->69|101->70|101->70|102->71|102->71|102->71|103->72|103->72|103->72|104->73|104->73|104->73|105->74|105->74|106->75|106->75|108->77|109->78|116->85|116->85|116->85|117->86
                  -- GENERATED --
              */
          