
package views.html.ionmobility

import play.twirl.api._


     object ionmobilityComposition_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class ionmobilityComposition extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template10[Integer,Integer,List[models.glycomobcomposition.Glycomobcomposition],List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.GlycoproteinStandard],String,List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.Glycomobcomposition],List[models.glycomobcomposition.Glycomobcomposition],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(numberCss: Integer, numberNaCss: Integer, comp: List[models.glycomobcomposition.Glycomobcomposition], sodiatedcomp: List[models.glycomobcomposition.SodiatedGlycomobComposition], proteinList: List[models.glycomobcomposition.GlycoproteinStandard], kbComp: String,
sodiatedcompN: List[models.glycomobcomposition.SodiatedGlycomobComposition], sodiatedcompHe: List[models.glycomobcomposition.SodiatedGlycomobComposition],
compN: List[models.glycomobcomposition.Glycomobcomposition], compHe: List[models.glycomobcomposition.Glycomobcomposition]
):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.2*/("""


"""),_display_(/*7.2*/main/*7.6*/{_display_(Seq[Any](format.raw/*7.7*/("""

"""),format.raw/*9.1*/("""<script src="http://wenzhixin.net.cn/p/bootstrap-table/src/extensions/filter/bootstrap-table-filter.js"></script>

<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>CCS Values - Composition Search</h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filter by CCS" />
                OR <a href=""""),_display_(/*26.30*/kbComp),format.raw/*26.36*/(""""><span class="label label-light">Query UniCarbKB Compositions</span></a>

                <ul class="nav nav-tabs" id="product-table">
                    """),format.raw/*32.29*/("""
                    """),format.raw/*33.21*/("""<li class="active"><a href="#3" data-toggle="tab">Sodiated Nitrogen Positive Mode</a>
                    </li>
                    <li><a href="#4" data-toggle="tab">Sodiated Helium Positive Mode</a>
                    </li>
                    <li><a href="#5" data-toggle="tab">Nitrogen Negative Mode</a>
                    </li>
                    <li><a href="#6" data-toggle="tab">Helium Negative Mode</a>
                    </li>
                </ul>
                <div class="tab-content">
                    """),format.raw/*117.29*/("""

                    """),format.raw/*119.21*/("""<div class="tab-pane active" id="3">
                        <h3>Sodiated Structures Nitrogen</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>
                                <th>Glycoprotein</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*138.30*/for(N <-  sodiatedcompN) yield /*138.54*/{_display_(Seq[Any](format.raw/*138.55*/("""
                            """),format.raw/*139.29*/("""<tr>
                                """),_display_(/*140.34*/for(s <- N.cssDatas) yield /*140.54*/ {_display_(Seq[Any](format.raw/*140.56*/("""
                                """),format.raw/*141.33*/("""<td>"""),_display_(/*141.38*/s/*141.39*/.css),format.raw/*141.43*/("""</td>
                                <td>"""),_display_(/*142.38*/("%.2f".format(s.sodiatedGlycomobCompositionList.massCharge))),format.raw/*142.99*/("""</td>
                                <td>"""),_display_(/*143.38*/s/*143.39*/.sodiatedGlycomobCompositionList.charge),format.raw/*143.78*/("""</td>
                                <td>"""),_display_(/*144.38*/s/*144.39*/.sodiatedGlycomobCompositionList.hex.replace("0", "")),format.raw/*144.92*/("""</td>
                                <td>"""),_display_(/*145.38*/s/*145.39*/.sodiatedGlycomobCompositionList.hexnac.replace("0", "")),format.raw/*145.95*/("""</td>
                                <td>"""),_display_(/*146.38*/s/*146.39*/.sodiatedGlycomobCompositionList.dhex.replace("0", "")),format.raw/*146.93*/("""</td>
                                <td>"""),_display_(/*147.38*/s/*147.39*/.sodiatedGlycomobCompositionList.neunac.replace("0", "")),format.raw/*147.95*/("""</td>
                                <td>"""),_display_(/*148.38*/s/*148.39*/.sodiatedGlycomobCompositionList.mi),format.raw/*148.74*/("""</td>
                                <td>"""),_display_(/*149.38*/s/*149.39*/.nativeStructure),format.raw/*149.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*150.70*/s/*150.71*/.glycoproteinStandardList.id),format.raw/*150.99*/("""">"""),_display_(/*150.102*/s/*150.103*/.glycoproteinStandardList.name),format.raw/*150.133*/("""</a></td>
                            </tr>
                            """)))})))}),format.raw/*152.31*/("""
                            """),format.raw/*153.29*/("""</tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="4">
                        <h3>Sodiated Structures Helium</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>
                                <th>Glycoprotein</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*176.30*/for(He <-  sodiatedcompHe) yield /*176.56*/{_display_(Seq[Any](format.raw/*176.57*/("""
                            """),format.raw/*177.29*/("""<tr>
                                """),_display_(/*178.34*/for(s <- He.cssDatas) yield /*178.55*/ {_display_(Seq[Any](format.raw/*178.57*/("""
                                """),format.raw/*179.33*/("""<td>"""),_display_(/*179.38*/s/*179.39*/.css),format.raw/*179.43*/("""</td>
                                <td>"""),_display_(/*180.38*/("%.2f".format(s.sodiatedGlycomobCompositionList.massCharge))),format.raw/*180.99*/("""</td>
                                <td>"""),_display_(/*181.38*/s/*181.39*/.sodiatedGlycomobCompositionList.charge),format.raw/*181.78*/("""</td>
                                <td>"""),_display_(/*182.38*/s/*182.39*/.sodiatedGlycomobCompositionList.hex.replace("0", "")),format.raw/*182.92*/("""</td>
                                <td>"""),_display_(/*183.38*/s/*183.39*/.sodiatedGlycomobCompositionList.hexnac.replace("0", "")),format.raw/*183.95*/("""</td>
                                <td>"""),_display_(/*184.38*/s/*184.39*/.sodiatedGlycomobCompositionList.dhex.replace("0", "")),format.raw/*184.93*/("""</td>
                                <td>"""),_display_(/*185.38*/s/*185.39*/.sodiatedGlycomobCompositionList.neunac.replace("0", "")),format.raw/*185.95*/("""</td>
                                <td>"""),_display_(/*186.38*/s/*186.39*/.sodiatedGlycomobCompositionList.mi),format.raw/*186.74*/("""</td>
                                <td>"""),_display_(/*187.38*/s/*187.39*/.nativeStructure),format.raw/*187.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*188.70*/s/*188.71*/.glycoproteinStandardList.id),format.raw/*188.99*/("""">"""),_display_(/*188.102*/s/*188.103*/.glycoproteinStandardList.name),format.raw/*188.133*/("""</a></td>
                            </tr>
                            """)))})))}),format.raw/*190.31*/("""
                            """),format.raw/*191.29*/("""</tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="5">
                        <h3>Structures Nitrogen</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>
                                <th>Glycoprotein</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*214.30*/for(N <-  compN) yield /*214.46*/{_display_(Seq[Any](format.raw/*214.47*/("""
                            """),format.raw/*215.29*/("""<tr>
                                """),_display_(/*216.34*/for(s <- N.cssDataGenerals) yield /*216.61*/ {_display_(Seq[Any](format.raw/*216.63*/("""
                                """),format.raw/*217.33*/("""<td>"""),_display_(/*217.38*/s/*217.39*/.css),format.raw/*217.43*/("""</td>
                                <td>"""),_display_(/*218.38*/("%.2f".format(s.glycomobcompositionList.mass))),format.raw/*218.85*/("""</td>
                                <td>"""),_display_(/*219.38*/s/*219.39*/.glycomobcompositionList.charge),format.raw/*219.70*/("""</td>
                                <td>"""),_display_(/*220.38*/s/*220.39*/.glycomobcompositionList.hex.replace("0", "")),format.raw/*220.84*/("""</td>
                                <td>"""),_display_(/*221.38*/s/*221.39*/.glycomobcompositionList.hexnac.replace("0", "")),format.raw/*221.87*/("""</td>
                                <td>"""),_display_(/*222.38*/s/*222.39*/.glycomobcompositionList.dhex.replace("0", "")),format.raw/*222.85*/("""</td>
                                <td>"""),_display_(/*223.38*/s/*223.39*/.glycomobcompositionList.neunac.replace("0", "")),format.raw/*223.87*/("""</td>
                                <td>"""),_display_(/*224.38*/s/*224.39*/.glycomobcompositionList.mi),format.raw/*224.66*/("""</td>
                                <td>"""),_display_(/*225.38*/s/*225.39*/.nativeStructure),format.raw/*225.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*226.70*/s/*226.71*/.glycoproteinStandardList.id),format.raw/*226.99*/("""">"""),_display_(/*226.102*/s/*226.103*/.glycoproteinStandardList.name),format.raw/*226.133*/("""</a></td>
                            </tr>
                            """)))})))}),format.raw/*228.31*/("""
                            """),format.raw/*229.29*/("""</tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="6">
                        <h3>Structures Helium</h3>
                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>
                                <th>Glycoprotein</th>
                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*252.30*/for(He <-  compHe) yield /*252.48*/{_display_(Seq[Any](format.raw/*252.49*/("""
                            """),format.raw/*253.29*/("""<tr>
                                """),_display_(/*254.34*/for(s <- He.cssDataGenerals) yield /*254.62*/ {_display_(Seq[Any](format.raw/*254.64*/("""
                                """),format.raw/*255.33*/("""<td>"""),_display_(/*255.38*/s/*255.39*/.css),format.raw/*255.43*/("""</td>
                                <td>"""),_display_(/*256.38*/("%.2f".format(s.glycomobcompositionList.mass))),format.raw/*256.85*/("""</td>
                                <td>"""),_display_(/*257.38*/s/*257.39*/.glycomobcompositionList.charge),format.raw/*257.70*/("""</td>
                                <td>"""),_display_(/*258.38*/s/*258.39*/.glycomobcompositionList.hex.replace("0", "")),format.raw/*258.84*/("""</td>
                                <td>"""),_display_(/*259.38*/s/*259.39*/.glycomobcompositionList.hexnac.replace("0", "")),format.raw/*259.87*/("""</td>
                                <td>"""),_display_(/*260.38*/s/*260.39*/.glycomobcompositionList.dhex.replace("0", "")),format.raw/*260.85*/("""</td>
                                <td>"""),_display_(/*261.38*/s/*261.39*/.glycomobcompositionList.neunac.replace("0", "")),format.raw/*261.87*/("""</td>
                                <td>"""),_display_(/*262.38*/s/*262.39*/.glycomobcompositionList.mi),format.raw/*262.66*/("""</td>
                                <td>"""),_display_(/*263.38*/s/*263.39*/.nativeStructure),format.raw/*263.55*/("""</td>
                                <td><a href="/ionmobCompleteProtein/"""),_display_(/*264.70*/s/*264.71*/.glycoproteinStandardList.id),format.raw/*264.99*/("""">"""),_display_(/*264.102*/s/*264.103*/.glycoproteinStandardList.name),format.raw/*264.133*/("""</a></td>
                            </tr>
                            """)))})))}),format.raw/*266.31*/("""
                            """),format.raw/*267.29*/("""</tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="span4">
                """),_display_(/*275.18*/views/*275.23*/.html.format.ionmob(proteinList)),format.raw/*275.55*/("""
            """),format.raw/*276.13*/("""</div>
        </div>
    </section>
</section>

<script>
    (function(document) """),format.raw/*282.25*/("""{"""),format.raw/*282.26*/("""
	"""),format.raw/*283.2*/("""'use strict';

	var LightTableFilter = (function(Arr) """),format.raw/*285.40*/("""{"""),format.raw/*285.41*/("""

		"""),format.raw/*287.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*289.29*/("""{"""),format.raw/*289.30*/("""
			"""),format.raw/*290.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) """),format.raw/*292.45*/("""{"""),format.raw/*292.46*/("""
				"""),format.raw/*293.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*293.53*/("""{"""),format.raw/*293.54*/("""
					"""),format.raw/*294.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*295.5*/("""}"""),format.raw/*295.6*/(""");
			"""),format.raw/*296.4*/("""}"""),format.raw/*296.5*/(""");
		"""),format.raw/*297.3*/("""}"""),format.raw/*297.4*/("""

		"""),format.raw/*299.3*/("""function _filter(row) """),format.raw/*299.25*/("""{"""),format.raw/*299.26*/("""
			"""),format.raw/*300.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*302.3*/("""}"""),format.raw/*302.4*/("""

		"""),format.raw/*304.3*/("""return """),format.raw/*304.10*/("""{"""),format.raw/*304.11*/("""
			"""),format.raw/*305.4*/("""init: function() """),format.raw/*305.21*/("""{"""),format.raw/*305.22*/("""
				"""),format.raw/*306.5*/("""var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) """),format.raw/*307.46*/("""{"""),format.raw/*307.47*/("""
					"""),format.raw/*308.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*309.5*/("""}"""),format.raw/*309.6*/(""");
			"""),format.raw/*310.4*/("""}"""),format.raw/*310.5*/("""
		"""),format.raw/*311.3*/("""}"""),format.raw/*311.4*/(""";
	"""),format.raw/*312.2*/("""}"""),format.raw/*312.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*314.59*/("""{"""),format.raw/*314.60*/("""
		"""),format.raw/*315.3*/("""if (document.readyState === 'complete') """),format.raw/*315.43*/("""{"""),format.raw/*315.44*/("""
			"""),format.raw/*316.4*/("""LightTableFilter.init();
		"""),format.raw/*317.3*/("""}"""),format.raw/*317.4*/("""
	"""),format.raw/*318.2*/("""}"""),format.raw/*318.3*/(""");

"""),format.raw/*320.1*/("""}"""),format.raw/*320.2*/(""")(document);

</script>

""")))}))
      }
    }
  }

  def render(numberCss:Integer,numberNaCss:Integer,comp:List[models.glycomobcomposition.Glycomobcomposition],sodiatedcomp:List[models.glycomobcomposition.SodiatedGlycomobComposition],proteinList:List[models.glycomobcomposition.GlycoproteinStandard],kbComp:String,sodiatedcompN:List[models.glycomobcomposition.SodiatedGlycomobComposition],sodiatedcompHe:List[models.glycomobcomposition.SodiatedGlycomobComposition],compN:List[models.glycomobcomposition.Glycomobcomposition],compHe:List[models.glycomobcomposition.Glycomobcomposition]): play.twirl.api.HtmlFormat.Appendable = apply(numberCss,numberNaCss,comp,sodiatedcomp,proteinList,kbComp,sodiatedcompN,sodiatedcompHe,compN,compHe)

  def f:((Integer,Integer,List[models.glycomobcomposition.Glycomobcomposition],List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.GlycoproteinStandard],String,List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.SodiatedGlycomobComposition],List[models.glycomobcomposition.Glycomobcomposition],List[models.glycomobcomposition.Glycomobcomposition]) => play.twirl.api.HtmlFormat.Appendable) = (numberCss,numberNaCss,comp,sodiatedcomp,proteinList,kbComp,sodiatedcompN,sodiatedcompHe,compN,compHe) => apply(numberCss,numberNaCss,comp,sodiatedcomp,proteinList,kbComp,sodiatedcompN,sodiatedcompHe,compN,compHe)

  def ref: this.type = this

}


}

/**/
object ionmobilityComposition extends ionmobilityComposition_Scope0.ionmobilityComposition
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/ionmobilityComposition.scala.html
                  HASH: d2fab9694c24ea00250f9ebb2dec2b38e3feb0b4
                  MATRIX: 1204->1|1839->542|1868->546|1879->550|1916->551|1944->553|2688->1270|2715->1276|2899->1670|2948->1691|3502->6293|3553->6315|4564->7298|4605->7322|4645->7323|4703->7352|4769->7390|4806->7410|4847->7412|4909->7445|4942->7450|4953->7451|4979->7455|5050->7498|5133->7559|5204->7602|5215->7603|5276->7642|5347->7685|5358->7686|5433->7739|5504->7782|5515->7783|5593->7839|5664->7882|5675->7883|5751->7937|5822->7980|5833->7981|5911->8037|5982->8080|5993->8081|6050->8116|6121->8159|6132->8160|6170->8176|6273->8251|6284->8252|6334->8280|6366->8283|6378->8284|6431->8314|6540->8388|6598->8417|7690->9481|7733->9507|7773->9508|7831->9537|7897->9575|7935->9596|7976->9598|8038->9631|8071->9636|8082->9637|8108->9641|8179->9684|8262->9745|8333->9788|8344->9789|8405->9828|8476->9871|8487->9872|8562->9925|8633->9968|8644->9969|8722->10025|8793->10068|8804->10069|8880->10123|8951->10166|8962->10167|9040->10223|9111->10266|9122->10267|9179->10302|9250->10345|9261->10346|9299->10362|9402->10437|9413->10438|9463->10466|9495->10469|9507->10470|9560->10500|9669->10574|9727->10603|10812->11660|10845->11676|10885->11677|10943->11706|11009->11744|11053->11771|11094->11773|11156->11806|11189->11811|11200->11812|11226->11816|11297->11859|11366->11906|11437->11949|11448->11950|11501->11981|11572->12024|11583->12025|11650->12070|11721->12113|11732->12114|11802->12162|11873->12205|11884->12206|11952->12252|12023->12295|12034->12296|12104->12344|12175->12387|12186->12388|12235->12415|12306->12458|12317->12459|12355->12475|12458->12550|12469->12551|12519->12579|12551->12582|12563->12583|12616->12613|12725->12687|12783->12716|13866->13771|13901->13789|13941->13790|13999->13819|14065->13857|14110->13885|14151->13887|14213->13920|14246->13925|14257->13926|14283->13930|14354->13973|14423->14020|14494->14063|14505->14064|14558->14095|14629->14138|14640->14139|14707->14184|14778->14227|14789->14228|14859->14276|14930->14319|14941->14320|15009->14366|15080->14409|15091->14410|15161->14458|15232->14501|15243->14502|15292->14529|15363->14572|15374->14573|15412->14589|15515->14664|15526->14665|15576->14693|15608->14696|15620->14697|15673->14727|15782->14801|15840->14830|16030->14992|16045->14997|16099->15029|16141->15042|16252->15124|16282->15125|16312->15127|16395->15181|16425->15182|16457->15186|16527->15227|16557->15228|16589->15232|16765->15379|16795->15380|16828->15385|16905->15433|16935->15434|16969->15440|17040->15483|17069->15484|17103->15490|17132->15491|17165->15496|17194->15497|17226->15501|17277->15523|17307->15524|17339->15528|17517->15678|17546->15679|17578->15683|17614->15690|17644->15691|17676->15695|17722->15712|17752->15713|17785->15718|17927->15831|17957->15832|17991->15838|18054->15873|18083->15874|18117->15880|18146->15881|18177->15884|18206->15885|18237->15888|18266->15889|18374->15968|18404->15969|18435->15972|18504->16012|18534->16013|18566->16017|18621->16044|18650->16045|18680->16047|18709->16048|18741->16052|18770->16053
                  LINES: 27->1|35->4|38->7|38->7|38->7|40->9|57->26|57->26|60->32|61->33|71->117|73->119|92->138|92->138|92->138|93->139|94->140|94->140|94->140|95->141|95->141|95->141|95->141|96->142|96->142|97->143|97->143|97->143|98->144|98->144|98->144|99->145|99->145|99->145|100->146|100->146|100->146|101->147|101->147|101->147|102->148|102->148|102->148|103->149|103->149|103->149|104->150|104->150|104->150|104->150|104->150|104->150|106->152|107->153|130->176|130->176|130->176|131->177|132->178|132->178|132->178|133->179|133->179|133->179|133->179|134->180|134->180|135->181|135->181|135->181|136->182|136->182|136->182|137->183|137->183|137->183|138->184|138->184|138->184|139->185|139->185|139->185|140->186|140->186|140->186|141->187|141->187|141->187|142->188|142->188|142->188|142->188|142->188|142->188|144->190|145->191|168->214|168->214|168->214|169->215|170->216|170->216|170->216|171->217|171->217|171->217|171->217|172->218|172->218|173->219|173->219|173->219|174->220|174->220|174->220|175->221|175->221|175->221|176->222|176->222|176->222|177->223|177->223|177->223|178->224|178->224|178->224|179->225|179->225|179->225|180->226|180->226|180->226|180->226|180->226|180->226|182->228|183->229|206->252|206->252|206->252|207->253|208->254|208->254|208->254|209->255|209->255|209->255|209->255|210->256|210->256|211->257|211->257|211->257|212->258|212->258|212->258|213->259|213->259|213->259|214->260|214->260|214->260|215->261|215->261|215->261|216->262|216->262|216->262|217->263|217->263|217->263|218->264|218->264|218->264|218->264|218->264|218->264|220->266|221->267|229->275|229->275|229->275|230->276|236->282|236->282|237->283|239->285|239->285|241->287|243->289|243->289|244->290|246->292|246->292|247->293|247->293|247->293|248->294|249->295|249->295|250->296|250->296|251->297|251->297|253->299|253->299|253->299|254->300|256->302|256->302|258->304|258->304|258->304|259->305|259->305|259->305|260->306|261->307|261->307|262->308|263->309|263->309|264->310|264->310|265->311|265->311|266->312|266->312|268->314|268->314|269->315|269->315|269->315|270->316|271->317|271->317|272->318|272->318|274->320|274->320
                  -- GENERATED --
              */
          