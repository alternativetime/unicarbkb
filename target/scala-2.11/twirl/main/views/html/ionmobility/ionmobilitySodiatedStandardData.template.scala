
package views.html.ionmobility

import play.twirl.api._


     object ionmobilitySodiatedStandardData_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class ionmobilitySodiatedStandardData extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData],models.glycomobcomposition.GlycoproteinStandard,List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.GlycoproteinStandard],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(summaryPositiveHe: List[models.glycomobcomposition.CssData],  summaryNegativeHe: List[models.glycomobcomposition.CssData], protein: models.glycomobcomposition.GlycoproteinStandard, summaryPositiveN: List[models.glycomobcomposition.CssData],  summaryNegativeN: List[models.glycomobcomposition.CssData], proteinList: List[models.glycomobcomposition.GlycoproteinStandard]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.372*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

"""),format.raw/*5.1*/("""<script src="http://wenzhixin.net.cn/p/bootstrap-table/src/extensions/filter/bootstrap-table-filter.js"></script>

<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GlycoMob<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>"""),_display_(/*16.18*/protein/*16.25*/.name),format.raw/*16.30*/(""" """),format.raw/*16.31*/("""CCS Values</h1>
        </div>

        <div class="row-fluid">
            <div class="span8">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filter by CCS" />
                <ul class="nav nav-tabs" id="product-table">

                    <li class="active"><a href="#1" data-toggle="tab">Positive Mode - Helium ("""),_display_(/*24.96*/summaryPositiveHe/*24.113*/.size),format.raw/*24.118*/(""")</a>
                    </li>

                    <li><a href="#2" data-toggle="tab">Positive Mode - Nitrogen ("""),_display_(/*27.83*/summaryPositiveN/*27.99*/.size),format.raw/*27.104*/(""")</a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="1">
                        <h3>Positive Mode (Helium)</h3>

                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CCS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>

                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*52.30*/for(positive <- summaryPositiveHe) yield /*52.64*/{_display_(Seq[Any](format.raw/*52.65*/("""
                            """),format.raw/*53.29*/("""<tr>
                                <td>"""),_display_(/*54.38*/positive/*54.46*/.css),format.raw/*54.50*/("""</td>
                                <td>"""),_display_(/*55.38*/positive/*55.46*/.sodiatedGlycomobCompositionList.massCharge),format.raw/*55.89*/("""</td>
                                <td>"""),_display_(/*56.38*/positive/*56.46*/.sodiatedGlycomobCompositionList.charge),format.raw/*56.85*/("""</td>
                                <td>"""),_display_(/*57.38*/positive/*57.46*/.sodiatedGlycomobCompositionList.hex),format.raw/*57.82*/("""</td>
                                <td>"""),_display_(/*58.38*/positive/*58.46*/.sodiatedGlycomobCompositionList.hexnac),format.raw/*58.85*/("""</td>
                                <td>"""),_display_(/*59.38*/positive/*59.46*/.sodiatedGlycomobCompositionList.dhex),format.raw/*59.83*/("""</td>
                                <td>"""),_display_(/*60.38*/positive/*60.46*/.sodiatedGlycomobCompositionList.neunac),format.raw/*60.85*/("""</td>
                                <td>"""),_display_(/*61.38*/positive/*61.46*/.sodiatedGlycomobCompositionList.mi),format.raw/*61.81*/("""</td>
                                <td>"""),_display_(/*62.38*/positive/*62.46*/.nativeStructure),format.raw/*62.62*/("""</td>
                            </tr>
                            """)))}),format.raw/*64.30*/("""
                            """),format.raw/*65.29*/("""</tbody>
                        </table>
                    </div>


                    <div class="tab-pane" id="2">
                        <h3>Positive Mode (Nitrogen)</h3>

                        <table class="table table-condensed table-bordered table-striped volumes order-table">
                            <col style="width:20%" span="5" />
                            <thead>
                            <tr>
                                <th>CSS</th>
                                <th>Mass (m/z)</th>
                                <th>Charge</th>
                                <th>Hex</th>
                                <th>HexNAc</th>
                                <th>dHex</th>
                                <th>NeuNAc</th>
                                <th>Multiple Isomers</th>
                                <th>Precursor Structure</th>

                            </tr>
                            </thead>
                            <tbody>
                            """),_display_(/*90.30*/for(positive <- summaryPositiveN) yield /*90.63*/{_display_(Seq[Any](format.raw/*90.64*/("""
                            """),format.raw/*91.29*/("""<tr>
                                <td>"""),_display_(/*92.38*/positive/*92.46*/.css),format.raw/*92.50*/("""</td>
                                <td>"""),_display_(/*93.38*/positive/*93.46*/.sodiatedGlycomobCompositionList.massCharge),format.raw/*93.89*/("""</td>
                                <td>"""),_display_(/*94.38*/positive/*94.46*/.sodiatedGlycomobCompositionList.charge),format.raw/*94.85*/("""</td>
                                <td>"""),_display_(/*95.38*/positive/*95.46*/.sodiatedGlycomobCompositionList.hex),format.raw/*95.82*/("""</td>
                                <td>"""),_display_(/*96.38*/positive/*96.46*/.sodiatedGlycomobCompositionList.hexnac),format.raw/*96.85*/("""</td>
                                <td>"""),_display_(/*97.38*/positive/*97.46*/.sodiatedGlycomobCompositionList.dhex),format.raw/*97.83*/("""</td>
                                <td>"""),_display_(/*98.38*/positive/*98.46*/.sodiatedGlycomobCompositionList.neunac),format.raw/*98.85*/("""</td>
                                <td>"""),_display_(/*99.38*/positive/*99.46*/.sodiatedGlycomobCompositionList.mi),format.raw/*99.81*/("""</td>
                                <td>"""),_display_(/*100.38*/positive/*100.46*/.nativeStructure),format.raw/*100.62*/("""</td>
                            </tr>
                            """)))}),format.raw/*102.30*/("""
                            """),format.raw/*103.29*/("""</tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="span4">
                """),_display_(/*111.18*/views/*111.23*/.html.format.ionmob(proteinList)),format.raw/*111.55*/("""
            """),format.raw/*112.13*/("""</div>
        </div>
    </section>
</section>

<script>
    (function(document) """),format.raw/*118.25*/("""{"""),format.raw/*118.26*/("""
	"""),format.raw/*119.2*/("""'use strict';

	var LightTableFilter = (function(Arr) """),format.raw/*121.40*/("""{"""),format.raw/*121.41*/("""

		"""),format.raw/*123.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*125.29*/("""{"""),format.raw/*125.30*/("""
			"""),format.raw/*126.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) """),format.raw/*128.45*/("""{"""),format.raw/*128.46*/("""
				"""),format.raw/*129.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*129.53*/("""{"""),format.raw/*129.54*/("""
					"""),format.raw/*130.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*131.5*/("""}"""),format.raw/*131.6*/(""");
			"""),format.raw/*132.4*/("""}"""),format.raw/*132.5*/(""");
		"""),format.raw/*133.3*/("""}"""),format.raw/*133.4*/("""

		"""),format.raw/*135.3*/("""function _filter(row) """),format.raw/*135.25*/("""{"""),format.raw/*135.26*/("""
			"""),format.raw/*136.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*138.3*/("""}"""),format.raw/*138.4*/("""

		"""),format.raw/*140.3*/("""return """),format.raw/*140.10*/("""{"""),format.raw/*140.11*/("""
			"""),format.raw/*141.4*/("""init: function() """),format.raw/*141.21*/("""{"""),format.raw/*141.22*/("""
				"""),format.raw/*142.5*/("""var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) """),format.raw/*143.46*/("""{"""),format.raw/*143.47*/("""
					"""),format.raw/*144.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*145.5*/("""}"""),format.raw/*145.6*/(""");
			"""),format.raw/*146.4*/("""}"""),format.raw/*146.5*/("""
		"""),format.raw/*147.3*/("""}"""),format.raw/*147.4*/(""";
	"""),format.raw/*148.2*/("""}"""),format.raw/*148.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*150.59*/("""{"""),format.raw/*150.60*/("""
		"""),format.raw/*151.3*/("""if (document.readyState === 'complete') """),format.raw/*151.43*/("""{"""),format.raw/*151.44*/("""
			"""),format.raw/*152.4*/("""LightTableFilter.init();
		"""),format.raw/*153.3*/("""}"""),format.raw/*153.4*/("""
	"""),format.raw/*154.2*/("""}"""),format.raw/*154.3*/(""");

"""),format.raw/*156.1*/("""}"""),format.raw/*156.2*/(""")(document);

</script>

<script>
    (function(document) """),format.raw/*161.25*/("""{"""),format.raw/*161.26*/("""
	"""),format.raw/*162.2*/("""'use strict';

	var LightTableFilter2 = (function(Arr) """),format.raw/*164.41*/("""{"""),format.raw/*164.42*/("""

		"""),format.raw/*166.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*168.29*/("""{"""),format.raw/*168.30*/("""
			"""),format.raw/*169.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table2'));
			Arr.forEach.call(tables, function(table) """),format.raw/*171.45*/("""{"""),format.raw/*171.46*/("""
				"""),format.raw/*172.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*172.53*/("""{"""),format.raw/*172.54*/("""
					"""),format.raw/*173.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*174.5*/("""}"""),format.raw/*174.6*/(""");
			"""),format.raw/*175.4*/("""}"""),format.raw/*175.5*/(""");
		"""),format.raw/*176.3*/("""}"""),format.raw/*176.4*/("""

		"""),format.raw/*178.3*/("""function _filter(row) """),format.raw/*178.25*/("""{"""),format.raw/*178.26*/("""
			"""),format.raw/*179.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*181.3*/("""}"""),format.raw/*181.4*/("""

		"""),format.raw/*183.3*/("""return """),format.raw/*183.10*/("""{"""),format.raw/*183.11*/("""
			"""),format.raw/*184.4*/("""init: function() """),format.raw/*184.21*/("""{"""),format.raw/*184.22*/("""
				"""),format.raw/*185.5*/("""var inputs = document.getElementsByClassName('light-table-filter2');
				Arr.forEach.call(inputs, function(input) """),format.raw/*186.46*/("""{"""),format.raw/*186.47*/("""
					"""),format.raw/*187.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*188.5*/("""}"""),format.raw/*188.6*/(""");
			"""),format.raw/*189.4*/("""}"""),format.raw/*189.5*/("""
		"""),format.raw/*190.3*/("""}"""),format.raw/*190.4*/(""";
	"""),format.raw/*191.2*/("""}"""),format.raw/*191.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*193.59*/("""{"""),format.raw/*193.60*/("""
		"""),format.raw/*194.3*/("""if (document.readyState === 'complete') """),format.raw/*194.43*/("""{"""),format.raw/*194.44*/("""
			"""),format.raw/*195.4*/("""LightTableFilter2.init();
		"""),format.raw/*196.3*/("""}"""),format.raw/*196.4*/("""
	"""),format.raw/*197.2*/("""}"""),format.raw/*197.3*/(""");

"""),format.raw/*199.1*/("""}"""),format.raw/*199.2*/(""")(document);

</script>

 """)))}))
      }
    }
  }

  def render(summaryPositiveHe:List[models.glycomobcomposition.CssData],summaryNegativeHe:List[models.glycomobcomposition.CssData],protein:models.glycomobcomposition.GlycoproteinStandard,summaryPositiveN:List[models.glycomobcomposition.CssData],summaryNegativeN:List[models.glycomobcomposition.CssData],proteinList:List[models.glycomobcomposition.GlycoproteinStandard]): play.twirl.api.HtmlFormat.Appendable = apply(summaryPositiveHe,summaryNegativeHe,protein,summaryPositiveN,summaryNegativeN,proteinList)

  def f:((List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData],models.glycomobcomposition.GlycoproteinStandard,List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.CssData],List[models.glycomobcomposition.GlycoproteinStandard]) => play.twirl.api.HtmlFormat.Appendable) = (summaryPositiveHe,summaryNegativeHe,protein,summaryPositiveN,summaryNegativeN,proteinList) => apply(summaryPositiveHe,summaryNegativeHe,protein,summaryPositiveN,summaryNegativeN,proteinList)

  def ref: this.type = this

}


}

/**/
object ionmobilitySodiatedStandardData extends ionmobilitySodiatedStandardData_Scope0.ionmobilitySodiatedStandardData
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ionmobility/ionmobilitySodiatedStandardData.scala.html
                  HASH: 27e0f1ac73d424b2693a6d6535a45a94107dfdd2
                  MATRIX: 1068->1|1534->371|1562->374|1573->378|1611->380|1639->382|2118->834|2134->841|2160->846|2189->847|2589->1220|2616->1237|2643->1242|2785->1357|2810->1373|2837->1378|3906->2420|3956->2454|3995->2455|4052->2484|4121->2526|4138->2534|4163->2538|4233->2581|4250->2589|4314->2632|4384->2675|4401->2683|4461->2722|4531->2765|4548->2773|4605->2809|4675->2852|4692->2860|4752->2899|4822->2942|4839->2950|4897->2987|4967->3030|4984->3038|5044->3077|5114->3120|5131->3128|5187->3163|5257->3206|5274->3214|5311->3230|5411->3299|5468->3328|6506->4339|6555->4372|6594->4373|6651->4402|6720->4444|6737->4452|6762->4456|6832->4499|6849->4507|6913->4550|6983->4593|7000->4601|7060->4640|7130->4683|7147->4691|7204->4727|7274->4770|7291->4778|7351->4817|7421->4860|7438->4868|7496->4905|7566->4948|7583->4956|7643->4995|7713->5038|7730->5046|7786->5081|7857->5124|7875->5132|7913->5148|8014->5217|8072->5246|8262->5408|8277->5413|8331->5445|8373->5458|8484->5540|8514->5541|8544->5543|8627->5597|8657->5598|8689->5602|8759->5643|8789->5644|8821->5648|8997->5795|9027->5796|9060->5801|9137->5849|9167->5850|9201->5856|9272->5899|9301->5900|9335->5906|9364->5907|9397->5912|9426->5913|9458->5917|9509->5939|9539->5940|9571->5944|9749->6094|9778->6095|9810->6099|9846->6106|9876->6107|9908->6111|9954->6128|9984->6129|10017->6134|10159->6247|10189->6248|10223->6254|10286->6289|10315->6290|10349->6296|10378->6297|10409->6300|10438->6301|10469->6304|10498->6305|10606->6384|10636->6385|10667->6388|10736->6428|10766->6429|10798->6433|10853->6460|10882->6461|10912->6463|10941->6464|10973->6468|11002->6469|11089->6527|11119->6528|11149->6530|11233->6585|11263->6586|11295->6590|11365->6631|11395->6632|11427->6636|11604->6784|11634->6785|11667->6790|11744->6838|11774->6839|11808->6845|11879->6888|11908->6889|11942->6895|11971->6896|12004->6901|12033->6902|12065->6906|12116->6928|12146->6929|12178->6933|12356->7083|12385->7084|12417->7088|12453->7095|12483->7096|12515->7100|12561->7117|12591->7118|12624->7123|12767->7237|12797->7238|12831->7244|12894->7279|12923->7280|12957->7286|12986->7287|13017->7290|13046->7291|13077->7294|13106->7295|13214->7374|13244->7375|13275->7378|13344->7418|13374->7419|13406->7423|13462->7451|13491->7452|13521->7454|13550->7455|13582->7459|13611->7460
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|47->16|47->16|47->16|47->16|55->24|55->24|55->24|58->27|58->27|58->27|83->52|83->52|83->52|84->53|85->54|85->54|85->54|86->55|86->55|86->55|87->56|87->56|87->56|88->57|88->57|88->57|89->58|89->58|89->58|90->59|90->59|90->59|91->60|91->60|91->60|92->61|92->61|92->61|93->62|93->62|93->62|95->64|96->65|121->90|121->90|121->90|122->91|123->92|123->92|123->92|124->93|124->93|124->93|125->94|125->94|125->94|126->95|126->95|126->95|127->96|127->96|127->96|128->97|128->97|128->97|129->98|129->98|129->98|130->99|130->99|130->99|131->100|131->100|131->100|133->102|134->103|142->111|142->111|142->111|143->112|149->118|149->118|150->119|152->121|152->121|154->123|156->125|156->125|157->126|159->128|159->128|160->129|160->129|160->129|161->130|162->131|162->131|163->132|163->132|164->133|164->133|166->135|166->135|166->135|167->136|169->138|169->138|171->140|171->140|171->140|172->141|172->141|172->141|173->142|174->143|174->143|175->144|176->145|176->145|177->146|177->146|178->147|178->147|179->148|179->148|181->150|181->150|182->151|182->151|182->151|183->152|184->153|184->153|185->154|185->154|187->156|187->156|192->161|192->161|193->162|195->164|195->164|197->166|199->168|199->168|200->169|202->171|202->171|203->172|203->172|203->172|204->173|205->174|205->174|206->175|206->175|207->176|207->176|209->178|209->178|209->178|210->179|212->181|212->181|214->183|214->183|214->183|215->184|215->184|215->184|216->185|217->186|217->186|218->187|219->188|219->188|220->189|220->189|221->190|221->190|222->191|222->191|224->193|224->193|225->194|225->194|225->194|226->195|227->196|227->196|228->197|228->197|230->199|230->199
                  -- GENERATED --
              */
          