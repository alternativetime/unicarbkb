
package views.html

import play.twirl.api._


     object unicarb_Scope0 {
import controllers._
import models._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc.Http.Context.Implicit._
import views.html._

class unicarb extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[com.avaje.ebean.PagedList[Unicarbdbreference],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.PagedList[Unicarbdbreference], currentSortBy: String, currentOrder: String, currentFilter: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*32.2*/header/*32.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*32.38*/("""
    """),format.raw/*33.5*/("""<th class=""""),_display_(/*33.17*/key/*33.20*/.replace(".","_")),format.raw/*33.37*/(""" """),format.raw/*33.38*/("""header """),_display_(/*33.46*/if(currentSortBy == key){/*33.72*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*33.136*/("""">
        <a href=""""),_display_(/*34.19*/link(0, key)),format.raw/*34.31*/("""">"""),_display_(/*34.34*/title),format.raw/*34.39*/("""</a>
    </th>
""")))};def /*6.2*/link/*6.6*/(newPage:Int, newSortBy:String) = {{
    
    var sortBy = currentSortBy
    var order = currentOrder
    
    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
                order = "asc"
            }
        } else {
            order = "asc"
        }
    }
    
    // Generate the link
    routes.Application.list2(newPage, sortBy, order, currentFilter)
    
}};
Seq[Any](format.raw/*1.130*/("""

"""),format.raw/*5.42*/("""
"""),format.raw/*27.2*/("""

"""),format.raw/*31.37*/("""
"""),format.raw/*36.2*/("""

"""),_display_(/*38.2*/main/*38.6*/ {_display_(Seq[Any](format.raw/*38.8*/("""
    
    """),format.raw/*40.5*/("""<h1 id="homeTitle">"""),_display_(/*40.25*/Messages("Reference - UniCarb-DB", currentPage.getTotalRowCount)),format.raw/*40.89*/("""</h1>

    """),_display_(/*42.6*/if(flash.containsKey("success"))/*42.38*/ {_display_(Seq[Any](format.raw/*42.40*/("""
        """),format.raw/*43.9*/("""<div class="alert-message warning">
            <strong>Done!</strong> """),_display_(/*44.37*/flash/*44.42*/.get("success")),format.raw/*44.57*/("""
        """),format.raw/*45.9*/("""</div>
    """)))}),format.raw/*46.6*/(""" 

    """),format.raw/*48.5*/("""<div id="actions">
        <form action=""""),_display_(/*49.24*/link(0, "name")),format.raw/*49.39*/("""" method="GET">
            <input type="search" id="searchbox" name="f" value=""""),_display_(/*50.66*/currentFilter),format.raw/*50.79*/("""" placeholder="Filter by reference title or author name...">
            <input type="submit" id="searchsubmit" value="Filter" class="btn primary">
        </form>
        
        <a class="btn success" id="add" href=""""),_display_(/*54.48*/routes/*54.54*/.Application.unicarb()),format.raw/*54.76*/("""">Show All References</a>
	<a class="btn success" id="add" href=""""),_display_(/*55.41*/routes/*55.47*/.Application.list2()),format.raw/*55.67*/("""">GlycoSuite References</a>  
        
    </div>
    
    """),_display_(/*59.6*/if(currentPage.getTotalRowCount == 0)/*59.43*/ {_display_(Seq[Any](format.raw/*59.45*/("""
        
        """),format.raw/*61.9*/("""<div class="well">
            <em>Nothing to display</em>
        </div>
        
    """)))}/*65.7*/else/*65.12*/{_display_(Seq[Any](format.raw/*65.13*/("""
        
        """),format.raw/*67.9*/("""<table class="computers zebra-striped">
            <thead>
                <tr>
                    """),_display_(/*70.22*/header("title", "Title")),format.raw/*70.46*/("""
                    """),_display_(/*71.22*/header("year", "Year")),format.raw/*71.44*/("""
                    """),_display_(/*72.22*/header("authors", "Authors")),format.raw/*72.50*/("""
                    """),_display_(/*73.22*/header("journal.name", "Journal")),format.raw/*73.55*/("""
		    """),_display_(/*74.8*/header("strference.id", "Glycan Structures")),format.raw/*74.52*/("""
                """),format.raw/*75.17*/("""</tr>
            </thead>
            <tbody>

                """),_display_(/*79.18*/for(reference <- currentPage.getList) yield /*79.55*/ {_display_(Seq[Any](format.raw/*79.57*/("""
                    """),format.raw/*80.21*/("""<tr>
                        <td><a href="">"""),_display_(/*81.41*/reference/*81.50*/.title),format.raw/*81.56*/("""</a></td>
                        <td>
                            """),_display_(/*83.30*/if(reference.year < 1)/*83.52*/ {_display_(Seq[Any](format.raw/*83.54*/("""
                                """),format.raw/*84.33*/("""<em>-</em>
                            """)))}/*85.31*/else/*85.36*/{_display_(Seq[Any](format.raw/*85.37*/("""
                                """),_display_(/*86.34*/reference/*86.43*/.year),format.raw/*86.48*/("""
                            """)))}),format.raw/*87.30*/("""
                        """),format.raw/*88.25*/("""</td>
                        <td>
                            """),_display_(/*90.30*/if(reference.authors == null)/*90.59*/ {_display_(Seq[Any](format.raw/*90.61*/("""
                                """),format.raw/*91.33*/("""<em>-</em>
                            """)))}/*92.31*/else/*92.36*/{_display_(Seq[Any](format.raw/*92.37*/("""
                                """),_display_(/*93.34*/reference/*93.43*/.authors),format.raw/*93.51*/("""
                            """)))}),format.raw/*94.30*/("""
                        """),format.raw/*95.25*/("""</td>
                        <td>
                            """),_display_(/*97.30*/if(reference.journal == null)/*97.59*/ {_display_(Seq[Any](format.raw/*97.61*/("""
                                """),format.raw/*98.33*/("""<em>-</em>
                            """)))}/*99.31*/else/*99.36*/{_display_(Seq[Any](format.raw/*99.37*/("""
                                """),_display_(/*100.34*/reference/*100.43*/.journal.name),format.raw/*100.56*/("""
                            """)))}),format.raw/*101.30*/("""
                        """),format.raw/*102.25*/("""</td>
			 <td>
                          	"""),_display_(/*104.29*/reference/*104.38*/.lcmucin.size()),format.raw/*104.53*/(""" 
                        """),format.raw/*105.25*/("""</td>

                    </tr>
                """)))}),format.raw/*108.18*/("""

            """),format.raw/*110.13*/("""</tbody>
        </table>

        <div id="pagination" class="pagination">
            <ul>
                """),_display_(/*115.18*/if(currentPage.hasPrev)/*115.41*/ {_display_(Seq[Any](format.raw/*115.43*/("""
                    """),format.raw/*116.21*/("""<li class="prev">
                        <a href=""""),_display_(/*117.35*/link(currentPage.getPageIndex - 1, null)),format.raw/*117.75*/("""">&larr; Previous</a>
                    </li>
                """)))}/*119.19*/else/*119.24*/{_display_(Seq[Any](format.raw/*119.25*/("""
                    """),format.raw/*120.21*/("""<li class="prev disabled">
                        <a>&larr; Previous</a>
                    </li>
                """)))}),format.raw/*123.18*/("""
                """),format.raw/*124.17*/("""<li class="current">
                    <a>Displaying """),_display_(/*125.36*/currentPage/*125.47*/.getDisplayXtoYofZ(" to "," of ")),format.raw/*125.80*/("""</a>
                </li>
                """),_display_(/*127.18*/if(currentPage.hasNext)/*127.41*/ {_display_(Seq[Any](format.raw/*127.43*/("""
                    """),format.raw/*128.21*/("""<li class="next">
                        <a href=""""),_display_(/*129.35*/link(currentPage.getPageIndex + 1, null)),format.raw/*129.75*/("""">Next &rarr;</a>
                    </li>
                """)))}/*131.19*/else/*131.24*/{_display_(Seq[Any](format.raw/*131.25*/("""
                    """),format.raw/*132.21*/("""<li class="next disabled">
                        <a>Next &rarr;</a>
                    </li>
                """)))}),format.raw/*135.18*/("""
            """),format.raw/*136.13*/("""</ul>
        </div>
        
    """)))}),format.raw/*139.6*/("""
        
""")))}),format.raw/*141.2*/("""

            
"""))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[Unicarbdbreference],currentSortBy:String,currentOrder:String,currentFilter:String): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def f:((com.avaje.ebean.PagedList[Unicarbdbreference],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter) => apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def ref: this.type = this

}


}

/**/
object unicarb extends unicarb_Scope0.unicarb
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/unicarb.scala.html
                  HASH: 275581802b1d14fdf07ee1c7cd03f19a8ba6a97c
                  MATRIX: 809->1|1016->876|1030->882|1137->912|1169->917|1208->929|1220->932|1258->949|1287->950|1322->958|1356->984|1443->1048|1491->1069|1524->1081|1554->1084|1580->1089|1618->258|1629->262|2162->129|2191->256|2219->764|2249->874|2277->1105|2306->1108|2318->1112|2357->1114|2394->1124|2441->1144|2526->1208|2564->1220|2605->1252|2645->1254|2681->1263|2780->1335|2794->1340|2830->1355|2866->1364|2908->1376|2942->1383|3011->1425|3047->1440|3155->1521|3189->1534|3436->1754|3451->1760|3494->1782|3587->1848|3602->1854|3643->1874|3729->1934|3775->1971|3815->1973|3860->1991|3966->2080|3979->2085|4018->2086|4063->2104|4192->2206|4237->2230|4286->2252|4329->2274|4378->2296|4427->2324|4476->2346|4530->2379|4564->2387|4629->2431|4674->2448|4766->2513|4819->2550|4859->2552|4908->2573|4980->2618|4998->2627|5025->2633|5120->2701|5151->2723|5191->2725|5252->2758|5311->2799|5324->2804|5363->2805|5424->2839|5442->2848|5468->2853|5529->2883|5582->2908|5673->2972|5711->3001|5751->3003|5812->3036|5871->3077|5884->3082|5923->3083|5984->3117|6002->3126|6031->3134|6092->3164|6145->3189|6236->3253|6274->3282|6314->3284|6375->3317|6434->3358|6447->3363|6486->3364|6548->3398|6567->3407|6602->3420|6664->3450|6718->3475|6789->3518|6808->3527|6845->3542|6900->3568|6982->3618|7025->3632|7163->3742|7196->3765|7237->3767|7287->3788|7367->3840|7429->3880|7514->3946|7528->3951|7568->3952|7618->3973|7767->4090|7813->4107|7897->4163|7918->4174|7973->4207|8045->4251|8078->4274|8119->4276|8169->4297|8249->4349|8311->4389|8392->4451|8406->4456|8446->4457|8496->4478|8641->4591|8683->4604|8749->4639|8791->4650
                  LINES: 27->1|31->32|31->32|33->32|34->33|34->33|34->33|34->33|34->33|34->33|34->33|34->33|35->34|35->34|35->34|35->34|37->6|37->6|59->1|61->5|62->27|64->31|65->36|67->38|67->38|67->38|69->40|69->40|69->40|71->42|71->42|71->42|72->43|73->44|73->44|73->44|74->45|75->46|77->48|78->49|78->49|79->50|79->50|83->54|83->54|83->54|84->55|84->55|84->55|88->59|88->59|88->59|90->61|94->65|94->65|94->65|96->67|99->70|99->70|100->71|100->71|101->72|101->72|102->73|102->73|103->74|103->74|104->75|108->79|108->79|108->79|109->80|110->81|110->81|110->81|112->83|112->83|112->83|113->84|114->85|114->85|114->85|115->86|115->86|115->86|116->87|117->88|119->90|119->90|119->90|120->91|121->92|121->92|121->92|122->93|122->93|122->93|123->94|124->95|126->97|126->97|126->97|127->98|128->99|128->99|128->99|129->100|129->100|129->100|130->101|131->102|133->104|133->104|133->104|134->105|137->108|139->110|144->115|144->115|144->115|145->116|146->117|146->117|148->119|148->119|148->119|149->120|152->123|153->124|154->125|154->125|154->125|156->127|156->127|156->127|157->128|158->129|158->129|160->131|160->131|160->131|161->132|164->135|165->136|168->139|170->141
                  -- GENERATED --
              */
          