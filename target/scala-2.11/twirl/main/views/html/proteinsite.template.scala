
package views.html

import play.twirl.api._


     object proteinsite_Scope0 {
import java.lang._
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class proteinsite extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template11[String,String,String,ArrayList[com.avaje.ebean.SqlRow],String,HashSet[Long],Proteinstaxonomy,List[Proteinstaxonomy],HashSet[String],String,List[composition_protein.CompSite],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ptmComment: String, sequence: String, accession: String, source: ArrayList[com.avaje.ebean.SqlRow], site: String, structures: HashSet[Long], taxsources: Proteinstaxonomy, taxsourceslist: List[Proteinstaxonomy], multicar: HashSet[String], proteinFromTax: String, compSite: List[composition_protein.CompSite]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.310*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

"""),format.raw/*5.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-map-marker" ></i><a href="/proteins"> Protein</a> <span class="divider">></span></li>
  <li><a href="/proteinsummary/"""),_display_(/*8.33*/accession),format.raw/*8.42*/(""""> """),_display_(/*8.46*/accession),format.raw/*8.55*/(""" """),format.raw/*8.56*/("""</a><span class="divider">></span></li>
  <li class="active"><i class="icon-th" ></i> Associated Structures</li> 
</ul>

<section id="layouts" class="proteinsummary">

  <div class="page-header row-fluid">
    <h1>Associated Structures</h1>
    """),_display_(/*16.6*/if(taxsources != null )/*16.29*/{_display_(Seq[Any](format.raw/*16.30*/("""
    """),format.raw/*17.5*/("""<h4 class="subheader span8">Accession: <a href='/proteinsummary/"""),_display_(/*17.70*/accession),format.raw/*17.79*/("""'>"""),_display_(/*17.82*/accession),format.raw/*17.91*/("""</a> <br/>"""),_display_(/*17.102*/taxsources/*17.112*/.protein),format.raw/*17.120*/("""
      """),format.raw/*18.7*/("""<span class="pull-right"><a href="http://uniprot.org/uniprot/"""),_display_(/*18.69*/accession),format.raw/*18.78*/(""""><span class='label label-light'>UniProtKB/SWISS-PROT Entry</span></a></span>
    </h4>
    """)))}),format.raw/*20.6*/("""
    """),_display_(/*21.6*/if(taxsources == null )/*21.29*/{_display_(Seq[Any](format.raw/*21.30*/("""
    """),format.raw/*22.5*/("""<h4 class="subheader span8">Accession: <a href='/proteinsummary/"""),_display_(/*22.70*/accession),format.raw/*22.79*/("""'>"""),_display_(/*22.82*/accession),format.raw/*22.91*/("""</a> """),_display_(/*22.97*/for(car <- multicar) yield /*22.117*/ {_display_(Seq[Any](format.raw/*22.119*/("""<br/>"""),_display_(/*22.125*/car),format.raw/*22.128*/(""" """)))}),format.raw/*22.130*/(""" 
      """),format.raw/*23.7*/("""<span class="pull-right"><a href="http://uniprot.org/uniprot/"""),_display_(/*23.69*/accession),format.raw/*23.78*/(""""><span class='label label-light'>UniProtkb/swiss-prot Entry</span></a></span>
    </h4>
    """)))}),format.raw/*25.6*/("""
  """),format.raw/*26.3*/("""</div>

  <div class="row-fluid">
    <div class="span8">
      """),_display_(/*30.8*/if(taxsources == null)/*30.30*/{_display_(Seq[Any](format.raw/*30.31*/("""<div class="alert alert-error">Warning: This entry corresponds to more than one protein description <ul> """),_display_(/*30.137*/for(car <- multicar) yield /*30.157*/{_display_(Seq[Any](format.raw/*30.158*/(""" """),format.raw/*30.159*/("""<li>"""),_display_(/*30.164*/car),format.raw/*30.167*/("""</li> """)))}),format.raw/*30.174*/(""" """),format.raw/*30.175*/("""</ul> </div> """)))}),format.raw/*30.189*/("""
      """),format.raw/*31.7*/("""<h3>Structures reported on """),_display_(/*31.35*/site/*31.39*/.replace("N-", "ASN-").replace("Asn-", "ASN-").replace("S-", "SER-").replace("Ser-", "SER-").replace("T-", "THR-").replace("Thr-", "THR-").replace("ASASN-", "ASN-").replace("AsnAsn", "ASN-")),format.raw/*31.229*/(""" """),format.raw/*31.230*/("""<a href="/proteinsummary/"""),_display_(/*31.256*/accession),format.raw/*31.265*/(""""><span class="pull-right" style="text-transform:uppercase;text-transform:uppercase;font-size:12px;text-align:center;color:#21b5fd;margin:0 0 5px;font-weight:600">Discover more sites</span></a></h3>
      <ul class="structures clearfix">

        """),_display_(/*34.10*/for(s <- structures) yield /*34.30*/ {_display_(Seq[Any](format.raw/*34.32*/("""
        """),format.raw/*35.9*/("""<li>
          """),_display_(/*36.12*/views/*36.17*/.html.format.structure(s)),format.raw/*36.42*/("""
        """),format.raw/*37.9*/("""</li>
        """)))}),format.raw/*38.10*/("""
      """),format.raw/*39.7*/("""</ul>
    </div><!-- /col -->

    <div class="span4 details">

      """),_display_(/*44.8*/views/*44.13*/.html.format.format()),format.raw/*44.34*/("""

      """),format.raw/*46.7*/("""<div class="info">
        <h3>Sequence</h3>
        <div class="sequence">
          """),_display_(/*49.12*/sequence),format.raw/*49.20*/(""" 
        """),format.raw/*50.9*/("""</div>
      </div>

      <div class="info">
        <h3>Biological Associations</h3>
        <div class='taxonomy'>
          <a id='toggle-taxonomy'><span class='label label-important'><span class='icon-tags icon-white'></span> Taxonomy """),_display_(/*56.124*/if(taxsources != null)/*56.146*/ {_display_(Seq[Any](format.raw/*56.148*/("""(1)""")))}/*56.153*/else/*56.158*/{_display_(Seq[Any](format.raw/*56.159*/("""(0)""")))}),format.raw/*56.163*/(""" """),format.raw/*56.164*/("""<span class="caret"></span></span></a>
          <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein """),_display_(/*57.126*/if(taxsources != null)/*57.148*/ {_display_(Seq[Any](format.raw/*57.150*/("""(1)""")))}/*57.155*/else/*57.160*/{_display_(Seq[Any](format.raw/*57.161*/("""(0)""")))}),format.raw/*57.165*/(""" """),format.raw/*57.166*/("""<span class="caret"></span></span></a>
        </div>
        <div class='taxonomy'>
          <ul id='more-taxonomy'>
            <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Taxonomies</h3>
            """),_display_(/*62.14*/if(taxsources != null )/*62.37*/ {_display_(Seq[Any](format.raw/*62.39*/("""
            """),format.raw/*63.13*/("""<li><span class='icon-map-marker icon-white'></span> <a href="../taxonomysearch?taxonomy="""),_display_(/*63.103*/taxsources/*63.113*/.species),format.raw/*63.121*/("""">"""),_display_(/*63.124*/taxsources/*63.134*/.species),format.raw/*63.142*/("""</a></li>
            """)))}),format.raw/*64.14*/("""
          """),format.raw/*65.11*/("""</ul>
          <ul id='more-protein'>
            <h3 id='less-protein'><span class='icon-map-marker icon-white'></span> Protein</h3>
            """),_display_(/*68.14*/if(taxsources != null)/*68.36*/ {_display_(Seq[Any](format.raw/*68.38*/("""
            """),format.raw/*69.13*/("""<li><span class='icon-map-marker icon-white'></span> <a href='../proteinsummary/"""),_display_(/*69.94*/taxsources/*69.104*/.protein),format.raw/*69.112*/("""'>"""),_display_(/*69.115*/taxsources/*69.125*/.protein),format.raw/*69.133*/("""</a></li>
            """)))}),format.raw/*70.14*/("""
          """),format.raw/*71.11*/("""</ul>
        </div>
      </div>
      <div class="info">
        <h3>Reference """),_display_(/*75.24*/if(source.size() > 5)/*75.45*/ {_display_(Seq[Any](format.raw/*75.47*/("""showing top 5""")))}),format.raw/*75.61*/(""" """),format.raw/*75.62*/("""<span id='show-references' class="label" style="font-size:16px"><a href="#4">"""),_display_(/*75.140*/source/*75.146*/.size()),format.raw/*75.153*/("""</a></span></h3>
        <ol>
          """),format.raw/*77.81*/("""

          """),_display_(/*79.12*/for((source,i) <- source.zipWithIndex; if (i < 5)) yield /*79.62*/ {_display_(Seq[Any](format.raw/*79.64*/("""
          """),format.raw/*80.11*/("""<li class="references">
            <p class="title"><a name='"""),_display_(/*81.40*/i),format.raw/*81.41*/("""' href='/references/"""),_display_(/*81.62*/source/*81.68*/.get("id")),format.raw/*81.78*/("""'>"""),_display_(/*81.81*/source/*81.87*/.get("title")),format.raw/*81.100*/("""</a></p>
            <p class="author">"""),_display_(/*82.32*/source/*82.38*/.get("authors")),format.raw/*82.53*/("""</p>
            <p class="ref">PubMed: <a href='http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*83.81*/source/*83.87*/.get("pmid")),format.raw/*83.99*/("""'>"""),_display_(/*83.102*/source/*83.108*/.get("pmid")),format.raw/*83.120*/("""</a> Year: """),_display_(/*83.132*/source/*83.138*/.get("year")),format.raw/*83.150*/("""</p>
          </li>
          """)))}),format.raw/*85.12*/("""
          """),format.raw/*86.11*/("""<div id='more-references'>
          """),_display_(/*87.12*/for((source,i) <- source.zipWithIndex; if (i > 5)) yield /*87.62*/ {_display_(Seq[Any](format.raw/*87.64*/("""
          """),format.raw/*88.11*/("""<li class="references">
            <p class="title"><a name='"""),_display_(/*89.40*/i),format.raw/*89.41*/("""' href='/references/"""),_display_(/*89.62*/source/*89.68*/.get("id")),format.raw/*89.78*/("""'>"""),_display_(/*89.81*/source/*89.87*/.get("title")),format.raw/*89.100*/("""</a></p>
            <p class="author">"""),_display_(/*90.32*/source/*90.38*/.get("authors")),format.raw/*90.53*/("""</p>
            <p class="ref">PubMed: <a href='http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*91.81*/source/*91.87*/.get("pmid")),format.raw/*91.99*/("""'>"""),_display_(/*91.102*/source/*91.108*/.get("pmid")),format.raw/*91.120*/("""</a> Year: """),_display_(/*91.132*/source/*91.138*/.get("year")),format.raw/*91.150*/("""</p>
          </li>
          """)))}),format.raw/*93.12*/("""
          """),format.raw/*94.11*/("""</div>
          """),_display_(/*95.12*/if(source.size() > 5)/*95.33*/ {_display_(Seq[Any](format.raw/*95.35*/("""
          """),format.raw/*96.11*/("""<div id='show-more-references' class='more-structures' href='#'>
            See more references
            <br />
            <span>&#9679; &#9679; &#9679;</span>
          </div>
          """)))}),format.raw/*101.12*/("""

        """),format.raw/*103.9*/("""</ol>
      </div>

    </div><!-- /col -->
  </div><!-- /row -->

  """),_display_(/*109.4*/views/*109.9*/.html.footerunicarb.footerunicarb()),format.raw/*109.44*/("""

"""),format.raw/*111.1*/("""</section>

""")))}),format.raw/*113.2*/("""
"""))
      }
    }
  }

  def render(ptmComment:String,sequence:String,accession:String,source:ArrayList[com.avaje.ebean.SqlRow],site:String,structures:HashSet[Long],taxsources:Proteinstaxonomy,taxsourceslist:List[Proteinstaxonomy],multicar:HashSet[String],proteinFromTax:String,compSite:List[composition_protein.CompSite]): play.twirl.api.HtmlFormat.Appendable = apply(ptmComment,sequence,accession,source,site,structures,taxsources,taxsourceslist,multicar,proteinFromTax,compSite)

  def f:((String,String,String,ArrayList[com.avaje.ebean.SqlRow],String,HashSet[Long],Proteinstaxonomy,List[Proteinstaxonomy],HashSet[String],String,List[composition_protein.CompSite]) => play.twirl.api.HtmlFormat.Appendable) = (ptmComment,sequence,accession,source,site,structures,taxsources,taxsourceslist,multicar,proteinFromTax,compSite) => apply(ptmComment,sequence,accession,source,site,structures,taxsources,taxsourceslist,multicar,proteinFromTax,compSite)

  def ref: this.type = this

}


}

/**/
object proteinsite extends proteinsite_Scope0.proteinsite
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/proteinsite.scala.html
                  HASH: 6b27d4ef2438aa81635930c688edd45a400bae14
                  MATRIX: 925->1|1329->309|1357->312|1368->316|1406->318|1434->320|1718->578|1747->587|1777->591|1806->600|1834->601|2106->847|2138->870|2177->871|2209->876|2301->941|2331->950|2361->953|2391->962|2430->973|2450->983|2480->991|2514->998|2603->1060|2633->1069|2757->1163|2789->1169|2821->1192|2860->1193|2892->1198|2984->1263|3014->1272|3044->1275|3074->1284|3107->1290|3144->1310|3185->1312|3219->1318|3244->1321|3278->1323|3313->1331|3402->1393|3432->1402|3556->1496|3586->1499|3677->1564|3708->1586|3747->1587|3881->1693|3918->1713|3958->1714|3988->1715|4021->1720|4046->1723|4085->1730|4115->1731|4161->1745|4195->1752|4250->1780|4263->1784|4475->1974|4505->1975|4559->2001|4590->2010|4865->2258|4901->2278|4941->2280|4977->2289|5020->2305|5034->2310|5080->2335|5116->2344|5162->2359|5196->2366|5293->2437|5307->2442|5349->2463|5384->2471|5498->2558|5527->2566|5564->2576|5833->2817|5865->2839|5906->2841|5930->2846|5944->2851|5984->2852|6020->2856|6050->2857|6242->3021|6274->3043|6315->3045|6339->3050|6353->3055|6393->3056|6429->3060|6459->3061|6712->3287|6744->3310|6784->3312|6825->3325|6943->3415|6963->3425|6993->3433|7024->3436|7044->3446|7074->3454|7128->3477|7167->3488|7342->3636|7373->3658|7413->3660|7454->3673|7562->3754|7582->3764|7612->3772|7643->3775|7663->3785|7693->3793|7747->3816|7786->3827|7895->3909|7925->3930|7965->3932|8010->3946|8039->3947|8145->4025|8161->4031|8190->4038|8258->4148|8298->4161|8364->4211|8404->4213|8443->4224|8533->4287|8555->4288|8603->4309|8618->4315|8649->4325|8679->4328|8694->4334|8729->4347|8796->4387|8811->4393|8847->4408|8959->4493|8974->4499|9007->4511|9038->4514|9054->4520|9088->4532|9128->4544|9144->4550|9178->4562|9241->4594|9280->4605|9345->4643|9411->4693|9451->4695|9490->4706|9580->4769|9602->4770|9650->4791|9665->4797|9696->4807|9726->4810|9741->4816|9776->4829|9843->4869|9858->4875|9894->4890|10006->4975|10021->4981|10054->4993|10085->4996|10101->5002|10135->5014|10175->5026|10191->5032|10225->5044|10288->5076|10327->5087|10372->5105|10402->5126|10442->5128|10481->5139|10706->5332|10744->5342|10841->5412|10855->5417|10912->5452|10942->5454|10986->5467
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|39->8|39->8|39->8|39->8|39->8|47->16|47->16|47->16|48->17|48->17|48->17|48->17|48->17|48->17|48->17|48->17|49->18|49->18|49->18|51->20|52->21|52->21|52->21|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|54->23|54->23|54->23|56->25|57->26|61->30|61->30|61->30|61->30|61->30|61->30|61->30|61->30|61->30|61->30|61->30|61->30|62->31|62->31|62->31|62->31|62->31|62->31|62->31|65->34|65->34|65->34|66->35|67->36|67->36|67->36|68->37|69->38|70->39|75->44|75->44|75->44|77->46|80->49|80->49|81->50|87->56|87->56|87->56|87->56|87->56|87->56|87->56|87->56|88->57|88->57|88->57|88->57|88->57|88->57|88->57|88->57|93->62|93->62|93->62|94->63|94->63|94->63|94->63|94->63|94->63|94->63|95->64|96->65|99->68|99->68|99->68|100->69|100->69|100->69|100->69|100->69|100->69|100->69|101->70|102->71|106->75|106->75|106->75|106->75|106->75|106->75|106->75|106->75|108->77|110->79|110->79|110->79|111->80|112->81|112->81|112->81|112->81|112->81|112->81|112->81|112->81|113->82|113->82|113->82|114->83|114->83|114->83|114->83|114->83|114->83|114->83|114->83|114->83|116->85|117->86|118->87|118->87|118->87|119->88|120->89|120->89|120->89|120->89|120->89|120->89|120->89|120->89|121->90|121->90|121->90|122->91|122->91|122->91|122->91|122->91|122->91|122->91|122->91|122->91|124->93|125->94|126->95|126->95|126->95|127->96|132->101|134->103|140->109|140->109|140->109|142->111|144->113
                  -- GENERATED --
              */
          