
package views.html

import play.twirl.api._


     object glycodigest_Scope0 {
import java.lang._
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class glycodigest extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Map[String, String],Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(map: Map[String,String], id: Long, ct: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

Seq[Any](format.raw/*1.49*/("""
"""),format.raw/*3.1*/("""

"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

"""),format.raw/*7.1*/("""<style type="text/css">
#wrapperpin """),format.raw/*8.13*/("""{"""),format.raw/*8.14*/("""
	"""),format.raw/*9.2*/("""width: 90%;
	max-width: 1100px;
	min-width: 800px;
	margin: 50px auto;
"""),format.raw/*13.1*/("""}"""),format.raw/*13.2*/("""

"""),format.raw/*15.1*/("""#columnspin """),format.raw/*15.13*/("""{"""),format.raw/*15.14*/("""
	"""),format.raw/*16.2*/("""-webkit-column-count: 3;
	-webkit-column-gap: 10px;
	-webkit-column-fill: auto;
	-moz-column-count: 3;
	-moz-column-gap: 10px;
	-moz-column-fill: auto;
	column-count: 3;
	column-gap: 15px;
	column-fill: auto;
"""),format.raw/*25.1*/("""}"""),format.raw/*25.2*/("""
"""),format.raw/*26.1*/(""".pin """),format.raw/*26.6*/("""{"""),format.raw/*26.7*/("""
	"""),format.raw/*27.2*/("""display: inline-block;
	background: #FEFEFE;
	border: 2px solid #FAFAFA;
	box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
	margin: 0 2px 15px;
	-webkit-column-break-inside: avoid;
	-moz-column-break-inside: avoid;
	column-break-inside: avoid;
	padding: 15px;
	padding-bottom: 5px;
	background: -webkit-linear-gradient(45deg, #FFF, #F9F9F9);
	opacity: 1;
	-webkit-transition: all .2s ease;
	-moz-transition: all .2s ease;
	-o-transition: all .2s ease;
	transition: all .2s ease;
"""),format.raw/*43.1*/("""}"""),format.raw/*43.2*/("""

"""),format.raw/*45.1*/(""".pin img """),format.raw/*45.10*/("""{"""),format.raw/*45.11*/("""
	"""),format.raw/*46.2*/("""width: 100%;
	border-bottom: 1px solid #ccc;
	padding-bottom: 15px;
	margin-bottom: 5px;
"""),format.raw/*50.1*/("""}"""),format.raw/*50.2*/("""

"""),format.raw/*52.1*/(""".pin2 p """),format.raw/*52.9*/("""{"""),format.raw/*52.10*/("""
	"""),format.raw/*53.2*/("""font: 12px/18px Arial, sans-serif;
	color: #333;
	margin: 0;
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/("""



"""),format.raw/*60.1*/("""#columns:hover .pin:not(:hover) """),format.raw/*60.33*/("""{"""),format.raw/*60.34*/("""
	"""),format.raw/*61.2*/("""opacity: 0.4;
"""),format.raw/*62.1*/("""}"""),format.raw/*62.2*/("""

"""),format.raw/*64.1*/("""</style>

<script>
        $(document).ready(function() """),format.raw/*67.38*/("""{"""),format.raw/*67.39*/("""  
        
        """),format.raw/*69.9*/("""$("#e20").select2("""),format.raw/*69.27*/("""{"""),format.raw/*69.28*/("""
            """),format.raw/*70.13*/("""tags:["ABS", "AMF", "BKF", "BTG", "GUH", "JBM", "NAN1", "SPG" ],
            tokenSeparators: [",", " "]"""),format.raw/*71.40*/("""}"""),format.raw/*71.41*/(""");
        """),format.raw/*72.9*/("""}"""),format.raw/*72.10*/(""");
</script>

<script>
	$('.container').hide();
	$('#game_container').show();

	$('.post_types button').click(function()"""),format.raw/*79.42*/("""{"""),format.raw/*79.43*/("""
        	"""),format.raw/*80.10*/("""var target = "#" + $(this).data("target");
	        $(".container").not(target).hide();
	        $(target).show();
	        $('#post_type').val($(target).text());
	"""),format.raw/*84.2*/("""}"""),format.raw/*84.3*/(""");
</script>

<ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider"></span></li>
    <li class="active"><i class="icon-map-marker" ></i> GlycoDigest<span class="divider"></span></li>
</ul>
   
<div class="page-header row-fluid"> 
    <h1 id="homeTitle">GlycoDigest</h1>
    <h4 class="subheader">A tool to predict exoglycosidase digestions</h4>
</div>

<div class="bs-callout bs-callout-warning" >
	<h4>Using Exoglycosidases <span class="glyphicon glyphicon-search"></span></h4>
    <p>Exoglycosidase enzyme array digestions, in combination with U/HPLC and LC-MS, can deliver semi-quantitative glycan analysis of sugars released from glycoproteins. A detailed description and example experimental protocols for using and applying exoglycosidases is published by <a href="http://link.springer.com/protocol/10.1385%2F1-59745-167-3%3A125" target="_blank">Royle et al., Methods Mol Biol. 2006;347:125-43</a>.</p>
    <p>A summary of the mode of action of exoglycosidases and examples can be found <a href="http://www.glycodigest.org/exoglycosidase.pdf" target="_blank">here</a>. Click the 'Information on Glycosidases' button in the 'Build Exoglycosidases' box below.</p>
    """),format.raw/*103.13*/("""



"""),format.raw/*107.1*/("""</div>

<div id="actions">
	<div>
           <img src="http://115.146.94.196:8123/eurocarb/get_sugar_image.action?download=false&amp;scale=1.0&amp;opaque=false&amp;outputType=png&notation=cfglink&inputType=glycoct_condensed&sequences="""),_display_(/*111.202*/helper/*111.208*/.urlEncode(ct)),format.raw/*111.222*/("""" />
   	</div>
	<div class="bs-callout bs-callout-info" >
    	   <h4>Build Exoglycosidase Array</h4>
	   <p>Use the search box below to select the panel of exoglycosidase to digest the structure shown:</p>	
	 <form class="form-search" action=""""),_display_(/*116.38*/routes/*116.44*/.Glycodigest.glycodigesttest({id})),format.raw/*116.78*/("""" method="GET">  	
       <div id="selection" class="row-fluid">
       <input name=digest  id="e20" id="listBox" class="span4"></input>
       <button type="submit" class="btn btn-primary">Digest</button>
       <a class='btn pull-right' href="/builderDigest"><i class="icon-refresh"></i> New Glycan</a>
       </div>
    </form>
<div class='more-exoglycosidases'>
    <a id='toggle-exoglycosidases'><span class='label'><span class='icon-tags icon-white'></span> Information on Exoglycosidases <span class="caret"></span></span></a>
   </div>

</div>

<div>
	          <ul id='more-exoglycosidases'>
<div class="table-responsive" id="exoglycosidases">
	<table class="table table-striped">
		<thead>
	        <tr>
	        <th>Short Name</th>
	        <th>Full Name</th>
	        <th>Source</th>
	        <th>Specificity</th>
		</tr>
		</thead>
		<tr><td>
			ABS</td>
		    <td>α(2-3,6,8,9)-Sialidase</td>
		    <td>Recombinant Arthrobacter ureafaciens gene, expressed in E. coli
			    </td><td>α(2-3,6,8,9)-specific, cleaves all non-reducing terminal branched and unbranched sialic acids</td></tr>
		<tr><td>NAN1
		    </td>
		    <td>α(2-3)-Sialidase
		    </td>
		    <td>Recombinant Streptococcus pneumoniae gene, expressed in E. coli
		    </td><td>Releases α(2-3)-linked sialic acid
		    </td>
	    	</tr>
		<tr><td>BKF
		    </td><td>α(1-2,3,4,6)-Fucosidase
		    </td><td>Bovine Kidney
		    </td><td>iReleases non-reducing terminal α(1-6) core-linked fucose more efficiently than other α-fucose linkages. Frequently used for release of core fucose residues
		    </td>
	    	</tr>
		<tr><td>XMF
		    </td>
		    <td>α(1-2)-Fucosidase
		    </td>
		    <td>Xanthomonas manihotis
		    </td>
	    	    <td>Releases non-reducing terminal α(1-2)-linked fucose
		    </td>
	    	</tr>
		<tr>
		    <td>AMF
		    </td>
		    <td>α(1-3,4)-Fucosidase
		    </td>
		    <td>Almond Meal
		    </td>
		    <td>Releases non-reducing terminal α(1-3,4)-linked fucose. Does not release core linked fucose in α(1-3,6) configuration
		    </td>
	    	</tr>
		<tr>
		   <td>BTG
		   </td>
		   <td>	β(1-3,4)-Galactosidase
		   </td>
		   <td>Bovine testis
		   </td>
		   <td>Releases non-reducing terminal β(1-3,4)-linked galactose residues
		   </td>
		</tr>
		<tr>
 		   <td>SPG
		   </td>
		   <td>β(1-4)-Galactosidase
		   </td>
		   <td>Streptococcus pneumoniae
		   </td>
		   <td>β(1-4) specific galactosidase removes galactose residues from non- reducing terminal
		   </td>
	   	</tr>
		<tr><td>CBG</td>
		<td>α(1-3,4,6)-Galactosidase</td>
		<td>Coffee Bean</td>
			<td>Hydrolyses α(1-3,4,6)-linked terminal galactose residues</td>
		</tr>
		<tr>
			<td>JBM</td>
			<td>α(1-2,3,6)-Mannosidase</td>
			<td>Jack Bean</td>
			<td>Releases non-reducing terminal α(1-2,6)-linked mannose residues more efficiently than α(1-3)</td>
		</tr>
		<tr>
			<td>GUH</td>
			<td>β-N-Acetylhexosaminidase</td>
			<td>Recombinant Streptococcus pneumoniae gene, expressed in E. coli</td>
			<td>Releases all non-reducing terminal β-linked N-acetylglucosamine but not bisecting GlcNAc β(1-4)Man residues</td>
		</tr>
		<tr>
			<td>JBH</td>
			<td>β-N-Acetylhexosaminidase</td>
			<td>Jack Bean</td>
			<td>Specific to all non-reducing terminal β(1-2,3,4,6)-linked N- acetylglucosamine and N-acetylgalactosamine residues</td>
		</tr>
		   


	</table>
</div>
</ul></div>

<h2>Results</h2>
<div>
"""),_display_(/*231.2*/for((key, value) <- map) yield /*231.26*/{_display_(Seq[Any](format.raw/*231.27*/("""
    """),format.raw/*232.5*/("""<div class="col-xs-6 col-md-3"">
      <img class="thumbnail" src="http://115.146.94.196:8123/eurocarb/get_sugar_image.action?download=false&amp;scale=0.5&amp;opaque=false&amp;outputType=png&notation=cfglink&inputType=glycoct_condensed&sequences="""),_display_(/*233.215*/value),format.raw/*233.220*/("""" />
      <p>"""),_display_(/*234.11*/key/*234.14*/.replace("ABS", "ABS - Sialidase").replace("BTG", "β(1-3,4)-Galactosidase").replace("NAN1", "NAN1 - α(2-3)-Sialidase").replace("BKF", "BKF α(1-2,3,4,6)-Fucosidase").replace("XMF", "XMF α(1-2)-Fucosidase").replace("AMF", "AMF α(1-3,4)-Fucosidase").replace("SPG", "SPG β(1-4)-Galactosidase").replace("CBG", "CBG α(1-3,4,6)-Galactosidase").replace("JBM", "JBM α(1-2,3,6)-Mannosidase").replace("GUH", "GUH β-N-Acetylhexosaminidase").replace("JBH", "JBH β-N-Acetylhexosaminidase")),format.raw/*234.489*/("""</p>
    </div>
""")))}),format.raw/*236.2*/("""
"""),format.raw/*237.1*/("""</div>



      """),_display_(/*241.8*/views/*241.13*/.html.footerunicarb.footerunicarb()),format.raw/*241.48*/("""    


  """),format.raw/*244.3*/("""</section>
        
""")))}),format.raw/*246.2*/("""
"""))
      }
    }
  }

  def render(map:Map[String, String],id:Long,ct:String): play.twirl.api.HtmlFormat.Appendable = apply(map,id,ct)

  def f:((Map[String, String],Long,String) => play.twirl.api.HtmlFormat.Appendable) = (map,id,ct) => apply(map,id,ct)

  def ref: this.type = this

}


}

/**/
object glycodigest extends glycodigest_Scope0.glycodigest
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/glycodigest.scala.html
                  HASH: a8b4d3d22f527a96408cbeb0e6ec5e47ccb9e7be
                  MATRIX: 782->1|939->48|966->66|994->69|1005->73|1043->75|1071->77|1134->113|1162->114|1190->116|1288->187|1316->188|1345->190|1385->202|1414->203|1443->205|1679->414|1707->415|1735->416|1767->421|1795->422|1824->424|2326->899|2354->900|2383->902|2420->911|2449->912|2478->914|2594->1003|2622->1004|2651->1006|2686->1014|2715->1015|2744->1017|2832->1078|2860->1079|2891->1083|2951->1115|2980->1116|3009->1118|3050->1132|3078->1133|3107->1135|3191->1191|3220->1192|3267->1212|3313->1230|3342->1231|3383->1244|3515->1348|3544->1349|3582->1360|3611->1361|3759->1481|3788->1482|3826->1492|4017->1656|4045->1657|5299->3100|5331->3104|5595->3339|5612->3345|5649->3359|5923->3605|5939->3611|5995->3645|9401->7024|9442->7048|9482->7049|9515->7054|9791->7301|9819->7306|9862->7321|9875->7324|10373->7799|10421->7816|10450->7817|10494->7834|10509->7839|10566->7874|10603->7883|10655->7904
                  LINES: 27->1|32->1|33->3|35->5|35->5|35->5|37->7|38->8|38->8|39->9|43->13|43->13|45->15|45->15|45->15|46->16|55->25|55->25|56->26|56->26|56->26|57->27|73->43|73->43|75->45|75->45|75->45|76->46|80->50|80->50|82->52|82->52|82->52|83->53|86->56|86->56|90->60|90->60|90->60|91->61|92->62|92->62|94->64|97->67|97->67|99->69|99->69|99->69|100->70|101->71|101->71|102->72|102->72|109->79|109->79|110->80|114->84|114->84|131->103|135->107|139->111|139->111|139->111|144->116|144->116|144->116|259->231|259->231|259->231|260->232|261->233|261->233|262->234|262->234|262->234|264->236|265->237|269->241|269->241|269->241|272->244|274->246
                  -- GENERATED --
              */
          