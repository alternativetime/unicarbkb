
package views.html

import play.twirl.api._


     object swissprotFT_Scope0 {
import java.util._

import models._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import views.html._

import scala.collection.JavaConversions._

class swissprotFT extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[HashSet[Reference],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(ft: HashSet[Reference], car: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.39*/("""


"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

  """),format.raw/*6.3*/("""<ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-book" ></i> SwissProt<span class="divider"></span></li>
    <!--<li class="active" > You are here</li>-->
  </ul>

  <section id="layouts">

    <div class="page-header row-fluid">
      <h1 id="homeTitle">"""),_display_(/*15.27*/Messages(" UniProtKB/Swiss-Prot Feature Table ")),format.raw/*15.75*/("""</h1>
      <h4 class="subheader"> Feature Table Link for """),_display_(/*16.54*/car),format.raw/*16.57*/(""" """),format.raw/*16.58*/("""</h4>
    </div>

    <table class="computers table table-striped">

      <thead>
        <tr>
          <th>Title</th>
          <th>Year</th>
          <th>Authors</th>
          <th>Journal</th>
          <th>Associated Glycans</th>
        </tr>
      </thead>

      <tbody>
        """),_display_(/*32.10*/for(reference <- ft) yield /*32.30*/ {_display_(Seq[Any](format.raw/*32.32*/("""
        """),format.raw/*33.9*/("""<tr>
          <td><a href="references/"""),_display_(/*34.36*/reference/*34.45*/.id),format.raw/*34.48*/("""" style="text-decoration : none, font-color: #404040; ">"""),_display_(/*34.105*/reference/*34.114*/.title),format.raw/*34.120*/("""</a></td>
          <td>
            """),_display_(/*36.14*/if(reference.year < 1)/*36.36*/ {_display_(Seq[Any](format.raw/*36.38*/("""
            """),format.raw/*37.13*/("""<em>-</em>
            """)))}/*38.15*/else/*38.20*/{_display_(Seq[Any](format.raw/*38.21*/("""
            """),_display_(/*39.14*/reference/*39.23*/.year),format.raw/*39.28*/("""
            """)))}),format.raw/*40.14*/("""
          """),format.raw/*41.11*/("""</td>
          <td>
            """),_display_(/*43.14*/if(reference.authors == null)/*43.43*/ {_display_(Seq[Any](format.raw/*43.45*/("""
            """),format.raw/*44.13*/("""<em>-</em>
            """)))}/*45.15*/else/*45.20*/{_display_(Seq[Any](format.raw/*45.21*/("""
            """),_display_(/*46.14*/reference/*46.23*/.authors),format.raw/*46.31*/("""
            """)))}),format.raw/*47.14*/("""
          """),format.raw/*48.11*/("""</td>
          <td>
            """),_display_(/*50.14*/if(reference.journal == null)/*50.43*/ {_display_(Seq[Any](format.raw/*50.45*/("""
            """),format.raw/*51.13*/("""<em>-</em>
            """)))}/*52.15*/else/*52.20*/{_display_(Seq[Any](format.raw/*52.21*/("""
            """),_display_(/*53.14*/reference/*53.23*/.journal.name),format.raw/*53.36*/("""                            """)))}),format.raw/*53.65*/("""
          """),format.raw/*54.11*/("""</td>
          <td>
            """),_display_(/*56.14*/reference/*56.23*/.streference.size()),format.raw/*56.42*/("""
          """),format.raw/*57.11*/("""</td>
        </tr>
        """)))}),format.raw/*59.10*/("""

      """),format.raw/*61.7*/("""</tbody>
    </table>

    """),_display_(/*64.6*/views/*64.11*/.html.footerunicarb.footerunicarb()),format.raw/*64.46*/("""    

  """),format.raw/*66.3*/("""</section>

  """)))}),format.raw/*68.4*/("""


"""))
      }
    }
  }

  def render(ft:HashSet[Reference],car:String): play.twirl.api.HtmlFormat.Appendable = apply(ft,car)

  def f:((HashSet[Reference],String) => play.twirl.api.HtmlFormat.Appendable) = (ft,car) => apply(ft,car)

  def ref: this.type = this

}


}

/**/
object swissprotFT extends swissprotFT_Scope0.swissprotFT
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/swissprotFT.scala.html
                  HASH: c277af3921801529c9339fdc1463bf9edb0896c5
                  MATRIX: 776->1|908->38|937->42|948->46|986->48|1016->52|1409->418|1478->466|1564->525|1588->528|1617->529|1934->819|1970->839|2010->841|2046->850|2113->890|2131->899|2155->902|2240->959|2259->968|2287->974|2352->1012|2383->1034|2423->1036|2464->1049|2507->1074|2520->1079|2559->1080|2600->1094|2618->1103|2644->1108|2689->1122|2728->1133|2789->1167|2827->1196|2867->1198|2908->1211|2951->1236|2964->1241|3003->1242|3044->1256|3062->1265|3091->1273|3136->1287|3175->1298|3236->1332|3274->1361|3314->1363|3355->1376|3398->1401|3411->1406|3450->1407|3491->1421|3509->1430|3543->1443|3603->1472|3642->1483|3703->1517|3721->1526|3761->1545|3800->1556|3860->1585|3895->1593|3949->1621|3963->1626|4019->1661|4054->1669|4099->1684
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|46->15|46->15|47->16|47->16|47->16|63->32|63->32|63->32|64->33|65->34|65->34|65->34|65->34|65->34|65->34|67->36|67->36|67->36|68->37|69->38|69->38|69->38|70->39|70->39|70->39|71->40|72->41|74->43|74->43|74->43|75->44|76->45|76->45|76->45|77->46|77->46|77->46|78->47|79->48|81->50|81->50|81->50|82->51|83->52|83->52|83->52|84->53|84->53|84->53|84->53|85->54|87->56|87->56|87->56|88->57|90->59|92->61|95->64|95->64|95->64|97->66|99->68
                  -- GENERATED --
              */
          