
package views.html

import play.twirl.api._


     object glycodigestBuilder_Scope0 {
import views.html._

class glycodigestBuilder extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(str: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.15*/("""


"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

"""),format.raw/*6.1*/("""<style type="text/css">
#wrapperpin """),format.raw/*7.13*/("""{"""),format.raw/*7.14*/("""
	"""),format.raw/*8.2*/("""width: 90%;
	max-width: 1100px;
	min-width: 800px;
	margin: 50px auto;
"""),format.raw/*12.1*/("""}"""),format.raw/*12.2*/("""

"""),format.raw/*14.1*/("""#columnspin """),format.raw/*14.13*/("""{"""),format.raw/*14.14*/("""
	"""),format.raw/*15.2*/("""-webkit-column-count: 3;
	-webkit-column-gap: 10px;
	-webkit-column-fill: auto;
	-moz-column-count: 3;
	-moz-column-gap: 10px;
	-moz-column-fill: auto;
	column-count: 3;
	column-gap: 15px;
	column-fill: auto;
"""),format.raw/*24.1*/("""}"""),format.raw/*24.2*/("""
"""),format.raw/*25.1*/(""".pin """),format.raw/*25.6*/("""{"""),format.raw/*25.7*/("""
	"""),format.raw/*26.2*/("""display: inline-block;
	background: #FEFEFE;
	border: 2px solid #FAFAFA;
	box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
	margin: 0 2px 15px;
	-webkit-column-break-inside: avoid;
	-moz-column-break-inside: avoid;
	column-break-inside: avoid;
	padding: 15px;
	padding-bottom: 5px;
	background: -webkit-linear-gradient(45deg, #FFF, #F9F9F9);
	opacity: 1;
	-webkit-transition: all .2s ease;
	-moz-transition: all .2s ease;
	-o-transition: all .2s ease;
	transition: all .2s ease;
"""),format.raw/*42.1*/("""}"""),format.raw/*42.2*/("""

"""),format.raw/*44.1*/(""".pin img """),format.raw/*44.10*/("""{"""),format.raw/*44.11*/("""
	"""),format.raw/*45.2*/("""width: 100%;
	border-bottom: 1px solid #ccc;
	padding-bottom: 15px;
	margin-bottom: 5px;
"""),format.raw/*49.1*/("""}"""),format.raw/*49.2*/("""

"""),format.raw/*51.1*/(""".pin2 p """),format.raw/*51.9*/("""{"""),format.raw/*51.10*/("""
	"""),format.raw/*52.2*/("""font: 12px/18px Arial, sans-serif;
	color: #333;
	margin: 0;
"""),format.raw/*55.1*/("""}"""),format.raw/*55.2*/("""



"""),format.raw/*59.1*/("""#columns:hover .pin:not(:hover) """),format.raw/*59.33*/("""{"""),format.raw/*59.34*/("""
	"""),format.raw/*60.2*/("""opacity: 0.4;
"""),format.raw/*61.1*/("""}"""),format.raw/*61.2*/("""

"""),format.raw/*63.1*/("""</style>

<script>
        $(document).ready(function() """),format.raw/*66.38*/("""{"""),format.raw/*66.39*/("""  
        
        """),format.raw/*68.9*/("""$("#e20").select2("""),format.raw/*68.27*/("""{"""),format.raw/*68.28*/("""
            """),format.raw/*69.13*/("""tags:["ABS", "AMF", "BKF", "BTG", "GUH", "JBM", "NAN1", "SPG" ],
            tokenSeparators: [",", " "]"""),format.raw/*70.40*/("""}"""),format.raw/*70.41*/(""");
        """),format.raw/*71.9*/("""}"""),format.raw/*71.10*/(""");
</script>

<ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider"></span></li>
    <li class="active"><i class="icon-map-marker" ></i> GlycoDigest<span class="divider"></span></li>
</ul>
   
<div class="page-header row-fluid"> 
    <h1 id="homeTitle">GlycoDigest</h1>
    <h4 class="subheader">A tool to predict exoglycosidase digestions</h4>
</div>

<div id="actions">
	<div>
           <img src="http://glycobase.nibrt.ie/glycobase/get_sugar_image.action?download=false&amp;scale=1.0&amp;opaque=false&amp;outputType=png&notation=cfglink&inputType=glycoct_condensed&sequences="""),_display_(/*86.202*/helper/*86.208*/.urlEncode(str)),format.raw/*86.223*/("""" />
        </div>
	<p>Use the search box below to select the panel of exoglycosidase to digest the structure shown:</p>	
	 <form class="form-search" action="/glycodigestBuilder/test/digest" method="GET">  	
       <div id="selection" class="row-fluid">
       <input name=digest  id="e20" id="listBox" class="span4"></input>
       <button type="submit" class="btn btn-primary">Digest</button>
       </div>
    </form>
</div>


      """),_display_(/*98.8*/views/*98.13*/.html.footerunicarb.footerunicarb()),format.raw/*98.48*/("""    


  """),format.raw/*101.3*/("""</section>
        
""")))}),format.raw/*103.2*/("""
"""))
      }
    }
  }

  def render(str:String): play.twirl.api.HtmlFormat.Appendable = apply(str)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (str) => apply(str)

  def ref: this.type = this

}


}

/**/
object glycodigestBuilder extends glycodigestBuilder_Scope0.glycodigestBuilder
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/glycodigestBuilder.scala.html
                  HASH: e3ca32080e88867f9b6e6f865c8f1e24276789b6
                  MATRIX: 771->1|879->14|908->18|919->22|957->24|985->26|1048->62|1076->63|1104->65|1202->136|1230->137|1259->139|1299->151|1328->152|1357->154|1593->363|1621->364|1649->365|1681->370|1709->371|1738->373|2240->848|2268->849|2297->851|2334->860|2363->861|2392->863|2508->952|2536->953|2565->955|2600->963|2629->964|2658->966|2746->1027|2774->1028|2805->1032|2865->1064|2894->1065|2923->1067|2964->1081|2992->1082|3021->1084|3105->1140|3134->1141|3181->1161|3227->1179|3256->1180|3297->1193|3429->1297|3458->1298|3496->1309|3525->1310|4186->1943|4202->1949|4239->1964|4703->2402|4717->2407|4773->2442|4810->2451|4862->2472
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|38->7|38->7|39->8|43->12|43->12|45->14|45->14|45->14|46->15|55->24|55->24|56->25|56->25|56->25|57->26|73->42|73->42|75->44|75->44|75->44|76->45|80->49|80->49|82->51|82->51|82->51|83->52|86->55|86->55|90->59|90->59|90->59|91->60|92->61|92->61|94->63|97->66|97->66|99->68|99->68|99->68|100->69|101->70|101->70|102->71|102->71|117->86|117->86|117->86|129->98|129->98|129->98|132->101|134->103
                  -- GENERATED --
              */
          