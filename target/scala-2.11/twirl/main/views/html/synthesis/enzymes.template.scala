
package views.html.synthesis

import play.twirl.api._


     object enzymes_Scope0 {
import java.util._

import models._

import scala.collection.JavaConversions._

class enzymes extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[Structure],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(references: List[Structure], reader: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*7.2*/header/*7.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*7.38*/("""
    """),format.raw/*8.5*/("""<th class="">
        <a href="">"""),_display_(/*9.21*/title),format.raw/*9.26*/("""</a>
    </th>
""")))};
Seq[Any](format.raw/*1.47*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*6.37*/("""
"""),format.raw/*11.2*/("""

"""),format.raw/*13.1*/("""<ul class="nav nav-tabs" data-tabs="tabs">
    <li class="active"><a data-toggle="tab" href="#red">References</a></li>
	<li><a data-toggle="tab" href="#annotated">Enzyme</a></li>
</ul>

<div class="tab-content">
      <div class="tab-pane active" id="red">
	  
	  
      <h3>References associated to structure</h3>
      <table class="computers table table-striped">
        <thead>
          <tr>
            """),_display_(/*26.14*/header("title", "Title")),format.raw/*26.38*/("""
            """),_display_(/*27.14*/header("year", "Year")),format.raw/*27.36*/("""
            """),_display_(/*28.14*/header("authors", "Authors")),format.raw/*28.42*/("""
          """),format.raw/*29.11*/("""</tr>
        </thead>
        <tbody>
          """),_display_(/*32.12*/for(stref <- references) yield /*32.36*/{_display_(Seq[Any](format.raw/*32.37*/("""
          """),_display_(/*33.12*/for(reference <- stref.references) yield /*33.46*/{_display_(Seq[Any](format.raw/*33.47*/("""
          """),format.raw/*34.11*/("""<tr>
            <td><a href="../references/"""),_display_(/*35.41*/reference/*35.50*/.reference.id),format.raw/*35.63*/("""">"""),_display_(/*35.66*/reference/*35.75*/.reference.title),format.raw/*35.91*/("""</a></td>
            <td>"""),_display_(/*36.18*/reference/*36.27*/.reference.year),format.raw/*36.42*/("""</td>
            <td>"""),_display_(/*37.18*/reference/*37.27*/.reference.authors),format.raw/*37.45*/("""</td>
          </tr>
          """)))}),format.raw/*39.12*/("""
          """)))}),format.raw/*40.12*/("""
        """),format.raw/*41.9*/("""</tbody>
      </table>
    
      </div>
      
      <div class="tab-pane" id="annotated">

      <table class="computers table table-striped">
        <thead>
          <tr>
          	<th>Gene</th>
          	<th>Name</th>
          	<th>Enzyme Commission</th>
          	<th>GO Term</th>
          	<th>Residue</th>
          	<th>Linked Residue</th>
          </tr>
        </thead>
        <tbody>
        """),_display_(/*60.10*/Html(reader)),format.raw/*60.22*/("""
        """),format.raw/*61.9*/("""</tbody>
        </table>
      </div>
      
</div>"""))
      }
    }
  }

  def render(references:List[Structure],reader:String): play.twirl.api.HtmlFormat.Appendable = apply(references,reader)

  def f:((List[Structure],String) => play.twirl.api.HtmlFormat.Appendable) = (references,reader) => apply(references,reader)

  def ref: this.type = this

}


}

/**/
object enzymes extends enzymes_Scope0.enzymes
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/synthesis/enzymes.scala.html
                  HASH: bb818615203c4753349bad0e98e8eec8435d3c75
                  MATRIX: 775->1|913->175|926->181|1032->211|1063->216|1123->250|1148->255|1203->46|1230->64|1258->173|1286->271|1315->273|1753->684|1798->708|1839->722|1882->744|1923->758|1972->786|2011->797|2088->847|2128->871|2167->872|2206->884|2256->918|2295->919|2334->930|2406->975|2424->984|2458->997|2488->1000|2506->1009|2543->1025|2597->1052|2615->1061|2651->1076|2701->1099|2719->1108|2758->1126|2822->1159|2865->1171|2901->1180|3342->1594|3375->1606|3411->1615
                  LINES: 27->1|31->7|31->7|33->7|34->8|35->9|35->9|38->1|39->3|40->6|41->11|43->13|56->26|56->26|57->27|57->27|58->28|58->28|59->29|62->32|62->32|62->32|63->33|63->33|63->33|64->34|65->35|65->35|65->35|65->35|65->35|65->35|66->36|66->36|66->36|67->37|67->37|67->37|69->39|70->40|71->41|90->60|90->60|91->61
                  -- GENERATED --
              */
          