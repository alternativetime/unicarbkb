
package views.html

import play.twirl.api._


     object news_Scope0 {
import controllers._

class news extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html class="about">
  <head>
    <title>UnICarbKB</title>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*5.66*/routes/*5.72*/.Assets.versioned("assets/stylesheets/bootstrap.min.css")),format.raw/*5.129*/("""">
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*6.66*/routes/*6.72*/.Assets.versioned("assets/stylesheets/unicarbkb.css")),format.raw/*6.125*/("""">
    <link rel="icon" href=""""),_display_(/*7.29*/routes/*7.35*/.Assets.versioned("assets/favicon.ico")),format.raw/*7.74*/("""" type="image/x-icon">
    <link rel="icon" href=""""),_display_(/*8.29*/routes/*8.35*/.Assets.versioned("assets/favicon.ico")),format.raw/*8.74*/("""" type="image/x-icon">
  </head>

  <header>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="/">UniCarbKB</a>
          <ul class="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/query">Search</a></li>
            <li><a href="/references">References</a></li>
            <li><a href="/builder">Glycan Builder</a></li>
          </ul>
          <div id="headersearch" class="pull-right">
            <ul class='nav'>
              <li class='active'><a href="/about">About</a></li>
              <li class='active'><a href="/about">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="container-fluid content">
    <div class="row-fluid info">
      <div class="span8 leftContent">
        <h3>GlycoSuiteDB redirected to UniCarbKB <em>(24th September 2013)</em></h3>
        <p>Welcome to UniCarbKB. 
        The UniCarbKB team in collaboration with the Swiss Institute of Bioinformatics are pleased 
        to announce the release of UniCarbKB.</p>
        <p>Thanks to this collaboration all UniProtKB and GlycoMod links associated with 
        GlycoSuiteDB have been refreshed and will automatically redirect users to relevant UniCarbKB pages.</p> 
        <p>Previously, GlycoSuiteDB was hosted and maintained by the Swiss Institute of Bioinformatics but has now been discontinued and all services and data have been integrated into UniCarbKB. 
        </p>

        
        
        <h4>GlycoSuiteDB Final Release 8.0</h4>
        <p>GlycoSuiteDB is a curated and annotated glycan database. Release 8.0 contains 9436 entries, sourced from 864 references. 
        This data and new content have been integrated into UniCarbKB with a new user interface and functionality. 
        GlycoSuiteDB, a product of Tyrian Diagnostics Ltd formerly Proteome Systems Ltd, will be safely archived by the Swiss Institute of Bioinformatics.  
        Thanks to on-going collaborations the glycan database will continue to be publicly accessible.</p>
      </div>

      <div class='span4 sidebar'>
        <ul class='nav nav-pills nav-stacked clearfix'>
          <h3>Contacts</h3>
          <li>
            <a href='mailto:nicki.packer&#64;mq.edu.au'>
              Prof. Nicki Packer; Director, Biomolecular Frontiers Research Centre, Macquarie University, Sydney 
            </a>
	    <a href='mailto:matthew.campbell&#64;mq.edu.au'>
	      Matthew Campbell,  Biomolecular Frontiers Research Centre, Macquarie University, Sydney
	    </a>
            <a href='mailto:frederique.lisacek&#64;isb-sib.ch'>
	      Frederique Lisacek, Swiss Institute for Bioinformatics
	    </a>
          </li>
        </ul>
	<div class="info clearfix">
          <h3>Social Connections</h3>
          <img src='/assets/images/qr/homeqr.png' alt='' /><br/>
	  	  <a href="../images/Macquarie_BiFoldBrochure_v1_HR.pdf">Check out our latest brochure</a><br/><br/>
          <a href="https://twitter.com/unicarbkb" class="twitter-follow-button" data-show-count="false">Follow unicarbkb</a>
          <script>!function(d,s,id)"""),format.raw/*74.36*/("""{"""),format.raw/*74.37*/("""var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id))"""),format.raw/*74.102*/("""{"""),format.raw/*74.103*/("""js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);"""),format.raw/*74.213*/("""}"""),format.raw/*74.214*/("""}"""),format.raw/*74.215*/("""(document,"script","twitter-wjs");</script>
          <!-- Place this tag where you want the +1 button to render -->
          <br><g:plusone size="medium" annotation="none"></g:plusone>

          <!-- Place this render call where appropriate -->
          <script type="text/javascript">
            (function() """),format.raw/*80.25*/("""{"""),format.raw/*80.26*/("""
              """),format.raw/*81.15*/("""var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/plusone.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
              """),format.raw/*84.15*/("""}"""),format.raw/*84.16*/(""")();
          </script>
          """),format.raw/*86.51*/("""
        """),format.raw/*87.9*/("""</div>
      </div>
    </div>
    
    <div class="row-fluid logos">
      <div class='span3 nectar'><a href="http://www.nectar.org.au"><img src='/assets/img/NeCTAR_Logo.png' alt='' /></a></div>
      <div class='span3'><a href="http://www.ands.org.au"><img src='/assets/img/ANDS_Logo.png' alt='' /></a></div>
      <div class='span3 stint'><a href="http://www.stint.se"><img src='/assets/img/STINT_Logo.png' alt='' /></a></div>
      <div class='span3'><a href="http://expasy.org/"><img src='/assets/img/SIB_Logo.png' alt='' /></a></div>
    </div>

    """),_display_(/*98.6*/views/*98.11*/.html.footerunicarb.footerunicarb()),format.raw/*98.46*/("""
  
  """),format.raw/*100.3*/("""</div>

</html>	
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object news extends news_Scope0.news
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/news.scala.html
                  HASH: 8120a5c0841325e11a110a703d06f7eb38275cde
                  MATRIX: 825->0|991->140|1005->146|1083->203|1177->271|1191->277|1265->330|1322->361|1336->367|1395->406|1472->457|1486->463|1545->502|4746->3675|4775->3676|4869->3741|4899->3742|5038->3852|5068->3853|5098->3854|5440->4168|5469->4169|5512->4184|5807->4451|5836->4452|5899->4527|5935->4536|6518->5093|6532->5098|6588->5133|6622->5139
                  LINES: 32->1|36->5|36->5|36->5|37->6|37->6|37->6|38->7|38->7|38->7|39->8|39->8|39->8|105->74|105->74|105->74|105->74|105->74|105->74|105->74|111->80|111->80|112->81|115->84|115->84|117->86|118->87|129->98|129->98|129->98|131->100
                  -- GENERATED --
              */
          