
package views.html.formdata

import play.twirl.api._


     object uploadGssSettings_Scope0 {
import controllers._
import views.html._

class uploadGssSettings extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*2.2*/helper/*2.8*/.form(action = routes.Glycospectrumscan.gssCore, 'method->"POST", 'enctype -> "multipart/form-data")/*2.108*/ {_display_(Seq[Any](format.raw/*2.110*/("""


"""),format.raw/*5.1*/("""<div class="well">
    <fieldset>
        <legend>Min Settings (for now)!</legend>
        <div style="position:relative;">
            UniProt Accession: <input type="text" name="uniprot"> -- or -- Paste single sequence: <input type="text" name="uniprot_sequence">
            <br/>
            <a>Choose File... <input type="file" name="fastafile" multiple> </a>
            &nbsp;
            <span class='label label-info' id="upload-file-info"></span>
            <br/>
	    Set mass/charge: <select name="massCharge">
	    <option value="M-H">M-H</option>
            <option value="M">M</option>
            <option value="M+H">M+H</option>
            <option value="M+2H">M+2H</option>
            <option value="M+3H">M+3H</option>
            </select>
            <br/>
            Set charge: <select name="charge">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
        </select>
	    <br/>
            Set missed cleavages: <select name="cleavage">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
            <br/>
            <label><input type="checkbox" value ="1" name="accumulate" class="checkbox" checked="checked">Accumulate</label>
            <label><input type="checkbox" value ="0" name="accumulate" class="checkbox">Disable Accumulate</label>
            <br/>
            <br/>
            Window: <input type="text" name="window">
            <br/>
            Intensity threshold: <input type="text" name="threshold">
            <br/>
            Enzyme: <select name="enzyme">
            <option value="ProteinSequence_Trypsin">Trypsin</option>
            <option value="ProteinSequence_Trypsin_H">Trypsin_H</option>
            <option value="ProteinSequence_LYS_C">LYS_C</option>
            <option value="ProteinSequence_LYS_N">LYS_N"</option>
            <option value="ProteinSequence_CNBR">CNBR</option>
            <option value="ProteinSequence_ARGC">ARGC</option>
            <option value="ProteinSequence_ASPN">ASPN</option>
            <option value="ProteinSequence_ASPN_LYSC">ASPN_LYSC</option>
            <option value="ProteinSequence_ASPN_NTERMINA_GLU">ASPN_NTERMINA_GLU</option>
            <option value="ProteinSequence_ASPN_GLUC">ASPN_GLUC</option>
            <option value="ProteinSequence_GLU_C_BICARBONATE">GLU_C_BICARBONATE</option>
            <option value="ProteinSequence_Glu_C_P">Glu_C_P</option>
            <option value="ProteinSequence_CHYMOTRYPSIN">CHYMOTRYPSIN</option>
            <option value="ProteinSequence_CHYMOTRYPSIN_FYW">CHYMOTRYPSIN_FYW</option>
            <option value="ProteinSequence_GLU_C_P_LYS_C">GLU_C_P_LYS_C</option>
            <option value="ProteinSequence_MICROWAVE_A_F_A_H">MICROWAVE_A_F_A_H</option>
            <option value="ProteinSequence_TRYPSIN_CHYMOTRYPSIN">TRYPSIN_CHYMOTRYPSIN</option>
            <option value="ProteinSequence_PEPSIN">PEPSIN</option>
            <option value="ProteinSequence_PEPSIN_PH_2">PEPSIN_PH_2</option>
            <option value="ProteinSequence_THERMOLYSIN">THERMOLYSIN</option>
            <option value="ProteinSequence_Trypsin_even_before_P">Trypsin_even_before_P</option>
            <option value="ProteinSequence_Proteinase_K">Proteinase_K</option>
        </select>
            <br/>




            <button id="submit" type="submit" value="Submit" class="btn btn-primary">Submit</button>
        </div>
    </fieldset>
</div>

""")))}),format.raw/*82.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object uploadGssSettings extends uploadGssSettings_Scope0.uploadGssSettings
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/formdata/uploadGssSettings.scala.html
                  HASH: 1f3c8f3b24a5e788668ee3bdbdbe8652bcb52a9d
                  MATRIX: 860->2|873->8|982->108|1022->110|1051->113|4828->3860
                  LINES: 32->2|32->2|32->2|32->2|35->5|112->82
                  -- GENERATED --
              */
          