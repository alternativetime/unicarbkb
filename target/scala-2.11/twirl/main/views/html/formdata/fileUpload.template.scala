
package views.html.formdata

import play.twirl.api._


     object fileUpload_Scope0 {
import controllers._
import views.html._

class fileUpload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /* <link rel="stylesheet" type="text/css" media="screen" href="@routes.Assets.at(" stylesheets/fileinput.min.css")"> */
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*4.2*/helper/*4.8*/.form(action = routes.Glycospectrumscan.glycanFileUpload, 'method->"POST", 'enctype -> "multipart/form-data")/*4.117*/ {_display_(Seq[Any](format.raw/*4.119*/("""


"""),format.raw/*7.1*/("""<div class="well">
    <fieldset>
        <legend>File Upload</legend>
        <div style="position:relative;">
            <a>
                Choose File...


                <input type="file" name="glycanfile" multiple>


            </a>
            &nbsp;
            <span class='label label-info' id="upload-file-info"></span>
            <button id="submit" type="submit" value="Submit" class="btn btn-primary">Submit</button>
        </div>
    </fieldset>
</div>

""")))}),format.raw/*26.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/* <link rel="stylesheet" type="text/css" media="screen" href="@routes.Assets.at(" stylesheets/fileinput.min.css")"> */
object fileUpload extends fileUpload_Scope0.fileUpload
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/formdata/fileUpload.scala.html
                  HASH: 6bc77c12d2990eba1166c106fa3c74d28cf460f9
                  MATRIX: 961->123|974->129|1092->238|1132->240|1161->243|1667->719
                  LINES: 32->4|32->4|32->4|32->4|35->7|54->26
                  -- GENERATED --
              */
          