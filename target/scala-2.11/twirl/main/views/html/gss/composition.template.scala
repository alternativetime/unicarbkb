
package views.html.gss

import play.twirl.api._


     object composition_Scope0 {
import controllers._
import views.html._

class composition extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.4*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""


"""),format.raw/*6.1*/("""<link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*6.62*/routes/*6.68*/.Assets.versioned("assets/stylesheets/bootstrap-editable.css")),format.raw/*6.130*/("""">
<script src=""""),_display_(/*7.15*/routes/*7.21*/.Assets.versioned("assets/javascripts/bootstrap-editable.js")),format.raw/*7.82*/(""""></script>


<div style="margin: 5px">
    This is the <span pk="1" id="username1" data-showbuttons="true" data-mode="popup" data-placement="right">awesome</span> user.<br>
    <table class="table table-striped">
        <thead><tr><th>Name</th><th>Value</th><th>Comments</th></tr></thead>
        <tbody>
        <tr><td>Parm 1</td><td><span class="edit">Value 1</span></td><td><span class="edit">Editable coments about the field</span></td></tr>
        <tr><td>Parm 2</td><td><span class="edit">Value 2</span></td><td>Some coments about the field. Errors maybe.</td></tr>
        <tr><td>Parm 3</td><td><span class="edit">Value 3</span></td><td>Errors maybe.</td></tr>
        <tr><td>Parm 4</td><td><span class="edit">Value 4</span></td><td>Some coments about the field</td></tr>
        </tbody>
    </table>
    <br><a href="#" class="btn" id="savebtn">Save</a> <a href="#" class="btn" id="resetbtn">Reset</a>
</div>



""")))}),format.raw/*26.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object composition extends composition_Scope0.composition
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/gss/composition.scala.html
                  HASH: 99ce0913d42981bf248ece69acfdf5b88f21ab47
                  MATRIX: 754->1|850->3|878->6|889->10|926->11|955->14|1042->75|1056->81|1139->143|1182->160|1196->166|1277->227|2235->1155
                  LINES: 27->1|32->1|34->3|34->3|34->3|37->6|37->6|37->6|37->6|38->7|38->7|38->7|57->26
                  -- GENERATED --
              */
          