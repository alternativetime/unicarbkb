
package views.html.gss

import play.twirl.api._


     object gssResults_Scope0 {
import java.util._

import views.html._

import scala.collection.JavaConversions._

class gssResults extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[Integer,Integer,List[org.unicarbkb.gss.Amino_acids.glycans],List[String],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment],play.twirl.api.HtmlFormat.Appendable] {

  /* (charge: Integer, glycanss: List[org.unicarbkb.gss.Amino_acids.glycans], file: List[String], charge1: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge2: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge3: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge4: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge5: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge6: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge7: List[org.unicarbkb.gss.Amino_acids.GPAssignment],
accumulate0: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate1: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate2: List[org.unicarbkb.gss.Amino_acids.GPAssignment]) */
  def apply/*4.2*/(cleavage: Integer, charge: Integer, glycanss: List[org.unicarbkb.gss.Amino_acids.glycans], file: List[String], charge1: List[org.unicarbkb.gss.Amino_acids.GPAssignment],
accumulate0: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate1: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate2: List[org.unicarbkb.gss.Amino_acids.GPAssignment]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*5.189*/("""

"""),_display_(/*7.2*/main/*7.6*/{_display_(Seq[Any](format.raw/*7.7*/("""

"""),format.raw/*9.1*/("""<script src="http://wenzhixin.net.cn/p/bootstrap-table/src/extensions/filter/bootstrap-table-filter.js"></script>

    <h1>Some Results</h1>
    <p>Automatically we have created result files for charge state 1 - 6 </p>

    <table class="table table-condensed table-bordered table-striped volumes">
        <thead>
        <tr>
        <th>Number</th>
        <th>Hexose</th>
        <th>HexNAc</th>
        <th>Deoxy</th>
        <th>NeuAc</th>
        <th>NeuGc</th>
        <th>Pentose</th>
        <th>Phosphate</th>
        <th>Sulphate</th>
    </tr>
    </thead>
    <tbody>

     """),_display_(/*30.7*/for((g,l) <- glycanss.zipWithIndex) yield /*30.42*/{_display_(Seq[Any](format.raw/*30.43*/("""
        """),format.raw/*31.9*/("""<tr>
            <td>"""),_display_(/*32.18*/(l+1)),format.raw/*32.23*/("""</td>
            <td>"""),_display_(/*33.18*/g/*33.19*/.Hexose),format.raw/*33.26*/("""</td>
            <td>"""),_display_(/*34.18*/g/*34.19*/.HexNAc),format.raw/*34.26*/("""</td>
            <td>"""),_display_(/*35.18*/g/*35.19*/.Deoxyhexose),format.raw/*35.31*/("""</td>
            <td>"""),_display_(/*36.18*/g/*36.19*/.NeuAc),format.raw/*36.25*/("""</td>
            <td>"""),_display_(/*37.18*/g/*37.19*/.NeuGc),format.raw/*37.25*/("""</td>
            <td>"""),_display_(/*38.18*/g/*38.19*/.Pentose),format.raw/*38.27*/("""</td>
            <td>"""),_display_(/*39.18*/g/*39.19*/.Phosphate),format.raw/*39.29*/("""</td>
            <td>"""),_display_(/*40.18*/g/*40.19*/.Sulphate),format.raw/*40.28*/("""</td>
        </tr>
        """)))}),format.raw/*42.10*/("""
     """),format.raw/*43.6*/("""</tbody>
     </table>

    """),format.raw/*53.13*/("""



"""),format.raw/*57.1*/("""<div class="row-fluid">
    <div class="span8">
        <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filter table..." />
        <ul class="nav nav-tabs" id="product-table">
            <li class="active"><a href="#1" data-toggle="tab">Charge """),_display_(/*61.71*/charge),format.raw/*61.77*/(""" """),format.raw/*61.78*/("""with MC """),_display_(/*61.87*/cleavage),format.raw/*61.95*/("""</a></li>
            """),format.raw/*67.68*/("""
            """),format.raw/*68.13*/("""<li><a href="#8" data-toggle="tab">Combine MC0</a></li>
            <li><a href="#9" data-toggle="tab">Combine MC1</a></li>
            <li><a href="#10" data-toggle="tab">Combine MC2</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="1">

                <h3>Charge """),_display_(/*75.29*/charge),format.raw/*75.35*/("""</h3>
                <table class="table table-condensed table-bordered table-striped volumes order-table" data-click-to-select="true">
                    <thead>
                    <tr>
                        <th>GP Sequence</th>
                        <th>Glycan Combination</th>
                        <th>Average Mass</th>
                        <th>Mono Mass</th>
                        <th>Expt. Mass</th>
                        <th>Average Diff Mass</th>
                        <th>Mono Diff Mass</th>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*90.22*/for(c1 <- charge1) yield /*90.40*/{_display_(Seq[Any](format.raw/*90.41*/("""
                    """),format.raw/*91.21*/("""<tr>
                        <td>"""),_display_(/*92.30*/c1/*92.32*/.glycoproteinSequence),format.raw/*92.53*/("""</td>
                        <td>"""),_display_(/*93.30*/c1/*93.32*/.glycanOccupancy),format.raw/*93.48*/("""</td>
                        <td>"""),_display_(/*94.30*/("%.2f".format(c1.averageMass))),format.raw/*94.61*/("""</td>
                        <td>"""),_display_(/*95.30*/("%.2f".format(c1.monoMass))),format.raw/*95.58*/("""</td>
                        <td>"""),_display_(/*96.30*/("%.2f".format(c1.experimentalMass))),format.raw/*96.66*/("""</td>
                        <td>"""),_display_(/*97.30*/("%.2f".format(c1.experimentalMass.toDouble - c1.averageMass.toDouble))),format.raw/*97.101*/("""</td>
                        <td>"""),_display_(/*98.30*/("%.2f".format(c1.experimentalMass.toDouble - c1.monoMass.toDouble))),format.raw/*98.98*/("""</td>
                    </tr>
                    """)))}),format.raw/*100.22*/("""
                    """),format.raw/*101.21*/("""</tbody>
                </table>
            </div>
            """),format.raw/*282.15*/("""
            """),format.raw/*283.13*/("""<div class="tab-pane" id="8">

                <h3>Accum MC0</h3>
                <table class="table table-condensed table-bordered table-striped volumes order-table">
                    <thead>
                    <tr>
                        <th>GP Sequence</th>
                        <th>Glycan Combination</th>
                        <th>Average Mass</th>
                        <th>Mono Mass</th>
                        <th>Expt. Mass</th>
                        <th>Average Diff Mass</th>
                        <th>Mono Diff Mass</th>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*300.22*/for(a0 <- accumulate0) yield /*300.44*/{_display_(Seq[Any](format.raw/*300.45*/("""
                    """),format.raw/*301.21*/("""<tr>
                        <td>"""),_display_(/*302.30*/a0/*302.32*/.glycoproteinSequence),format.raw/*302.53*/("""</td>
                        <td>"""),_display_(/*303.30*/a0/*303.32*/.glycanOccupancy),format.raw/*303.48*/("""</td>
                        <td>"""),_display_(/*304.30*/("%.2f".format(a0.averageMass))),format.raw/*304.61*/("""</td>
                        <td>"""),_display_(/*305.30*/("%.2f".format(a0.monoMass))),format.raw/*305.58*/("""</td>
                        <td>"""),_display_(/*306.30*/("%.2f".format(a0.experimentalMass))),format.raw/*306.66*/("""</td>
                        <td>"""),_display_(/*307.30*/("%.2f".format(a0.experimentalMass.toDouble - a0.averageMass.toDouble))),format.raw/*307.101*/("""</td>
                        <td>"""),_display_(/*308.30*/("%.2f".format(a0.experimentalMass.toDouble - a0.monoMass.toDouble))),format.raw/*308.98*/("""</td>
                    </tr>
                    """)))}),format.raw/*310.22*/("""
                    """),format.raw/*311.21*/("""</tbody>
                </table>
            </div>

            <div class="tab-pane" id="9">

                <h3>Accum MC1</h3>
                <table class="table table-condensed table-bordered table-striped volumes order-table">
                    <thead>
                    <tr>
                        <th>GP Sequence</th>
                        <th>Glycan Combination</th>
                        <th>Average Mass</th>
                        <th>Mono Mass</th>
                        <th>Expt. Mass</th>
                        <th>Average Diff Mass</th>
                        <th>Mono Diff Mass</th>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*332.22*/for(a1 <- accumulate1) yield /*332.44*/{_display_(Seq[Any](format.raw/*332.45*/("""
                    """),format.raw/*333.21*/("""<tr>
                        <td>"""),_display_(/*334.30*/a1/*334.32*/.glycoproteinSequence),format.raw/*334.53*/("""</td>
                        <td>"""),_display_(/*335.30*/a1/*335.32*/.glycanOccupancy),format.raw/*335.48*/("""</td>
                        <td>"""),_display_(/*336.30*/("%.2f".format(a1.averageMass))),format.raw/*336.61*/("""</td>
                        <td>"""),_display_(/*337.30*/("%.2f".format(a1.monoMass))),format.raw/*337.58*/("""</td>
                        <td>"""),_display_(/*338.30*/("%.2f".format(a1.experimentalMass))),format.raw/*338.66*/("""</td>
                        <td>"""),_display_(/*339.30*/("%.2f".format(a1.experimentalMass.toDouble - a1.averageMass.toDouble))),format.raw/*339.101*/("""</td>
                        <td>"""),_display_(/*340.30*/("%.2f".format(a1.experimentalMass.toDouble - a1.monoMass.toDouble))),format.raw/*340.98*/("""</td>
                    </tr>
                    """)))}),format.raw/*342.22*/("""
                    """),format.raw/*343.21*/("""</tbody>
                </table>
            </div>

            <div class="tab-pane" id="10">

                <h3>Accum MC2</h3>
                <table class="table table-condensed table-bordered table-striped volumes order-table">
                    <thead>
                    <tr>
                        <th>GP Sequence</th>
                        <th>Glycan Combination</th>
                        <th>Average Mass</th>
                        <th>Mono Mass</th>
                        <th>Expt. Mass</th>
                        <th>Average Diff Mass</th>
                        <th>Mono Diff Mass</th>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*364.22*/for(a2 <- accumulate2) yield /*364.44*/{_display_(Seq[Any](format.raw/*364.45*/("""
                    """),format.raw/*365.21*/("""<tr>
                        <td>"""),_display_(/*366.30*/a2/*366.32*/.glycoproteinSequence),format.raw/*366.53*/("""</td>
                        <td>"""),_display_(/*367.30*/a2/*367.32*/.glycanOccupancy),format.raw/*367.48*/("""</td>
                        <td>"""),_display_(/*368.30*/("%.2f".format(a2.averageMass))),format.raw/*368.61*/("""</td>
                        <td>"""),_display_(/*369.30*/("%.2f".format(a2.monoMass))),format.raw/*369.58*/("""</td>
                        <td>"""),_display_(/*370.30*/("%.2f".format(a2.experimentalMass))),format.raw/*370.66*/("""</td>
                        <td>"""),_display_(/*371.30*/("%.2f".format(a2.experimentalMass.toDouble - a2.averageMass.toDouble))),format.raw/*371.101*/("""</td>
                        <td>"""),_display_(/*372.30*/("%.2f".format(a2.experimentalMass.toDouble - a2.monoMass.toDouble))),format.raw/*372.98*/("""</td>
                    </tr>
                    """)))}),format.raw/*374.22*/("""
                    """),format.raw/*375.21*/("""</tbody>
                </table>
            </div>

        </div>
    </div>
</div>


<script>
    (function(document) """),format.raw/*385.25*/("""{"""),format.raw/*385.26*/("""
	"""),format.raw/*386.2*/("""'use strict';

	var LightTableFilter = (function(Arr) """),format.raw/*388.40*/("""{"""),format.raw/*388.41*/("""

		"""),format.raw/*390.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*392.29*/("""{"""),format.raw/*392.30*/("""
			"""),format.raw/*393.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) """),format.raw/*395.45*/("""{"""),format.raw/*395.46*/("""
				"""),format.raw/*396.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*396.53*/("""{"""),format.raw/*396.54*/("""
					"""),format.raw/*397.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*398.5*/("""}"""),format.raw/*398.6*/(""");
			"""),format.raw/*399.4*/("""}"""),format.raw/*399.5*/(""");
		"""),format.raw/*400.3*/("""}"""),format.raw/*400.4*/("""

		"""),format.raw/*402.3*/("""function _filter(row) """),format.raw/*402.25*/("""{"""),format.raw/*402.26*/("""
			"""),format.raw/*403.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*405.3*/("""}"""),format.raw/*405.4*/("""

		"""),format.raw/*407.3*/("""return """),format.raw/*407.10*/("""{"""),format.raw/*407.11*/("""
			"""),format.raw/*408.4*/("""init: function() """),format.raw/*408.21*/("""{"""),format.raw/*408.22*/("""
				"""),format.raw/*409.5*/("""var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) """),format.raw/*410.46*/("""{"""),format.raw/*410.47*/("""
					"""),format.raw/*411.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*412.5*/("""}"""),format.raw/*412.6*/(""");
			"""),format.raw/*413.4*/("""}"""),format.raw/*413.5*/("""
		"""),format.raw/*414.3*/("""}"""),format.raw/*414.4*/(""";
	"""),format.raw/*415.2*/("""}"""),format.raw/*415.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*417.59*/("""{"""),format.raw/*417.60*/("""
		"""),format.raw/*418.3*/("""if (document.readyState === 'complete') """),format.raw/*418.43*/("""{"""),format.raw/*418.44*/("""
			"""),format.raw/*419.4*/("""LightTableFilter.init();
		"""),format.raw/*420.3*/("""}"""),format.raw/*420.4*/("""
	"""),format.raw/*421.2*/("""}"""),format.raw/*421.3*/(""");

"""),format.raw/*423.1*/("""}"""),format.raw/*423.2*/(""")(document);

</script>

<script>
    (function(document) """),format.raw/*428.25*/("""{"""),format.raw/*428.26*/("""
	"""),format.raw/*429.2*/("""'use strict';

	var LightTableFilter2 = (function(Arr) """),format.raw/*431.41*/("""{"""),format.raw/*431.42*/("""

		"""),format.raw/*433.3*/("""var _input;

		function _onInputEvent(e) """),format.raw/*435.29*/("""{"""),format.raw/*435.30*/("""
			"""),format.raw/*436.4*/("""_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table2'));
			Arr.forEach.call(tables, function(table) """),format.raw/*438.45*/("""{"""),format.raw/*438.46*/("""
				"""),format.raw/*439.5*/("""Arr.forEach.call(table.tBodies, function(tbody) """),format.raw/*439.53*/("""{"""),format.raw/*439.54*/("""
					"""),format.raw/*440.6*/("""Arr.forEach.call(tbody.rows, _filter);
				"""),format.raw/*441.5*/("""}"""),format.raw/*441.6*/(""");
			"""),format.raw/*442.4*/("""}"""),format.raw/*442.5*/(""");
		"""),format.raw/*443.3*/("""}"""),format.raw/*443.4*/("""

		"""),format.raw/*445.3*/("""function _filter(row) """),format.raw/*445.25*/("""{"""),format.raw/*445.26*/("""
			"""),format.raw/*446.4*/("""var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		"""),format.raw/*448.3*/("""}"""),format.raw/*448.4*/("""

		"""),format.raw/*450.3*/("""return """),format.raw/*450.10*/("""{"""),format.raw/*450.11*/("""
			"""),format.raw/*451.4*/("""init: function() """),format.raw/*451.21*/("""{"""),format.raw/*451.22*/("""
				"""),format.raw/*452.5*/("""var inputs = document.getElementsByClassName('light-table-filter2');
				Arr.forEach.call(inputs, function(input) """),format.raw/*453.46*/("""{"""),format.raw/*453.47*/("""
					"""),format.raw/*454.6*/("""input.oninput = _onInputEvent;
				"""),format.raw/*455.5*/("""}"""),format.raw/*455.6*/(""");
			"""),format.raw/*456.4*/("""}"""),format.raw/*456.5*/("""
		"""),format.raw/*457.3*/("""}"""),format.raw/*457.4*/(""";
	"""),format.raw/*458.2*/("""}"""),format.raw/*458.3*/(""")(Array.prototype);

	document.addEventListener('readystatechange', function() """),format.raw/*460.59*/("""{"""),format.raw/*460.60*/("""
		"""),format.raw/*461.3*/("""if (document.readyState === 'complete') """),format.raw/*461.43*/("""{"""),format.raw/*461.44*/("""
			"""),format.raw/*462.4*/("""LightTableFilter2.init();
		"""),format.raw/*463.3*/("""}"""),format.raw/*463.4*/("""
	"""),format.raw/*464.2*/("""}"""),format.raw/*464.3*/(""");

"""),format.raw/*466.1*/("""}"""),format.raw/*466.2*/(""")(document);

</script>

""")))}))
      }
    }
  }

  def render(cleavage:Integer,charge:Integer,glycanss:List[org.unicarbkb.gss.Amino_acids.glycans],file:List[String],charge1:List[org.unicarbkb.gss.Amino_acids.GPAssignment],accumulate0:List[org.unicarbkb.gss.Amino_acids.GPAssignment],accumulate1:List[org.unicarbkb.gss.Amino_acids.GPAssignment],accumulate2:List[org.unicarbkb.gss.Amino_acids.GPAssignment]): play.twirl.api.HtmlFormat.Appendable = apply(cleavage,charge,glycanss,file,charge1,accumulate0,accumulate1,accumulate2)

  def f:((Integer,Integer,List[org.unicarbkb.gss.Amino_acids.glycans],List[String],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment],List[org.unicarbkb.gss.Amino_acids.GPAssignment]) => play.twirl.api.HtmlFormat.Appendable) = (cleavage,charge,glycanss,file,charge1,accumulate0,accumulate1,accumulate2) => apply(cleavage,charge,glycanss,file,charge1,accumulate0,accumulate1,accumulate2)

  def ref: this.type = this

}


}

/* (charge: Integer, glycanss: List[org.unicarbkb.gss.Amino_acids.glycans], file: List[String], charge1: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge2: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge3: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge4: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge5: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge6: List[org.unicarbkb.gss.Amino_acids.GPAssignment], charge7: List[org.unicarbkb.gss.Amino_acids.GPAssignment],
accumulate0: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate1: List[org.unicarbkb.gss.Amino_acids.GPAssignment], accumulate2: List[org.unicarbkb.gss.Amino_acids.GPAssignment]) */
object gssResults extends gssResults_Scope0.gssResults
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/gss/gssResults.scala.html
                  HASH: 1a8cdd8c139a873b3215fb6f80572f5d0cbe219c
                  MATRIX: 1717->703|2172->1062|2200->1065|2211->1069|2248->1070|2276->1072|2891->1661|2942->1696|2981->1697|3017->1706|3066->1728|3092->1733|3142->1756|3152->1757|3180->1764|3230->1787|3240->1788|3268->1795|3318->1818|3328->1819|3361->1831|3411->1854|3421->1855|3448->1861|3498->1884|3508->1885|3535->1891|3585->1914|3595->1915|3624->1923|3674->1946|3684->1947|3715->1957|3765->1980|3775->1981|3805->1990|3865->2019|3898->2025|3954->2571|3985->2575|4297->2860|4324->2866|4353->2867|4389->2876|4418->2884|4468->3289|4509->3302|4855->3621|4882->3627|5533->4251|5567->4269|5606->4270|5655->4291|5716->4325|5727->4327|5769->4348|5831->4383|5842->4385|5879->4401|5941->4436|5993->4467|6055->4502|6104->4530|6166->4565|6223->4601|6285->4636|6378->4707|6440->4742|6529->4810|6614->4863|6664->4884|6758->12198|6800->12211|7484->12867|7523->12889|7563->12890|7613->12911|7675->12945|7687->12947|7730->12968|7793->13003|7805->13005|7843->13021|7906->13056|7959->13087|8022->13122|8072->13150|8135->13185|8193->13221|8256->13256|8350->13327|8413->13362|8503->13430|8588->13483|8638->13504|9388->14226|9427->14248|9467->14249|9517->14270|9579->14304|9591->14306|9634->14327|9697->14362|9709->14364|9747->14380|9810->14415|9863->14446|9926->14481|9976->14509|10039->14544|10097->14580|10160->14615|10254->14686|10317->14721|10407->14789|10492->14842|10542->14863|11293->15586|11332->15608|11372->15609|11422->15630|11484->15664|11496->15666|11539->15687|11602->15722|11614->15724|11652->15740|11715->15775|11768->15806|11831->15841|11881->15869|11944->15904|12002->15940|12065->15975|12159->16046|12222->16081|12312->16149|12397->16202|12447->16223|12598->16345|12628->16346|12658->16348|12741->16402|12771->16403|12803->16407|12873->16448|12903->16449|12935->16453|13111->16600|13141->16601|13174->16606|13251->16654|13281->16655|13315->16661|13386->16704|13415->16705|13449->16711|13478->16712|13511->16717|13540->16718|13572->16722|13623->16744|13653->16745|13685->16749|13863->16899|13892->16900|13924->16904|13960->16911|13990->16912|14022->16916|14068->16933|14098->16934|14131->16939|14273->17052|14303->17053|14337->17059|14400->17094|14429->17095|14463->17101|14492->17102|14523->17105|14552->17106|14583->17109|14612->17110|14720->17189|14750->17190|14781->17193|14850->17233|14880->17234|14912->17238|14967->17265|14996->17266|15026->17268|15055->17269|15087->17273|15116->17274|15203->17332|15233->17333|15263->17335|15347->17390|15377->17391|15409->17395|15479->17436|15509->17437|15541->17441|15718->17589|15748->17590|15781->17595|15858->17643|15888->17644|15922->17650|15993->17693|16022->17694|16056->17700|16085->17701|16118->17706|16147->17707|16179->17711|16230->17733|16260->17734|16292->17738|16470->17888|16499->17889|16531->17893|16567->17900|16597->17901|16629->17905|16675->17922|16705->17923|16738->17928|16881->18042|16911->18043|16945->18049|17008->18084|17037->18085|17071->18091|17100->18092|17131->18095|17160->18096|17191->18099|17220->18100|17328->18179|17358->18180|17389->18183|17458->18223|17488->18224|17520->18228|17576->18256|17605->18257|17635->18259|17664->18260|17696->18264|17725->18265
                  LINES: 28->4|34->5|36->7|36->7|36->7|38->9|59->30|59->30|59->30|60->31|61->32|61->32|62->33|62->33|62->33|63->34|63->34|63->34|64->35|64->35|64->35|65->36|65->36|65->36|66->37|66->37|66->37|67->38|67->38|67->38|68->39|68->39|68->39|69->40|69->40|69->40|71->42|72->43|75->53|79->57|83->61|83->61|83->61|83->61|83->61|84->67|85->68|92->75|92->75|107->90|107->90|107->90|108->91|109->92|109->92|109->92|110->93|110->93|110->93|111->94|111->94|112->95|112->95|113->96|113->96|114->97|114->97|115->98|115->98|117->100|118->101|121->282|122->283|139->300|139->300|139->300|140->301|141->302|141->302|141->302|142->303|142->303|142->303|143->304|143->304|144->305|144->305|145->306|145->306|146->307|146->307|147->308|147->308|149->310|150->311|171->332|171->332|171->332|172->333|173->334|173->334|173->334|174->335|174->335|174->335|175->336|175->336|176->337|176->337|177->338|177->338|178->339|178->339|179->340|179->340|181->342|182->343|203->364|203->364|203->364|204->365|205->366|205->366|205->366|206->367|206->367|206->367|207->368|207->368|208->369|208->369|209->370|209->370|210->371|210->371|211->372|211->372|213->374|214->375|224->385|224->385|225->386|227->388|227->388|229->390|231->392|231->392|232->393|234->395|234->395|235->396|235->396|235->396|236->397|237->398|237->398|238->399|238->399|239->400|239->400|241->402|241->402|241->402|242->403|244->405|244->405|246->407|246->407|246->407|247->408|247->408|247->408|248->409|249->410|249->410|250->411|251->412|251->412|252->413|252->413|253->414|253->414|254->415|254->415|256->417|256->417|257->418|257->418|257->418|258->419|259->420|259->420|260->421|260->421|262->423|262->423|267->428|267->428|268->429|270->431|270->431|272->433|274->435|274->435|275->436|277->438|277->438|278->439|278->439|278->439|279->440|280->441|280->441|281->442|281->442|282->443|282->443|284->445|284->445|284->445|285->446|287->448|287->448|289->450|289->450|289->450|290->451|290->451|290->451|291->452|292->453|292->453|293->454|294->455|294->455|295->456|295->456|296->457|296->457|297->458|297->458|299->460|299->460|300->461|300->461|300->461|301->462|302->463|302->463|303->464|303->464|305->466|305->466
                  -- GENERATED --
              */
          