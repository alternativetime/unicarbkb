
package views.html.gss

import play.twirl.api._


     object gssHome_Scope0 {
import views.html._

class gssHome extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(filename: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.20*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""

"""),format.raw/*5.1*/("""<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-th"></i> GSS<span class="divider"></span></li>
    <li class="active"><i class="icon-th"></i> Step 1<span class="divider"></span></li>
</ul>

<section id="structureLayout">
    <section id="layouts">

        <div class="page-header row-fluid">
            <h1>Start building GSS</h1>
        </div>

        """),_display_(/*18.10*/if(!filename.isEmpty() && !filename.matches("test"))/*18.62*/{_display_(Seq[Any](format.raw/*18.63*/("""
        """),format.raw/*19.9*/("""<p>Filename: """),_display_(/*19.23*/filename),format.raw/*19.31*/("""</p>
        <p>Next upload the peak list</p>
        """)))}),format.raw/*21.10*/("""

"""),_display_(/*23.2*/formdata/*23.10*/.fileUpload()),format.raw/*23.23*/("""

    """),format.raw/*25.5*/("""</section>
</section>

""")))}))
      }
    }
  }

  def render(filename:String): play.twirl.api.HtmlFormat.Appendable = apply(filename)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (filename) => apply(filename)

  def ref: this.type = this

}


}

/**/
object gssHome extends gssHome_Scope0.gssHome
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/gss/gssHome.scala.html
                  HASH: 406c5d1b002381e6a263c58057b4d7972f696cb6
                  MATRIX: 753->1|866->19|894->22|905->26|942->27|970->29|1465->497|1526->549|1565->550|1601->559|1642->573|1671->581|1757->636|1786->639|1803->647|1837->660|1870->666
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|49->18|49->18|49->18|50->19|50->19|50->19|52->21|54->23|54->23|54->23|56->25
                  -- GENERATED --
              */
          