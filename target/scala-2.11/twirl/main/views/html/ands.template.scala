
package views.html

import play.twirl.api._


     object ands_Scope0 {
import controllers._

class ands extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html class="about">
  <head>
    <title>UnICarbKB</title>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*5.66*/routes/*5.72*/.Assets.versioned("assets/stylesheets/bootstrap.min.css")),format.raw/*5.129*/("""">
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*6.66*/routes/*6.72*/.Assets.versioned("assets/stylesheets/unicarbkb.css")),format.raw/*6.125*/("""">
    <link rel="icon" href=""""),_display_(/*7.29*/routes/*7.35*/.Assets.versioned("assets/favicon.ico")),format.raw/*7.74*/("""" type="image/x-icon">
  </head>

  <header>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="/">UniCarbKB</a>
          <ul class="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/query">Search</a></li>
            <li><a href="/references">References</a></li>
            <li><a href="/builder">Glycan Builder</a></li>
          </ul>
          <div id="headersearch" class="pull-right">
            <ul class='nav'>
              <li class='active'><a href="/about">About</a></li>
              <li class='active'><a href="/about">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="container-fluid content">
    <div class="row-fluid info">
      <div class="span8 leftContent">
        <h3>About UniCarbKB</h3>
        <p>
          The rapidly expanding glycomics field (structure and function of sugars) is being increasingly recognized as an important component of life science research, for application in diagnosis of disease, preventive medicines (vaccines) and therapeutic drugs. In contrast to the genomics and proteomics fields, and despite several international initiatives, glycomics still lacks maintained, accessible, curated, integrated and comprehensive data collections that summarize the structure, characteristics, biological origin and potential function of carbohydrates.
        </p>
        <p>
          UniCarbKB is an initiative that aims to promote the creation of an online information storage and search platform for glycomics and glycobiology research. The knowledgebase will offer a freely accessible and information rich resource supported by querying interfaces, annotation technologies and the adoption of common standards to integrate structural, experimental and functional data. Through cross-referencing existing databases and information resources, the UniCarbKB framework endeavours to support the growth of glycobioinformatics and the dissemination of knowledge through the provision of an open and unified portal to encourage the sharing of data.
        </p>
        <p>
          The initiative aims to make the technology available to all glycomics researchers and other ‘omics’ disciplines to explore and integrate cross-disciplinary boundaries to gain new insights. Australia has a unique opportunity to become a global focal point for building and enabling a capability area in glycomics that integrates and leverages the technology and data produced by previous international and Australian infrastructure initiatives.
        </p>
	<h3>Acknowledgements</h3>
	<p>
	  The UniCarbKB team acknowledge the work and efforts of EUROCarbDB and GlycoSuiteDB including associated projects MonosaccharideDB, GlycoWorkBench, GlycanBuilder and GlycoBase. These actvities have enabled us to develop and provide the community with a growing resource for glycomics and glycoproteomic research. Furthermore, we welcome new collaborations with members from the JCCGDB, Rings and BCSDB databases.
	</p>
	<p>
	<h3>License</h3>
	<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">UniCarbKB</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.
	</p>
      </div>

      <div class='span4 sidebar'>
        <ul class='nav nav-pills nav-stacked clearfix'>
          <h3>Contacts</h3>
          <li>
            <a href='mailto:nicki.packer&#64;mq.edu.au'>
              Prof. Nicki Packer; Director, Biomolecular Frontiers Research Centre, Macquarie University, Sydney 
            </a>
	    <a href='mailto:matthew.campbell&#64;mq.edu.au'>
	      Matthew Campbell,  Biomolecular Frontiers Research Centre, Macquarie University, Sydney
	    </a>
            <a href='mailto:frederique.lisacek&#64;isb-sib.ch'>
	      Frederique Lisacek, Swiss Institute for Bioinformatics
	    </a>
          </li>
        </ul>
	<div class="info clearfix">
          <h3>Social Connections</h3>
          <img src='/assets/images/qr/homeqr.png' alt='' /><br/>
	  	  <a href="../images/Macquarie_BiFoldBrochure_v1_HR.pdf">Check out our latest brochure</a><br/><br/>
          <a href="https://twitter.com/unicarbkb" class="twitter-follow-button" data-show-count="false">Follow unicarbkb</a>
          <script>!function(d,s,id)"""),format.raw/*75.36*/("""{"""),format.raw/*75.37*/("""var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id))"""),format.raw/*75.102*/("""{"""),format.raw/*75.103*/("""js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);"""),format.raw/*75.213*/("""}"""),format.raw/*75.214*/("""}"""),format.raw/*75.215*/("""(document,"script","twitter-wjs");</script>
          <!-- Place this tag where you want the +1 button to render -->
          <br><g:plusone size="medium" annotation="none"></g:plusone>

          <!-- Place this render call where appropriate -->
          <script type="text/javascript">
            (function() """),format.raw/*81.25*/("""{"""),format.raw/*81.26*/("""
              """),format.raw/*82.15*/("""var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/plusone.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
              """),format.raw/*85.15*/("""}"""),format.raw/*85.16*/(""")();
          </script>
          """),format.raw/*87.51*/("""
        """),format.raw/*88.9*/("""</div>
      </div>
    </div>
    
    <div class="row-fluid logos">
      <div class='span3 nectar'><a href="http://www.nectar.org.au"><img src='/assets/img/NeCTAR_Logo.png' alt='' /></a></div>
      <div class='span3'><a href="http://www.ands.org.au"><img src='/assets/img/ANDS_Logo.png' alt='' /></a></div>
      <div class='span3 stint'><a href="http://www.stint.se"><img src='/assets/img/STINT_Logo.png' alt='' /></a></div>
      <div class='span3'><a href="http://expasy.org/"><img src='/assets/img/SIB_Logo.png' alt='' /></a></div>
    </div>

    """),_display_(/*99.6*/views/*99.11*/.html.footerunicarb.footerunicarb()),format.raw/*99.46*/("""
  
  """),format.raw/*101.3*/("""</div>

</html>	
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object ands extends ands_Scope0.ands
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:00 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/ands.scala.html
                  HASH: 5bac60ffff82acb3210f1cfda0845d978708e5b0
                  MATRIX: 825->0|991->140|1005->146|1083->203|1177->271|1191->277|1265->330|1322->361|1336->367|1395->406|6156->5139|6185->5140|6279->5205|6309->5206|6448->5316|6478->5317|6508->5318|6850->5632|6879->5633|6922->5648|7217->5915|7246->5916|7309->5991|7345->6000|7928->6557|7942->6562|7998->6597|8032->6603
                  LINES: 32->1|36->5|36->5|36->5|37->6|37->6|37->6|38->7|38->7|38->7|106->75|106->75|106->75|106->75|106->75|106->75|106->75|112->81|112->81|113->82|116->85|116->85|118->87|119->88|130->99|130->99|130->99|132->101
                  -- GENERATED --
              */
          