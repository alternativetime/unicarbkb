
package views.html

import play.twirl.api._


     object proteinsummary_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

     object proteinsummary_Scope1 {

class proteinsummary extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template19[String,String,String,ArrayList[Biolsource],List[Proteins],List[String],List[GsProteinStr2],HashSet[Reference],List[SitesReferences],String,ArrayList[String],List[GeneralSites],List[DefinedSites],String,String,List[com.avaje.ebean.SqlRow],List[composition_protein.CompSite],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(warning: String, proteinName: String, accession: String, biol: ArrayList[Biolsource], protein: List[Proteins], uniprot: List[String], gssites: List[GsProteinStr2], source: HashSet[Reference], description: List[SitesReferences], sequence: String, proteinMultiple: ArrayList[String], generalSites: List[GeneralSites], definedSites: List[DefinedSites], typeEntry: String, swissProtName: String, proteinsource:  List[com.avaje.ebean.SqlRow], compSite: List[composition_protein.CompSite], ptmComment: String, comment: String ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.524*/("""


"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""


"""),format.raw/*8.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="#"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-map-marker" ></i><a href="/proteins"> Protein</a> <span class="divider">></span></li>
  <li class="active" > """),_display_(/*11.25*/proteinName),format.raw/*11.36*/("""</li>
</ul>

<section id="layouts" class="proteinsummary">

  <div class="page-header row-fluid">
    <h1>Associated Structures</h1>
    <h4 class="subheader span8">Accession:
      """),_display_(/*19.8*/if(typeEntry.matches("not swiss prot"))/*19.47*/ {_display_(Seq[Any](format.raw/*19.49*/("""<a href="http://uniprot.org/uniprot/?query="""),_display_(/*19.93*/proteinName/*19.104*/.replaceAll(" ","+")),format.raw/*19.124*/("""+AND+reviewed:yes&sort=score">"""),_display_(/*19.155*/accession),format.raw/*19.164*/(""" """),format.raw/*19.165*/("""</a><span class="pull-right"><a href="http://uniprot.org/uniprot/?query="""),_display_(/*19.238*/proteinName/*19.249*/.replaceAll(" ","+")),format.raw/*19.269*/("""+AND+reviewed:yes&sort=score"><span class='label label-light'>UniProtKB/Swiss-Prot</span></a></span> """)))}),format.raw/*19.371*/("""
      """),_display_(/*20.8*/if(!typeEntry.matches("not swiss prot"))/*20.48*/ {_display_(Seq[Any](format.raw/*20.50*/("""<a href="http://uniprot.org/uniprot/"""),_display_(/*20.87*/accession),format.raw/*20.96*/("""">"""),_display_(/*20.99*/accession),format.raw/*20.108*/(""" """),format.raw/*20.109*/("""</a><span class="pull-right"><a href="http://uniprot.org/uniprot/"""),_display_(/*20.175*/accession),format.raw/*20.184*/(""""><span class='label label-light'>UniProtKB/Swiss-Prot Entry</span></a></span> """)))}),format.raw/*20.264*/("""
      """),format.raw/*21.7*/("""</h4>
  </div>


  <div class="row-fluid">
    <div class="span8">
      <div class='name'>
        """),format.raw/*31.11*/("""
      """),format.raw/*32.7*/("""</div>



      <div class='ptm-details'>
        <h3>UniProtKB/Swiss-Prot PTM Description</h3>
        """),_display_(/*38.10*/ptmComment),format.raw/*38.20*/("""
        """),format.raw/*41.6*/("""
	"""),_display_(/*42.3*/for(uniprot <- uniprot) yield /*42.26*/ {_display_(Seq[Any](format.raw/*42.28*/("""
        """),_display_(/*43.10*/if(uniprot.isEmpty())/*43.31*/ {_display_(Seq[Any](format.raw/*43.33*/("""  """),format.raw/*43.35*/("""<div class="alert alert-error">No UniProt PTM information available. Unable to query UniProt for this accession number(s).</div> """)))}),format.raw/*43.165*/("""
        """),_display_(/*44.10*/if(!uniprot.matches("No info"))/*44.41*/ {_display_(Seq[Any](format.raw/*44.43*/("""
        """),format.raw/*45.9*/("""<p>"""),_display_(/*45.13*/uniprot),format.raw/*45.20*/("""</p>""")))}),format.raw/*45.25*/("""
        """)))}),format.raw/*46.10*/("""
      """),format.raw/*47.7*/("""</div>



      <div class='glycosylation-sites'>
        """),_display_(/*52.10*/if(generalSites.size() > 0 )/*52.38*/ {_display_(Seq[Any](format.raw/*52.40*/("""
        """),format.raw/*53.9*/("""<h3>Glycosylation Sites</h3>
        """),_display_(/*54.10*/for(generalSites <- generalSites) yield /*54.43*/ {_display_(Seq[Any](format.raw/*54.45*/("""
        """),_display_(/*55.10*/if(generalSites.strSiteGeneral.size() > 0 )/*55.53*/ {_display_(Seq[Any](format.raw/*55.55*/("""
        """),format.raw/*56.9*/("""<table class="computers table table-striped">
          <thead>
            <tr>
              <th>Position</th>
              <th>Structures</th>
              <th>Description</th>
              <th>Evidence</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>"""),_display_(/*68.20*/generalSites/*68.32*/.glyco_aa_site.replace("N-", "ASN-").replace("Asn-", "ASN-").replace("S-", "SER-").replace("Ser-", "SER-").replace("T-", "THR-").replace("Thr-", "THR-").replace("ASASN-", "ASN-").replace("AsnAsn", "ASN-")),format.raw/*68.236*/("""</td>
              <td>
                """),format.raw/*74.24*/("""
		"""),format.raw/*75.3*/("""<a href="/proteinsite?position="""),_display_(/*75.35*/generalSites/*75.47*/.glyco_aa_site),format.raw/*75.61*/("""&protein="""),_display_(/*75.71*/generalSites/*75.83*/.swiss_prot),format.raw/*75.94*/("""&type=general">
                  <span class="label label-notice"><i class='icon-th icon-white'></i> 
                    Associated Structures: <b>"""),_display_(/*77.48*/generalSites/*77.60*/.strSiteGeneral.size()),format.raw/*77.82*/("""</b></span>
                </a>
              </td>
              <td><span class="label">global</span></td>
              <td>GlycoSuite</td>
            </tr>
          </tbody>
        </table>
        """)))})))})))}),format.raw/*85.12*/("""
      """),format.raw/*86.7*/("""</div>

      <div class='glycosylation-sites'>
        """),_display_(/*89.10*/if(definedSites.size() > 0 )/*89.38*/ {_display_(Seq[Any](format.raw/*89.40*/("""
        """),format.raw/*90.9*/("""<h5>Site-Specific Information</h5>
        <p>A number of glycan structures have been assigned to specific glycosylation sites</p>
        <table class="computers table table-striped">
          <thead>
            <tr>
              <th>Position</th>
              <th>Structures</th>
              <th>Description</th>
              <th>Evidence</th>
            </tr>
          </thead>

          <tbody>
            """),_display_(/*103.14*/for(definedSites <- definedSites) yield /*103.47*/ {_display_(Seq[Any](format.raw/*103.49*/("""
            """),format.raw/*104.13*/("""<tr>
              <td>"""),_display_(/*105.20*/definedSites/*105.32*/.amino_acid_position.replace("N-", "ASN-").replace("Asn-", "ASN-").replace("S-", "SER-").replace("Ser-", "SER-").replace("T-", "THR-").replace("Thr-", "THR-").replace("ASASN-", "ASN-").replace("AsnAsn", "ASN-")),format.raw/*105.242*/("""</td>
              <td>
		"""),format.raw/*111.24*/("""
		 """),format.raw/*112.4*/("""<a href="/proteinsite?position="""),_display_(/*112.36*/definedSites/*112.48*/.amino_acid_position),format.raw/*112.68*/("""&protein="""),_display_(/*112.78*/definedSites/*112.90*/.swiss_prot),format.raw/*112.101*/("""&type=defined">
                  <span class="label label-notice"><i class='icon-th icon-white'></i>
                    Associated Structures: <b>"""),_display_(/*114.48*/definedSites/*114.60*/.strSiteDefined.size()),format.raw/*114.82*/("""</b></span>
                </a>
              </td>
              <td><span class="label">Site specific</span></td>
              <td>GlycoSuite</td>
            </tr>
            """)))}),format.raw/*120.14*/("""
          """),format.raw/*121.11*/("""</tbody>
        </table>
        """)))}),format.raw/*123.10*/("""
      """),format.raw/*124.7*/("""</div>

        """),_display_(/*126.10*/if(compSite.size() > 1)/*126.33*/{_display_(Seq[Any](format.raw/*126.34*/("""
        """),format.raw/*127.9*/("""<div class='glycosylation-sites'>
            <div class="alert alert-info">Compositional data available: """),_display_(/*128.74*/compSite/*128.82*/.size()),format.raw/*128.89*/(""" """),format.raw/*128.90*/("""compositions reported:
            <p>"""),_display_(/*129.17*/for(cp <- compSite) yield /*129.36*/ {}),format.raw/*129.39*/(""" """),format.raw/*129.40*/("""</p>
                """),_display_(/*130.18*/compSite/*130.26*/.groupBy(_.aminoAcidPosition).map/*130.59*/{ case (y,z) =>_display_(Seq[Any](format.raw/*130.74*/(""" """),format.raw/*130.75*/("""<p>"""),_display_(/*130.79*/y),format.raw/*130.80*/("""
                """),_display_(/*131.18*/z/*131.19*/.map/*131.23*/{ a =>_display_(Seq[Any](format.raw/*131.29*/(""" """),_display_(/*131.31*/a/*131.32*/.composition)))}),format.raw/*131.45*/("""</p>
                    """)))}),format.raw/*132.22*/("""
            """),format.raw/*133.13*/("""</div>
        </div>
        """)))}),format.raw/*135.10*/("""

      """),format.raw/*137.7*/("""<div class='glycosylation-sites'>
        <h3>Glycan Structures</h3>
        <ul class="structures clearfix">

          """),_display_(/*141.12*/for(s <- proteinMultiple) yield /*141.37*/ {_display_(Seq[Any](format.raw/*141.39*/("""
          """),format.raw/*142.11*/("""<li>
             """),_display_(/*143.15*/views/*143.20*/.html.format.structure(s.toLong)),format.raw/*143.52*/("""
          """),format.raw/*144.11*/("""</li>
          """)))}),format.raw/*145.12*/("""
        """),format.raw/*146.9*/("""</ul>

      </div>



      <div class='notes'>
        <h3>Notes</h3>
        <p>Accompanying information </p>
        """),_display_(/*155.10*/comment),format.raw/*155.17*/("""<br/>
        """),_display_(/*156.10*/if(!description.isEmpty() )/*156.37*/ {_display_(Seq[Any](format.raw/*156.39*/("""
	"""),format.raw/*157.2*/("""<ol>
        """),_display_(/*158.10*/for(d <- description) yield /*158.31*/{_display_(Seq[Any](format.raw/*158.32*/("""
          """),format.raw/*159.11*/("""<li class="references">"""),_display_(/*159.35*/d/*159.36*/.glyco_aa_site.replace("N-", "ASN-").replace("Asn-", "ASN-").replace("S-", "SER-").replace("Ser-", "SER-").replace("T-", "THR-").replace("Thr-", "THR-").replace("ASASN-", "ASN-").replace("AsnAsn", "ASN-").replaceAll("(^, )", "").trim),format.raw/*159.269*/("""</li>
        """)))}),format.raw/*160.10*/("""
	"""),format.raw/*161.2*/("""</ol>""")))}),format.raw/*161.8*/("""
      """),format.raw/*162.7*/("""</div>

      """),format.raw/*170.15*/("""

    """),format.raw/*172.5*/("""</div><!-- /col -->

    <div class="span4 details">

      """),_display_(/*176.8*/views/*176.13*/.html.format.format()),format.raw/*176.34*/("""

      """),_display_(/*178.8*/if(sequence.length > 1)/*178.31*/ {_display_(Seq[Any](format.raw/*178.33*/("""
      """),format.raw/*179.7*/("""<div class="info">
        <h3>Sequence</h3>
        <div class="sequence">
          """),_display_(/*182.12*/sequence),format.raw/*182.20*/("""
        """),format.raw/*183.9*/("""</div>
      </div>""")))}),format.raw/*184.14*/("""

        """),format.raw/*186.9*/("""<div class='info'>
          <h3>Biological Associations</h4>
          <div class='source'>
            <a id='toggle-taxonomy'><span class='label label-important'><span class='icon-leaf icon-white'></span> Taxonomy ("""),_display_(/*189.127*/proteinsource/*189.140*/.size()),format.raw/*189.147*/(""") <span class="caret"></span></span></a>
            <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein (1) <span class="caret"></span></span></a>
            <a id='toggle-source'><span class='label label-success'><span class='icon-leaf icon-white'></span> Source <span class="caret"></span></span></a>
          </div>

          <div>
            <ul id='more-taxonomy'>
              <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Taxonomies</h3>
              """),_display_(/*197.16*/for(p <- proteinsource) yield /*197.39*/{_display_(Seq[Any](format.raw/*197.40*/("""
              """),format.raw/*198.15*/("""<li><span class='icon-tag icon-white'></span> <a href="/taxonomy/"""),_display_(/*198.81*/p/*198.82*/.get("taxonomy_id")),format.raw/*198.101*/("""">"""),_display_(/*198.104*/p/*198.105*/.get("species")),format.raw/*198.120*/("""</a> &gt; <a href="/tissuesummary/"""),_display_(/*198.155*/p/*198.156*/.get("tissue_id")),format.raw/*198.173*/("""">"""),_display_(/*198.176*/p/*198.177*/.get("system")),format.raw/*198.191*/("""</a> </li>
              """)))}),format.raw/*199.16*/("""
            """),format.raw/*200.13*/("""</ul>
            <ul id='more-protein'>
              <h3 id='less-protein'><span class='icon-map-marker icon-white'></span> Protein</h3>
		"""),_display_(/*203.4*/if(!typeEntry.matches("not swiss prot"))/*203.44*/ {_display_(Seq[Any](format.raw/*203.46*/("""
                 """),format.raw/*204.18*/("""<li><span class='icon-map-marker icon-white'></span> <a href='/proteinsummary/"""),_display_(/*204.97*/accession),format.raw/*204.106*/("""/annotated'>"""),_display_(/*204.119*/proteinName),format.raw/*204.130*/("""</a></li>
		""")))}),format.raw/*205.4*/("""
		"""),_display_(/*206.4*/if(typeEntry.matches("not swiss prot"))/*206.43*/ {_display_(Seq[Any](format.raw/*206.45*/("""
			"""),_display_(/*207.5*/for(p <- proteinsource) yield /*207.28*/{_display_(Seq[Any](format.raw/*207.29*/("""
		 	"""),format.raw/*208.5*/("""<li><span class='icon-map-marker icon-white'></span> <a href='/proteinsummary/"""),_display_(/*208.84*/proteinName),format.raw/*208.95*/("""/"""),_display_(/*208.97*/p/*208.98*/.get("species")),format.raw/*208.113*/("""'>"""),_display_(/*208.116*/proteinName),format.raw/*208.127*/("""</a></li>
			""")))}),format.raw/*209.5*/("""
		""")))}),format.raw/*210.4*/("""
            """),format.raw/*211.13*/("""</ul>
            <ul id='more-source'>
              <h3 id='less-source'><span class='icon-leaf icon-white'></span> Source</h3>
              """),_display_(/*214.16*/for(p <- proteinsource) yield /*214.39*/{_display_(Seq[Any](format.raw/*214.40*/("""
              """),format.raw/*215.15*/("""<li><span class='icon-leaf icon-white'></span> <a href='/tissuesummary/"""),_display_(/*215.87*/p/*215.88*/.get("tissue_id")),format.raw/*215.105*/("""'>"""),_display_(/*215.108*/p/*215.109*/.get("div1")),format.raw/*215.121*/(""" """),format.raw/*215.122*/("""> """),_display_(/*215.125*/p/*215.126*/.get("div2")),format.raw/*215.138*/(""" """),format.raw/*215.139*/("""> """),_display_(/*215.142*/p/*215.143*/.get("div3")),format.raw/*215.155*/(""" """),format.raw/*215.156*/("""> """),_display_(/*215.159*/p/*215.160*/.get("div4")),format.raw/*215.172*/("""</a></li>
             """)))}),format.raw/*216.15*/("""
            """),format.raw/*217.13*/("""</ul>
          </div>
        </div>


        <div class="info">
          <h3>References """),_display_(/*223.27*/if(source.size() > 5)/*223.48*/ {_display_(Seq[Any](format.raw/*223.50*/("""showing top 5""")))}),format.raw/*223.64*/(""" """),format.raw/*223.65*/("""<span id='show-references' class="label" style="font-size:16px"><a href="#4">"""),_display_(/*223.143*/source/*223.149*/.size()),format.raw/*223.156*/("""</a></span></h3>
          <ol>
            """),_display_(/*225.14*/for((source,i) <- source.zipWithIndex; if (i < 5)) yield /*225.64*/ {_display_(Seq[Any](format.raw/*225.66*/("""
            """),format.raw/*226.13*/("""<li class="references">
              <p class="title"><a name='"""),_display_(/*227.42*/i),format.raw/*227.43*/("""' href='/references/"""),_display_(/*227.64*/source/*227.70*/.id),format.raw/*227.73*/("""'>"""),_display_(/*227.76*/source/*227.82*/.title),format.raw/*227.88*/("""</a></p>
              <p class="author">"""),_display_(/*228.34*/source/*228.40*/.authors),format.raw/*228.48*/("""</p>
              <p class="ref">PubMed: <a href='http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*229.83*/source/*229.89*/.pmid),format.raw/*229.94*/("""'>"""),_display_(/*229.97*/source/*229.103*/.pmid),format.raw/*229.108*/("""</a> Year: """),_display_(/*229.120*/source/*229.126*/.year),format.raw/*229.131*/("""</p>
            </li>
            """)))}),format.raw/*231.14*/("""
            """),format.raw/*232.13*/("""<div id='more-references'>
              """),_display_(/*233.16*/for((source,i) <- source.zipWithIndex; if (i > 5)) yield /*233.66*/ {_display_(Seq[Any](format.raw/*233.68*/("""
              """),format.raw/*234.15*/("""<li class="references">
                <p class="title"><a name='"""),_display_(/*235.44*/i),format.raw/*235.45*/("""' href='/references/"""),_display_(/*235.66*/source/*235.72*/.id),format.raw/*235.75*/("""'>"""),_display_(/*235.78*/source/*235.84*/.title),format.raw/*235.90*/("""</a></p>
                <p class="author">"""),_display_(/*236.36*/source/*236.42*/.authors),format.raw/*236.50*/("""</p>
                <p class="ref">PubMed: <a href='http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*237.85*/source/*237.91*/.pmid),format.raw/*237.96*/("""'>"""),_display_(/*237.99*/source/*237.105*/.pmid),format.raw/*237.110*/("""</a> Year: """),_display_(/*237.122*/source/*237.128*/.year),format.raw/*237.133*/("""</p>
              </li>
              """)))}),format.raw/*239.16*/("""
            """),format.raw/*240.13*/("""</div>
            """),_display_(/*241.14*/if(source.size() > 5)/*241.35*/ {_display_(Seq[Any](format.raw/*241.37*/("""
            """),format.raw/*242.13*/("""<div id='show-more-references' class='more-structures' href='#'>
	      <span class="linktext">See more references</span>
      	      <span class="linktext" style="display:none">See less references</span>
              <br />
              <span>&#9679; &#9679; &#9679;</span>
            </div>
            """)))}),format.raw/*248.14*/("""


          """),format.raw/*251.11*/("""</ol>
        </div>

      </div><!-- /col -->
    </div><!-- /row -->

    """),_display_(/*257.6*/views/*257.11*/.html.footerunicarb.footerunicarb()),format.raw/*257.46*/("""

  """),format.raw/*259.3*/("""</section>

  """)))}),format.raw/*261.4*/("""
"""))
      }
    }
  }

  def render(warning:String,proteinName:String,accession:String,biol:ArrayList[Biolsource],protein:List[Proteins],uniprot:List[String],gssites:List[GsProteinStr2],source:HashSet[Reference],description:List[SitesReferences],sequence:String,proteinMultiple:ArrayList[String],generalSites:List[GeneralSites],definedSites:List[DefinedSites],typeEntry:String,swissProtName:String,proteinsource:List[com.avaje.ebean.SqlRow],compSite:List[composition_protein.CompSite],ptmComment:String,comment:String): play.twirl.api.HtmlFormat.Appendable = apply(warning,proteinName,accession,biol,protein,uniprot,gssites,source,description,sequence,proteinMultiple,generalSites,definedSites,typeEntry,swissProtName,proteinsource,compSite,ptmComment,comment)

  def f:((String,String,String,ArrayList[Biolsource],List[Proteins],List[String],List[GsProteinStr2],HashSet[Reference],List[SitesReferences],String,ArrayList[String],List[GeneralSites],List[DefinedSites],String,String,List[com.avaje.ebean.SqlRow],List[composition_protein.CompSite],String,String) => play.twirl.api.HtmlFormat.Appendable) = (warning,proteinName,accession,biol,protein,uniprot,gssites,source,description,sequence,proteinMultiple,generalSites,definedSites,typeEntry,swissProtName,proteinsource,compSite,ptmComment,comment) => apply(warning,proteinName,accession,biol,protein,uniprot,gssites,source,description,sequence,proteinMultiple,generalSites,definedSites,typeEntry,swissProtName,proteinsource,compSite,ptmComment,comment)

  def ref: this.type = this

}


}
}

/**/
object proteinsummary extends proteinsummary_Scope0.proteinsummary_Scope1.proteinsummary
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:02 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/proteinsummary.scala.html
                  HASH: 94edd6c7ea095922500e34510c351efb9de1767b
                  MATRIX: 1145->66|1763->588|1792->592|1803->596|1841->598|1870->601|2147->851|2179->862|2388->1045|2436->1084|2476->1086|2547->1130|2568->1141|2610->1161|2669->1192|2700->1201|2730->1202|2831->1275|2852->1286|2894->1306|3028->1408|3062->1416|3111->1456|3151->1458|3215->1495|3245->1504|3275->1507|3306->1516|3336->1517|3430->1583|3461->1592|3573->1672|3607->1679|3735->1907|3769->1914|3901->2019|3932->2029|3968->2170|3997->2173|4036->2196|4076->2198|4113->2208|4143->2229|4183->2231|4213->2233|4375->2363|4412->2373|4452->2404|4492->2406|4528->2415|4559->2419|4587->2426|4623->2431|4664->2441|4698->2448|4784->2507|4821->2535|4861->2537|4897->2546|4962->2584|5011->2617|5051->2619|5088->2629|5140->2672|5180->2674|5216->2683|5549->2989|5570->3001|5796->3205|5865->3618|5895->3621|5954->3653|5975->3665|6010->3679|6047->3689|6068->3701|6100->3712|6277->3862|6298->3874|6341->3896|6587->4105|6621->4112|6705->4169|6742->4197|6782->4199|6818->4208|7268->4630|7318->4663|7359->4665|7401->4678|7453->4702|7475->4714|7708->4924|7764->5302|7796->5306|7856->5338|7878->5350|7920->5370|7958->5380|7980->5392|8014->5403|8191->5552|8213->5564|8257->5586|8471->5768|8511->5779|8578->5814|8613->5821|8658->5838|8691->5861|8731->5862|8768->5871|8903->5978|8921->5986|8950->5993|8980->5994|9047->6033|9083->6052|9108->6055|9138->6056|9188->6078|9206->6086|9249->6119|9303->6134|9333->6135|9365->6139|9388->6140|9434->6158|9445->6159|9459->6163|9504->6169|9534->6171|9545->6172|9583->6185|9641->6211|9683->6224|9746->6255|9782->6263|9932->6385|9974->6410|10015->6412|10055->6423|10102->6442|10117->6447|10171->6479|10211->6490|10260->6507|10297->6516|10447->6638|10476->6645|10519->6660|10556->6687|10597->6689|10627->6691|10669->6705|10707->6726|10747->6727|10787->6738|10839->6762|10850->6763|11106->6996|11153->7011|11183->7013|11220->7019|11255->7026|11298->7269|11332->7275|11420->7336|11435->7341|11478->7362|11514->7371|11547->7394|11588->7396|11623->7403|11738->7490|11768->7498|11805->7507|11857->7527|11895->7537|12143->7756|12167->7769|12197->7776|12773->8324|12813->8347|12853->8348|12897->8363|12991->8429|13002->8430|13044->8449|13076->8452|13088->8453|13126->8468|13190->8503|13202->8504|13242->8521|13274->8524|13286->8525|13323->8539|13381->8565|13423->8578|13592->8720|13642->8760|13683->8762|13730->8780|13837->8859|13869->8868|13911->8881|13945->8892|13989->8905|14020->8909|14069->8948|14110->8950|14142->8955|14182->8978|14222->8979|14255->8984|14362->9063|14395->9074|14425->9076|14436->9077|14474->9092|14506->9095|14540->9106|14585->9120|14620->9124|14662->9137|14835->9282|14875->9305|14915->9306|14959->9321|15059->9393|15070->9394|15110->9411|15142->9414|15154->9415|15189->9427|15220->9428|15252->9431|15264->9432|15299->9444|15330->9445|15362->9448|15374->9449|15409->9461|15440->9462|15472->9465|15484->9466|15519->9478|15575->9502|15617->9515|15738->9608|15769->9629|15810->9631|15856->9645|15886->9646|15993->9724|16010->9730|16040->9737|16113->9782|16180->9832|16221->9834|16263->9847|16356->9912|16379->9913|16428->9934|16444->9940|16469->9943|16500->9946|16516->9952|16544->9958|16614->10000|16630->10006|16660->10014|16775->10101|16791->10107|16818->10112|16849->10115|16866->10121|16894->10126|16935->10138|16952->10144|16980->10149|17048->10185|17090->10198|17160->10240|17227->10290|17268->10292|17312->10307|17407->10374|17430->10375|17479->10396|17495->10402|17520->10405|17551->10408|17567->10414|17595->10420|17667->10464|17683->10470|17713->10478|17830->10567|17846->10573|17873->10578|17904->10581|17921->10587|17949->10592|17990->10604|18007->10610|18035->10615|18107->10655|18149->10668|18197->10688|18228->10709|18269->10711|18311->10724|18653->11034|18695->11047|18800->11125|18815->11130|18872->11165|18904->11169|18950->11184
                  LINES: 30->2|35->2|38->5|38->5|38->5|41->8|44->11|44->11|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|52->19|53->20|53->20|53->20|53->20|53->20|53->20|53->20|53->20|53->20|53->20|53->20|54->21|61->31|62->32|68->38|68->38|69->41|70->42|70->42|70->42|71->43|71->43|71->43|71->43|71->43|72->44|72->44|72->44|73->45|73->45|73->45|73->45|74->46|75->47|80->52|80->52|80->52|81->53|82->54|82->54|82->54|83->55|83->55|83->55|84->56|96->68|96->68|96->68|98->74|99->75|99->75|99->75|99->75|99->75|99->75|99->75|101->77|101->77|101->77|109->85|110->86|113->89|113->89|113->89|114->90|127->103|127->103|127->103|128->104|129->105|129->105|129->105|131->111|132->112|132->112|132->112|132->112|132->112|132->112|132->112|134->114|134->114|134->114|140->120|141->121|143->123|144->124|146->126|146->126|146->126|147->127|148->128|148->128|148->128|148->128|149->129|149->129|149->129|149->129|150->130|150->130|150->130|150->130|150->130|150->130|150->130|151->131|151->131|151->131|151->131|151->131|151->131|151->131|152->132|153->133|155->135|157->137|161->141|161->141|161->141|162->142|163->143|163->143|163->143|164->144|165->145|166->146|175->155|175->155|176->156|176->156|176->156|177->157|178->158|178->158|178->158|179->159|179->159|179->159|179->159|180->160|181->161|181->161|182->162|184->170|186->172|190->176|190->176|190->176|192->178|192->178|192->178|193->179|196->182|196->182|197->183|198->184|200->186|203->189|203->189|203->189|211->197|211->197|211->197|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|212->198|213->199|214->200|217->203|217->203|217->203|218->204|218->204|218->204|218->204|218->204|219->205|220->206|220->206|220->206|221->207|221->207|221->207|222->208|222->208|222->208|222->208|222->208|222->208|222->208|222->208|223->209|224->210|225->211|228->214|228->214|228->214|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|229->215|230->216|231->217|237->223|237->223|237->223|237->223|237->223|237->223|237->223|237->223|239->225|239->225|239->225|240->226|241->227|241->227|241->227|241->227|241->227|241->227|241->227|241->227|242->228|242->228|242->228|243->229|243->229|243->229|243->229|243->229|243->229|243->229|243->229|243->229|245->231|246->232|247->233|247->233|247->233|248->234|249->235|249->235|249->235|249->235|249->235|249->235|249->235|249->235|250->236|250->236|250->236|251->237|251->237|251->237|251->237|251->237|251->237|251->237|251->237|251->237|253->239|254->240|255->241|255->241|255->241|256->242|262->248|265->251|271->257|271->257|271->257|273->259|275->261
                  -- GENERATED --
              */
          