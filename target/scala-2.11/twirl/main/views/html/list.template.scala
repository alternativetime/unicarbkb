
package views.html

import play.twirl.api._


     object list_Scope0 {
import controllers._
import models._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc.Http.Context.Implicit._
import views.html._

class list extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[com.avaje.ebean.PagedList[Reference],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.PagedList[Reference], currentSortBy: String, currentOrder: String, currentFilter: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*32.2*/header/*32.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*32.38*/("""
    """),format.raw/*33.5*/("""<th class=""""),_display_(/*33.17*/key/*33.20*/.replace(".","_")),format.raw/*33.37*/(""" """),format.raw/*33.38*/("""header """),_display_(/*33.46*/if(currentSortBy == key){/*33.72*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*33.136*/("""">
        <a href=""""),_display_(/*34.19*/link(0, key)),format.raw/*34.31*/("""">"""),_display_(/*34.34*/title),format.raw/*34.39*/("""</a>
    </th>
""")))};def /*6.2*/link/*6.6*/(newPage:Int, newSortBy:String) = {{
    
    var sortBy = currentSortBy
    var order = currentOrder
    
    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
                order = "asc"
            }
        } else {
            order = "asc"
        }
    }
    
    // Generate the link
    routes.Application.list2(newPage, sortBy, order, currentFilter)
    
}};
Seq[Any](format.raw/*1.121*/("""

"""),format.raw/*5.42*/("""
"""),format.raw/*27.2*/("""

"""),format.raw/*31.37*/("""
"""),format.raw/*36.2*/("""

"""),_display_(/*38.2*/main/*38.6*/ {_display_(Seq[Any](format.raw/*38.8*/("""

  """),format.raw/*40.3*/("""<ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-book" ></i> References<span class="divider"></span></li>
    <!--<li class="active" > You are here</li>-->
  </ul>

  <section id="layouts">

    <div class="page-header row-fluid">
      <h1 id="homeTitle">"""),_display_(/*49.27*/Messages(" Reference Search ", currentPage.getTotalRowCount)),format.raw/*49.87*/("""</h1>
      <h4 class="subheader">Search the curated collection of publication data by author or title descriptions</h4>
    </div>

    """),_display_(/*53.6*/if(flash.containsKey("success"))/*53.38*/ {_display_(Seq[Any](format.raw/*53.40*/("""
        """),format.raw/*54.9*/("""<div class="alert-message warning">
            <strong>Done!</strong> """),_display_(/*55.37*/flash/*55.42*/.get("success")),format.raw/*55.57*/("""
        """),format.raw/*56.9*/("""</div>
    """)))}),format.raw/*57.6*/(""" 

    """),format.raw/*59.5*/("""<div id="actions">
 
        <form class='input-append' action=""""),_display_(/*61.45*/link(0, "name")),format.raw/*61.60*/("""" method="GET">
          <input type="search" id="searchbox" class='input-xxlarge' name="f" value=""""),_display_(/*62.86*/currentFilter),format.raw/*62.99*/("""" placeholder="Reference title or author name...">
          <input type="submit" id="searchsubmit" value="Filter" class="btn btn-primary">
          <a class="btn success" id="add" href="/references?s=authors">Show All References</a> 
        </form>
        
    </div>
    
    <div class="row-fluid">
    """),_display_(/*70.6*/if(currentPage.getTotalRowCount == 0)/*70.43*/ {_display_(Seq[Any](format.raw/*70.45*/("""
        
        """),format.raw/*72.9*/("""<div class="well">
            <em>Nothing to display</em>
        </div>
        
    """)))}/*76.7*/else/*76.12*/{_display_(Seq[Any](format.raw/*76.13*/("""
        
        """),format.raw/*78.9*/("""<table class="computers table table-striped">
            <thead>
                <tr>
                    """),_display_(/*81.22*/header("title", "Title")),format.raw/*81.46*/("""
                    """),_display_(/*82.22*/header("year", "Year")),format.raw/*82.44*/("""
                    """),_display_(/*83.22*/header("authors", "Authors")),format.raw/*83.50*/("""
                    """),_display_(/*84.22*/header("journal.name", "Journal")),format.raw/*84.55*/("""
                    """),_display_(/*85.22*/header("streference.id", "Associated Glycans")),format.raw/*85.68*/("""
                """),format.raw/*86.17*/("""</tr>
            </thead>
            <tbody>

                """),_display_(/*90.18*/for(reference <- currentPage.getList) yield /*90.55*/ {_display_(Seq[Any](format.raw/*90.57*/("""
                    """),format.raw/*91.21*/("""<tr>
                        <td><a href="references/"""),_display_(/*92.50*/reference/*92.59*/.id),format.raw/*92.62*/("""" style="text-decoration : none, font-color: #404040; ">"""),_display_(/*92.119*/reference/*92.128*/.title),format.raw/*92.134*/("""</a></td>
                        <td>
                            """),_display_(/*94.30*/if(reference.year < 1)/*94.52*/ {_display_(Seq[Any](format.raw/*94.54*/("""
                                """),format.raw/*95.33*/("""<em>-</em>
                            """)))}/*96.31*/else/*96.36*/{_display_(Seq[Any](format.raw/*96.37*/("""
                                """),_display_(/*97.34*/reference/*97.43*/.year),format.raw/*97.48*/("""
                            """)))}),format.raw/*98.30*/("""
                        """),format.raw/*99.25*/("""</td>
                        <td>
                            """),_display_(/*101.30*/if(reference.authors == null)/*101.59*/ {_display_(Seq[Any](format.raw/*101.61*/("""
                                """),format.raw/*102.33*/("""<em>-</em>
                            """)))}/*103.31*/else/*103.36*/{_display_(Seq[Any](format.raw/*103.37*/("""
                                """),_display_(/*104.34*/reference/*104.43*/.authors),format.raw/*104.51*/("""
                            """)))}),format.raw/*105.30*/("""
                        """),format.raw/*106.25*/("""</td>
                        <td>
                            """),_display_(/*108.30*/if(reference.journal == null)/*108.59*/ {_display_(Seq[Any](format.raw/*108.61*/("""
                                """),format.raw/*109.33*/("""<em>-</em>
                            """)))}/*110.31*/else/*110.36*/{_display_(Seq[Any](format.raw/*110.37*/("""
                                """),_display_(/*111.34*/reference/*111.43*/.journal.name),format.raw/*111.56*/("""
                            """)))}),format.raw/*112.30*/("""
                        """),format.raw/*113.25*/("""</td>
			<td>
			   """),_display_(/*115.8*/reference/*115.17*/.streference.size()),format.raw/*115.36*/("""
			"""),format.raw/*116.4*/("""</td>
                    </tr>
                """)))}),format.raw/*118.18*/("""

            """),format.raw/*120.13*/("""</tbody>
        </table>

        <div id="pagination" class="pagination">
            <ul class="clearfix">
              """),_display_(/*125.16*/if(currentPage.hasPrev)/*125.39*/ {_display_(Seq[Any](format.raw/*125.41*/("""
                """),format.raw/*126.17*/("""<li class="prev"><a href=""""),_display_(/*126.44*/link(currentPage.getPageIndex - 1, null)),format.raw/*126.84*/("""">&larr; Previous</a></li>
              """)))}/*127.17*/else/*127.22*/{_display_(Seq[Any](format.raw/*127.23*/("""
                """),format.raw/*128.17*/("""<li class="prev disabled"><a>&larr; Previous</a></li>
              """)))}),format.raw/*129.16*/("""
                """),format.raw/*130.17*/("""<li class="current"><a>Displaying """),_display_(/*130.52*/currentPage/*130.63*/.getDisplayXtoYofZ(" to "," of ")),format.raw/*130.96*/("""</a></li>
              """),_display_(/*131.16*/if(currentPage.hasNext)/*131.39*/ {_display_(Seq[Any](format.raw/*131.41*/("""
                """),format.raw/*132.17*/("""<li class="next"><a href=""""),_display_(/*132.44*/link(currentPage.getPageIndex + 1, null)),format.raw/*132.84*/("""">Next &rarr;</a></li>
              """)))}/*133.17*/else/*133.22*/{_display_(Seq[Any](format.raw/*133.23*/("""
                """),format.raw/*134.17*/("""<li class="next disabled"><a>Next &rarr;</a></li>
              """)))}),format.raw/*135.16*/("""
            """),format.raw/*136.13*/("""</ul>
        </div>
      </div>
	
      """),_display_(/*140.8*/views/*140.13*/.html.footerunicarb.footerunicarb()),format.raw/*140.48*/("""    
    
    """)))}),format.raw/*142.6*/("""

  """),format.raw/*144.3*/("""</section>
        
""")))}),format.raw/*146.2*/("""

            
"""))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[Reference],currentSortBy:String,currentOrder:String,currentFilter:String): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def f:((com.avaje.ebean.PagedList[Reference],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter) => apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def ref: this.type = this

}


}

/**/
object list extends list_Scope0.list
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/list.scala.html
                  HASH: 8647036dd5e2abead8c04139e5236aa7b2ab5552
                  MATRIX: 794->1|992->867|1006->873|1113->903|1145->908|1184->920|1196->923|1234->940|1263->941|1298->949|1332->975|1419->1039|1467->1060|1500->1072|1530->1075|1556->1080|1594->249|1605->253|2138->120|2167->247|2195->755|2225->865|2253->1096|2282->1099|2294->1103|2333->1105|2364->1109|2758->1476|2839->1536|3003->1674|3044->1706|3084->1708|3120->1717|3219->1789|3233->1794|3269->1809|3305->1818|3347->1830|3381->1837|3473->1902|3509->1917|3637->2018|3671->2031|4007->2341|4053->2378|4093->2380|4138->2398|4244->2487|4257->2492|4296->2493|4341->2511|4476->2619|4521->2643|4570->2665|4613->2687|4662->2709|4711->2737|4760->2759|4814->2792|4863->2814|4930->2860|4975->2877|5067->2942|5120->2979|5160->2981|5209->3002|5290->3056|5308->3065|5332->3068|5417->3125|5436->3134|5464->3140|5559->3208|5590->3230|5630->3232|5691->3265|5750->3306|5763->3311|5802->3312|5863->3346|5881->3355|5907->3360|5968->3390|6021->3415|6113->3479|6152->3508|6193->3510|6255->3543|6315->3584|6329->3589|6369->3590|6431->3624|6450->3633|6480->3641|6542->3671|6596->3696|6688->3760|6727->3789|6768->3791|6830->3824|6890->3865|6904->3870|6944->3871|7006->3905|7025->3914|7060->3927|7122->3957|7176->3982|7224->4003|7243->4012|7284->4031|7316->4035|7397->4084|7440->4098|7593->4223|7626->4246|7667->4248|7713->4265|7768->4292|7830->4332|7892->4375|7906->4380|7946->4381|7992->4398|8093->4467|8139->4484|8202->4519|8223->4530|8278->4563|8331->4588|8364->4611|8405->4613|8451->4630|8506->4657|8568->4697|8626->4736|8640->4741|8680->4742|8726->4759|8823->4824|8865->4837|8935->4880|8950->4885|9007->4920|9053->4935|9085->4939|9137->4960
                  LINES: 27->1|31->32|31->32|33->32|34->33|34->33|34->33|34->33|34->33|34->33|34->33|34->33|35->34|35->34|35->34|35->34|37->6|37->6|59->1|61->5|62->27|64->31|65->36|67->38|67->38|67->38|69->40|78->49|78->49|82->53|82->53|82->53|83->54|84->55|84->55|84->55|85->56|86->57|88->59|90->61|90->61|91->62|91->62|99->70|99->70|99->70|101->72|105->76|105->76|105->76|107->78|110->81|110->81|111->82|111->82|112->83|112->83|113->84|113->84|114->85|114->85|115->86|119->90|119->90|119->90|120->91|121->92|121->92|121->92|121->92|121->92|121->92|123->94|123->94|123->94|124->95|125->96|125->96|125->96|126->97|126->97|126->97|127->98|128->99|130->101|130->101|130->101|131->102|132->103|132->103|132->103|133->104|133->104|133->104|134->105|135->106|137->108|137->108|137->108|138->109|139->110|139->110|139->110|140->111|140->111|140->111|141->112|142->113|144->115|144->115|144->115|145->116|147->118|149->120|154->125|154->125|154->125|155->126|155->126|155->126|156->127|156->127|156->127|157->128|158->129|159->130|159->130|159->130|159->130|160->131|160->131|160->131|161->132|161->132|161->132|162->133|162->133|162->133|163->134|164->135|165->136|169->140|169->140|169->140|171->142|173->144|175->146
                  -- GENERATED --
              */
          