
package views.html.rdf

import play.twirl.api._


     object showEpitopes_Scope0 {
import controllers._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import views.html._

class showEpitopes extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[com.avaje.ebean.PagedList[models.rdf.RdfEpitopes],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.PagedList[models.rdf.RdfEpitopes], currentSortBy: String, currentOrder: String, currentFilter: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*33.2*/header/*33.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*33.38*/("""
    """),format.raw/*34.5*/("""<th class=""""),_display_(/*34.17*/key/*34.20*/.replace(".","_")),format.raw/*34.37*/(""" """),format.raw/*34.38*/("""header """),_display_(/*34.46*/if(currentSortBy == key){/*34.72*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*34.136*/("""">
        <a href=""""),_display_(/*35.19*/link(0, key)),format.raw/*35.31*/("""">"""),_display_(/*35.34*/title),format.raw/*35.39*/("""</a>
    </th>
""")))};def /*7.2*/link/*7.6*/(newPage:Int, newSortBy:String) = {{

    var sortBy = currentSortBy
    var order = currentOrder

    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
                order = "asc"
            }
        } else {
            order = "asc"
        }
    }

    // Generate the link
    routes.GlycoRDF.ShowEpitopes(newPage, sortBy, order, currentFilter)

}};
Seq[Any](format.raw/*1.134*/("""
"""),format.raw/*3.1*/("""
    """),format.raw/*6.46*/("""
"""),format.raw/*28.2*/("""

    """),format.raw/*32.41*/("""
"""),format.raw/*37.2*/("""

"""),format.raw/*39.1*/("""<style>
.list-group li """),format.raw/*40.16*/("""{"""),format.raw/*40.17*/("""
"""),format.raw/*41.1*/("""list-style: none;

"""),format.raw/*43.1*/("""}"""),format.raw/*43.2*/("""
"""),format.raw/*44.1*/(""".panel-info, .panel-rating, .panel-more1 """),format.raw/*44.42*/("""{"""),format.raw/*44.43*/("""
"""),format.raw/*45.1*/("""float: left;
margin: 0 10px;

"""),format.raw/*48.1*/("""}"""),format.raw/*48.2*/("""

"""),format.raw/*50.1*/("""</style>

"""),_display_(/*52.2*/main/*52.6*/ {_display_(Seq[Any](format.raw/*52.8*/("""

    """),format.raw/*54.5*/("""<ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-book" ></i> Epitopes<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts">

        <div class="page-header row-fluid">
            <h1 id="homeTitle">"""),_display_(/*63.33*/Messages(" Epitope Search ", currentPage.getTotalRowCount)),format.raw/*63.91*/("""</h1>
            <h4 class="subheader">Search the curated collection of publication data by author or title descriptions</h4>
        </div>

        <div id="actions">

            <form class='input-append' action=""""),_display_(/*69.49*/link(0, "originalSequence")),format.raw/*69.76*/("""" method="GET">
                <input type="search" id="searchbox" class='input-xxlarge' name="f" value=""""),_display_(/*70.92*/currentFilter),format.raw/*70.105*/("""" placeholder="Sequence...">
                <input type="submit" id="searchsubmit" value="Filter" class="btn btn-primary">
                <a class="btn success" id="add" href="/showEpitopes?s=inputSequence">Show All References</a>
            </form>

        </div>

        <div class="row-fluid">
        """),_display_(/*78.10*/if(currentPage.getTotalRowCount == 0)/*78.47*/ {_display_(Seq[Any](format.raw/*78.49*/("""

            """),format.raw/*80.13*/("""<div class="well">
                <em>Nothing to display</em>
            </div>

        """)))}/*84.11*/else/*84.16*/{_display_(Seq[Any](format.raw/*84.17*/("""

            """),_display_(/*86.14*/for(epitope <- currentPage.getList) yield /*86.49*/ {_display_(Seq[Any](format.raw/*86.51*/("""
                """),_display_(/*87.18*/if(epitope.description != null )/*87.50*/{_display_(Seq[Any](format.raw/*87.51*/("""
                """),format.raw/*88.17*/("""<div class="container" >



                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-inf span6">
                                        """),format.raw/*95.89*/("""
                                        """),format.raw/*96.41*/("""<p>"""),_display_(/*96.45*/epitope/*96.52*/.originalSequence),format.raw/*96.69*/("""</p>
                                    </div>
                                    <div class="panel-rating">
                                        """),format.raw/*99.66*/("""
                                    """),format.raw/*100.37*/("""</div>
                                    <div class="panel-more1 span3">
                                        """),format.raw/*103.44*/("""

                                    """),format.raw/*105.37*/("""<a href="/substructureRDFResultEpitope/"""),_display_(/*105.77*/epitope/*105.84*/.id),format.raw/*105.87*/(""""><img src="""),_display_(/*105.99*/{routes.GlycoRDF.ImageGenerator(epitope.id)}),format.raw/*105.143*/("""></a>
                                        """),format.raw/*106.72*/("""
                                    """),format.raw/*107.37*/("""</div>
                                </div>
                            </div>

                </div>
                """)))}),format.raw/*112.18*/("""
            """)))}),format.raw/*113.14*/("""
             """),format.raw/*138.23*/("""


            """),format.raw/*141.13*/("""<div id="pagination" class="pagination">

                <ul class="clearfix">
                    """),_display_(/*144.22*/if(currentPage.hasPrev)/*144.45*/ {_display_(Seq[Any](format.raw/*144.47*/("""
                        """),format.raw/*145.25*/("""<li class="prev"><a href=""""),_display_(/*145.52*/link(currentPage.getPageIndex - 1, null)),format.raw/*145.92*/("""">&larr; Previous</a></li>
                    """)))}/*146.23*/else/*146.28*/{_display_(Seq[Any](format.raw/*146.29*/("""
                        """),format.raw/*147.25*/("""<li class="prev disabled"><a>&larr; Previous</a></li>
                    """)))}),format.raw/*148.22*/("""
                    """),format.raw/*149.21*/("""<li class="current"><a>Displaying """),_display_(/*149.56*/currentPage/*149.67*/.getDisplayXtoYofZ(" to "," of ")),format.raw/*149.100*/("""</a></li>
                    """),_display_(/*150.22*/if(currentPage.hasNext)/*150.45*/ {_display_(Seq[Any](format.raw/*150.47*/("""
                        """),format.raw/*151.25*/("""<li class="next"><a href=""""),_display_(/*151.52*/link(currentPage.getPageIndex + 1, null)),format.raw/*151.92*/("""">Next &rarr;</a></li>
                    """)))}/*152.23*/else/*152.28*/{_display_(Seq[Any](format.raw/*152.29*/("""
                        """),format.raw/*153.25*/("""<li class="next disabled"><a>Next &rarr;</a></li>
                    """)))}),format.raw/*154.22*/("""
                """),format.raw/*155.17*/("""</ul>
            </div>
        </div>

        """),_display_(/*159.10*/views/*159.15*/.html.footerunicarb.footerunicarb()),format.raw/*159.50*/("""

        """)))}),format.raw/*161.10*/("""

    """),format.raw/*163.5*/("""</section>
""")))}))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[models.rdf.RdfEpitopes],currentSortBy:String,currentOrder:String,currentFilter:String): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def f:((com.avaje.ebean.PagedList[models.rdf.RdfEpitopes],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter) => apply(currentPage,currentSortBy,currentOrder,currentFilter)

  def ref: this.type = this

}


}

/**/
object showEpitopes extends showEpitopes_Scope0.showEpitopes
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/rdf/showEpitopes.scala.html
                  HASH: 4aef19ac74e9ec0c2f81e5ddcac1b7a1833f3e15
                  MATRIX: 827->1|1053->909|1067->915|1174->945|1206->950|1245->962|1257->965|1295->982|1324->983|1359->991|1393->1017|1480->1081|1528->1102|1561->1114|1591->1117|1617->1122|1655->291|1666->295|2187->133|2214->151|2246->289|2274->785|2308->907|2336->1138|2365->1140|2416->1163|2445->1164|2473->1165|2519->1184|2547->1185|2575->1186|2644->1227|2673->1228|2701->1229|2758->1259|2786->1260|2815->1262|2852->1273|2864->1277|2903->1279|2936->1285|3358->1680|3437->1738|3683->1957|3731->1984|3865->2091|3900->2104|4238->2415|4284->2452|4324->2454|4366->2468|4477->2561|4490->2566|4529->2567|4571->2582|4622->2617|4662->2619|4707->2637|4748->2669|4787->2670|4832->2687|5113->2988|5182->3029|5213->3033|5229->3040|5267->3057|5446->3233|5512->3270|5656->3681|5723->3719|5791->3759|5808->3766|5833->3769|5873->3781|5940->3825|6015->3902|6081->3939|6235->4061|6281->4075|6324->5056|6368->5071|6497->5172|6530->5195|6571->5197|6625->5222|6680->5249|6742->5289|6810->5338|6824->5343|6864->5344|6918->5369|7025->5444|7075->5465|7138->5500|7159->5511|7215->5544|7274->5575|7307->5598|7348->5600|7402->5625|7457->5652|7519->5692|7583->5737|7597->5742|7637->5743|7691->5768|7794->5839|7840->5856|7918->5906|7933->5911|7990->5946|8033->5957|8067->5963
                  LINES: 27->1|31->33|31->33|33->33|34->34|34->34|34->34|34->34|34->34|34->34|34->34|34->34|35->35|35->35|35->35|35->35|37->7|37->7|59->1|60->3|61->6|62->28|64->32|65->37|67->39|68->40|68->40|69->41|71->43|71->43|72->44|72->44|72->44|73->45|76->48|76->48|78->50|80->52|80->52|80->52|82->54|91->63|91->63|97->69|97->69|98->70|98->70|106->78|106->78|106->78|108->80|112->84|112->84|112->84|114->86|114->86|114->86|115->87|115->87|115->87|116->88|123->95|124->96|124->96|124->96|124->96|127->99|128->100|130->103|132->105|132->105|132->105|132->105|132->105|132->105|133->106|134->107|139->112|140->113|141->138|144->141|147->144|147->144|147->144|148->145|148->145|148->145|149->146|149->146|149->146|150->147|151->148|152->149|152->149|152->149|152->149|153->150|153->150|153->150|154->151|154->151|154->151|155->152|155->152|155->152|156->153|157->154|158->155|162->159|162->159|162->159|164->161|166->163
                  -- GENERATED --
              */
          