
package views.html.rdf

import play.twirl.api._


     object substructureRDFQuery_Scope0 {
import controllers._
import views.html._

class substructureRDFQuery extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/{_display_(Seq[Any](format.raw/*1.7*/("""
"""),format.raw/*2.1*/("""<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


<section id="layouts">

    <div class="page-header row-fluid">
        <h1 id="homeTitle" class="about-rdf">GlycoRDF Powering (Sub)Structure Searching</h1>
     </div>

    """),_display_(/*11.6*/helper/*11.12*/.form(action = routes.GlycoRDF.SubstructureSPARQLQueryResult(), 'class -> "form-vertical", 'id -> "rdfStructureForm", 'method -> "POST")/*11.148*/ {_display_(Seq[Any](format.raw/*11.150*/("""
    """),format.raw/*12.5*/("""<div class="row blue-background">
        <h2 class="section-header align-centre">Start querying</h2>
        <div class=”span3″><i class="fa fa-pencil-square-o fa-3x"></i></div>
        <div class=”span4″>
            <textarea class=”form-control” id=”description” name=”glycoct” placeholder="Copy GlycoCT"></textarea>
        </div>
        <div class=”span8″>
            <p>RES<br/>
            1b:a-dgal-HEX-1:5<br/>
            2s:n-acetyl<br/>
            3b:b-dgal-HEX-1:5<br/>
            4b:a-dgal-HEX-1:5<br/>
            5b:a-dgal-HEX-1:5<br/>
            6b:a-lgal-HEX-1:5|6:d<br/>
            LIN<br/>
            1:1d(2+1)2n<br/>
            2:1o(3+1)3d<br/>
            3:3o(4+1)4d<br/>
            4:4o(3+1)5d<br/>
            5:5o(2+1)6d</p>
        </div>
    </div>
    <div class="row"><button type="submit" class="btn btn-default">Submit</button></div>
    """)))}),format.raw/*35.6*/("""

    """),format.raw/*37.5*/("""</div>

</section>



""")))}),format.raw/*43.2*/("""

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object substructureRDFQuery extends substructureRDFQuery_Scope0.substructureRDFQuery
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/rdf/substructureRDFQuery.scala.html
                  HASH: 78dd082edfa1bc633530f0c23e444517b388df8a
                  MATRIX: 861->1|872->5|909->6|936->7|1240->285|1255->291|1401->427|1442->429|1474->434|2385->1315|2418->1321|2471->1344
                  LINES: 32->1|32->1|32->1|33->2|42->11|42->11|42->11|42->11|43->12|66->35|68->37|74->43
                  -- GENERATED --
              */
          