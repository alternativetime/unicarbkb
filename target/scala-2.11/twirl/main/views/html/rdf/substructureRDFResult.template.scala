
package views.html.rdf

import play.twirl.api._


     object substructureRDFResult_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class substructureRDFResult extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(rdf: List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.21*/("""


"""),_display_(/*4.2*/main/*4.6*/{_display_(Seq[Any](format.raw/*4.7*/("""

"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.GlycoRDF.SubstructureSPARQLQueryResult(), 'name -> "rdfStructureForm", 'class -> "form-vertical", 'id -> "rdfStructureForm", 'method -> "POST")/*6.173*/ {_display_(Seq[Any](format.raw/*6.175*/("""



"""),format.raw/*10.1*/("""<div class="well">
    <fieldset>
        <legend>(Sub)Structure Searching - Test Phase</<legend>>
        <div class=”form-group”>
            <label class=”col-sm-3 control-label no-padding-right” for=”description”>Description</label>
            <div class=”col-lg-4 col-md-4 col-sm-5″>
            <textarea class=”form-control” id=”description” name=”glycoct”></textarea>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </fieldset>

<fieldset>
        <legend>Results """),_display_(/*22.26*/rdf/*22.29*/.size()),format.raw/*22.36*/("""</legend>

        """),_display_(/*24.10*/for(s <- rdf) yield /*24.23*/ {_display_(Seq[Any](format.raw/*24.25*/("""
            """),format.raw/*25.13*/("""<p><a href='"""),_display_(/*25.26*/s/*25.27*/.replace("http://rdf.unicarbkb.org/structureConnection/", "/structure/")),format.raw/*25.99*/("""'>"""),_display_(/*25.102*/s),format.raw/*25.103*/("""</a> <img src="http://www.glycome-db.org/getSugarImage.action?id=19955&type=cfg&filetype=svg"/></p>
        """)))}),format.raw/*26.10*/("""
"""),format.raw/*27.1*/("""</div>



""")))}),format.raw/*31.2*/("""


""")))}),format.raw/*34.2*/("""
"""))
      }
    }
  }

  def render(rdf:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(rdf)

  def f:((List[String]) => play.twirl.api.HtmlFormat.Appendable) = (rdf) => apply(rdf)

  def ref: this.type = this

}


}

/**/
object substructureRDFResult extends substructureRDFResult_Scope0.substructureRDFResult
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/rdf/substructureRDFResult.scala.html
                  HASH: c93833eeab91030750710f526c48c9830b7cedd3
                  MATRIX: 787->1|901->20|930->24|941->28|978->29|1006->32|1019->38|1193->203|1233->205|1264->209|1810->728|1822->731|1850->738|1897->758|1926->771|1966->773|2007->786|2047->799|2057->800|2150->872|2181->875|2204->876|2344->985|2372->986|2413->997|2447->1001
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|37->6|37->6|37->6|41->10|53->22|53->22|53->22|55->24|55->24|55->24|56->25|56->25|56->25|56->25|56->25|56->25|57->26|58->27|62->31|65->34
                  -- GENERATED --
              */
          