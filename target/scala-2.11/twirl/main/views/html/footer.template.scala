
package views.html

import play.twirl.api._


     object footer_Scope0 {

class footer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(footer: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.16*/(""" """),format.raw/*1.17*/("""= """),format.raw/*1.19*/("""{"""),format.raw/*1.20*/("""

"""),format.raw/*3.1*/("""<div class="footer row-fluid">
  <div class="span12">
    <p class="pull-left">UniCarbKB</p>
    <p class="pull-right">Supported by
      <a href="http://www.nectar.org.au">NeCTAR</a> &nbsp;|&nbsp;
      <a href="http://www.ands.org.au">ANDS</a> &nbsp;|&nbsp;
      <a href="http://www.stint.se"> STINT</a> &nbsp;|&nbsp;
      <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png" /></a>
    </p>
  </div>
</div>
"""),format.raw/*14.1*/("""}"""),format.raw/*14.2*/("""
"""))
      }
    }
  }

  def render(footer:Html): play.twirl.api.HtmlFormat.Appendable = apply(footer)

  def f:((Html) => play.twirl.api.HtmlFormat.Appendable) = (footer) => apply(footer)

  def ref: this.type = this

}


}

/**/
object footer extends footer_Scope0.footer
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/footer.scala.html
                  HASH: 2319a24c890c83c64c16dd04ffb0a5bffe472d00
                  MATRIX: 745->1|854->15|882->16|911->18|939->19|967->21|1556->583|1584->584
                  LINES: 27->1|32->1|32->1|32->1|32->1|34->3|45->14|45->14
                  -- GENERATED --
              */
          