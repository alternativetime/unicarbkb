
package views.html.unicorn

import play.twirl.api._


     object showUnicornStructures_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class showUnicornStructures extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.unicorn.Nlink],Integer,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structures: List[models.unicorn.Nlink], total: Integer):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.58*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

    """),format.raw/*5.5*/("""<ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider"></span></li>
        /IU
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>UniCorn - Theoretical N-link Glycan Library</h1>
        </div>
        <p>UniCorn is a theoretical N-glycan structure database built from a library of 50 human glycosyltransferases.
            Enzyme specificities were sourced from major databases including Kyoto Encyclopedia of Genes and Genomes (KEGG) Glycan, Consortium for Functional Glycomics (CFG),
            Carbohydrate-Active enZymes (CAZy), GlycoGene DataBase (GGDB) and BRENDA. Based on the known activities, a parameter-free model of glycosylation is used to generate
            a library of theoretically possible N-glycans. UniCorn also aims to provide an effective curation and quality-checking tool of newly discovered curated glycan structures populating UniCarbKB.
        </p>

    <form class="form-vertical" action =""""),_display_(/*22.43*/(routes.Unicorn.searchCompositionUnicorn())),format.raw/*22.86*/("""">
        <fieldset>

                <!-- Form Name -->
            <legend>Composition and IUPAC Search</legend>

                <!-- Text input-->
            <div class="row">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span4">
                            <label class="control-label" for="textinput">Gal</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Gal" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_gal"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Gal2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">Fuc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Fuc" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_fuc"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Fuc2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">GalNAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Galnac" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_galnac"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Galnac2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                        </div>
                        <div class="span4">
                            <label class="control-label" for="textinput">Kdn</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Kdn" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_kdn"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Kdn2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">Man</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Man" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_glc"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Man2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">GlcNAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Glcnac" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_glcnac"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Glcnac2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                        </div>

                        <div class="span4">
                            <label class="control-label" for="textinput">NeuAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_NeuAc" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_neuac"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_NeuAc2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">NeuGc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_NeuGc" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_neugc"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_NeuGc2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">HexA</label>
                            <div class="controls">
                                <input id="textinput" name="comp_HexA" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_hexa"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_HexA2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                        </div>


                        """),format.raw/*150.33*/("""
                    """),format.raw/*151.21*/("""</div>

                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="row-fluid">
                        <p>IUPAC search will before a partial match against the database only</p>
                        <label class="control-label" for="textinput">IUPAC </label>
                        <div class="controls">
                            <textarea id="textinput" name="IUPAC" type="text" placeholder="Gal(b1-4)[Fuc(a1-3)]GlcNAc(b1-3)Gal(b1-4)[Fuc(a1-3)]Glc" class="input-large span6" ></textarea>
                        </div>

                        <label class="control-label" for="textinput">Mass </label>
                        <div class="controls">
                            <input id="textinput" name="mass" type="text" placeholder="Mass" value="0.0" class="input-large span3">
                        </div>

                        """),format.raw/*173.33*/("""
                        """),format.raw/*174.25*/("""<button type="submit" class="btn btn-primary">Search Database</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <h3>New Sub(structure) Search </h3>
    <table class="table table-condensed table-bordered table-striped volumes">
        <thead>
            <tr>
                <th>Accession Number</th>
                <th>Structure</th>
                <th>Average Mass</th>
                <th>Mono. Mass</th>
            </tr>
        </thead>
        <tbody>
            """),_display_(/*192.14*/for(s <- structures) yield /*192.34*/{_display_(Seq[Any](format.raw/*192.35*/("""
               """),format.raw/*193.16*/("""<tr>
                <td>UCORN"""),_display_(/*194.27*/s/*194.28*/.id),format.raw/*194.31*/("""</td>
                   """),format.raw/*195.171*/("""
                   """),format.raw/*196.20*/("""<td><img class="sugar_image" src="""),_display_(/*196.54*/{routes.Image.showImageUnicorn(s.id, "cfgl", "extended" )}),format.raw/*196.112*/(""" """),format.raw/*196.113*/("""height="25%", width="25%" alt=""/></td>
                   <td>"""),_display_(/*197.25*/{"%.2f".format(s.averageMass)}),format.raw/*197.55*/("""</td>
                   <td>"""),_display_(/*198.25*/{"%.2f".format(s.monoMass)}),format.raw/*198.52*/("""</td>
               </tr>
            """)))}),format.raw/*200.14*/("""
        """),format.raw/*201.9*/("""</tbody>
    </table>f

    </section>

    <script>
        $(function() """),format.raw/*207.22*/("""{"""),format.raw/*207.23*/("""
    """),format.raw/*208.5*/("""//  changes mouse cursor when highlighting loawer right of box
    $(document).on('mousemove', 'textarea', function(e) """),format.raw/*209.57*/("""{"""),format.raw/*209.58*/("""
		"""),format.raw/*210.3*/("""var a = $(this).offset().top + $(this).outerHeight() - 16,	//	top border of bottom-right-corner-box area
			b = $(this).offset().left + $(this).outerWidth() - 16;	//	left border of bottom-right-corner-box area
		$(this).css("""),format.raw/*212.15*/("""{"""),format.raw/*212.16*/("""
			"""),format.raw/*213.4*/("""cursor: e.pageY > a && e.pageX > b ? 'nw-resize' : ''
		"""),format.raw/*214.3*/("""}"""),format.raw/*214.4*/(""");
	"""),format.raw/*215.2*/("""}"""),format.raw/*215.3*/(""")
    //  the following simple make the textbox "Auto-Expand" as it is typed in
    .on('keyup', 'textarea', function(e) """),format.raw/*217.42*/("""{"""),format.raw/*217.43*/("""
        """),format.raw/*218.9*/("""//  the following will help the text expand as typing takes place
        while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) """),format.raw/*219.149*/("""{"""),format.raw/*219.150*/("""
            """),format.raw/*220.13*/("""$(this).height($(this).height()+1);
        """),format.raw/*221.9*/("""}"""),format.raw/*221.10*/(""";
    """),format.raw/*222.5*/("""}"""),format.raw/*222.6*/(""");
"""),format.raw/*223.1*/("""}"""),format.raw/*223.2*/(""");
    </script>

""")))}),format.raw/*226.2*/("""
"""))
      }
    }
  }

  def render(structures:List[models.unicorn.Nlink],total:Integer): play.twirl.api.HtmlFormat.Appendable = apply(structures,total)

  def f:((List[models.unicorn.Nlink],Integer) => play.twirl.api.HtmlFormat.Appendable) = (structures,total) => apply(structures,total)

  def ref: this.type = this

}


}

/**/
object showUnicornStructures extends showUnicornStructures_Scope0.showUnicornStructures
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/unicorn/showUnicornStructures.scala.html
                  HASH: b837bd3371921bb8c438e8e52451c2fa73c9910d
                  MATRIX: 813->1|964->57|992->60|1003->64|1041->66|1073->72|2299->1271|2363->1314|9620->9189|9670->9210|10626->10352|10680->10377|11248->10917|11285->10937|11325->10938|11370->10954|11429->10985|11440->10986|11465->10989|11520->11165|11569->11185|11631->11219|11712->11277|11743->11278|11835->11342|11887->11372|11945->11402|11994->11429|12066->11469|12103->11478|12206->11552|12236->11553|12269->11558|12417->11677|12447->11678|12478->11681|12731->11905|12761->11906|12793->11910|12877->11966|12906->11967|12938->11971|12967->11972|13117->12093|13147->12094|13184->12103|13428->12317|13459->12318|13501->12331|13573->12375|13603->12376|13637->12382|13666->12383|13697->12386|13726->12387|13776->12406
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|53->22|53->22|171->150|172->151|190->173|191->174|209->192|209->192|209->192|210->193|211->194|211->194|211->194|212->195|213->196|213->196|213->196|213->196|214->197|214->197|215->198|215->198|217->200|218->201|224->207|224->207|225->208|226->209|226->209|227->210|229->212|229->212|230->213|231->214|231->214|232->215|232->215|234->217|234->217|235->218|236->219|236->219|237->220|238->221|238->221|239->222|239->222|240->223|240->223|243->226
                  -- GENERATED --
              */
          