
package views.html.unicorn

import play.twirl.api._


     object searchUnicornCompositions_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class searchUnicornCompositions extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.unicorn.Nlink],Integer,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structures: List[models.unicorn.Nlink], count: Integer ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.59*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

    """),format.raw/*5.5*/("""<ul class="breadcrumb" xmlns="http://www.w3.org/1999/html">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider"></span></li>
        <li><i class="icon-home" ></i><a href="/"> Unicorn N-links</a> <span class="divider"></span></li>
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>UniCorn - Theoretical N-link Glycan Library</h1>
        </div>

        <p>UniCorn is a theoretical N-glycan structure database built from a library of 50 human glycosyltransferases.
            Enzyme specificities were sourced from major databases including Kyoto Encyclopedia of Genes and Genomes (KEGG) Glycan, Consortium for Functional Glycomics (CFG),
            Carbohydrate-Active enZymes (CAZy), GlycoGene DataBase (GGDB) and BRENDA. Based on the known activities, a parameter-free model of glycosylation is used to generate
            a library of theoretically possible N-glycans. UniCorn also aims to provide an effective curation and quality-checking tool of newly discovered curated glycan structures populating UniCarbKB.
        </p>



        <form class="form-vertical" action =""""),_display_(/*25.47*/(routes.Unicorn.searchCompositionUnicorn())),format.raw/*25.90*/("""">
            <fieldset>

                    <!-- Form Name -->
                <legend>Composition and IUPAC Search</legend>

                    <!-- Text input-->
                <div class="row">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span4">
                                <label class="control-label" for="textinput">Gal</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Gal" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_gal"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Gal2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">Fuc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Fuc" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_fuc"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Fuc2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">GalNAc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Galnac" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_galnac"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Galnac2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                            </div>
                            <div class="span4">
                                <label class="control-label" for="textinput">Kdn</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Kdn" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_kdn"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Kdn2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">Man</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Man" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_glc"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Man2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">GlcNAc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Glcnac" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_glcnac"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Glcnac2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                            </div>

                            <div class="span4">
                                <label class="control-label" for="textinput">NeuAc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_NeuAc" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_neuac"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_NeuAc2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">NeuGc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_NeuGc" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_neugc"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_NeuGc2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">HexA</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_HexA" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_hexa"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_HexA2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                            </div>

                            """),format.raw/*152.37*/("""
                        """),format.raw/*153.25*/("""</div>

                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <div class="row-fluid">
                            <p>IUPAC search will before a partial match against the database only</p>
                            <label class="control-label" for="textinput">IUPAC </label>
                            <div class="controls">
                                <textarea id="textinput" name="IUPAC" type="text" placeholder="Gal(b1-4)[Fuc(a1-3)]GlcNAc(b1-3)Gal(b1-4)[Fuc(a1-3)]Glc" class="input-xlarge span6" ></textarea>
                            </div>
                            <label class="control-label" for="textinput">Mass </label>
                            <div class="controls">
                                <input id="textinput" name="mass" type="text" value="0.0" placeholder="Mass" class="input-large span3">
                            </div>

                            """),format.raw/*174.37*/("""
                                """),format.raw/*175.33*/("""<button type="submit" class="btn btn-primary">Search Database</button>

                        </div>

                    </div>
                </div>
            </fieldset>
        </form>

        <h3>New Sub(structure) Search """),_display_(/*184.40*/if(count > 100)/*184.55*/{_display_(Seq[Any](format.raw/*184.56*/(""" """),format.raw/*184.57*/("""- Showing 100 out of """),_display_(/*184.79*/count),format.raw/*184.84*/(""" """)))}/*184.86*/else/*184.90*/{_display_(Seq[Any](format.raw/*184.91*/(""" """),format.raw/*184.92*/("""- Showing """),_display_(/*184.103*/count),format.raw/*184.108*/(""" """),format.raw/*184.109*/("""Entries """)))}),format.raw/*184.118*/(""" """),format.raw/*184.119*/("""</h3>
        <table class="table table-condensed table-bordered table-striped volumes">
            <thead>
                <tr>
                    <th>Accession Number</th>
                    <th>Structure</th>
                    <th>Average Mass</th>
                    <th>Mono. Mass</th>
                </tr>
            </thead>
            <tbody>
            """),_display_(/*195.14*/for(s <- structures) yield /*195.34*/{_display_(Seq[Any](format.raw/*195.35*/("""
                """),format.raw/*196.17*/("""<tr>
                    <td>UCORN"""),_display_(/*197.31*/s/*197.32*/.id),format.raw/*197.35*/("""</td>
                    """),format.raw/*198.210*/("""
                    """),format.raw/*199.21*/("""<td><img class="sugar_image" src="""),_display_(/*199.55*/{routes.Image.showImageUnicorn(s.id, "cfgl", "extended" )}),format.raw/*199.113*/(""" """),format.raw/*199.114*/("""height="25%", width="25%" alt=""/></td>
                    <td>"""),_display_(/*200.26*/{"%.2f".format(s.averageMass)}),format.raw/*200.56*/("""</td>
                    <td>"""),_display_(/*201.26*/{"%.2f".format(s.monoMass)}),format.raw/*201.53*/("""</td>
                </tr>
            """)))}),format.raw/*203.14*/("""
            """),format.raw/*204.13*/("""</tbody>
        </table>

    </section>

""")))}),format.raw/*209.2*/("""
"""))
      }
    }
  }

  def render(structures:List[models.unicorn.Nlink],count:Integer): play.twirl.api.HtmlFormat.Appendable = apply(structures,count)

  def f:((List[models.unicorn.Nlink],Integer) => play.twirl.api.HtmlFormat.Appendable) = (structures,count) => apply(structures,count)

  def ref: this.type = this

}


}

/**/
object searchUnicornCompositions extends searchUnicornCompositions_Scope0.searchUnicornCompositions
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/unicorn/searchUnicornCompositions.scala.html
                  HASH: bc256b8d24ceddf790fc13309f49145f01353e65
                  MATRIX: 821->1|973->58|1001->61|1012->65|1050->67|1082->73|2445->1409|2509->1452|10181->9738|10235->9763|11251->10981|11313->11014|11575->11248|11600->11263|11640->11264|11670->11265|11720->11287|11747->11292|11769->11294|11783->11298|11823->11299|11853->11300|11893->11311|11921->11316|11952->11317|11994->11326|12025->11327|12426->11700|12463->11720|12503->11721|12549->11738|12612->11773|12623->11774|12648->11777|12704->11992|12754->12013|12816->12047|12897->12105|12928->12106|13021->12171|13073->12201|13132->12232|13181->12259|13254->12300|13296->12313|13371->12357
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|56->25|56->25|173->152|174->153|191->174|192->175|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|201->184|212->195|212->195|212->195|213->196|214->197|214->197|214->197|215->198|216->199|216->199|216->199|216->199|217->200|217->200|218->201|218->201|220->203|221->204|226->209
                  -- GENERATED --
              */
          