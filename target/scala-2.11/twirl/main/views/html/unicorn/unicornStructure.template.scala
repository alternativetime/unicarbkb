
package views.html.unicorn

import play.twirl.api._


     object unicornStructure_Scope0 {
import java.util._

import controllers._
import play.mvc.Http.Context.Implicit._
import views.html._

import scala.collection.JavaConversions._

     object unicornStructure_Scope1 {
import play.libs.F.Promise

class unicornStructure extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[models.unicorn.Nlink,Promise[List[String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(structures: models.unicorn.Nlink, hits: Promise[List[String]] ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.66*/("""

"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

    """),format.raw/*6.5*/("""<ul class="breadcrumb" xmlns="http://www.w3.org/1999/html">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        <li><i class="icon-home" ></i><a href="/"> Milk Glycome</a> <span class="divider">></span></li>ç
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>UniCorn - Theoretical N-link Glycan Library</h1>
        </div>


        <div class="row-fluid">
            <div class="span8">

        <img class="sugar_image" src="""),_display_(/*22.39*/{routes.Image.showImageUnicorn(structures.id, session.get("notation"), "extended" )}),format.raw/*22.123*/(""" """),format.raw/*22.124*/("""height="75%", width="75%" alt=""/></a>

                <h3>GlycoCT</h3>
                <p>"""),_display_(/*25.21*/Html(HtmlFormat.escape(structures.glycoct).toString.replace("\n", "<br />"))),format.raw/*25.97*/("""</p>

                <h3>Sub(structure) Search with RDF</h3>
                <table class="table table-condensed table-bordered table-striped volumes">
                    <thead>
                        <tr>
                            <th>Accession Number</th>
                            <th>Structure</th>
                        </tr>
                    </thead>
                    <tbody>
                    """),_display_(/*36.22*/for(ph <- hits.get(1)) yield /*36.44*/ {_display_(Seq[Any](format.raw/*36.46*/("""
                        """),format.raw/*37.25*/("""<tr>
                            <td>UNC"""),_display_(/*38.37*/ph/*38.39*/.replaceAll("structureConnection", "").replaceAll("\\> \\)", "").replaceAll("\\( \\?", "").split("=")),format.raw/*38.140*/("""</td>
                            <td><a href=""><img class="sugar_image" src="""),_display_(/*39.74*/{
                                routes.Image.showImageUnicorn(ph.replaceAll("structureConnection", "").replaceAll("\\> \\)", "").replaceAll("\\( \\?", "").replaceAll("=", "").replaceAll("""^\s+(?m)""","").toLong, "cfgl", "extended")
                            }),format.raw/*41.30*/(""" """),format.raw/*41.31*/("""height="50%", width="50%" alt=""/>
                            </a>
                            </td>
                        </tr>
                    """)))}),format.raw/*45.22*/("""
                    """),format.raw/*46.21*/("""</tbody>
                </table>



        </div>
            <div class="span4 sidebar">

                """),_display_(/*54.18*/views/*54.23*/.html.format.format()),format.raw/*54.44*/("""

                """),format.raw/*56.17*/("""<div class="info">
                    <h3>Structure Details</h3>
                    <p>GlcNAc: """),_display_(/*58.33*/structures/*58.43*/.glcnac),format.raw/*58.50*/("""</p>
                    <p>Fucose: """),_display_(/*59.33*/structures/*59.43*/.fuc),format.raw/*59.47*/("""</p>
                    <p>Galactose: """),_display_(/*60.36*/structures/*60.46*/.gal),format.raw/*60.50*/("""</p>
                    <p>Mannose: """),_display_(/*61.34*/structures/*61.44*/.man),format.raw/*61.48*/("""</p>
                    <p>Sialic acid: """),_display_(/*62.38*/structures/*62.48*/.kdn),format.raw/*62.52*/("""</p>
                    <p>GalNAc: """),_display_(/*63.33*/structures/*63.43*/.galnac),format.raw/*63.50*/("""</p>
                </div>


                <div class="info">
                    <h3>Contribution Information</h3>
                    <div class='taxonomy'>
                        <span class='label label-success'>N-link Library Model</span>
                        <span class='label label-important'>Theoretical</span>
                    </div>
                </div>

                <div class="info">
                    <h3>Substructure Search</h3>

                </div>


            </div>
        </div>
    </section>

""")))}))
      }
    }
  }

  def render(structures:models.unicorn.Nlink,hits:Promise[List[String]]): play.twirl.api.HtmlFormat.Appendable = apply(structures,hits)

  def f:((models.unicorn.Nlink,Promise[List[String]]) => play.twirl.api.HtmlFormat.Appendable) = (structures,hits) => apply(structures,hits)

  def ref: this.type = this

}


}
}

/**/
object unicornStructure extends unicornStructure_Scope0.unicornStructure_Scope1.unicornStructure
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/unicorn/unicornStructure.scala.html
                  HASH: 371a46d11d0c7f1ce38c49b31b8354f92c911194
                  MATRIX: 877->29|1036->93|1064->96|1075->100|1113->102|1145->108|1875->811|1981->895|2011->896|2131->989|2228->1065|2674->1484|2712->1506|2752->1508|2805->1533|2873->1574|2884->1576|3007->1677|3113->1756|3398->2020|3427->2021|3611->2174|3660->2195|3797->2305|3811->2310|3853->2331|3899->2349|4024->2447|4043->2457|4071->2464|4135->2501|4154->2511|4179->2515|4246->2555|4265->2565|4290->2569|4355->2607|4374->2617|4399->2621|4468->2663|4487->2673|4512->2677|4576->2714|4595->2724|4623->2731
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|55->22|55->22|55->22|58->25|58->25|69->36|69->36|69->36|70->37|71->38|71->38|71->38|72->39|74->41|74->41|78->45|79->46|87->54|87->54|87->54|89->56|91->58|91->58|91->58|92->59|92->59|92->59|93->60|93->60|93->60|94->61|94->61|94->61|95->62|95->62|95->62|96->63|96->63|96->63
                  -- GENERATED --
              */
          