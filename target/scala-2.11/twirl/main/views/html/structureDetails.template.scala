
package views.html

import play.twirl.api._


     object structureDetails_Scope0 {
import java.lang._
import java.util._

import controllers._
import models._
import views.html._

import scala.collection.JavaConversions._

class structureDetails extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template15[Structure,String,List[Composition],List[Structure],Long,ArrayList[String],HashSet[Proteins],ArrayList[String],HashSet[Tissue],List[Array[String]],ArrayList[String],HashSet[Taxonomy],ArrayList[String],String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(strInfo: Structure, glycantype: String, strcomp: List[Composition], references: List[Structure], structureId: Long, proteinNames: ArrayList[String], proteinItems: HashSet[Proteins], sourceNames: ArrayList[String], sourceItems: HashSet[Tissue], test: List[Array[String]], uniprot: ArrayList[String], taxItems: HashSet[Taxonomy], taxNames: ArrayList[String], pubchem: String, reader: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*7.2*/header/*7.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*7.38*/("""
    """),format.raw/*8.5*/("""<th class="">
        <a href="">"""),_display_(/*9.21*/title),format.raw/*9.26*/("""</a>
    </th>
""")))};
Seq[Any](format.raw/*1.392*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*6.37*/("""
"""),format.raw/*11.2*/("""

"""),format.raw/*13.1*/("""<script src=""""),_display_(/*13.15*/routes/*13.21*/.Assets.versioned("assets/javascripts/bootstrap-tab.js")),format.raw/*13.77*/(""""></script>


"""),_display_(/*16.2*/main/*16.6*/ {_display_(Seq[Any](format.raw/*16.8*/("""

"""),format.raw/*18.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-th" ></i> Structure (UC """),_display_(/*20.62*/structureId),format.raw/*20.73*/(""")<span class="divider"></span></li>
  <!--<li class="active" > You are here</li>-->
</ul>

<div class="modal hide fade" id="windowDescription">
  <div class="modal-header">
    <a href="#" class="close" data-dismiss="modal">&times;</a>
    <h3>Protein and Taxonomy Information <span class="label label-success">Curated Entry</span> <span class="label label-notice">GlycoSuiteDB</span></h3>
  </div>
  <div class="modal-body">
    <h2 class="fullname">This structure has been associated with various records:</h2>

    <div id="myDiv"><p></p></div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn primary" onclick="okClicked ();">OK</a>
  </div>
</div>


<section id="structureLayout">
  <section id="layouts">	

  <div class="page-header row-fluid">
    <h1>Structure</h1>
    <h4 class="subheader span8">Details for this """),_display_(/*45.51*/glycantype),format.raw/*45.61*/(""" """),format.raw/*45.62*/("""glycan structure</h4>
  </div>

 <div class="row-fluid">
    <div class="span8">
      <ul class='structures'>
      """),_display_(/*51.8*/views/*51.13*/.html.format.structureLarge(structureId)),format.raw/*51.53*/("""
      """),format.raw/*52.7*/("""</ul>
      
       
      
      """),_display_(/*56.8*/if(reader.length > 10)/*56.30*/{_display_(Seq[Any](format.raw/*56.31*/("""
       """),_display_(/*57.9*/views/*57.14*/.html.synthesis.enzymes(references, reader)),format.raw/*57.57*/("""
      """)))}/*58.8*/else/*58.12*/{_display_(Seq[Any](format.raw/*58.13*/("""
      
      """),format.raw/*60.7*/("""<h3>References associated to structure</h3>
      <table class="computers table table-striped">
        <thead>
          <tr>
            """),_display_(/*64.14*/header("title", "Title")),format.raw/*64.38*/("""
            """),_display_(/*65.14*/header("year", "Year")),format.raw/*65.36*/("""
            """),_display_(/*66.14*/header("authors", "Authors")),format.raw/*66.42*/("""
          """),format.raw/*67.11*/("""</tr>
        </thead>
        <tbody>
          """),_display_(/*70.12*/for(stref <- references) yield /*70.36*/{_display_(Seq[Any](format.raw/*70.37*/("""
          """),_display_(/*71.12*/for(reference <- stref.references) yield /*71.46*/{_display_(Seq[Any](format.raw/*71.47*/("""
          """),format.raw/*72.11*/("""<tr>
            <td><a href="../references/"""),_display_(/*73.41*/reference/*73.50*/.reference.id),format.raw/*73.63*/("""">"""),_display_(/*73.66*/reference/*73.75*/.reference.title),format.raw/*73.91*/("""</a></td>
            <td>"""),_display_(/*74.18*/reference/*74.27*/.reference.year),format.raw/*74.42*/("""</td>
            <td>"""),_display_(/*75.18*/reference/*75.27*/.reference.authors),format.raw/*75.45*/("""</td>
          </tr>
          """)))}),format.raw/*77.12*/("""
          """)))}),format.raw/*78.12*/("""
        """),format.raw/*79.9*/("""</tbody>
      </table>
      
      """)))}),format.raw/*82.8*/("""
      
      
      """),format.raw/*137.9*/("""
    
    """),format.raw/*139.5*/("""</div>





    <div class="span4 sidebar">

      """),_display_(/*147.8*/views/*147.13*/.html.format.format()),format.raw/*147.34*/("""
      	
      """),format.raw/*149.7*/("""<div class='info'>
      	<h3>Mass Details</h3>
      	"""),_display_(/*151.9*/for(c <- strcomp) yield /*151.26*/ {_display_(Seq[Any](format.raw/*151.28*/("""
      		"""),format.raw/*152.9*/("""<p>Average Mass: """),_display_(/*152.27*/c/*152.28*/.glycan_mass),format.raw/*152.40*/("""</p>
      		<p>Monoisotopic Mass: """),_display_(/*153.32*/c/*153.33*/.glycan_mass_monoisotopic),format.raw/*153.58*/("""</p>
      	""")))}),format.raw/*154.9*/("""
      """),format.raw/*155.7*/("""</div>

      <div class='info'>
	<h3>Linkage Type</h3>
	<p>"""),_display_(/*159.6*/glycantype),format.raw/*159.16*/(""" """),format.raw/*159.17*/("""glycan structure</p>
      </div>

      <div class='info'>
        <h3>Biological Associations</h4>
        <div class='taxonomy'>
          <a id='toggle-taxonomy'><span class='label label-important'><span class='icon-tags icon-white'></span> Taxonomy ("""),_display_(/*165.125*/taxItems/*165.133*/.size()),format.raw/*165.140*/(""") <span class="caret"></span></span></a>
          <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein ("""),_display_(/*166.127*/proteinItems/*166.139*/.size()),format.raw/*166.146*/(""") <span class="caret"></span></span></a>
          <a id='toggle-source'><span class='label label-success'><span class='icon-leaf icon-white'></span> Source ("""),_display_(/*167.119*/sourceItems/*167.130*/.size()),format.raw/*167.137*/(""") <span class="caret"></span></span></a>
        </div>
        <div>
          <ul id='more-taxonomy'>
            <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Taxonomies</h3>
            """),_display_(/*172.14*/for(tax <- taxItems) yield /*172.34*/{_display_(Seq[Any](format.raw/*172.35*/("""
            """),format.raw/*173.13*/("""<li><span class='icon-tag icon-white'></span> <a href='../taxonomy/"""),_display_(/*173.81*/tax/*173.84*/.id),format.raw/*173.87*/("""'>"""),_display_(/*173.90*/tax/*173.93*/.species),format.raw/*173.101*/("""</a></li>
            """)))}),format.raw/*174.14*/("""
          """),format.raw/*175.11*/("""</ul>
          <ul id='more-protein'>
            <h3 id='less-protein'><span class='icon-map-marker icon-white'></span> Protein</h3>
            """),_display_(/*178.14*/for(protein <- proteinItems) yield /*178.42*/{_display_(Seq[Any](format.raw/*178.43*/("""
	    """),_display_(/*179.7*/if(protein.swissProt == null )/*179.37*/ {_display_(Seq[Any](format.raw/*179.39*/("""
	    """),format.raw/*180.6*/("""<li><span class='icon-map-marker icon-white'></span> <a href='../proteinsummary/"""),_display_(/*180.87*/protein/*180.94*/.name),format.raw/*180.99*/("""'>"""),_display_(/*180.102*/protein/*180.109*/.name),format.raw/*180.114*/("""</a></li>
	    """)))}),format.raw/*181.7*/("""
	    """),_display_(/*182.7*/if(protein.swissProt != null  )/*182.38*/ {_display_(Seq[Any](format.raw/*182.40*/("""
            """),format.raw/*183.13*/("""<li><span class='icon-map-marker icon-white'></span> <a href='../proteinsummary/"""),_display_(/*183.94*/protein/*183.101*/.swissProt),format.raw/*183.111*/("""'>"""),_display_(/*183.114*/protein/*183.121*/.name),format.raw/*183.126*/("""</a></li>
            """)))}),format.raw/*184.14*/("""
	    """)))}),format.raw/*185.7*/("""
          """),format.raw/*186.11*/("""</ul>
          <ul id='more-source'>
            <h3 id='less-source'><span class='icon-leaf icon-white'></span> Source</h3>
            """),_display_(/*189.14*/for(source <- sourceItems) yield /*189.40*/{_display_(Seq[Any](format.raw/*189.41*/("""
            """),format.raw/*190.13*/("""<li><span class='icon-leaf icon-white'></span> <a href='../tissuesummary/"""),_display_(/*190.87*/source/*190.93*/.id),format.raw/*190.96*/("""'>"""),_display_(/*190.99*/source/*190.105*/.div1),format.raw/*190.110*/(""" """),format.raw/*190.111*/("""> """),_display_(/*190.114*/source/*190.120*/.div2),format.raw/*190.125*/(""" """),_display_(/*190.127*/source/*190.133*/.div3),format.raw/*190.138*/(""" """),format.raw/*190.139*/("""> """),_display_(/*190.142*/source/*190.148*/.div4),format.raw/*190.153*/("""</a></li>
            """)))}),format.raw/*191.14*/("""
          """),format.raw/*192.11*/("""</ul>
        </div>
      </div>

      <div class='info'>
        <h3>Defined Content</h3>
      <div class='taxonomy'>
	<a id='toggle-defined'><span class='label label-success'><span class='icon-leaf icon-white'></span> Source ("""),_display_(/*199.111*/sourceItems/*199.122*/.size()),format.raw/*199.129*/(""") <span class="caret"></span></span></a>
      </div>
      <div>
         <ul id='more-defined'>
            <h3 id='less-defined'><span class='icon-leaf icon-white'></span> More Defined Info</h3>
            """),_display_(/*204.14*/for(d <- strInfo.strproteintaxbiolsource) yield /*204.55*/{_display_(Seq[Any](format.raw/*204.56*/("""
            """),format.raw/*205.13*/("""<li><span class='icon-leaf icon-white'></span> <a href=''>"""),_display_(/*205.72*/if(d.taxonomy != null )/*205.95*/ {_display_(Seq[Any](format.raw/*205.97*/(""" """),_display_(/*205.99*/{d.taxonomy.species}),format.raw/*205.119*/("""  """),format.raw/*205.121*/(""">""")))}),format.raw/*205.123*/(""" """),_display_(/*205.125*/if(d.proteins !=null )/*205.147*/{_display_(Seq[Any](format.raw/*205.148*/(""" """),_display_(/*205.150*/{d.proteins.name}),format.raw/*205.167*/(""" """)))}),format.raw/*205.169*/("""</a></li>
            """)))}),format.raw/*206.14*/("""
         """),format.raw/*207.10*/("""</ul>
      </div>
      </div>

      """),_display_(/*211.8*/if(pubchem != null)/*211.27*/ {_display_(Seq[Any](format.raw/*211.29*/("""
	 """),_display_(/*212.4*/views/*212.9*/.html.format.pubchem( pubchem )),format.raw/*212.40*/("""
      """)))}),format.raw/*213.8*/("""
      """),_display_(/*214.8*/if(strInfo.glycanst matches ".*\\?.*" )/*214.47*/ {_display_(Seq[Any](format.raw/*214.49*/("""
      """),format.raw/*215.7*/("""<h3></h3>
      
      """)))}/*217.9*/else/*217.14*/{_display_(Seq[Any](format.raw/*217.15*/(""" """),_display_(/*217.17*/views/*217.22*/.html.digest.digest( strInfo )),format.raw/*217.52*/(""" """)))}),format.raw/*217.54*/("""

    """),format.raw/*219.5*/("""</div>
  </div>

  <div class="row-fluid">

  </div>
</section>
</section>	

"""),_display_(/*228.2*/views/*228.7*/.html.footerunicarb.footerunicarb()),format.raw/*228.42*/("""

""")))}),format.raw/*230.2*/("""
"""))
      }
    }
  }

  def render(strInfo:Structure,glycantype:String,strcomp:List[Composition],references:List[Structure],structureId:Long,proteinNames:ArrayList[String],proteinItems:HashSet[Proteins],sourceNames:ArrayList[String],sourceItems:HashSet[Tissue],test:List[Array[String]],uniprot:ArrayList[String],taxItems:HashSet[Taxonomy],taxNames:ArrayList[String],pubchem:String,reader:String): play.twirl.api.HtmlFormat.Appendable = apply(strInfo,glycantype,strcomp,references,structureId,proteinNames,proteinItems,sourceNames,sourceItems,test,uniprot,taxItems,taxNames,pubchem,reader)

  def f:((Structure,String,List[Composition],List[Structure],Long,ArrayList[String],HashSet[Proteins],ArrayList[String],HashSet[Tissue],List[Array[String]],ArrayList[String],HashSet[Taxonomy],ArrayList[String],String,String) => play.twirl.api.HtmlFormat.Appendable) = (strInfo,glycantype,strcomp,references,structureId,proteinNames,proteinItems,sourceNames,sourceItems,test,uniprot,taxItems,taxNames,pubchem,reader) => apply(strInfo,glycantype,strcomp,references,structureId,proteinNames,proteinItems,sourceNames,sourceItems,test,uniprot,taxItems,taxNames,pubchem,reader)

  def ref: this.type = this

}


}

/**/
object structureDetails extends structureDetails_Scope0.structureDetails
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/structureDetails.scala.html
                  HASH: 209295d50ca6ad6871ead2f543d9c1f35199c206
                  MATRIX: 975->1|1458->520|1471->526|1577->556|1608->561|1668->595|1693->600|1749->391|1776->409|1804->518|1832->616|1861->618|1902->632|1917->638|1994->694|2035->709|2047->713|2086->715|2115->717|2322->897|2354->908|3221->1748|3252->1758|3281->1759|3425->1877|3439->1882|3500->1922|3534->1929|3595->1964|3626->1986|3665->1987|3700->1996|3714->2001|3778->2044|3804->2052|3817->2056|3856->2057|3897->2071|4064->2211|4109->2235|4150->2249|4193->2271|4234->2285|4283->2313|4322->2324|4399->2374|4439->2398|4478->2399|4517->2411|4567->2445|4606->2446|4645->2457|4717->2502|4735->2511|4769->2524|4799->2527|4817->2536|4854->2552|4908->2579|4926->2588|4962->2603|5012->2626|5030->2635|5069->2653|5133->2686|5176->2698|5212->2707|5280->2745|5329->4034|5367->4044|5446->4096|5461->4101|5504->4122|5547->4137|5630->4193|5664->4210|5705->4212|5742->4221|5788->4239|5799->4240|5833->4252|5897->4288|5908->4289|5955->4314|5999->4327|6034->4334|6122->4395|6154->4405|6184->4406|6469->4662|6488->4670|6518->4677|6714->4844|6737->4856|6767->4863|6955->5022|6977->5033|7007->5040|7246->5251|7283->5271|7323->5272|7365->5285|7461->5353|7474->5356|7499->5359|7530->5362|7543->5365|7574->5373|7629->5396|7669->5407|7845->5555|7890->5583|7930->5584|7964->5591|8004->5621|8045->5623|8079->5629|8188->5710|8205->5717|8232->5722|8264->5725|8282->5732|8310->5737|8357->5753|8391->5760|8432->5791|8473->5793|8515->5806|8624->5887|8642->5894|8675->5904|8707->5907|8725->5914|8753->5919|8808->5942|8846->5949|8886->5960|9053->6099|9096->6125|9136->6126|9178->6139|9280->6213|9296->6219|9321->6222|9352->6225|9369->6231|9397->6236|9428->6237|9460->6240|9477->6246|9505->6251|9536->6253|9553->6259|9581->6264|9612->6265|9644->6268|9661->6274|9689->6279|9744->6302|9784->6313|10045->6545|10067->6556|10097->6563|10336->6774|10394->6815|10434->6816|10476->6829|10563->6888|10596->6911|10637->6913|10667->6915|10710->6935|10742->6937|10777->6939|10808->6941|10841->6963|10882->6964|10913->6966|10953->6983|10988->6985|11043->7008|11082->7018|11149->7058|11178->7077|11219->7079|11250->7083|11264->7088|11317->7119|11356->7127|11391->7135|11440->7174|11481->7176|11516->7183|11559->7208|11573->7213|11613->7214|11643->7216|11658->7221|11710->7251|11744->7253|11778->7259|11883->7337|11897->7342|11954->7377|11988->7380
                  LINES: 27->1|31->7|31->7|33->7|34->8|35->9|35->9|38->1|39->3|40->6|41->11|43->13|43->13|43->13|43->13|46->16|46->16|46->16|48->18|50->20|50->20|75->45|75->45|75->45|81->51|81->51|81->51|82->52|86->56|86->56|86->56|87->57|87->57|87->57|88->58|88->58|88->58|90->60|94->64|94->64|95->65|95->65|96->66|96->66|97->67|100->70|100->70|100->70|101->71|101->71|101->71|102->72|103->73|103->73|103->73|103->73|103->73|103->73|104->74|104->74|104->74|105->75|105->75|105->75|107->77|108->78|109->79|112->82|115->137|117->139|125->147|125->147|125->147|127->149|129->151|129->151|129->151|130->152|130->152|130->152|130->152|131->153|131->153|131->153|132->154|133->155|137->159|137->159|137->159|143->165|143->165|143->165|144->166|144->166|144->166|145->167|145->167|145->167|150->172|150->172|150->172|151->173|151->173|151->173|151->173|151->173|151->173|151->173|152->174|153->175|156->178|156->178|156->178|157->179|157->179|157->179|158->180|158->180|158->180|158->180|158->180|158->180|158->180|159->181|160->182|160->182|160->182|161->183|161->183|161->183|161->183|161->183|161->183|161->183|162->184|163->185|164->186|167->189|167->189|167->189|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|168->190|169->191|170->192|177->199|177->199|177->199|182->204|182->204|182->204|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|183->205|184->206|185->207|189->211|189->211|189->211|190->212|190->212|190->212|191->213|192->214|192->214|192->214|193->215|195->217|195->217|195->217|195->217|195->217|195->217|195->217|197->219|206->228|206->228|206->228|208->230
                  -- GENERATED --
              */
          