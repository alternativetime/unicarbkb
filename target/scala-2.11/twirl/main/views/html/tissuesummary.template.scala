
package views.html

import play.twirl.api._


     object tissuesummary_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class tissuesummary extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template11[Tissue,HashSet[String],TreeMap[String, String],String,String,ArrayList[Taxonomy],ArrayList[Taxonomy],ArrayList[String],ArrayList[Proteins],ArrayList[String],ArrayList[Tissue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tissue: Tissue, test: HashSet[String], strtax: TreeMap[String, String], message: String, description: String, taxNames: ArrayList[Taxonomy], taxItems: ArrayList[Taxonomy], proteinNames: ArrayList[String], proteinItems: ArrayList[Proteins], sourceNames: ArrayList[String], sourceItems: ArrayList[Tissue] ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*8.2*/header/*8.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*8.38*/("""
"""),format.raw/*9.1*/("""<th class="">
  <a href="">"""),_display_(/*10.15*/title),format.raw/*10.20*/("""</a>
</th>
""")))};
Seq[Any](format.raw/*1.307*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*6.37*/("""

"""),format.raw/*12.2*/("""

"""),_display_(/*14.2*/main/*14.6*/ {_display_(Seq[Any](format.raw/*14.8*/("""
"""),format.raw/*15.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-book" ></i><a href="/references"> References</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-file" ></i> """),_display_(/*18.50*/message),format.raw/*18.57*/("""<span class="divider"></span></li>
</ul>

<section id="layouts">

  <div class="page-header row-fluid">
    <h1 class="">"""),_display_(/*24.19*/message),format.raw/*24.26*/("""</h1>
    <h4 class="subheader span8">"""),_display_(/*25.34*/description),format.raw/*25.45*/(""" """),format.raw/*25.46*/("""<span class="pull-right"><span class="label label-light">Curated Entry</span> <span class="label label-light">GlycoSuiteDB</span></span></h4>
  </div>

  <div class="row-fluid">
    <div class="span8">




      <ul class="structures clearfix">
        """),format.raw/*35.75*/("""
	"""),_display_(/*36.3*/for(t <- test) yield /*36.17*/ {_display_(Seq[Any](format.raw/*36.19*/("""
        """),format.raw/*37.9*/("""<li class="span5">
           """),_display_(/*38.13*/views/*38.18*/.html.format.structure(t.toLong)),format.raw/*38.50*/("""

        """),format.raw/*40.9*/("""</li>
        """)))}),format.raw/*41.10*/("""
      """),format.raw/*42.7*/("""</ul>
      <ul id='more-thumbnails' class='structures clearfix'>
      </ul>
      """),_display_(/*45.8*/if( test.size > 5)/*45.26*/ {_display_(Seq[Any](format.raw/*45.28*/("""
      """),format.raw/*46.7*/("""<div id='show-structures' class='more-structures' href='#'>
        <span class="linktext">See more structures</span>
        <span class="linktext" style="display:none">See less structures</span> 
        <br />
        <span>&#9679; &#9679; &#9679;</span>
      </div>
      """)))}),format.raw/*52.8*/("""

    """),format.raw/*54.5*/("""</div>

    <div class="span4 sidebar">

	  	"""),_display_(/*58.6*/views/*58.11*/.html.format.format()),format.raw/*58.32*/("""

      """),format.raw/*60.7*/("""<div class='info'>
        <h3>Biological Associations</h4>
        <div class='taxonomy'>
          <a id='toggle-taxonomy'><span class='label label-important'><span class='icon-tags icon-white'></span> Taxonomy ("""),_display_(/*63.125*/tissue/*63.131*/.taxtissue.size()),format.raw/*63.148*/(""") <span class="caret"></span></span></a>
          <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein ("""),_display_(/*64.127*/tissue/*64.133*/.proteintissue.size()),format.raw/*64.154*/(""") <span class="caret"></span></span></a> 
          """),format.raw/*65.183*/("""
        """),format.raw/*66.9*/("""</div>
	
	<div>
	  """),_display_(/*69.5*/if(tissue.taxtissue.size() > 0)/*69.36*/ {_display_(Seq[Any](format.raw/*69.38*/("""
          """),format.raw/*70.11*/("""<ul id='more-taxonomy'>
            <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Taxonomies</h3>
            """),format.raw/*75.17*/("""
	    """),_display_(/*76.7*/for(t <- tissue.taxtissue) yield /*76.33*/ {_display_(Seq[Any](format.raw/*76.35*/("""
	     """),format.raw/*77.7*/("""<li><span class='icon-tag icon-white'></span> <a href='../taxonomy/"""),_display_(/*77.75*/t/*77.76*/.taxonomy.id),format.raw/*77.88*/("""'>"""),_display_(/*77.91*/t/*77.92*/.species),format.raw/*77.100*/("""</a></li>
            """)))}),format.raw/*78.14*/("""
          """),format.raw/*79.11*/("""</ul> """)))}),format.raw/*79.18*/(""" 
	  """),_display_(/*80.5*/if(tissue.proteintissue.size() > 0)/*80.40*/ {_display_(Seq[Any](format.raw/*80.42*/("""
	  """),format.raw/*81.4*/("""<ul id='more-protein'>
	    """),_display_(/*82.7*/for(t <- tissue.proteintissue) yield /*82.37*/{_display_(Seq[Any](format.raw/*82.38*/("""
	    """),format.raw/*83.6*/("""<li><span class='icon-map-marker icon-white'></span> <a href='../proteinsummary/"""),_display_(/*83.87*/t/*83.88*/.swiss),format.raw/*83.94*/("""'>"""),_display_(/*83.97*/t/*83.98*/.protein),format.raw/*83.106*/(""" """),format.raw/*83.107*/("""("""),_display_(/*83.109*/t/*83.110*/.swiss),format.raw/*83.116*/(""")</a></li>
	    """)))}),format.raw/*84.7*/("""
	  """),format.raw/*85.4*/("""</ul>
	  """)))}),format.raw/*86.5*/("""
	"""),format.raw/*87.2*/("""</div>

      </div>


    </div>

  </div>
  """),_display_(/*95.4*/views/*95.9*/.html.footerunicarb.footerunicarb()),format.raw/*95.44*/("""
"""),format.raw/*96.1*/("""</section>


""")))}),format.raw/*99.2*/("""
"""))
      }
    }
  }

  def render(tissue:Tissue,test:HashSet[String],strtax:TreeMap[String, String],message:String,description:String,taxNames:ArrayList[Taxonomy],taxItems:ArrayList[Taxonomy],proteinNames:ArrayList[String],proteinItems:ArrayList[Proteins],sourceNames:ArrayList[String],sourceItems:ArrayList[Tissue]): play.twirl.api.HtmlFormat.Appendable = apply(tissue,test,strtax,message,description,taxNames,taxItems,proteinNames,proteinItems,sourceNames,sourceItems)

  def f:((Tissue,HashSet[String],TreeMap[String, String],String,String,ArrayList[Taxonomy],ArrayList[Taxonomy],ArrayList[String],ArrayList[Proteins],ArrayList[String],ArrayList[Tissue]) => play.twirl.api.HtmlFormat.Appendable) = (tissue,test,strtax,message,description,taxNames,taxItems,proteinNames,proteinItems,sourceNames,sourceItems) => apply(tissue,test,strtax,message,description,taxNames,taxItems,proteinNames,proteinItems,sourceNames,sourceItems)

  def ref: this.type = this

}


}

/**/
object tissuesummary extends tissuesummary_Scope0.tissuesummary
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/tissuesummary.scala.html
                  HASH: 0f2b132b845f8e2b8127234bda4eddaea3631ea4
                  MATRIX: 930->1|1328->436|1341->442|1447->472|1474->473|1529->501|1555->506|1607->306|1634->324|1662->433|1691->518|1720->521|1732->525|1771->527|1799->528|2100->802|2128->809|2277->931|2305->938|2371->977|2403->988|2432->989|2713->1308|2742->1311|2772->1325|2812->1327|2848->1336|2906->1367|2920->1372|2973->1404|3010->1414|3056->1429|3090->1436|3201->1521|3228->1539|3268->1541|3302->1548|3610->1826|3643->1832|3715->1878|3729->1883|3771->1904|3806->1912|4049->2127|4065->2133|4104->2150|4299->2317|4315->2323|4358->2344|4439->2568|4475->2577|4521->2597|4561->2628|4601->2630|4640->2641|4798->2927|4831->2934|4873->2960|4913->2962|4947->2969|5042->3037|5052->3038|5085->3050|5115->3053|5125->3054|5155->3062|5209->3085|5248->3096|5286->3103|5318->3109|5362->3144|5402->3146|5433->3150|5488->3179|5534->3209|5573->3210|5606->3216|5714->3297|5724->3298|5751->3304|5781->3307|5791->3308|5821->3316|5851->3317|5881->3319|5892->3320|5920->3326|5967->3343|5998->3347|6038->3357|6067->3359|6140->3406|6153->3411|6209->3446|6237->3447|6281->3461
                  LINES: 27->1|31->8|31->8|33->8|34->9|35->10|35->10|38->1|39->3|40->6|42->12|44->14|44->14|44->14|45->15|48->18|48->18|54->24|54->24|55->25|55->25|55->25|65->35|66->36|66->36|66->36|67->37|68->38|68->38|68->38|70->40|71->41|72->42|75->45|75->45|75->45|76->46|82->52|84->54|88->58|88->58|88->58|90->60|93->63|93->63|93->63|94->64|94->64|94->64|95->65|96->66|99->69|99->69|99->69|100->70|102->75|103->76|103->76|103->76|104->77|104->77|104->77|104->77|104->77|104->77|104->77|105->78|106->79|106->79|107->80|107->80|107->80|108->81|109->82|109->82|109->82|110->83|110->83|110->83|110->83|110->83|110->83|110->83|110->83|110->83|110->83|110->83|111->84|112->85|113->86|114->87|122->95|122->95|122->95|123->96|126->99
                  -- GENERATED --
              */
          