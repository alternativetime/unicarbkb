
package views.html

import play.twirl.api._


     object glycodigestHome_Scope0 {
import views.html._

     object glycodigestHome_Scope1 {

class glycodigestHome extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

"""),format.raw/*5.1*/("""<style>
.break-block """),format.raw/*6.14*/("""{"""),format.raw/*6.15*/("""
    """),format.raw/*7.5*/("""margin: 50px 0;
    padding: 30px;
    background-color: #f2f6f7;
    font: 16px/24px 'Open Sans', 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
    font-weight: 300
"""),format.raw/*12.1*/("""}"""),format.raw/*12.2*/("""

"""),format.raw/*14.1*/(""".break-block .text-only """),format.raw/*14.25*/("""{"""),format.raw/*14.26*/("""
    """),format.raw/*15.5*/("""position: relative;
    top: 15px
"""),format.raw/*17.1*/("""}"""),format.raw/*17.2*/("""

"""),format.raw/*19.1*/(""".break-block .reviewer """),format.raw/*19.24*/("""{"""),format.raw/*19.25*/("""
    """),format.raw/*20.5*/("""font-size: 30px;
    font-weight: 600;
    margin-bottom: 0
"""),format.raw/*23.1*/("""}"""),format.raw/*23.2*/("""

"""),format.raw/*25.1*/(""".break-block .right """),format.raw/*25.21*/("""{"""),format.raw/*25.22*/("""
    """),format.raw/*26.5*/("""width: 600px;
    background: url(../img/quote.png) no-repeat 0 0;
    padding-left: 40px
"""),format.raw/*29.1*/("""}"""),format.raw/*29.2*/("""
"""),format.raw/*30.1*/("""</style>

<div class="container-fluid content">
	<div class="row-fluid">
		<div class="span12">
			<h1 style="text-align: center">GlycoDigest: a tool for exoglycosidase digestions</h1>
			<p style="text-align: center"><img src="http://www.glycodigest.org/enzymes.png" /></p>

			<h4 style="text-align: center">GlycoDigest a tool that simulates exoglycosidase digestion, based on controlled rules acquired from expert knowledge and experimental evidence available in GlycoBase. The tool allows the targeted design of glycosidase enzyme mixtures by allowing researchers to model the action of exoglycosidases, thereby validating and improving the efficiency and accuracy of glycan analysis.</h4>
"""),format.raw/*43.3*/("""
"""),format.raw/*44.1*/("""<hr/>
<p style="text-align: center">
<a href="http://www.glycodigest.org"><button type="button" class="btn-success btn-large">Have a Look at Glycodigest.org</button></a>
<a href="/builderDigest"><button type="button" class="btn-success btn-large">Get Started: Build a Structure</button></a>
</p>
<div class="break-block"><div class="container">
		For help and instructions have a look at our <a href="https://github.com/alternativeTime/glycomics/wiki/GlycoDigest-Guide-(UniCarbKB)">wiki site</a>. Alternatively, select options from the 'GlycoDigest' drop-down list on the navigation bar including links to experimental protocols, information on supported exoglycosidases, or example data....
</div></div>
		</div>
	</div>


</div>

""")))}),format.raw/*58.2*/("""
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}
}

/**/
object glycodigestHome extends glycodigestHome_Scope0.glycodigestHome_Scope1.glycodigestHome
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/glycodigestHome.scala.html
                  HASH: 8914bbeb5f927a518ffb5116b0f4713f84a8b542
                  MATRIX: 901->19|912->23|950->25|978->27|1026->48|1054->49|1085->54|1284->226|1312->227|1341->229|1393->253|1422->254|1454->259|1515->293|1543->294|1572->296|1623->319|1652->320|1684->325|1771->385|1799->386|1828->388|1876->408|1905->409|1937->414|2054->504|2082->505|2110->506|2831->1469|2859->1470|3622->2203
                  LINES: 35->3|35->3|35->3|37->5|38->6|38->6|39->7|44->12|44->12|46->14|46->14|46->14|47->15|49->17|49->17|51->19|51->19|51->19|52->20|55->23|55->23|57->25|57->25|57->25|58->26|61->29|61->29|62->30|71->43|72->44|86->58
                  -- GENERATED --
              */
          