
package views.html

import play.twirl.api._


     object search_Scope0 {
import views.html._

class search extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""

"""),format.raw/*3.1*/("""<script type="text/javascript"> 
var vaadin = """),format.raw/*4.14*/("""{"""),format.raw/*4.15*/("""
 """),format.raw/*5.2*/("""vaadinConfigurations: """),format.raw/*5.24*/("""{"""),format.raw/*5.25*/("""
"""),format.raw/*6.1*/("""'fb' :"""),format.raw/*6.7*/("""{"""),format.raw/*6.8*/("""
"""),format.raw/*7.1*/("""appUri:'/GlycanBuilder', 
 pathInfo: '/GlycanBuilder', 
themeUri: '/GlycanBuilder/VAADIN/themes/ucdb_2011theme', 
versionInfo : """),format.raw/*10.15*/("""{"""),format.raw/*10.16*/("""vaadinVersion:"6.0.0-INTERNAL-NONVERSIONED-DEBUG-BUILD",applicationVersion:"NONVERSIONED""""),format.raw/*10.105*/("""}"""),format.raw/*10.106*/("""
 """),format.raw/*11.2*/("""}"""),format.raw/*11.3*/("""
"""),format.raw/*12.1*/("""}"""),format.raw/*12.2*/("""}"""),format.raw/*12.3*/(""";
</script> 
  
<script language='javascript' src='GlycanBuilder/VAADIN/widgetsets/ac.uk.icl.dell.vaadin.glycanbuilder.widgetset.GlycanbuilderWidgetset/ac.uk.icl.dell.vaadin.glycanbuilder.widgetset.GlycanbuilderWidgetset.nocache.js'></script> 
<link rel="stylesheet" type="text/css" href="/GlycanBuilder/VAADIN/themes/ucdb_2011theme/styles.css"/>
          

<body onLoad="Write()">

  <ul class="breadcrumb">
    <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
    <li class="active"><i class="icon-book" ></i> References<span class="divider"></span></li>
    <!--<li class="active" > You are here</li>-->
  </ul>

<section id="layouts" class="browse">
  <div class="page-header row-fluid">
    <h1>Glycan Builder</h1>
    <h4 class="subheader span8">An intuitive interface for building and displaying glycan structures using a collection of notation formats.</h4>
  </div>
  <div class="row-fluid">
	
	<div class="span6">
	<h3 class="builder">Find Structures</h3>	
	<p class="builder">Build your glycan or epitope and search the UniCarbKB structure database. This new design is built using new technologies and discussed by the developer David Damerell.</p>
	</div>
	
	<div class="span6">
	<h3 class="builder">Publication</h3>
	<p class="builder">The GlycanBuilder: a fast, intuitive and flexible software tool for building and displaying glycan structures, Alessio Ceroni, Anne Dell, and Stuart M Haslam, Source Code Biol Med. 2007; 2: 3. PubMed</p>
	</div>
	
	
      </div>
</section>



<div id="wrap">
<a class="btn" href=""><i class="icon-refresh"></i> Refresh</a> <a class="btn btn-warning" href="#"> Builder Applet</a>
<iframe id="__gwt_historyFrame" style="width:0;height:0;border:0;overflow:hidden" src="javascript:false"></iframe>

        <div id="fb" style="height:450px; width:80%;margin:0"></div>

	<script type="text/javascript"> 
	var callBack=[];
        callBack.run=function(response)"""),format.raw/*58.40*/("""{"""),format.raw/*58.41*/("""
          """),format.raw/*59.11*/("""//document.write(response);
	 var r = response;
	  //document.write(r);
	  //document.form.frm1.Search = 'test this';
	  //document.form.frm1.submit();
	//document.forms["frm2"].submit();
	var url = "../saySearch/s?";
	var x = url + encodeURI(r);
	//var s = [ url, "" , r].join("");
	//document.write(x);
window.location = x ;
        """),format.raw/*70.9*/("""}"""),format.raw/*70.10*/("""
       """),format.raw/*71.8*/("""</script>
       
       
<script type="text/javascript"> 
function Write()
"""),format.raw/*76.1*/("""{"""),format.raw/*76.2*/("""
"""),format.raw/*77.1*/("""var Scr = new ActiveXObject("Scripting.FileSystemObject");
var CTF = Scr.CreateTextFile("/tmp/tmp.out", true);
CTF.WriteLine('Roseindia');
CTF.Close();
"""),format.raw/*81.1*/("""}"""),format.raw/*81.2*/("""
"""),format.raw/*82.1*/("""</script>

<script language="JavaScript">
 cookie_name = "Basic_Cookie";
 function write_cookie() """),format.raw/*86.26*/("""{"""),format.raw/*86.27*/("""
 """),format.raw/*87.2*/("""if(document.cookie) """),format.raw/*87.22*/("""{"""),format.raw/*87.23*/("""
 """),format.raw/*88.2*/("""index = document.cookie.indexOf(cookie_name);
 """),format.raw/*89.2*/("""}"""),format.raw/*89.3*/(""" """),format.raw/*89.4*/("""else """),format.raw/*89.9*/("""{"""),format.raw/*89.10*/("""
 """),format.raw/*90.2*/("""index = -1;
 """),format.raw/*91.2*/("""}"""),format.raw/*91.3*/("""
 
 """),format.raw/*93.2*/("""if (index == -1) """),format.raw/*93.19*/("""{"""),format.raw/*93.20*/("""
 """),format.raw/*94.2*/("""document.cookie=cookie_name+"=1; expires=Wednesday, 01-Aug-2040 08:00:00 GMT";
 """),format.raw/*95.2*/("""}"""),format.raw/*95.3*/(""" """),format.raw/*95.4*/("""else """),format.raw/*95.9*/("""{"""),format.raw/*95.10*/("""
 """),format.raw/*96.2*/("""countbegin = (document.cookie.indexOf("=", index) + 1);
 countend = document.cookie.indexOf(";", index);
 if (countend == -1) """),format.raw/*98.22*/("""{"""),format.raw/*98.23*/("""
 """),format.raw/*99.2*/("""countend = document.cookie.length;
 """),format.raw/*100.2*/("""}"""),format.raw/*100.3*/("""
 """),format.raw/*101.2*/("""count = eval(document.cookie.substring(countbegin, countend)) + 1;
 document.cookie=cookie_name+"="+count+"; expires=Wednesday, 01-Aug-2040 08:00:00 GMT";
 """),format.raw/*103.2*/("""}"""),format.raw/*103.3*/("""
 """),format.raw/*104.2*/("""}"""),format.raw/*104.3*/("""
 """),format.raw/*105.2*/("""</script>
       
"""),format.raw/*114.21*/("""

	"""),format.raw/*116.2*/("""<form id="frm1" name="frm1" action="ms" method="POST" class="form-search">
	<input type="button" class="btn" name="Search" value="Search" onclick='window["glycanCanvas"].runCommand("export~glycoct_condensed",callBack);'/>
        </form>
</div>


</div>

""")))}),format.raw/*124.2*/("""


"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object search extends search_Scope0.search
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/search.scala.html
                  HASH: fb9c0977b61146bdb6115457b37c1b54b9c36e90
                  MATRIX: 829->1|840->5|878->7|906->9|979->55|1007->56|1035->58|1084->80|1112->81|1139->82|1171->88|1198->89|1225->90|1381->218|1410->219|1528->308|1558->309|1587->311|1615->312|1643->313|1671->314|1699->315|3677->2265|3706->2266|3745->2277|4107->2612|4136->2613|4171->2621|4274->2697|4302->2698|4330->2699|4509->2851|4537->2852|4565->2853|4691->2951|4720->2952|4749->2954|4797->2974|4826->2975|4855->2977|4929->3024|4957->3025|4985->3026|5017->3031|5046->3032|5075->3034|5115->3047|5143->3048|5174->3052|5219->3069|5248->3070|5277->3072|5384->3152|5412->3153|5440->3154|5472->3159|5501->3160|5530->3162|5684->3288|5713->3289|5742->3291|5806->3327|5835->3328|5865->3330|6049->3486|6078->3487|6108->3489|6137->3490|6167->3492|6214->3744|6245->3747|6532->4003
                  LINES: 32->1|32->1|32->1|34->3|35->4|35->4|36->5|36->5|36->5|37->6|37->6|37->6|38->7|41->10|41->10|41->10|41->10|42->11|42->11|43->12|43->12|43->12|89->58|89->58|90->59|101->70|101->70|102->71|107->76|107->76|108->77|112->81|112->81|113->82|117->86|117->86|118->87|118->87|118->87|119->88|120->89|120->89|120->89|120->89|120->89|121->90|122->91|122->91|124->93|124->93|124->93|125->94|126->95|126->95|126->95|126->95|126->95|127->96|129->98|129->98|130->99|131->100|131->100|132->101|134->103|134->103|135->104|135->104|136->105|138->114|140->116|148->124
                  -- GENERATED --
              */
          