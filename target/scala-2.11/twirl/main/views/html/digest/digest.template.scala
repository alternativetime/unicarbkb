
package views.html.digest

import play.twirl.api._


     object digest_Scope0 {
import models._

class digest extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Structure,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(strInfo : Structure):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""
"""),format.raw/*2.1*/("""<div class="info">
	<h3>GlycoDigest</h3>
	<a href="/glycodigest/"""),_display_(/*4.25*/strInfo/*4.32*/.id),format.raw/*4.35*/(""""><span class='label label-success'>Simulate exoglycosidase digestion</span></a>
</div>

"""))
      }
    }
  }

  def render(strInfo:Structure): play.twirl.api.HtmlFormat.Appendable = apply(strInfo)

  def f:((Structure) => play.twirl.api.HtmlFormat.Appendable) = (strInfo) => apply(strInfo)

  def ref: this.type = this

}


}

/**/
object digest extends digest_Scope0.digest
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:04 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/digest/digest.scala.html
                  HASH: 9e8a20d5fc568cb7c813b625f0cafb2eb84a61b7
                  MATRIX: 757->1|873->22|900->23|991->88|1006->95|1029->98
                  LINES: 27->1|32->1|33->2|35->4|35->4|35->4
                  -- GENERATED --
              */
          