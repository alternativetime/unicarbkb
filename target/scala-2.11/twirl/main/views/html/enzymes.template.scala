
package views.html

import play.twirl.api._


     object enzymes_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class enzymes extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[Enzyme],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(enzyme: List[Enzyme]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.24*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""
	
	"""),format.raw/*5.2*/("""<h1>Enzyme Descriptions</h1>
	
	<p>A new feature is the integration of known genes and enzymes involved in the biosynthesis of N-glycans stored in UniCarbKB.</p>
	<p>A description of the enzymes used is provided:</p>
 
        	<table class="computers table table-striped">
            <thead>
                <tr>
                	
                    <th>Enzyme Name</th>
                    <th>JCGGDB Description</th>
                    <th>GO Term</th>
                    <th>Enzyme Accession</th>
		            <th>CAZy Family</th>
                </tr>
            </thead>
            <tbody>
            	"""),_display_(/*22.15*/for(e <- enzyme) yield /*22.31*/{_display_(Seq[Any](format.raw/*22.32*/("""
            	"""),_display_(/*23.15*/if(!e.name.contains("NA"))/*23.41*/{_display_(Seq[Any](format.raw/*23.42*/("""
            	"""),format.raw/*24.14*/("""<tr>
            	<td>"""),_display_(/*25.19*/e/*25.20*/.name),format.raw/*25.25*/("""</td><td>"""),_display_(/*25.35*/e/*25.36*/.jcggdb),format.raw/*25.43*/("""</td><td>"""),_display_(/*25.53*/e/*25.54*/.goterm),format.raw/*25.61*/("""</td><td>"""),_display_(/*25.71*/e/*25.72*/.kegg),format.raw/*25.77*/("""</td><td><a href="http://www.cazy.org/"""),_display_(/*25.116*/{e.cazyfamily}),format.raw/*25.130*/(""".html">"""),_display_(/*25.138*/e/*25.139*/.cazyfamily),format.raw/*25.150*/("""</a></td>
            	</tr>
            	""")))}),format.raw/*27.15*/("""
            	""")))}),format.raw/*28.15*/("""
            	
            """),format.raw/*30.13*/("""</tbody>
            </table>	
          
  
""")))}),format.raw/*34.2*/("""
    
"""))
      }
    }
  }

  def render(enzyme:List[Enzyme]): play.twirl.api.HtmlFormat.Appendable = apply(enzyme)

  def f:((List[Enzyme]) => play.twirl.api.HtmlFormat.Appendable) = (enzyme) => apply(enzyme)

  def ref: this.type = this

}


}

/**/
object enzymes extends enzymes_Scope0.enzymes
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/enzymes.scala.html
                  HASH: b3fe9ad315c075f5a8d3cb1d7156214e3af66080
                  MATRIX: 755->1|872->23|900->26|911->30|948->31|978->35|1622->652|1654->668|1693->669|1735->684|1770->710|1809->711|1851->725|1901->748|1911->749|1937->754|1974->764|1984->765|2012->772|2049->782|2059->783|2087->790|2124->800|2134->801|2160->806|2227->845|2263->859|2299->867|2310->868|2343->879|2417->922|2463->937|2518->964|2594->1010
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|53->22|53->22|53->22|54->23|54->23|54->23|55->24|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|56->25|58->27|59->28|61->30|65->34
                  -- GENERATED --
              */
          