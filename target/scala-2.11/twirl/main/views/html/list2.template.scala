
package views.html

import play.twirl.api._


     object list2_Scope0 {
import controllers._
import models._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc.Http.Context.Implicit._
import views.html._

class list2 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[com.avaje.ebean.PagedList[Reference],String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(currentPage: com.avaje.ebean.PagedList[Reference], currentSortBy: String, currentOrder: String, currentFilter: String, proteinFilter: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*32.2*/header/*32.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*32.38*/("""
    """),format.raw/*33.5*/("""<th class=""""),_display_(/*33.17*/key/*33.20*/.replace(".","_")),format.raw/*33.37*/(""" """),format.raw/*33.38*/("""header """),_display_(/*33.46*/if(currentSortBy == key){/*33.72*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*33.136*/("""">
        <a href=""""),_display_(/*34.19*/link(0, key)),format.raw/*34.31*/("""">"""),_display_(/*34.34*/title),format.raw/*34.39*/("""</a>
    </th>
""")))};def /*6.2*/link/*6.6*/(newPage:Int, newSortBy:String) = {{
    
    var sortBy = currentSortBy
    var order = currentOrder
    
    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
                order = "asc"
            }
        } else {
            order = "asc"
        }
    }
    
    // Generate the link
    routes.Application.list2(newPage, sortBy, order, currentFilter)
    
}};
Seq[Any](format.raw/*1.144*/("""

"""),format.raw/*5.42*/("""
"""),format.raw/*27.2*/("""

"""),format.raw/*31.37*/("""
"""),format.raw/*36.2*/("""

"""),_display_(/*38.2*/main/*38.6*/ {_display_(Seq[Any](format.raw/*38.8*/("""
    
    """),format.raw/*40.5*/("""<h1 id="homeTitle">"""),_display_(/*40.25*/Messages("GlycoSuite Reference ", currentPage.getTotalRowCount)),format.raw/*40.88*/("""</h1>

    """),_display_(/*42.6*/if(flash.containsKey("success"))/*42.38*/ {_display_(Seq[Any](format.raw/*42.40*/("""
        """),format.raw/*43.9*/("""<div class="alert-message warning">
            <strong>Done!</strong> """),_display_(/*44.37*/flash/*44.42*/.get("success")),format.raw/*44.57*/("""
        """),format.raw/*45.9*/("""</div>
    """)))}),format.raw/*46.6*/(""" 

    """),format.raw/*48.5*/("""<div id="actions">
        <form action=""""),_display_(/*49.24*/link(0, "name")),format.raw/*49.39*/("""" method="GET">
            <input type="search" id="searchbox" name="f" value=""""),_display_(/*50.66*/currentFilter),format.raw/*50.79*/("""" placeholder="Filter by reference title or author name...">
            <input type="submit" id="searchsubmit" value="Filter" class="btn primary">
        </form>
        
        """),format.raw/*54.105*/("""
	"""),format.raw/*55.2*/("""<a class="btn success" id="add" href="/references?s=authors">Show All References</a>  
        
    </div>
    
    """),_display_(/*59.6*/if(currentPage.getTotalRowCount == 0)/*59.43*/ {_display_(Seq[Any](format.raw/*59.45*/("""
        
        """),format.raw/*61.9*/("""<div class="well">
            <em>Nothing to display</em>
        </div>
        
    """)))}/*65.7*/else/*65.12*/{_display_(Seq[Any](format.raw/*65.13*/("""
        
        """),format.raw/*67.9*/("""<table class="computers zebra-striped">
            <thead>
                <tr>
                    """),_display_(/*70.22*/header("title", "Title")),format.raw/*70.46*/("""
                    """),_display_(/*71.22*/header("year", "Year")),format.raw/*71.44*/("""
                    """),_display_(/*72.22*/header("authors", "Authors")),format.raw/*72.50*/("""
                    """),_display_(/*73.22*/header("journal.name", "Journalss")),format.raw/*73.57*/("""
		    """),_display_(/*74.8*/header("strference.id", "Streference")),format.raw/*74.46*/("""
                """),format.raw/*75.17*/("""</tr>
            </thead>
            <tbody>

                """),_display_(/*79.18*/for(reference <- currentPage.getList) yield /*79.55*/ {_display_(Seq[Any](format.raw/*79.57*/("""
                    """),format.raw/*80.21*/("""<tr>
                        <td><a href="">"""),_display_(/*81.41*/reference/*81.50*/.title),format.raw/*81.56*/("""</a></td>
                        <td>
                            """),_display_(/*83.30*/if(reference.year < 1)/*83.52*/ {_display_(Seq[Any](format.raw/*83.54*/("""
                                """),format.raw/*84.33*/("""<em>-</em>
                            """)))}/*85.31*/else/*85.36*/{_display_(Seq[Any](format.raw/*85.37*/("""
                                """),_display_(/*86.34*/reference/*86.43*/.year),format.raw/*86.48*/("""
                            """)))}),format.raw/*87.30*/("""
                        """),format.raw/*88.25*/("""</td>
                        <td>
                            """),_display_(/*90.30*/if(reference.authors == null)/*90.59*/ {_display_(Seq[Any](format.raw/*90.61*/("""
                                """),format.raw/*91.33*/("""<em>-</em>
                            """)))}/*92.31*/else/*92.36*/{_display_(Seq[Any](format.raw/*92.37*/("""
                                """),_display_(/*93.34*/reference/*93.43*/.authors),format.raw/*93.51*/("""
                            """)))}),format.raw/*94.30*/("""
                        """),format.raw/*95.25*/("""</td>
                        <td>
                            """),_display_(/*97.30*/if(reference.journal == null)/*97.59*/ {_display_(Seq[Any](format.raw/*97.61*/("""
                                """),format.raw/*98.33*/("""<em>-</em>
                            """)))}/*99.31*/else/*99.36*/{_display_(Seq[Any](format.raw/*99.37*/("""
                                """),_display_(/*100.34*/reference/*100.43*/.journal.name),format.raw/*100.56*/("""
                            """)))}),format.raw/*101.30*/("""
                        """),format.raw/*102.25*/("""</td>
                    </tr>
                """)))}),format.raw/*104.18*/("""

            """),format.raw/*106.13*/("""</tbody>
        </table>

        <div id="pagination" class="pagination">
            <ul>
                """),_display_(/*111.18*/if(currentPage.hasPrev)/*111.41*/ {_display_(Seq[Any](format.raw/*111.43*/("""
                    """),format.raw/*112.21*/("""<li class="prev">
                        <a href=""""),_display_(/*113.35*/link(currentPage.getPageIndex - 1, null)),format.raw/*113.75*/("""">&larr; Previous</a>
                    </li>
                """)))}/*115.19*/else/*115.24*/{_display_(Seq[Any](format.raw/*115.25*/("""
                    """),format.raw/*116.21*/("""<li class="prev disabled">
                        <a>&larr; Previous</a>
                    </li>
                """)))}),format.raw/*119.18*/("""
                """),format.raw/*120.17*/("""<li class="current">
                    <a>Displaying """),_display_(/*121.36*/currentPage/*121.47*/.getDisplayXtoYofZ(" to "," of ")),format.raw/*121.80*/("""</a>
                </li>
                """),_display_(/*123.18*/if(currentPage.hasNext)/*123.41*/ {_display_(Seq[Any](format.raw/*123.43*/("""
                    """),format.raw/*124.21*/("""<li class="next">
                        <a href=""""),_display_(/*125.35*/link(currentPage.getPageIndex + 1, null)),format.raw/*125.75*/("""">Next &rarr;</a>
                    </li>
                """)))}/*127.19*/else/*127.24*/{_display_(Seq[Any](format.raw/*127.25*/("""
                    """),format.raw/*128.21*/("""<li class="next disabled">
                        <a>Next &rarr;</a>
                    </li>
                """)))}),format.raw/*131.18*/("""
            """),format.raw/*132.13*/("""</ul>
        </div>
        
    """)))}),format.raw/*135.6*/("""
        
""")))}),format.raw/*137.2*/("""

            
"""))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[Reference],currentSortBy:String,currentOrder:String,currentFilter:String,proteinFilter:String): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter,proteinFilter)

  def f:((com.avaje.ebean.PagedList[Reference],String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter,proteinFilter) => apply(currentPage,currentSortBy,currentOrder,currentFilter,proteinFilter)

  def ref: this.type = this

}


}

/**/
object list2 extends list2_Scope0.list2
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:01 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/list2.scala.html
                  HASH: c93b198a70d0333ab6e8bba90d9116ea196c4d51
                  MATRIX: 803->1|1024->890|1038->896|1145->926|1177->931|1216->943|1228->946|1266->963|1295->964|1330->972|1364->998|1451->1062|1499->1083|1532->1095|1562->1098|1588->1103|1626->272|1637->276|2170->143|2199->270|2227->778|2257->888|2285->1119|2314->1122|2326->1126|2365->1128|2402->1138|2449->1158|2533->1221|2571->1233|2612->1265|2652->1267|2688->1276|2787->1348|2801->1353|2837->1368|2873->1377|2915->1389|2949->1396|3018->1438|3054->1453|3162->1534|3196->1547|3406->1824|3435->1826|3578->1943|3624->1980|3664->1982|3709->2000|3815->2089|3828->2094|3867->2095|3912->2113|4041->2215|4086->2239|4135->2261|4178->2283|4227->2305|4276->2333|4325->2355|4381->2390|4415->2398|4474->2436|4519->2453|4611->2518|4664->2555|4704->2557|4753->2578|4825->2623|4843->2632|4870->2638|4965->2706|4996->2728|5036->2730|5097->2763|5156->2804|5169->2809|5208->2810|5269->2844|5287->2853|5313->2858|5374->2888|5427->2913|5518->2977|5556->3006|5596->3008|5657->3041|5716->3082|5729->3087|5768->3088|5829->3122|5847->3131|5876->3139|5937->3169|5990->3194|6081->3258|6119->3287|6159->3289|6220->3322|6279->3363|6292->3368|6331->3369|6393->3403|6412->3412|6447->3425|6509->3455|6563->3480|6644->3529|6687->3543|6825->3653|6858->3676|6899->3678|6949->3699|7029->3751|7091->3791|7176->3857|7190->3862|7230->3863|7280->3884|7429->4001|7475->4018|7559->4074|7580->4085|7635->4118|7707->4162|7740->4185|7781->4187|7831->4208|7911->4260|7973->4300|8054->4362|8068->4367|8108->4368|8158->4389|8303->4502|8345->4515|8411->4550|8453->4561
                  LINES: 27->1|31->32|31->32|33->32|34->33|34->33|34->33|34->33|34->33|34->33|34->33|34->33|35->34|35->34|35->34|35->34|37->6|37->6|59->1|61->5|62->27|64->31|65->36|67->38|67->38|67->38|69->40|69->40|69->40|71->42|71->42|71->42|72->43|73->44|73->44|73->44|74->45|75->46|77->48|78->49|78->49|79->50|79->50|83->54|84->55|88->59|88->59|88->59|90->61|94->65|94->65|94->65|96->67|99->70|99->70|100->71|100->71|101->72|101->72|102->73|102->73|103->74|103->74|104->75|108->79|108->79|108->79|109->80|110->81|110->81|110->81|112->83|112->83|112->83|113->84|114->85|114->85|114->85|115->86|115->86|115->86|116->87|117->88|119->90|119->90|119->90|120->91|121->92|121->92|121->92|122->93|122->93|122->93|123->94|124->95|126->97|126->97|126->97|127->98|128->99|128->99|128->99|129->100|129->100|129->100|130->101|131->102|133->104|135->106|140->111|140->111|140->111|141->112|142->113|142->113|144->115|144->115|144->115|145->116|148->119|149->120|150->121|150->121|150->121|152->123|152->123|152->123|153->124|154->125|154->125|156->127|156->127|156->127|157->128|160->131|161->132|164->135|166->137
                  -- GENERATED --
              */
          