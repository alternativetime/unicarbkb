
package views.html.hmg

import play.twirl.api._


     object hmgStructure_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

     object hmgStructure_Scope1 {
import play.libs.F.Promise

class hmgStructure extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[models.hmg.Hmg,Promise[List[String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(structures: models.hmg.Hmg, hits: Promise[List[String]]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.59*/("""

    """),_display_(/*4.6*/main/*4.10*/ {_display_(Seq[Any](format.raw/*4.12*/("""

        """),format.raw/*6.9*/("""<ul class="breadcrumb" xmlns="http://www.w3.org/1999/html">
            <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
            <li><i class="icon-home" ></i><a href="/"> Milk Glycome</a> <span class="divider">></span></li>
            <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
                <!--<li class="active" > You are here</li>-->
        </ul>

        <section id="layouts" class="browse">
            <div class="page-header row-fluid">
                <h1>Virtual Milk Glycome - GlycoRDF Structure Search</h1>
            </div>

            <div class="row-fluid">
                <div class="span8">

                    <img class="sugar_image" src="""),_display_(/*21.51*/{
                        routes.Image.showImageHmg(structures.id, "cfgl", "extended")
                    }),format.raw/*23.22*/(""" """),format.raw/*23.23*/("""height="50%", width="50%" alt=""/></a>

                    <h3>IUPAC</h3>
                    <p>"""),_display_(/*26.25*/structures/*26.35*/.iupac),format.raw/*26.41*/("""</p>

                    <h3>GlycoCT</h3>
                    <p>"""),_display_(/*29.25*/Html(HtmlFormat.escape(structures.glycoct).toString.replace("\n", "<br />"))),format.raw/*29.101*/("""</p>


                    <h3>Sub(structure) Search with RDF</h3>
                    <table class="table table-condensed table-bordered table-striped volumes">
                        <thead>
                            <tr>
                                <th>Accession Number</th>
                                <th>Structure</th>
                            </tr>
                        </thead>
                        <tbody>
                        """),_display_(/*41.26*/for(ph <- hits.get(1)) yield /*41.48*/ {_display_(Seq[Any](format.raw/*41.50*/("""
                            """),format.raw/*42.29*/("""<tr>
                                <td>UHMG"""),_display_(/*43.42*/ph/*43.44*/.replaceAll("structureConnection", "").replaceAll("\\> \\)", "").replaceAll("\\( \\?", "").split("=")),format.raw/*43.145*/("""</td>
                                <td><a href=""><img class="sugar_image" src="""),_display_(/*44.78*/{
                                    routes.Image.showImageHmg(ph.replaceAll("structureConnection", "").replaceAll("\\> \\)", "").replaceAll("\\( \\?", "").replaceAll("=", "").replaceAll("""^\s+(?m)""","").toLong, "cfgl", "extended")
                                }),format.raw/*46.34*/(""" """),format.raw/*46.35*/("""height="50%", width="50%" alt=""/>
                                </a>
                                </td>
                            </tr>
                        """)))}),format.raw/*50.26*/("""
                        """),format.raw/*51.25*/("""</tbody>
                    </table>

                </div>
                <div class="span4 sidebar">

                    """),_display_(/*57.22*/views/*57.27*/.html.format.format()),format.raw/*57.48*/("""

                    """),format.raw/*59.21*/("""<div class="info">
                        <h3>Structure Details</h3>
                        <p>GlcNAc: """),_display_(/*61.37*/structures/*61.47*/.glcnac),format.raw/*61.54*/("""</p>
                        <p>Fucose: """),_display_(/*62.37*/structures/*62.47*/.fuc),format.raw/*62.51*/("""</p>
                        <p>Galactose: """),_display_(/*63.40*/structures/*63.50*/.gal),format.raw/*63.54*/("""</p>
                        <p>Sialic acid: """),_display_(/*64.42*/structures/*64.52*/.kdn),format.raw/*64.56*/("""</p>
                        """),format.raw/*65.64*/("""
                    """),format.raw/*66.21*/("""</div>

                    <div class="info">
                        <h3>Contribution Information</h3>
                        <div class='taxonomy'>
                            <span class='label label-success'>Emory University - David Smith and Sanjay Agravat</span>
                            <span class='label label-important'>Virtual Milk Glycome</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    """)))}),format.raw/*79.6*/("""
"""))
      }
    }
  }

  def render(structures:models.hmg.Hmg,hits:Promise[List[String]]): play.twirl.api.HtmlFormat.Appendable = apply(structures,hits)

  def f:((models.hmg.Hmg,Promise[List[String]]) => play.twirl.api.HtmlFormat.Appendable) = (structures,hits) => apply(structures,hits)

  def ref: this.type = this

}


}
}

/**/
object hmgStructure extends hmgStructure_Scope0.hmgStructure_Scope1.hmgStructure
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/hmg/hmgStructure.scala.html
                  HASH: f01f960bafd0740f720cf33b70cf08cdb9188c16
                  MATRIX: 855->29|1007->86|1039->93|1051->97|1090->99|1126->109|1915->871|2044->979|2073->980|2199->1079|2218->1089|2245->1095|2339->1162|2437->1238|2924->1698|2962->1720|3002->1722|3059->1751|3132->1797|3143->1799|3266->1900|3376->1983|3665->2251|3694->2252|3894->2421|3947->2446|4102->2574|4116->2579|4158->2600|4208->2622|4341->2728|4360->2738|4388->2745|4456->2786|4475->2796|4500->2800|4571->2844|4590->2854|4615->2858|4688->2904|4707->2914|4732->2918|4789->2986|4838->3007|5356->3495
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|54->21|56->23|56->23|59->26|59->26|59->26|62->29|62->29|74->41|74->41|74->41|75->42|76->43|76->43|76->43|77->44|79->46|79->46|83->50|84->51|90->57|90->57|90->57|92->59|94->61|94->61|94->61|95->62|95->62|95->62|96->63|96->63|96->63|97->64|97->64|97->64|98->65|99->66|112->79
                  -- GENERATED --
              */
          