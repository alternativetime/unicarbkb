
package views.html.hmg

import play.twirl.api._


     object showMilkPublishedStructures_Scope0 {
import java.util._

import controllers._
import views.html._

import scala.collection.JavaConversions._

class showMilkPublishedStructures extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Streference],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structures: List[models.Streference], temp: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

Seq[Any](format.raw/*1.54*/("""
"""),format.raw/*3.1*/("""
"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

    """),format.raw/*6.5*/("""<ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        /IU
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>Virtual Milk Glycome</h1>
        </div>

    <form class="form-vertical" action =""""),_display_(/*18.43*/(routes.HmgStructures.searchCompositionHmg())),format.raw/*18.88*/("""">
        <fieldset>

                <!-- Form Name -->
            <legend>Composition and IUPAC Search</legend>

                <!-- Text input-->
            <div class="row">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span6">
                            <label class="control-label" for="textinput">Gal</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Gal" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_gal"  class="span3">
                                    <option value="between">Between</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Gal2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">Fuc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Fuc" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_fuc"  class="span3">
                                    <option value="between">Exact</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Fuc2" type="text" placeholder="0" class="span2" value="0">
                            </div>

			   <label class="control-label" for="textinput">GlcNAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Glcnac" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_glcnac"  class="span3">
                                    <option value="between">Exact</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Glcnac2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            """),format.raw/*71.37*/("""

                        """),format.raw/*73.25*/("""</div>
                        <div class="span6">
                            <label class="control-label" for="textinput">NeuAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Kdn" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_kdn"  class="span3">
                                    <option value="between">Exact</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Kdn2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            <label class="control-label" for="textinput">Glc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Glc" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_glc"  class="span3">
                                    <option value="between">Exact</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Glc2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                            """),format.raw/*107.10*/("""


                        """),format.raw/*110.25*/("""</div>

                        """),format.raw/*122.33*/("""
                    """),format.raw/*123.21*/("""</div>

                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="row-fluid">
                        <p>IUPAC search will before a partial match against the database only</p>
                        <label class="control-label" for="textinput">IUPAC </label>
                        <div class="controls">
                            <textarea id="textinput" name="IUPAC" type="text" placeholder="Gal(b1-4)[Fuc(a1-3)]GlcNAc(b1-3)Gal(b1-4)[Fuc(a1-3)]Glc" class="input-large span6" ></textarea>
                        </div>

                        <label class="control-label" for="textinput">Mass </label>
                        <div class="controls">
                            <input id="textinput" name="mass" type="text" placeholder="Mass" value="0.0" class="input-large span3">
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name=rdf"> Try the RDF Search
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Search Database</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <h3>New Sub(structure) Search </h3>
    <table class="table table-condensed table-bordered table-striped volumes">
        <thead>
            <tr>
                <th>Accession Number</th>
                <th>Structure</th>
            </tr>
        </thead>
        <tbody>
            """),_display_(/*162.14*/for(s <- structures) yield /*162.34*/{_display_(Seq[Any](format.raw/*162.35*/("""
               """),format.raw/*163.16*/("""<tr>
                <td>UHMG"""),_display_(/*164.26*/s/*164.27*/.structure.id),format.raw/*164.40*/("""</td>
                <td><a href=""""),_display_(/*165.31*/{routes.HmgStructures.searchHmgRDF(s.structure.id)}),format.raw/*165.82*/(""""><img class="sugar_image" src="""),_display_(/*165.114*/{routes.Image.showImage(s.structure.id, "cfgl", "extended" )}),format.raw/*165.175*/(""" """),format.raw/*165.176*/("""height="25%", width="25%" alt=""/></a></td>
               </tr>
            """)))}),format.raw/*167.14*/("""
        """),format.raw/*168.9*/("""</tbody>
    </table>

    </section>

    <script>
        $(function() """),format.raw/*174.22*/("""{"""),format.raw/*174.23*/("""
    """),format.raw/*175.5*/("""//  changes mouse cursor when highlighting loawer right of box
    $(document).on('mousemove', 'textarea', function(e) """),format.raw/*176.57*/("""{"""),format.raw/*176.58*/("""
		"""),format.raw/*177.3*/("""var a = $(this).offset().top + $(this).outerHeight() - 16,	//	top border of bottom-right-corner-box area
			b = $(this).offset().left + $(this).outerWidth() - 16;	//	left border of bottom-right-corner-box area
		$(this).css("""),format.raw/*179.15*/("""{"""),format.raw/*179.16*/("""
			"""),format.raw/*180.4*/("""cursor: e.pageY > a && e.pageX > b ? 'nw-resize' : ''
		"""),format.raw/*181.3*/("""}"""),format.raw/*181.4*/(""");
	"""),format.raw/*182.2*/("""}"""),format.raw/*182.3*/(""")
    //  the following simple make the textbox "Auto-Expand" as it is typed in
    .on('keyup', 'textarea', function(e) """),format.raw/*184.42*/("""{"""),format.raw/*184.43*/("""
        """),format.raw/*185.9*/("""//  the following will help the text expand as typing takes place
        while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) """),format.raw/*186.149*/("""{"""),format.raw/*186.150*/("""
            """),format.raw/*187.13*/("""$(this).height($(this).height()+1);
        """),format.raw/*188.9*/("""}"""),format.raw/*188.10*/(""";
    """),format.raw/*189.5*/("""}"""),format.raw/*189.6*/(""");
"""),format.raw/*190.1*/("""}"""),format.raw/*190.2*/(""");
    </script>

""")))}),format.raw/*193.2*/("""
"""))
      }
    }
  }

  def render(structures:List[models.Streference],temp:String): play.twirl.api.HtmlFormat.Appendable = apply(structures,temp)

  def f:((List[models.Streference],String) => play.twirl.api.HtmlFormat.Appendable) = (structures,temp) => apply(structures,temp)

  def ref: this.type = this

}


}

/**/
object showMilkPublishedStructures extends showMilkPublishedStructures_Scope0.showMilkPublishedStructures
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/hmg/showMilkPublishedStructures.scala.html
                  HASH: d5808b288f1220d8a47e7d51201946f34434acd1
                  MATRIX: 818->1|980->53|1007->71|1034->73|1045->77|1083->79|1115->85|1631->574|1697->619|4269->3889|4323->3915|5911->6208|5967->6235|6028->6914|6078->6935|7735->8564|7772->8584|7812->8585|7857->8601|7915->8631|7926->8632|7961->8645|8025->8681|8098->8732|8159->8764|8243->8825|8274->8826|8384->8904|8421->8913|8523->8986|8553->8987|8586->8992|8734->9111|8764->9112|8795->9115|9048->9339|9078->9340|9110->9344|9194->9400|9223->9401|9255->9405|9284->9406|9434->9527|9464->9528|9501->9537|9745->9751|9776->9752|9818->9765|9890->9809|9920->9810|9954->9816|9983->9817|10014->9820|10043->9821|10093->9840
                  LINES: 27->1|32->1|33->3|34->4|34->4|34->4|36->6|48->18|48->18|92->71|94->73|118->107|121->110|123->122|124->123|163->162|163->162|163->162|164->163|165->164|165->164|165->164|166->165|166->165|166->165|166->165|166->165|168->167|169->168|175->174|175->174|176->175|177->176|177->176|178->177|180->179|180->179|181->180|182->181|182->181|183->182|183->182|185->184|185->184|186->185|187->186|187->186|188->187|189->188|189->188|190->189|190->189|191->190|191->190|194->193
                  -- GENERATED --
              */
          