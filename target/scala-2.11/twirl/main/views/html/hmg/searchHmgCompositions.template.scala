
package views.html.hmg

import play.twirl.api._


     object searchHmgCompositions_Scope0 {
import controllers._
import views.html._

import scala.collection.JavaConversions._

class searchHmgCompositions extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[java.util.List[models.hmg.Hmg],Integer,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(structures: java.util.List[models.hmg.Hmg], count: Integer ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

Seq[Any](format.raw/*1.63*/("""
"""),format.raw/*3.1*/("""
"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

    """),format.raw/*6.5*/("""<ul class="breadcrumb" xmlns="http://www.w3.org/1999/html">
        <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
        <li><i class="icon-home" ></i><a href="/"> Milk Glycome</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
            <!--<li class="active" > You are here</li>-->
    </ul>

    <section id="layouts" class="browse">
        <div class="page-header row-fluid">
            <h1>Virtual Milk Glycome</h1>
        </div>

        <form class="form-vertical" action =""""),_display_(/*18.47*/(routes.HmgStructures.searchCompositionHmg())),format.raw/*18.92*/("""">
            <fieldset>

                    <!-- Form Name -->
                <legend>Composition and IUPAC Search</legend>

                    <!-- Text input-->
                <div class="row">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span6">
                                <label class="control-label" for="textinput">Gal</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Gal" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_gal"  class="span3">
                                        <option value="between">Between</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Gal2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">Fuc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Fuc" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_fuc"  class="span3">
                                        <option value="between">Exact</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Fuc2" type="text" placeholder="0" class="span2" value="0">
                                </div>

				<label class="control-label" for="textinput">GlcNAc</label>
                            <div class="controls">
                                <input id="textinput" name="comp_Glcnac" type="text" placeholder="0" class="span2" value="0">
                                <select name="sel_glcnac"  class="span3">
                                    <option value="between">Exact</option>
                                    <option value="gt">+</option>
                                    <option value="lt">-</option>
                                </select>
                                <input id="textinput" name="comp_Glcnac2" type="text" placeholder="0" class="span2" value="0">
                            </div>

                                """),format.raw/*71.41*/("""

                            """),format.raw/*73.29*/("""</div>
                            <div class="span6">
                                <label class="control-label" for="textinput">NeuAc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Kdn" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_kdn"  class="span3">
                                        <option value="between">Exact</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Kdn2" type="text" placeholder="0" class="span2" value="0">
                                </div>

                                <label class="control-label" for="textinput">Glc</label>
                                <div class="controls">
                                    <input id="textinput" name="comp_Glc" type="text" placeholder="0" class="span2" value="0">
                                    <select name="sel_glc"  class="span3">
                                        <option value="between">Exact</option>
                                        <option value="gt">+</option>
                                        <option value="lt">-</option>
                                    </select>
                                    <input id="textinput" name="comp_Glc2" type="text" placeholder="0" class="span2" value="0">
                                </div>
"""),format.raw/*107.3*/("""


                            """),format.raw/*110.29*/("""</div>

                            """),format.raw/*122.37*/("""
                        """),format.raw/*123.25*/("""</div>

                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <div class="row-fluid">
                            <p>IUPAC search will before a partial match against the database only</p>
                            <label class="control-label" for="textinput">IUPAC </label>
                            <div class="controls">
                                <textarea id="textinput" name="IUPAC" type="text" placeholder="Gal(b1-4)[Fuc(a1-3)]GlcNAc(b1-3)Gal(b1-4)[Fuc(a1-3)]Glc" class="input-xlarge span6" ></textarea>
                            </div>
                            <label class="control-label" for="textinput">Mass </label>
                            <div class="controls">
                                <input id="textinput" name="mass" type="text" value="0.0" placeholder="Mass" class="input-large span3">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name=rdf"> Try the RDF Search
                                </label>
                            </div>
                                <button type="submit" class="btn btn-primary">Search Database</button>

                        </div>

                    </div>
                </div>
            </fieldset>
        </form>

        <h3>New Sub(structure) Search """),_display_(/*154.40*/if(count > 100)/*154.55*/{_display_(Seq[Any](format.raw/*154.56*/(""" """),format.raw/*154.57*/("""- Showing 100 out of """),_display_(/*154.79*/count),format.raw/*154.84*/(""" """)))}/*154.86*/else/*154.90*/{_display_(Seq[Any](format.raw/*154.91*/(""" """),format.raw/*154.92*/("""- Showing """),_display_(/*154.103*/count),format.raw/*154.108*/(""" """),format.raw/*154.109*/("""Entries """)))}),format.raw/*154.118*/(""" """),format.raw/*154.119*/("""</h3>
        <table class="table table-condensed table-bordered table-striped volumes">
            <thead>
                <tr>
                    <th>Accession Number</th>
                    <th>Structure</th>
                    <th>Mass</th>
                </tr>
            </thead>
            <tbody>
            """),_display_(/*164.14*/for(s <- structures) yield /*164.34*/{_display_(Seq[Any](format.raw/*164.35*/("""
                """),format.raw/*165.17*/("""<tr>
                    <td>UHMG"""),_display_(/*166.30*/s/*166.31*/.id),format.raw/*166.34*/("""</td>
                    <td><a href=""""),_display_(/*167.35*/{routes.HmgStructures.searchHmgRDF(s.id)}),format.raw/*167.76*/(""""><img class="sugar_image" src="""),_display_(/*167.108*/{routes.Image.showImageHmg(s.id, "cfgl", "extended" )}),format.raw/*167.162*/(""" """),format.raw/*167.163*/("""height="25%", width="25%"  alt=""/></a></td>
                    <td>"""),_display_(/*168.26*/{"%.2f".format(s.calcmass)}),format.raw/*168.53*/("""</td>
                </tr>
            """)))}),format.raw/*170.14*/("""
            """),format.raw/*171.13*/("""</tbody>
        </table>

    </section>

""")))}),format.raw/*176.2*/("""
"""))
      }
    }
  }

  def render(structures:java.util.List[models.hmg.Hmg],count:Integer): play.twirl.api.HtmlFormat.Appendable = apply(structures,count)

  def f:((java.util.List[models.hmg.Hmg],Integer) => play.twirl.api.HtmlFormat.Appendable) = (structures,count) => apply(structures,count)

  def ref: this.type = this

}


}

/**/
object searchHmgCompositions extends searchHmgCompositions_Scope0.searchHmgCompositions
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:05 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/hmg/searchHmgCompositions.scala.html
                  HASH: 24ae0eda935bac7bfe527fa6942fde72ce5b7475
                  MATRIX: 813->1|984->62|1011->80|1038->82|1049->86|1087->88|1119->94|1767->715|1833->760|4519->4179|4577->4209|6219->6618|6279->6649|6344->7328|6398->7353|7907->8834|7932->8849|7972->8850|8002->8851|8052->8873|8079->8878|8101->8880|8115->8884|8155->8885|8185->8886|8225->8897|8253->8902|8284->8903|8326->8912|8357->8913|8710->9238|8747->9258|8787->9259|8833->9276|8895->9310|8906->9311|8931->9314|8999->9354|9062->9395|9123->9427|9200->9481|9231->9482|9329->9552|9378->9579|9451->9620|9493->9633|9568->9677
                  LINES: 27->1|32->1|33->3|34->4|34->4|34->4|36->6|48->18|48->18|92->71|94->73|117->107|120->110|122->122|123->123|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|154->154|164->164|164->164|164->164|165->165|166->166|166->166|166->166|167->167|167->167|167->167|167->167|167->167|168->168|168->168|170->170|171->171|176->176
                  -- GENERATED --
              */
          