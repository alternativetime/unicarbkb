
package views.html

import play.twirl.api._


     object browse_Scope0 {
import java.util._

import controllers._
import models._
import views.html._

import scala.collection.JavaConversions._

class browse extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template16[List[String],ArrayList[Taxonomy],List[Biolsource],List[List[String]],HashSet[String],HashSet[String],ArrayList[Proteins],ArrayList[Tissue],List[Tissue],List[GlycobaseSource],List[String],Integer,List[String],Integer,List[String],Integer,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(taxonomy: List[String], taxon: ArrayList[Taxonomy],  biolsource: List[Biolsource], groupTax: List[List[String]], sources: HashSet[String], proteins: HashSet[String], proteinList: ArrayList[Proteins], tissueList: ArrayList[Tissue], tissueTerms: List[Tissue], glycobasePerturbation: List[GlycobaseSource], output: List[String], count: Integer, outputtissue: List[String], counttissue: Integer, outputprotein: List[String], countprotein: Integer):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.446*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""

"""),format.raw/*5.1*/("""<script>
$(document).ready(function() """),format.raw/*6.30*/("""{"""),format.raw/*6.31*/("""
"""),format.raw/*7.1*/("""$('#myTabBar a[data-toggle="tab"]').on('shown', function (e) """),format.raw/*7.62*/("""{"""),format.raw/*7.63*/("""
  """),format.raw/*8.3*/("""e.target // activated tab
  var filterstring = e.target.toString();
  var filtervalue =filterstring.split('#');
  var name = filtervalue[1];

  if(name == "filter3")"""),format.raw/*13.24*/("""{"""),format.raw/*13.25*/("""
   """),format.raw/*14.4*/("""$('.glycobase').show();
  """),format.raw/*15.3*/("""}"""),format.raw/*15.4*/("""
  """),format.raw/*16.3*/("""if(name != "filter3")"""),format.raw/*16.24*/("""{"""),format.raw/*16.25*/("""
   """),format.raw/*17.4*/("""$('.glycobase').hide();
  """),format.raw/*18.3*/("""}"""),format.raw/*18.4*/("""

  """),format.raw/*20.3*/("""if(name == "filter1")"""),format.raw/*20.24*/("""{"""),format.raw/*20.25*/("""
   """),format.raw/*21.4*/("""$('.glycosuiteResult').show();
  """),format.raw/*22.3*/("""}"""),format.raw/*22.4*/("""

  """),format.raw/*24.3*/("""if(name != "filter1")"""),format.raw/*24.24*/("""{"""),format.raw/*24.25*/("""
   """),format.raw/*25.4*/("""$('.glycosuiteResult').hide();
  """),format.raw/*26.3*/("""}"""),format.raw/*26.4*/("""

  
"""),format.raw/*29.1*/("""}"""),format.raw/*29.2*/(""");
"""),format.raw/*30.1*/("""}"""),format.raw/*30.2*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*34.35*/("""{"""),format.raw/*34.36*/("""
        """),format.raw/*35.9*/("""$("#e1").select2("""),format.raw/*35.26*/("""{"""),format.raw/*35.27*/("""
          """),format.raw/*36.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	minimumInputLength: 1,
        """),format.raw/*39.9*/("""}"""),format.raw/*39.10*/(""");
	"""),format.raw/*40.2*/("""}"""),format.raw/*40.3*/(""");
</script>
<script>
      $(document).ready(function()"""),format.raw/*43.35*/("""{"""),format.raw/*43.36*/("""
        """),format.raw/*44.9*/("""$("#e2").select2("""),format.raw/*44.26*/("""{"""),format.raw/*44.27*/("""
          """),format.raw/*45.11*/("""placeholder: "Select multiple tissues... ",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*48.9*/("""}"""),format.raw/*48.10*/(""");
        """),format.raw/*49.9*/("""}"""),format.raw/*49.10*/(""");
</script>
<script>
      $(document).ready(function()"""),format.raw/*52.35*/("""{"""),format.raw/*52.36*/("""
        """),format.raw/*53.9*/("""$("#e3").select2("""),format.raw/*53.26*/("""{"""),format.raw/*53.27*/("""
          """),format.raw/*54.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*57.9*/("""}"""),format.raw/*57.10*/(""");
        """),format.raw/*58.9*/("""}"""),format.raw/*58.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*62.35*/("""{"""),format.raw/*62.36*/("""
        """),format.raw/*63.9*/("""$("#e4").select2("""),format.raw/*63.26*/("""{"""),format.raw/*63.27*/("""
          """),format.raw/*64.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*67.9*/("""}"""),format.raw/*67.10*/(""");
        """),format.raw/*68.9*/("""}"""),format.raw/*68.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*72.35*/("""{"""),format.raw/*72.36*/("""
        """),format.raw/*73.9*/("""$("#e5").select2("""),format.raw/*73.26*/("""{"""),format.raw/*73.27*/("""
          """),format.raw/*74.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*77.9*/("""}"""),format.raw/*77.10*/(""");
        """),format.raw/*78.9*/("""}"""),format.raw/*78.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*82.35*/("""{"""),format.raw/*82.36*/("""
        """),format.raw/*83.9*/("""$("#e6").select2("""),format.raw/*83.26*/("""{"""),format.raw/*83.27*/("""
          """),format.raw/*84.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*87.9*/("""}"""),format.raw/*87.10*/(""");
        """),format.raw/*88.9*/("""}"""),format.raw/*88.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*92.35*/("""{"""),format.raw/*92.36*/("""
        """),format.raw/*93.9*/("""$("#e7").select2("""),format.raw/*93.26*/("""{"""),format.raw/*93.27*/("""
          """),format.raw/*94.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*97.9*/("""}"""),format.raw/*97.10*/(""");
        """),format.raw/*98.9*/("""}"""),format.raw/*98.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*102.35*/("""{"""),format.raw/*102.36*/("""
        """),format.raw/*103.9*/("""$("#e8").select2("""),format.raw/*103.26*/("""{"""),format.raw/*103.27*/("""
          """),format.raw/*104.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*107.9*/("""}"""),format.raw/*107.10*/(""");
        """),format.raw/*108.9*/("""}"""),format.raw/*108.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*112.35*/("""{"""),format.raw/*112.36*/("""
        """),format.raw/*113.9*/("""$("#e9").select2("""),format.raw/*113.26*/("""{"""),format.raw/*113.27*/("""
          """),format.raw/*114.11*/("""placeholder: "Select multiple species...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*117.9*/("""}"""),format.raw/*117.10*/(""");
        """),format.raw/*118.9*/("""}"""),format.raw/*118.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*122.35*/("""{"""),format.raw/*122.36*/("""
        """),format.raw/*123.9*/("""$("#e10").select2("""),format.raw/*123.27*/("""{"""),format.raw/*123.28*/("""
          """),format.raw/*124.11*/("""placeholder: "Select multiple tissues...",
          allowClear: true,
          minimumInputLength: 1,
        """),format.raw/*127.9*/("""}"""),format.raw/*127.10*/(""");
        """),format.raw/*128.9*/("""}"""),format.raw/*128.10*/(""");
</script>

<script>
      $(document).ready(function()"""),format.raw/*132.35*/("""{"""),format.raw/*132.36*/("""
        """),format.raw/*133.9*/("""$("#e11").select2("""),format.raw/*133.27*/("""{"""),format.raw/*133.28*/("""
          """),format.raw/*134.11*/("""placeholder: "Select multiple proteins...",
          allowClear: true,
	  minimumInputLength: 1,
        """),format.raw/*137.9*/("""}"""),format.raw/*137.10*/(""");
        """),format.raw/*138.9*/("""}"""),format.raw/*138.10*/(""");
</script>

<script>
        $(function() """),format.raw/*142.22*/("""{"""),format.raw/*142.23*/("""
        """),format.raw/*143.9*/("""var myArray = new Array();
        """),_display_(/*144.10*/for(a <- taxonomy) yield /*144.28*/{_display_(Seq[Any](format.raw/*144.29*/("""
                """),format.raw/*145.17*/("""myArray.push(""""),_display_(/*145.32*/a),format.raw/*145.33*/("""");
        """)))}),format.raw/*146.10*/("""
	"""),format.raw/*147.2*/("""myArray.sort();
        options = '';
        for (var i = 0; i < myArray.length; i++) """),format.raw/*149.50*/("""{"""),format.raw/*149.51*/("""
                """),format.raw/*150.17*/("""options += '<option value="' + myArray[i] + '">' + myArray[i] + '</option>';
        """),format.raw/*151.9*/("""}"""),format.raw/*151.10*/("""
        """),format.raw/*152.9*/("""$('#e1').html(options);
	$('#e5').html(options);
	$('#e7').html(options);
	$('#e9').html(options);
        """),format.raw/*156.9*/("""}"""),format.raw/*156.10*/(""");
</script>

<script>
	$(function() """),format.raw/*160.15*/("""{"""),format.raw/*160.16*/("""
        """),format.raw/*161.9*/("""var mySources = new Array();
	"""),_display_(/*162.3*/for(s <- sources) yield /*162.20*/{_display_(Seq[Any](format.raw/*162.21*/("""
		"""),format.raw/*163.3*/("""mySources.push(""""),_display_(/*163.20*/s),format.raw/*163.21*/("""");
	""")))}),format.raw/*164.3*/("""
	"""),format.raw/*165.2*/("""mySources.sort();
	options = '';
	for (var i = 0; i < mySources.length; i++) """),format.raw/*167.45*/("""{"""),format.raw/*167.46*/("""
		"""),format.raw/*168.3*/("""options += '<option value="' + mySources[i] + '">' + mySources[i] + '</option>';
        """),format.raw/*169.9*/("""}"""),format.raw/*169.10*/("""
	"""),format.raw/*170.2*/("""$('#e2').html(options);
	$('#e4').html(options);
	$('#e8').html(options);
	$('#e10').html(options);
	"""),format.raw/*174.2*/("""}"""),format.raw/*174.3*/(""");
</script>

<script>
        $(function() """),format.raw/*178.22*/("""{"""),format.raw/*178.23*/("""
        """),format.raw/*179.9*/("""var myProteins = new Array();
        """),_display_(/*180.10*/for(s <- proteins ) yield /*180.29*/{_display_(Seq[Any](format.raw/*180.30*/("""
                """),format.raw/*181.17*/("""myProteins.push(""""),_display_(/*181.35*/s),format.raw/*181.36*/("""");
        """)))}),format.raw/*182.10*/("""
        """),format.raw/*183.9*/("""myProteins.sort();
        options = '';
        for (var i = 0; i < myProteins.length; i++) """),format.raw/*185.53*/("""{"""),format.raw/*185.54*/("""
                """),format.raw/*186.17*/("""options += '<option value="' + myProteins[i] + '">' + myProteins[i] + '</option>';
        """),format.raw/*187.9*/("""}"""),format.raw/*187.10*/("""
        """),format.raw/*188.9*/("""$('#e3').html(options);
	$('#e6').html(options);
	$('#e11').html(options);
        """),format.raw/*191.9*/("""}"""),format.raw/*191.10*/(""");
</script>


<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
  <!--<li class="active" > You are here</li>-->
</ul>

<section id="layouts" class="browse">

  <div class="page-header row-fluid">
    <h1>Search UniCarbKB</h1>
    <h4 class="subheader">Retrieve records from GlycosuiteDB, EUROCarbDB and GlycoBase</h4>
  </div>

<div class="row-fluid">
  <div class="span3 search">
    <div class="row-fluid">
      <div class="filterbar tabbable clearfix">
        <ul class="nav nav-tabs" id="myTabBar">
          <li class="title">Select a Structure Database: </li>
          <li><a href="#filter1" data-toggle="tab">GlycoSuiteDB <span class="pull-right count">"""),_display_(/*214.97*/if(taxon.size() != 0)/*214.118*/ {_display_(_display_(/*214.121*/taxon/*214.126*/.size()))}),format.raw/*214.134*/(""" """),_display_(/*214.136*/if(proteinList.size() != 0)/*214.163*/ {_display_(_display_(/*214.166*/proteinList/*214.177*/.size()))}),format.raw/*214.185*/(""" """),_display_(/*214.187*/if(tissueList.size() != 0)/*214.213*/ {_display_(_display_(/*214.216*/tissueList/*214.226*/.size()))}),format.raw/*214.234*/("""</span></a></li>
          <li class=""><a href="#filter2" data-toggle="tab">EUROCarbDB</a></li>
          <li><a href="#filter3" data-toggle="tab">GlycoBase <span class="pull-right count"> """),_display_(/*216.95*/if(count != 0)/*216.109*/ {_display_(_display_(/*216.112*/count))}),format.raw/*216.118*/(""" """),_display_(/*216.120*/if(counttissue != 0)/*216.140*/ {_display_(_display_(/*216.143*/counttissue))}),format.raw/*216.155*/("""</span></a></li> </ul>
      </div>

      <div class="info">
        <h4>Other Options:</h4>
        <p><b>Glycan Builder:</b> Build and search a glycan structure<a href="/search"> using the new interface</a>.</p>
        <p><b>Curated Publications:</b> Search the <a href="/references">growing list of publications, associated structures, and metadata</a>.</p>
      </div>
    </div>
  </div>

  <div class="span9 rightpanel">
    <div class="tabbable"> <!-- Only required for left/right tabs -->

      <ul class="nav nav-tabs row-fluid" id="myTab">
        <li class="active"><a href="#tab1" data-toggle="tab"><h2>Construct a Query:</h2></a></li>
        <li class="span12"><a class="returnedresults" href="#tab2" data-toggle="tab">
          <h2>Search returned """),_display_(/*233.32*/if(taxon.size() != 0)/*233.53*/ {_display_(_display_(/*233.56*/taxon/*233.61*/.size()))}),format.raw/*233.69*/(""" """),_display_(/*233.71*/if(proteinList.size() != 0)/*233.98*/ {_display_(_display_(/*233.101*/proteinList/*233.112*/.size()))}),format.raw/*233.120*/(""" """),_display_(/*233.122*/if(tissueList.size() != 0)/*233.148*/ {_display_(_display_(/*233.151*/tissueList/*233.161*/.size()))}),format.raw/*233.169*/(""" """),format.raw/*233.170*/("""results...
          </h2>
          <a href="/browse"><input type="button" value="Search Again" href="/browse" class="btn btn-small pull-right"></a>
        </a></li>
        <li><a href="#tab3" data-toggle="tab">Related Results</a></li>
      </ul>

      <div class="tab-content results">

        <div class="tab-pane active" id="tab1">

          <!--<h2><img src="../images/icon_search.png"> Search Features &amp; Results</h2>-->
          <div id="filter1" class="control-group row-fluid glycosuitedb">
            <div id="tab1" class="span12" >

              <div class="multi-option clearfix">
                <span class="pull-left label">Search by: </span>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Taxonomy" checked> <h4>Taxonomy</h4>
                </label>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Localisation"> <h4>Tissue</h4>
                </label>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="ProteinLocalisation"> <h4>Protein</h4>
                </label>
                <label class="radio">
                  <input type="radio" name="glycosuitedbSearch" value="Composition"> <h4>Composition</h4>
                </label>
              </div>

              <div id="glycosuitedbTaxonomy" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Taxonomy</h4>-->
                    <div id="selection" class="input-append row-fluid">
                      <select  name=taxonomy MULTIPLE id="e1" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
              <div id="glycosuitedbLocalisation" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Localisation</h4>-->
                    <div id="selection" class="row-fluid">
                      <select  name=tissue MULTIPLE id="e2" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
              <div id="glycosuitedbAssociation" class="controls">
                <label class="checkbox">
                  <form class="form-search">
                    <!--<h4>Protein Association</h4>-->
                    <div id="selection" class="row-fluid">
                      <select  name=protein MULTIPLE id="e3" id="listBox" class="span10"></select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </form>
                </label>
              </div>
              <form id="glycosuitedbComposition" class="form-search controls" action="/compositions">
                <!--<h4>Query by Composition</h4>-->
                <div class='row-fluid'>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Hex</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> HexNAc</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> dHex</label>
                </div>

                <div class='row-fluid'>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> NeuAc</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> NeuGc</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Pentose</label>
                </div>

                <div class='row-fluid'>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Sulphate</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Phosphate</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> KDN</label>
                </div>

                <div class='row-fluid'>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> KDO</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> HexA</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Methyl</label>
                </div>

                <div class='row-fluid'>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Acetyl</label>
                  <label class='checkbox span4'><input class="span2" name=comp type=text> Other</label>
                </div>

                <div class='row-fluid'>
                  <button type="submit" class="btn btn-primary">Composition Search</button>
                </div>

              </form>
            </div> """),format.raw/*333.45*/("""

          """),format.raw/*335.11*/("""</div> """),format.raw/*335.35*/("""

          """),format.raw/*337.11*/("""<div id="filter2" class="tab-pane control-group row-fluid eurocarb">

	    <p>Support for EUROCarbDB will be available in the next release</p>

	    """),format.raw/*390.8*/("""

          """),format.raw/*392.11*/("""</div> """),format.raw/*392.38*/("""

          """),format.raw/*394.11*/("""<div id="filter3" class="tab-pane control-group nextprot">
            <div class="row-fluid">

                <div class="multi-option clearfix">
                  <span class="pull-left label">Search by: </span>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="Taxonomy" checked> <h4>Taxonomy</h4>
                  </label>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="Localisation"> <h4>Localisation</h4>
                  </label>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="ProteinLocalisation"> <h4>Protein</h4>
                  </label>
                  <label class="radio">
                    <input type="radio" name="glycobaseSearch" value="Composition"> <h4>Composition</h4>
                  </label>
                </div>

                <div id="glycobaseTaxonomy" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Taxonomy</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=taxonomy MULTIPLE id="e5" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>

                <div id="glycobaseLocalisation" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Localisation</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=tissue MULTIPLE id="e10" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>

                <div id="glycobaseAssociation" class="controls">
                  <label class="checkbox">
                    <form class="form-search">
                      <!--<h4>Protein Association</h4>-->
                      <div id="selection" class="row-fluid">
                        <select  name=protein MULTIPLE id="e11" id="listBox" class="span10"></select>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </div>
                    </form>
                  </label>
                </div>


                <!--<h4>Query by Composition</h4>-->
                <form id="glycobaseComposition" class="form-search controls">
                  <div class="row-fluid">
                    <label class="checkbox span4"><input class="span2" name=mscomp_Hexose type=text> Hex</label>
                    <label class="checkbox span4"><input class="span2" name=mscomp_HexNAc type=text> HexNAc</label>
                    <label class="checkbox span4"><input class="span2" name=mscomp_deoxyHexose type=text> dHex</label>
                  </div>

                  <div class="row-fluid">
                    <label class="checkbox span4"><input class="span2" name=mscomp_pentose type=text> Pent</label>
                    <label class="checkbox span4"><input class="span2" name=mscomp_NeuAc type=text> NeuAc</label>
                    <label class="checkbox span4"><input class="span2" name=mscomp_NeuGc type=text> NeuGc</label>
                  </div>

                  <div class="row-fluid">
                    <label class="checkbox span4"><input class="span2" name=mscomp_KDN   type=text> KDN</label>
                    <label class="checkbox span4"><input class="span2" name=mscomp_HexA type=text> HexA</label>
                  </div>
                  <div class="row-fluid">
                    <button type="submit" class="btn btn-primary">Composition Search</button>
                  </div>
                </form>

            </div> """),format.raw/*473.40*/("""

          """),format.raw/*475.11*/("""</div> """),format.raw/*475.35*/("""
        """),format.raw/*476.9*/("""</div> """),format.raw/*476.33*/("""

      """),format.raw/*478.7*/("""<div class="tab-pane" id="tab2">

        <hr />

    """),_display_(/*482.6*/for(out <- output) yield /*482.24*/ {_display_(Seq[Any](format.raw/*482.26*/("""
    """),format.raw/*483.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*484.8*/Html(out)),format.raw/*484.17*/("""
    """),format.raw/*485.5*/("""</div>
    """)))}),format.raw/*486.6*/("""

    """),_display_(/*488.6*/for(out <- outputtissue) yield /*488.30*/ {_display_(Seq[Any](format.raw/*488.32*/("""
    """),format.raw/*489.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*490.8*/Html(out)),format.raw/*490.17*/("""
    """),format.raw/*491.5*/("""</div>
    """)))}),format.raw/*492.6*/("""

    """),_display_(/*494.6*/for(out <- outputprotein) yield /*494.31*/ {_display_(Seq[Any](format.raw/*494.33*/("""
    """),format.raw/*495.5*/("""<div class="row-fluid glycobase">
      """),_display_(/*496.8*/Html(out)),format.raw/*496.17*/("""
    """),format.raw/*497.5*/("""</div>
    """)))}),format.raw/*498.6*/("""


    """),_display_(/*501.6*/if(proteinList != null)/*501.29*/ {_display_(Seq[Any](format.raw/*501.31*/("""

    """),_display_(/*503.6*/if(proteinList.size() >=1 )/*503.33*/{_display_(Seq[Any](format.raw/*503.34*/("""
    """),format.raw/*504.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="controls">
        <label class="checkbox">
          <form class="form-search">
            <div id="selection">
              <select  name=protein MULTIPLE id="e6" style="width:350px"  id="listBox"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </form>
        </label>
      </div>
    </div>
    """)))}),format.raw/*516.6*/("""


    """),_display_(/*519.6*/for(p <- proteinList) yield /*519.27*/ {_display_(Seq[Any](format.raw/*519.29*/("""
    """),format.raw/*520.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="details span9">
        <h3>"""),_display_(/*522.14*/if(p.swissProt != null)/*522.37*/ {_display_(Seq[Any](format.raw/*522.39*/("""<a href=""""),_display_(/*522.49*/routes/*522.55*/.Application.proteinsummary(p.swissProt, "annotated" )),format.raw/*522.109*/("""">""")))}),format.raw/*522.112*/("""
	     """),_display_(/*523.8*/if(p.swissProt == null)/*523.31*/ {_display_(Seq[Any](format.raw/*523.33*/("""<a href=""""),_display_(/*523.43*/routes/*523.49*/.Application.proteinsummary(p.name, "name" )),format.raw/*523.93*/("""">""")))}),format.raw/*523.96*/("""
	    """),_display_(/*524.7*/p/*524.8*/.name),format.raw/*524.13*/("""</a></h3>  
        <p class="">"""),_display_(/*525.22*/p/*525.23*/.stproteins.size()),format.raw/*525.41*/(""" """),format.raw/*525.42*/("""structures are associated with this protein</p>
      </div>
      <div class="span3">
         <p class="pull-right structures"><a href=""""),_display_(/*528.53*/routes/*528.59*/.Application.proteinsummary(p.swissProt, "name" )),format.raw/*528.108*/("""">"""),_display_(/*528.111*/p/*528.112*/.stproteins.size()),format.raw/*528.130*/(""" """),format.raw/*528.131*/("""structures </a></p> 
      </div>
    </div>
    """)))}),format.raw/*531.6*/("""
    """)))}),format.raw/*532.6*/("""



    """),_display_(/*536.6*/if(tissueList != null && tissueTerms != null)/*536.51*/ {_display_(Seq[Any](format.raw/*536.53*/("""
    """),_display_(/*537.6*/if(tissueList.size() >=1 )/*537.32*/{_display_(Seq[Any](format.raw/*537.33*/("""
    """),format.raw/*538.5*/("""<div class="row-fluid glycosuiteResult">
      <div class="controls">
        <label class="checkbox">
          <form class="form-search">
            <div id="selection">
              <select  name=tissue MULTIPLE id="e4" style="width:350px"  id="listBox"></select>
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </form>
        </label>
      </div>
    </div>
    """)))}),format.raw/*550.6*/("""

    """),_display_(/*552.6*/for(p <- tissueList) yield /*552.26*/ {_display_(Seq[Any](format.raw/*552.28*/("""
    """),format.raw/*553.5*/("""<div class="row-fluid glycosuiteResult">
      <h3><a href="#">"""),_display_(/*554.24*/p/*554.25*/.div1),format.raw/*554.30*/(""", """),_display_(/*554.33*/p/*554.34*/.div2),format.raw/*554.39*/(""", """),_display_(/*554.42*/p/*554.43*/.div3),format.raw/*554.48*/(""", """),_display_(/*554.51*/p/*554.52*/.div4),format.raw/*554.57*/("""</a></h3>
      <div class="details span9">
        <p class="">"""),_display_(/*556.22*/p/*556.23*/.stsource.size()),format.raw/*556.39*/(""" """),format.raw/*556.40*/("""structures are associated with this protein</p>
      </div>
      <div class="span3">
        <p class="pull-right structures"><a href="#">"""),_display_(/*559.55*/p/*559.56*/.stsource.size()),format.raw/*559.72*/(""" """),format.raw/*559.73*/("""structures </a></p>
      </div>
    </div>
    """)))}),format.raw/*562.6*/("""
    """)))}),format.raw/*563.6*/("""

    """),_display_(/*565.6*/for( (taxon, ii) <- taxon.zipWithIndex) yield /*565.45*/ {_display_(Seq[Any](format.raw/*565.47*/("""        """),format.raw/*565.55*/("""<div class="row-fluid glycosuiteResult">
      <div class="details span9">
        <h3><a href="/taxonomysearch?taxonomy="""),_display_(/*567.48*/taxon/*567.53*/.species),format.raw/*567.61*/("""">"""),_display_(/*567.64*/taxon/*567.69*/.species),format.raw/*567.77*/("""</a>"""),format.raw/*567.212*/("""</h3>
	<p class="">"""),_display_(/*568.15*/taxon/*568.20*/.taxprotein.size()),format.raw/*568.38*/(""" """),format.raw/*568.39*/("""glycoproteins are associated with this taxonomy including: """),_display_(/*568.99*/for( (tax, i) <- taxon.taxprotein.zipWithIndex; if (i <= 6)) yield /*568.159*/ {_display_(Seq[Any](format.raw/*568.161*/("""
	"""),_display_(/*569.3*/if(tax.swiss_prot != null)/*569.29*/ {_display_(Seq[Any](format.raw/*569.31*/(""" """),format.raw/*569.32*/("""<a href=""""),_display_(/*569.42*/routes/*569.48*/.Application.proteinsummary(tax.swiss_prot, "annotated" )),format.raw/*569.105*/("""">"""),_display_(/*569.108*/tax/*569.111*/.protein),format.raw/*569.119*/(""", </a> """)))}),format.raw/*569.127*/("""
	"""),_display_(/*570.3*/if(tax.swiss_prot == null)/*570.29*/ {_display_(Seq[Any](format.raw/*570.31*/("""<a href=""""),_display_(/*570.41*/routes/*570.47*/.Application.proteinsummary(tax.protein, "name" )),format.raw/*570.96*/("""">"""),_display_(/*570.99*/tax/*570.102*/.protein),format.raw/*570.110*/(""", </a> """)))}),format.raw/*570.118*/(""" 
	
  """)))}),format.raw/*572.4*/("""</p>
      </div>
      <div class="span3">
        <p class="pull-right structures"><a href="/taxonomysearch?taxonomy="""),_display_(/*575.77*/taxon/*575.82*/.species),format.raw/*575.90*/("""">"""),_display_(/*575.93*/taxon/*575.98*/.strtaxonomy.size()),format.raw/*575.117*/(""" """),format.raw/*575.118*/("""structures </a></p>
      </div>

    </div>
    """)))})))}),format.raw/*579.7*/("""
  """),format.raw/*580.3*/("""</div>
  """),format.raw/*581.17*/("""

  """),format.raw/*583.3*/("""<div class="tab-pane" id="tab3">
  </div>

</div>
    </div>
  </div><!-- /col -->
</div><!-- /row -->
"""),_display_(/*590.2*/views/*590.7*/.html.footerunicarb.footerunicarb()),format.raw/*590.42*/("""
"""),format.raw/*591.1*/("""</section>

"""),_display_(/*593.2*/if(taxon.size() != 0 )/*593.24*/ {_display_(Seq[Any](format.raw/*593.26*/("""
"""),format.raw/*594.1*/("""<script>
  $(function () """),format.raw/*595.17*/("""{"""),format.raw/*595.18*/("""
    """),format.raw/*596.5*/("""var test = 'first'; """),format.raw/*596.61*/("""
    """),format.raw/*597.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*599.3*/("""}"""),format.raw/*599.4*/(""")
</script>
""")))}),format.raw/*601.2*/("""

"""),_display_(/*603.2*/if(taxon.size() == 0  )/*603.25*/ {_display_(Seq[Any](format.raw/*603.27*/("""
"""),format.raw/*604.1*/("""<script>
  $(function () """),format.raw/*605.17*/("""{"""),format.raw/*605.18*/("""
    """),format.raw/*606.5*/("""var test = 'first'; """),format.raw/*606.61*/("""
    """),format.raw/*607.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab1"]').tab('show');
  """),format.raw/*609.3*/("""}"""),format.raw/*609.4*/(""")
</script>
""")))}),format.raw/*611.2*/("""

"""),_display_(/*613.2*/if(proteinList.size() != 0 )/*613.30*/ {_display_(Seq[Any](format.raw/*613.32*/("""
"""),format.raw/*614.1*/("""<script>
  $(function () """),format.raw/*615.17*/("""{"""),format.raw/*615.18*/("""
    """),format.raw/*616.5*/("""var test = 'first'; """),format.raw/*616.61*/("""
    """),format.raw/*617.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*619.3*/("""}"""),format.raw/*619.4*/(""")
</script>
""")))}),format.raw/*621.2*/("""

"""),_display_(/*623.2*/if(tissueList.size() != 0 )/*623.29*/ {_display_(Seq[Any](format.raw/*623.31*/("""
"""),format.raw/*624.1*/("""<script>
  $(function () """),format.raw/*625.17*/("""{"""),format.raw/*625.18*/("""
    """),format.raw/*626.5*/("""var test = 'first'; """),format.raw/*626.61*/("""
    """),format.raw/*627.5*/("""$('#myTabBar a:' + test).tab('show');
    $('#myTab a[href="#tab2"]').tab('show');
  """),format.raw/*629.3*/("""}"""),format.raw/*629.4*/(""")
</script>
""")))}),format.raw/*631.2*/("""
"""))
      }
    }
  }

  def render(taxonomy:List[String],taxon:ArrayList[Taxonomy],biolsource:List[Biolsource],groupTax:List[List[String]],sources:HashSet[String],proteins:HashSet[String],proteinList:ArrayList[Proteins],tissueList:ArrayList[Tissue],tissueTerms:List[Tissue],glycobasePerturbation:List[GlycobaseSource],output:List[String],count:Integer,outputtissue:List[String],counttissue:Integer,outputprotein:List[String],countprotein:Integer): play.twirl.api.HtmlFormat.Appendable = apply(taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein)

  def f:((List[String],ArrayList[Taxonomy],List[Biolsource],List[List[String]],HashSet[String],HashSet[String],ArrayList[Proteins],ArrayList[Tissue],List[Tissue],List[GlycobaseSource],List[String],Integer,List[String],Integer,List[String],Integer) => play.twirl.api.HtmlFormat.Appendable) = (taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein) => apply(taxonomy,taxon,biolsource,groupTax,sources,proteins,proteinList,tissueList,tissueTerms,glycobasePerturbation,output,count,outputtissue,counttissue,outputprotein,countprotein)

  def ref: this.type = this

}


}

/**/
object browse extends browse_Scope0.browse
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:00 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/browse.scala.html
                  HASH: 16142bcb1852df2e512f994f2e1a0f8bc8031fc2
                  MATRIX: 978->1|1518->445|1546->448|1557->452|1594->453|1622->455|1687->493|1715->494|1742->495|1830->556|1858->557|1887->560|2080->725|2109->726|2140->730|2193->756|2221->757|2251->760|2300->781|2329->782|2360->786|2413->812|2441->813|2472->817|2521->838|2550->839|2581->843|2641->876|2669->877|2700->881|2749->902|2778->903|2809->907|2869->940|2897->941|2929->946|2957->947|2987->950|3015->951|3100->1008|3129->1009|3165->1018|3210->1035|3239->1036|3278->1047|3408->1150|3437->1151|3468->1155|3496->1156|3580->1212|3609->1213|3645->1222|3690->1239|3719->1240|3758->1251|3891->1357|3920->1358|3958->1369|3987->1370|4071->1426|4100->1427|4136->1436|4181->1453|4210->1454|4249->1465|4382->1571|4411->1572|4449->1583|4478->1584|4563->1641|4592->1642|4628->1651|4673->1668|4702->1669|4741->1680|4873->1785|4902->1786|4940->1797|4969->1798|5054->1855|5083->1856|5119->1865|5164->1882|5193->1883|5232->1894|5364->1999|5393->2000|5431->2011|5460->2012|5545->2069|5574->2070|5610->2079|5655->2096|5684->2097|5723->2108|5856->2214|5885->2215|5923->2226|5952->2227|6037->2284|6066->2285|6102->2294|6147->2311|6176->2312|6215->2323|6347->2428|6376->2429|6414->2440|6443->2441|6529->2498|6559->2499|6596->2508|6642->2525|6672->2526|6712->2537|6845->2642|6875->2643|6914->2654|6944->2655|7030->2712|7060->2713|7097->2722|7143->2739|7173->2740|7213->2751|7346->2856|7376->2857|7415->2868|7445->2869|7531->2926|7561->2927|7598->2936|7645->2954|7675->2955|7715->2966|7855->3078|7885->3079|7924->3090|7954->3091|8040->3148|8070->3149|8107->3158|8154->3176|8184->3177|8224->3188|8358->3294|8388->3295|8427->3306|8457->3307|8530->3351|8560->3352|8597->3361|8661->3397|8696->3415|8736->3416|8782->3433|8825->3448|8848->3449|8893->3462|8923->3464|9039->3551|9069->3552|9115->3569|9228->3654|9258->3655|9295->3664|9430->3771|9460->3772|9526->3809|9556->3810|9593->3819|9651->3850|9685->3867|9725->3868|9756->3871|9801->3888|9824->3889|9861->3895|9891->3897|9997->3974|10027->3975|10058->3978|10175->4067|10205->4068|10235->4070|10364->4171|10393->4172|10466->4216|10496->4217|10533->4226|10600->4265|10636->4284|10676->4285|10722->4302|10768->4320|10791->4321|10836->4334|10873->4343|10995->4436|11025->4437|11071->4454|11190->4545|11220->4546|11257->4555|11368->4638|11398->4639|12251->5464|12283->5485|12316->5488|12332->5493|12365->5501|12396->5503|12434->5530|12467->5533|12489->5544|12522->5552|12553->5554|12590->5580|12623->5583|12644->5593|12677->5601|12896->5792|12921->5806|12954->5809|12985->5815|13016->5817|13047->5837|13080->5840|13117->5852|13914->6621|13945->6642|13977->6645|13992->6650|14024->6658|14054->6660|14091->6687|14124->6690|14146->6701|14179->6709|14210->6711|14247->6737|14280->6740|14301->6750|14334->6758|14365->6759|19536->11926|19577->11938|19613->11962|19654->11974|19831->14323|19872->14335|19908->14362|19949->14374|24057->18473|24098->18485|24134->18509|24171->18518|24207->18542|24243->18550|24325->18605|24360->18623|24401->18625|24434->18630|24502->18671|24533->18680|24566->18685|24609->18697|24643->18704|24684->18728|24725->18730|24758->18735|24826->18776|24857->18785|24890->18790|24933->18802|24967->18809|25009->18834|25050->18836|25083->18841|25151->18882|25182->18891|25215->18896|25258->18908|25293->18916|25326->18939|25367->18941|25401->18948|25438->18975|25478->18976|25511->18981|25971->19410|26006->19418|26044->19439|26085->19441|26118->19446|26234->19534|26267->19557|26308->19559|26346->19569|26362->19575|26439->19629|26475->19632|26510->19640|26543->19663|26584->19665|26622->19675|26638->19681|26704->19725|26739->19728|26773->19735|26783->19736|26810->19741|26871->19774|26882->19775|26922->19793|26952->19794|27119->19933|27135->19939|27207->19988|27239->19991|27251->19992|27292->20010|27323->20011|27404->20061|27441->20067|27477->20076|27532->20121|27573->20123|27606->20129|27642->20155|27682->20156|27715->20161|28174->20589|28208->20596|28245->20616|28286->20618|28319->20623|28411->20687|28422->20688|28449->20693|28480->20696|28491->20697|28518->20702|28549->20705|28560->20706|28587->20711|28618->20714|28629->20715|28656->20720|28749->20785|28760->20786|28798->20802|28828->20803|28997->20944|29008->20945|29046->20961|29076->20962|29156->21011|29193->21017|29227->21024|29283->21063|29324->21065|29361->21073|29511->21195|29526->21200|29556->21208|29587->21211|29602->21216|29632->21224|29666->21359|29714->21379|29729->21384|29769->21402|29799->21403|29887->21463|29965->21523|30007->21525|30037->21528|30073->21554|30114->21556|30144->21557|30182->21567|30198->21573|30278->21630|30310->21633|30324->21636|30355->21644|30396->21652|30426->21655|30462->21681|30503->21683|30541->21693|30557->21699|30628->21748|30659->21751|30673->21754|30704->21762|30745->21770|30783->21777|30931->21897|30946->21902|30976->21910|31007->21913|31022->21918|31064->21937|31095->21938|31180->21989|31211->21992|31249->22015|31281->22019|31412->22123|31426->22128|31483->22163|31512->22164|31552->22177|31584->22199|31625->22201|31654->22202|31708->22227|31738->22228|31771->22233|31820->22289|31853->22294|31966->22379|31995->22380|32039->22393|32069->22396|32102->22419|32143->22421|32172->22422|32226->22447|32256->22448|32289->22453|32338->22509|32371->22514|32484->22599|32513->22600|32557->22613|32587->22616|32625->22644|32666->22646|32695->22647|32749->22672|32779->22673|32812->22678|32861->22734|32894->22739|33007->22824|33036->22825|33080->22838|33110->22841|33147->22868|33188->22870|33217->22871|33271->22896|33301->22897|33334->22902|33383->22958|33416->22963|33529->23048|33558->23049|33602->23062
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|37->6|37->6|38->7|38->7|38->7|39->8|44->13|44->13|45->14|46->15|46->15|47->16|47->16|47->16|48->17|49->18|49->18|51->20|51->20|51->20|52->21|53->22|53->22|55->24|55->24|55->24|56->25|57->26|57->26|60->29|60->29|61->30|61->30|65->34|65->34|66->35|66->35|66->35|67->36|70->39|70->39|71->40|71->40|74->43|74->43|75->44|75->44|75->44|76->45|79->48|79->48|80->49|80->49|83->52|83->52|84->53|84->53|84->53|85->54|88->57|88->57|89->58|89->58|93->62|93->62|94->63|94->63|94->63|95->64|98->67|98->67|99->68|99->68|103->72|103->72|104->73|104->73|104->73|105->74|108->77|108->77|109->78|109->78|113->82|113->82|114->83|114->83|114->83|115->84|118->87|118->87|119->88|119->88|123->92|123->92|124->93|124->93|124->93|125->94|128->97|128->97|129->98|129->98|133->102|133->102|134->103|134->103|134->103|135->104|138->107|138->107|139->108|139->108|143->112|143->112|144->113|144->113|144->113|145->114|148->117|148->117|149->118|149->118|153->122|153->122|154->123|154->123|154->123|155->124|158->127|158->127|159->128|159->128|163->132|163->132|164->133|164->133|164->133|165->134|168->137|168->137|169->138|169->138|173->142|173->142|174->143|175->144|175->144|175->144|176->145|176->145|176->145|177->146|178->147|180->149|180->149|181->150|182->151|182->151|183->152|187->156|187->156|191->160|191->160|192->161|193->162|193->162|193->162|194->163|194->163|194->163|195->164|196->165|198->167|198->167|199->168|200->169|200->169|201->170|205->174|205->174|209->178|209->178|210->179|211->180|211->180|211->180|212->181|212->181|212->181|213->182|214->183|216->185|216->185|217->186|218->187|218->187|219->188|222->191|222->191|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|245->214|247->216|247->216|247->216|247->216|247->216|247->216|247->216|247->216|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|264->233|364->333|366->335|366->335|368->337|372->390|374->392|374->392|376->394|455->473|457->475|457->475|458->476|458->476|460->478|464->482|464->482|464->482|465->483|466->484|466->484|467->485|468->486|470->488|470->488|470->488|471->489|472->490|472->490|473->491|474->492|476->494|476->494|476->494|477->495|478->496|478->496|479->497|480->498|483->501|483->501|483->501|485->503|485->503|485->503|486->504|498->516|501->519|501->519|501->519|502->520|504->522|504->522|504->522|504->522|504->522|504->522|504->522|505->523|505->523|505->523|505->523|505->523|505->523|505->523|506->524|506->524|506->524|507->525|507->525|507->525|507->525|510->528|510->528|510->528|510->528|510->528|510->528|510->528|513->531|514->532|518->536|518->536|518->536|519->537|519->537|519->537|520->538|532->550|534->552|534->552|534->552|535->553|536->554|536->554|536->554|536->554|536->554|536->554|536->554|536->554|536->554|536->554|536->554|536->554|538->556|538->556|538->556|538->556|541->559|541->559|541->559|541->559|544->562|545->563|547->565|547->565|547->565|547->565|549->567|549->567|549->567|549->567|549->567|549->567|549->567|550->568|550->568|550->568|550->568|550->568|550->568|550->568|551->569|551->569|551->569|551->569|551->569|551->569|551->569|551->569|551->569|551->569|551->569|552->570|552->570|552->570|552->570|552->570|552->570|552->570|552->570|552->570|552->570|554->572|557->575|557->575|557->575|557->575|557->575|557->575|557->575|561->579|562->580|563->581|565->583|572->590|572->590|572->590|573->591|575->593|575->593|575->593|576->594|577->595|577->595|578->596|578->596|579->597|581->599|581->599|583->601|585->603|585->603|585->603|586->604|587->605|587->605|588->606|588->606|589->607|591->609|591->609|593->611|595->613|595->613|595->613|596->614|597->615|597->615|598->616|598->616|599->617|601->619|601->619|603->621|605->623|605->623|605->623|606->624|607->625|607->625|608->626|608->626|609->627|611->629|611->629|613->631
                  -- GENERATED --
              */
          