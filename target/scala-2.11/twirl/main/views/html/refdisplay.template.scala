
package views.html

import play.twirl.api._


     object refdisplay_Scope0 {
import java.util._

import models._
import views.html._

import scala.collection.JavaConversions._

class refdisplay extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[String,List[Reference],List[Reference],ArrayList[String],ArrayList[Biolsource],ArrayList[String],List[Reference],List[composition_protein.CompRef],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, references: List[Reference], refmethods: List[Reference], taxsources: ArrayList[String], proteinsources: ArrayList[Biolsource], prot: ArrayList[String],  refmethodsgp: List[Reference], compRefs: List[composition_protein.CompRef], abstractPMID: String ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
def /*4.2*/title/*4.7*/(text: String) = {{
  text.split(' ').map(_.capitalize).mkString(" ")
}};def /*8.2*/ands/*8.6*/(text: String) = {{
  text.replaceAll(" ", "-").replaceAll("(\\.$)", "")
}};
Seq[Any](format.raw/*1.272*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*6.2*/("""

"""),format.raw/*10.2*/("""

"""),format.raw/*12.1*/("""<script>
    var ands = "";
    var andsTitle = "title";
	"""),_display_(/*15.3*/for(ands <- references) yield /*15.26*/{_display_(Seq[Any](format.raw/*15.27*/("""
		"""),format.raw/*16.3*/("""andsTitle = ands.title
	""")))}),format.raw/*17.3*/("""
	"""),format.raw/*18.2*/("""andsTitle.replace(/\s+/g, '-').toLowerCase();
	console.log("ands title " + andsTitle);	

</script>

"""),_display_(/*23.2*/main/*23.6*/ {_display_(Seq[Any](format.raw/*23.8*/("""
"""),_display_(/*24.2*/for(r <- references) yield /*24.22*/ {_display_(Seq[Any](format.raw/*24.24*/("""
"""),format.raw/*25.1*/("""<ul class="breadcrumb">
  <li><i class="icon-home" ></i><a href="/"> UniCarbKB</a> <span class="divider">></span></li>
  <li><i class="icon-book" ></i><a href="/references"> References</a> <span class="divider">></span></li>
  <li class="active"><i class="icon-file" ></i> """),_display_(/*28.50*/r/*28.51*/.authors),format.raw/*28.59*/(""", """),_display_(/*28.62*/r/*28.63*/.year),format.raw/*28.68*/(""", """),_display_(/*28.71*/r/*28.72*/.journal.name),format.raw/*28.85*/("""<span class="divider"></span></li>
</ul>
""")))}),format.raw/*30.2*/("""
"""),format.raw/*31.1*/("""<section id="layouts">

  """),_display_(/*33.4*/for(r <- references) yield /*33.24*/ {_display_(Seq[Any](format.raw/*33.26*/("""
  """),format.raw/*34.3*/("""<div class="page-header row-fluid">
    <h1 class="">"""),_display_(/*35.19*/r/*35.20*/.journal.name),format.raw/*35.33*/(""", """),_display_(/*35.36*/r/*35.37*/.year),format.raw/*35.42*/("""</h1>
    <h4 class="subheader span8">Reference details <span class="pull-right"><span class="label label-light">Curated Entry</span> <span class="label label-light">GlycoSuiteDB</span></span></h4>
  </div>

  <div class="row-fluid">
    <div class="span8">

      <p><b>"""),_display_(/*42.14*/title(r.title)),format.raw/*42.28*/("""</b></p>
        <p id="abstract">"""),_display_(/*43.27*/abstractPMID),format.raw/*43.39*/("""</p>
      <p>PubMed Entry: <a href="http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*44.69*/r/*44.70*/.pmid),format.raw/*44.75*/("""">"""),_display_(/*44.78*/r/*44.79*/.pmid),format.raw/*44.84*/("""</a></p>
      <div class="mini-layout">
        <div class="mini-layout-body"></div>
      </div>

        """),_display_(/*49.10*/if(compRefs.size() > 0 )/*49.34*/ {_display_(Seq[Any](format.raw/*49.36*/("""
        """),format.raw/*50.9*/("""<div class='glycosylation-sites'>
            <div class="alert alert-info">Compositional data available for this curated publication</div>
        </div>
        """)))}),format.raw/*53.10*/("""

      """),format.raw/*55.7*/("""<ul id="31" class="structures">
        """),_display_(/*56.10*/for(s <- r.streference) yield /*56.33*/ {_display_(Seq[Any](format.raw/*56.35*/("""
        """),format.raw/*57.9*/("""<li>
          """),_display_(/*58.12*/views/*58.17*/.html.format.structure(s.structure.id)),format.raw/*58.55*/("""
        """),format.raw/*59.9*/("""</li>
        """)))}),format.raw/*60.10*/("""
        """),_display_(/*61.10*/if(compRefs.size() > 0)/*61.33*/ {_display_(Seq[Any](format.raw/*61.35*/("""
            """),_display_(/*62.14*/for(c <- compRefs) yield /*62.32*/{_display_(Seq[Any](format.raw/*62.33*/("""
            """),format.raw/*63.13*/("""<li>"""),_display_(/*63.18*/c/*63.19*/.composition_structure.composition),format.raw/*63.53*/("""</li>
            """)))}),format.raw/*64.14*/("""
        """)))}),format.raw/*65.10*/("""
      """),format.raw/*66.7*/("""</ul>
    </div><!-- /col -->
    """)))}),format.raw/*68.6*/(""" 
    """),format.raw/*69.5*/("""<div class="span4 sidebar">

      """),_display_(/*71.8*/views/*71.13*/.html.format.format()),format.raw/*71.34*/("""

      """),format.raw/*73.7*/("""<div class="info">
        <h3>Biological Associations</h3>
        <div class='taxonomy'>
          <a id='toggle-taxonomy'><span class='label label-important'><span class='icon-tags icon-white'></span> Taxonomy  <span class="caret"></span></span></a>
          <a id='toggle-protein'><span class='label label-warning'><span class='icon-map-marker icon-white'></span> Protein <span class="caret"></span></span></a>
          <a id='toggle-source'><span class='label label-success'><span class='icon-leaf icon-white'></span> Source <span class="caret"></span></span></a>
        </div>

        <div>
          <ul id='more-taxonomy'>
            <h3 id='less-taxonomy'><span class='icon-tags icon-white'></span> Taxonomies</h3>
            """),_display_(/*84.14*/for(tax <- taxsources) yield /*84.36*/{_display_(Seq[Any](format.raw/*84.37*/("""
            """),format.raw/*85.13*/("""<li><span class='icon-tag icon-white'></span> <a href='../taxonomysearch?taxonomy="""),_display_(/*85.96*/tax),format.raw/*85.99*/("""'>"""),_display_(/*85.102*/tax),format.raw/*85.105*/("""</a></li>
            """)))}),format.raw/*86.14*/("""
          """),format.raw/*87.11*/("""</ul>
          <ul id='more-protein'>
            <h3 id='less-protein'><span class='icon-map-marker icon-white'></span> Protein</h3>
            """),_display_(/*90.14*/for(protein <- proteinsources) yield /*90.44*/{_display_(Seq[Any](format.raw/*90.45*/("""

		    """),_display_(/*92.8*/if(protein.swiss_prot != null)/*92.38*/ {_display_(Seq[Any](format.raw/*92.40*/("""
           	    """),format.raw/*93.17*/("""<li><span class='icon-map-marker icon-white'></span> <a href='/proteinsummary/"""),_display_(/*93.96*/protein/*93.103*/.swiss_prot),format.raw/*93.114*/("""/annotated'>"""),_display_(/*93.127*/protein/*93.134*/.protein),format.raw/*93.142*/(""", """),_display_(/*93.145*/protein/*93.152*/.swiss_prot),format.raw/*93.163*/("""</a></li>
		    """)))}),format.raw/*94.8*/("""
		    """),_display_(/*95.8*/if(protein.swiss_prot == null && protein.protein != null)/*95.65*/ {_display_(Seq[Any](format.raw/*95.67*/("""
                 """),format.raw/*96.18*/("""<li><span class='icon-map-marker icon-white'></span> <a href="/proteinsummary/"""),_display_(/*96.97*/protein/*96.104*/.protein),format.raw/*96.112*/("""/"""),_display_(/*96.114*/protein/*96.121*/.taxonomy),format.raw/*96.130*/("""">"""),_display_(/*96.133*/protein/*96.140*/.protein),format.raw/*96.148*/("""  """),format.raw/*96.150*/("""xxx</a></li>
            """)))}),format.raw/*97.14*/("""
		
            """)))}),format.raw/*99.14*/("""
          """),format.raw/*100.11*/("""</ul>
           <ul id='more-source'>
                <h3 id='less-source'><span class='icon-leaf icon-white'></span> Source</h3>
                """),_display_(/*103.18*/for(protein <- proteinsources) yield /*103.48*/{_display_(Seq[Any](format.raw/*103.49*/("""
                """),format.raw/*104.17*/("""<li><span class='icon-tag icon-white'></span> """),_display_(/*104.64*/protein/*104.71*/.whole),format.raw/*104.77*/("""</li>
                """)))}),format.raw/*105.18*/("""
            """),format.raw/*106.13*/("""</ul>
        </div>

      </div>

      <div class="info">
        <h3>Validation Method</h3>
        """),_display_(/*113.10*/for(methods <- refmethods) yield /*113.36*/ {_display_(Seq[Any](format.raw/*113.38*/("""
        """),_display_(/*114.10*/for(rmethod <- methods.refmethod) yield /*114.43*/ {_display_(Seq[Any](format.raw/*114.45*/(""" """),format.raw/*114.46*/("""<p>"""),_display_(/*114.50*/rmethod/*114.57*/.method.description.toUpperCase()),format.raw/*114.90*/("""</p>
        """)))}),format.raw/*115.10*/("""
        """)))}),format.raw/*116.10*/("""

        """),_display_(/*118.10*/for(methodsgp <- refmethodsgp) yield /*118.40*/ {_display_(Seq[Any](format.raw/*118.42*/("""
        """),_display_(/*119.10*/for(rmethodgp <- methodsgp.refmethodgps) yield /*119.50*/ {_display_(Seq[Any](format.raw/*119.52*/("""

          """),format.raw/*121.11*/("""<p>"""),_display_(/*121.15*/rmethodgp/*121.24*/.methodgp.description.toUpperCase()),format.raw/*121.59*/("""</p>
        """)))}),format.raw/*122.10*/("""
        """)))}),format.raw/*123.10*/("""
        
        
        """),format.raw/*126.9*/("""<div class="mini-layout fluid">
          <div class="mini-layout-sidebar"></div>
          <div class="mini-layout-body"></div>
        </div>
      </div>
      
      <div class="info">
      	 <h3>Connections</h3>
      	 """),_display_(/*134.10*/for(r <- references) yield /*134.30*/{_display_(Seq[Any](format.raw/*134.31*/("""
      	 """),format.raw/*135.9*/("""<a href='http://researchdata.ands.org.au/"""),_display_(/*135.51*/ands(r.title)),format.raw/*135.64*/("""'> Research Data Australia </a>
     	 """)))}),format.raw/*136.9*/("""
      """),format.raw/*137.7*/("""</div>
    </div><!-- /col -->
  </div><!-- /row -->
  """),_display_(/*140.4*/views/*140.9*/.html.footerunicarb.footerunicarb()),format.raw/*140.44*/("""
"""),format.raw/*141.1*/("""</section>



""")))}),format.raw/*145.2*/("""
"""))
      }
    }
  }

  def render(message:String,references:List[Reference],refmethods:List[Reference],taxsources:ArrayList[String],proteinsources:ArrayList[Biolsource],prot:ArrayList[String],refmethodsgp:List[Reference],compRefs:List[composition_protein.CompRef],abstractPMID:String): play.twirl.api.HtmlFormat.Appendable = apply(message,references,refmethods,taxsources,proteinsources,prot,refmethodsgp,compRefs,abstractPMID)

  def f:((String,List[Reference],List[Reference],ArrayList[String],ArrayList[Biolsource],ArrayList[String],List[Reference],List[composition_protein.CompRef],String) => play.twirl.api.HtmlFormat.Appendable) = (message,references,refmethods,taxsources,proteinsources,prot,refmethodsgp,compRefs,abstractPMID) => apply(message,references,refmethods,taxsources,proteinsources,prot,refmethodsgp,compRefs,abstractPMID)

  def ref: this.type = this

}


}

/**/
object refdisplay extends refdisplay_Scope0.refdisplay
              /*
                  -- GENERATED --
                  DATE: Mon Apr 25 18:05:03 AEST 2016
                  SOURCE: /Users/matthew/IdeaProjects/unicarbkb/app/views/refdisplay.scala.html
                  HASH: 4d38bf51a99c1395e3abd304b8302d7009083b2b
                  MATRIX: 902->1|1265->291|1277->296|1361->370|1372->374|1477->271|1504->289|1531->367|1560->448|1589->450|1674->509|1713->532|1752->533|1782->536|1837->561|1866->563|1993->664|2005->668|2044->670|2072->672|2108->692|2148->694|2176->695|2477->969|2487->970|2516->978|2546->981|2556->982|2582->987|2612->990|2622->991|2656->1004|2728->1046|2756->1047|2809->1074|2845->1094|2885->1096|2915->1099|2996->1153|3006->1154|3040->1167|3070->1170|3080->1171|3106->1176|3405->1448|3440->1462|3502->1497|3535->1509|3635->1582|3645->1583|3671->1588|3701->1591|3711->1592|3737->1597|3873->1706|3906->1730|3946->1732|3982->1741|4177->1905|4212->1913|4280->1954|4319->1977|4359->1979|4395->1988|4438->2004|4452->2009|4511->2047|4547->2056|4593->2071|4630->2081|4662->2104|4702->2106|4743->2120|4777->2138|4816->2139|4857->2152|4889->2157|4899->2158|4954->2192|5004->2211|5045->2221|5079->2228|5144->2263|5177->2269|5239->2305|5253->2310|5295->2331|5330->2339|6099->3081|6137->3103|6176->3104|6217->3117|6327->3200|6351->3203|6382->3206|6407->3209|6461->3232|6500->3243|6675->3391|6721->3421|6760->3422|6795->3431|6834->3461|6874->3463|6919->3480|7025->3559|7042->3566|7075->3577|7116->3590|7133->3597|7163->3605|7194->3608|7211->3615|7244->3626|7291->3643|7325->3651|7391->3708|7431->3710|7477->3728|7583->3807|7600->3814|7630->3822|7660->3824|7677->3831|7708->3840|7739->3843|7756->3850|7786->3858|7817->3860|7874->3886|7922->3903|7962->3914|8138->4062|8185->4092|8225->4093|8271->4110|8346->4157|8363->4164|8391->4170|8446->4193|8488->4206|8621->4311|8664->4337|8705->4339|8743->4349|8793->4382|8834->4384|8864->4385|8896->4389|8913->4396|8968->4429|9014->4443|9056->4453|9095->4464|9142->4494|9183->4496|9221->4506|9278->4546|9319->4548|9360->4560|9392->4564|9411->4573|9468->4608|9514->4622|9556->4632|9611->4659|9866->4886|9903->4906|9943->4907|9980->4916|10050->4958|10085->4971|10156->5011|10191->5018|10274->5074|10288->5079|10345->5114|10374->5115|10420->5130
                  LINES: 27->1|31->4|31->4|33->8|33->8|36->1|37->3|38->6|40->10|42->12|45->15|45->15|45->15|46->16|47->17|48->18|53->23|53->23|53->23|54->24|54->24|54->24|55->25|58->28|58->28|58->28|58->28|58->28|58->28|58->28|58->28|58->28|60->30|61->31|63->33|63->33|63->33|64->34|65->35|65->35|65->35|65->35|65->35|65->35|72->42|72->42|73->43|73->43|74->44|74->44|74->44|74->44|74->44|74->44|79->49|79->49|79->49|80->50|83->53|85->55|86->56|86->56|86->56|87->57|88->58|88->58|88->58|89->59|90->60|91->61|91->61|91->61|92->62|92->62|92->62|93->63|93->63|93->63|93->63|94->64|95->65|96->66|98->68|99->69|101->71|101->71|101->71|103->73|114->84|114->84|114->84|115->85|115->85|115->85|115->85|115->85|116->86|117->87|120->90|120->90|120->90|122->92|122->92|122->92|123->93|123->93|123->93|123->93|123->93|123->93|123->93|123->93|123->93|123->93|124->94|125->95|125->95|125->95|126->96|126->96|126->96|126->96|126->96|126->96|126->96|126->96|126->96|126->96|126->96|127->97|129->99|130->100|133->103|133->103|133->103|134->104|134->104|134->104|134->104|135->105|136->106|143->113|143->113|143->113|144->114|144->114|144->114|144->114|144->114|144->114|144->114|145->115|146->116|148->118|148->118|148->118|149->119|149->119|149->119|151->121|151->121|151->121|151->121|152->122|153->123|156->126|164->134|164->134|164->134|165->135|165->135|165->135|166->136|167->137|170->140|170->140|170->140|171->141|175->145
                  -- GENERATED --
              */
          