package models.sub;

import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="sub", name="glycan_sequence")
public class GlycanSequence extends Model {

	@Id
	public Long glycanSequenceId;

	public String sequenceGws;
	public String sequenceCtCondensed;
	
	@OneToMany
	public List<GlycanResidueUnicarb> glycanResidueUnicarb;
	
	
}
