package models.glycomobcomposition;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 8/01/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="glycomobcomposition", name="ionmob_gas")
public class IonmobGas extends Model {

    @Id
    public Long id;
    public String name;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    public List<IonmobGas> ionmobGasList;

    public static Finder<Long,IonmobGas> find = new Finder<Long,IonmobGas>( IonmobGas.class);

}
