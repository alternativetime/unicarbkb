package models.glycomobcomposition;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 14/01/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="glycomobcomposition", name="glycomobcomposition")
public class Glycomobcomposition extends Model {

    @Id
    public Long id;
    //public String mass;
    public Double mass;
    public String hex;
    public String hexnac;
    public String dhex;
    public String neunac;
    public int charge;
    public String mi; //multiple isomers

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    public List<CssDataGeneral> cssDataGenerals;

    public static Finder<Long,Glycomobcomposition> find = new Finder<Long,Glycomobcomposition>( Glycomobcomposition.class);

    /* will match compositions referenced to native structures only */
    public static List<Glycomobcomposition> getMatchingCompositions(String hex, String hexnac, String dhex, String neunac){
        List<Glycomobcomposition> glycomobcompositions = find.query().fetch("cssDataGenerals").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("native_structure", "y").findList();
        return glycomobcompositions;
    }

    public static List<Glycomobcomposition> getMatchingCompositionsAll(String hex, String hexnac, String dhex, String neunac){
        List<Glycomobcomposition> glycomobcompositions = find.query().fetch("cssDataGenerals").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).findList();
        return glycomobcompositions;
    }


    public static List<Glycomobcomposition> getMatchingCompositionsHeAll(String hex, String hexnac, String dhex, String neunac){
        List<Glycomobcomposition> glycomobCompositions = find.query().fetch("cssDataGenerals").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("cssDataGenerals.ionmobGasList.id", 1).findList();
        return glycomobCompositions;
    }

    public static List<Glycomobcomposition> getMatchingCompositionsNAll(String hex, String hexnac, String dhex, String neunac){
        List<Glycomobcomposition> glycomobCompositions = find.query().fetch("cssDataGenerals").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("cssDataGenerals.ionmobGasList.id", 2).findList();
        return glycomobCompositions;
    }
}
