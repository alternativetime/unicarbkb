package models.glycomobcomposition;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 8/01/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="glycomobcomposition", name="css_data")
public class CssData extends Model {

    @Id
    public Long id;
    public Double css;
    public String mode;
    public String nativeStructure;


    @ManyToOne
    @JoinColumn(name="adduct_id")
    public Adduct adductList;

    @ManyToOne
    @JoinColumn(name="glycoprotein_standard_id")
    public GlycoproteinStandard glycoproteinStandardList;

    @ManyToOne
    @JoinColumn(name="ionmob_gas_id")
    public IonmobGas ionmobGasList;

    @ManyToOne
    @JoinColumn(name="sodiated_glycomob_composition_id")
    public SodiatedGlycomobComposition sodiatedGlycomobCompositionList;

    public static Finder<Long,CssData> find = new Finder<Long,CssData>( CssData.class);

    public static List<CssData> getMatchingCSS(Double css) {
        List<CssData> cssDatas = find.query().where().between("css", css-4, css+4).findList();
        return cssDatas;
    }


}
