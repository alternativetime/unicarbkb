package models.glycomobcomposition;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 8/01/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="glycomobcomposition", name="sodiated_glycomob_composition")
public class SodiatedGlycomobComposition extends Model {

    @Id
    public Long id;
    //public String massCharge; //with Na
    public Double massCharge;
    public Double mass;
    public String hex;
    public String hexnac;
    public String dhex;
    public String neunac;
    public int charge;
    public String mi; //multiple isomers

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    public List<CssData> cssDatas;

    public static Finder<Long,SodiatedGlycomobComposition> find = new Finder<Long,SodiatedGlycomobComposition>( SodiatedGlycomobComposition.class);

    /* will match compositions referenced to native structures only */
    public static List<SodiatedGlycomobComposition> getMatchingCompositions(String hex, String hexnac, String dhex, String neunac){
        List<SodiatedGlycomobComposition> sodiatedGlycomobCompositions = find.query().fetch("cssDatas").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("native_structure", "y").findList();
        return sodiatedGlycomobCompositions;
    }

    public static List<SodiatedGlycomobComposition> getMatchingCompositionsAll(String hex, String hexnac, String dhex, String neunac){
        List<SodiatedGlycomobComposition> sodiatedGlycomobCompositions = find.query().fetch("cssDatas").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).findList();
        return sodiatedGlycomobCompositions;
    }

    /*
    gas id 1 = He
    gas id 2 = N
     */
    public static List<SodiatedGlycomobComposition> getMatchingCompositionsHeAll(String hex, String hexnac, String dhex, String neunac){
        List<SodiatedGlycomobComposition> sodiatedGlycomobCompositions = find.query().fetch("cssDatas").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("cssDatas.ionmobGasList.id", 1).findList();
        return sodiatedGlycomobCompositions;
    }

    public static List<SodiatedGlycomobComposition> getMatchingCompositionsNAll(String hex, String hexnac, String dhex, String neunac){
        List<SodiatedGlycomobComposition> sodiatedGlycomobCompositions = find.query().fetch("cssDatas").where().eq("hex", hex).eq("hexnac", hexnac).eq("dhex", dhex).eq("neunac", neunac).eq("cssDatas.ionmobGasList.id", 2).findList();
        return sodiatedGlycomobCompositions;
    }
}

