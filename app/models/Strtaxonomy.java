package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity 
@Table(schema="public", name="strtaxonomy")
public class Strtaxonomy extends Model {

    @Id
    public Long id;

    @ManyToOne
    public Taxonomy taxonomy;

    @ManyToOne
    public Structure structure; 
    
    public String species;

    public static Finder<Long,Strtaxonomy> find = new Finder<Long,Strtaxonomy>( Strtaxonomy.class);

    
}

