package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Taxprotein entity managed by Ebean
 */
@Entity 
@Table(schema="public", name="taxprotein")
public class Taxprotein extends Model {

    @Id
    public Long id;
    
    public String species;
   
    public String protein;

    public String swiss_prot;

	@ManyToOne
	public Taxonomy taxonomy;

    //public static Finder<Long,Taxprotein> find = new Finder<Long,Taxprotein>( Taxprotein.class);
    public static Finder<Long,Taxprotein> find = new Finder<Long,Taxprotein>(Taxprotein.class);
}

