package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;
/**
 * Reference entity managed by Ebean
 */
@Entity 
@Table(schema="public", name="ftmerge")
public class Ftmerge extends Model {

    @Id
    public Long id;
    public String glycodb_no;
    public String ft;
    public String amino_acid;
    public String swiss_prot;

    @ManyToOne
    public Reference reference;

    public static Finder<Long,Ftmerge> find = new Finder<Long,Ftmerge>( Ftmerge.class);
    

    public static List<Ftmerge> findFt(String ft) {
       return find //.fetch("reference")
           .query().where()
                .eq("ft", ft)
           .findList();
    }
    
}

