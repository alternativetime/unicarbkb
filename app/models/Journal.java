package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * Journal entity managed by Ebean
 */
@Entity
@Table(schema="public", name="journal")
public class Journal extends Model {

    @Id
    public Long id;
    
    public String shortname;
   
    public String name;

    public static Finder<Long,Journal> find = new Finder<Long,Journal>( Journal.class);

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Journal c: Journal.find.query().orderBy("name").findList()) {
            options.put(c.id.toString(), c.name);
        }
        return options;
    }

    
}

