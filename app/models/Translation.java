package models;

import io.ebean.Finder;
import io.ebean.Model;
import org.eurocarbdb.application.glycanbuilder.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

import static org.eurocarbdb.application.glycanbuilder.MassOptions.ISOTOPE_MONO;

@Entity 
@Table(schema="public", name="translation")
public class Translation extends Model {

    @Id
    public Long id;
    
    public String iupac;
   
    public String ct;

    public String gws;

    @ManyToOne    
    public Structure structure;
    
    //public Long gs;

    public static Finder<Long,Translation> find = new Finder<Long,Translation>( Translation.class);

    public static Translation findIupac(String iupac){
        return find.query().where().like("iupac", iupac).findUnique();
    }


    public static List<Translation> searchTranslation(String glycoct) { 
        return
            find.query().where()
            	.like("ct", glycoct)  
                //.join("strtaxonomy")
		.findList();
    }

    public static Translation searchTransUnique(String glycoct) {
	Translation translation = null;		
	//return 
	translation = find.query().where().like("ct", glycoct).findUnique();
	if (translation == null ) {
		try{
		//throw new NullPointerException(); // RuntimeException("Query is empty");
		//
		}catch(RuntimeException e) { String err = e.getMessage(); } 
	}
	return translation;
    }
	

    public static Translation translationCT(Long id){
    	return	
	    find.query().where()
	    	.eq("structure_id", id)
		.findUnique();
    
    }
    
    public static Double builder(String glycoct){
    	Glycan g = new Glycan();
    	
    	System.out.println(glycoct + "MASS---- " + g.computeMZ());
    	//g.fromGlycoCT(glycoct);

    	
    	String option = MassOptions.NO_DERIVATIZATION;

        MassOptions masso = new MassOptions();

        masso.ION_CLOUD.clear();
        masso.setDerivatization(option);


        g.setMassOptions(masso);
        //g.setCharges(ic);

        double mass = g.computeMass();
        System.out.println("mass result is --- " + mass); 
    	
    	return mass;
    	
     }


     public static Long checkDigestStructure(String ct) {
	Long gsId = -1L;
	Translation translation = new Translation();
	translation = Translation.searchTransUnique(ct);
	if(translation != null && translation.structure.id > 0 ) {  //test change
		return translation.structure.id; //test change
	} else {
		return gsId;
	}

     }

    public static Double calculateMass(String glycoct) throws Exception {
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text
        //workspace.setDefaultMassOptions(MassOptions.fromString(ISOTOPE_MONO));


        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);
        String op = MassOptions.NO_DERIVATIZATION;
        String m = MassOptions.ISOTOPE_AVG;

        String cloud = "";
        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);
        masso.setIsotope(m);

        String redEnd = "freeEnd";




        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(glycoct.trim(), masso);

        return glycan.computeMass(ISOTOPE_MONO);
    }

}
