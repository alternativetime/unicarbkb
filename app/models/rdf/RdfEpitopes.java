package models.rdf;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 11/05/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="rdftest", name="rdf_epitopes")
public class RdfEpitopes extends Model {

    @Id
    public Long id;

    public String originalSequence;
    public String inputSequence;
    public String glycoctRings;
    public String wurcs;
    public String glytoucan;
    public String description;

    @Transient
    public int countStructures;

    @OneToMany
    public List<UniCarbkbEpitopes> uniCarbkbEpitopesList;

    public int getCountStructures() {
        return countStructures;
    }

    public void setCountStructures(int countStructures) {
        this.countStructures = countStructures;
    }

    public static Finder<Long,RdfEpitopes> find = new Finder<Long,RdfEpitopes>( RdfEpitopes.class);

    public static PagedList<RdfEpitopes> page(int page, int pageSize, String sortBy, String order, String filter) {
        return
                find.query().where().disjunction()
                        .ilike("inputSequence", "%" + filter + "%")
                        .endJunction()
                        .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
                        //.getPage(page);
    }
}
