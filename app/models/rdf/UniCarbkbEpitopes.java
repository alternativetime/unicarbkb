package models.rdf;

import io.ebean.*;
import models.Structure;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by mcampbell on 25/5/17.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="rdftest", name="unicarbkb_epitopes")
public class UniCarbkbEpitopes extends Model {

    @Id
    public Long id;

    @ManyToOne
    public Structure structure;

    @ManyToOne
    public RdfEpitopes rdfEpitopes;

    public String description;

    public static Finder<Long,UniCarbkbEpitopes> find = new Finder<Long,UniCarbkbEpitopes>( UniCarbkbEpitopes.class);


    public static List getEpitopesGroup(){

        String sql = "select r.id,r.description, count(u.structure_id) as countStructures " +
                "from rdftest.rdf_epitopes as r, rdftest.unicarbkb_epitopes as u " +
                "where r.id = u.rdf_epitopes_id " +
                "group by r.id,r.description order by countStructures desc ";

        RawSql rawSql = RawSqlBuilder
                .parse(sql)
                .columnMapping("r.id", "id")
                .columnMapping("r.description", "description")
                .create();


        Query<RdfEpitopes> query = Ebean.find(RdfEpitopes.class);
        query.setRawSql(rawSql);//.where().gt("rdfEpitopes.id", 0); //.having().gt("totalAmount", 2); //.where().gt("structure_id", 1);

        List<RdfEpitopes> rdfEpitopesList = query.findList();

        return rdfEpitopesList;
    }

    public static PagedList<UniCarbkbEpitopes> page(int page, int pageSize, String sortBy, int epitopeId) {
        return
                find.query().where().disjunction()
                        .eq("rdf_epitopes_id", 10 )
                        .endJunction()
                        .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        //.getPage(page);
    }

}
