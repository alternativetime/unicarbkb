package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity 
@Table(schema="public", name="proteinstaxonomy")
public class Proteinstaxonomy extends Model {

    @Id
    public Long id;
    
    public String swissProt;

    public String species;

    public String common;
    
    public String protein;
  
    //public String strain;


    @ManyToOne
    public Proteins proteins;

    public static Finder<Long,Proteinstaxonomy> find = new Finder<Long,Proteinstaxonomy>( Proteinstaxonomy.class);
    
    public static Proteinstaxonomy findProteinTax(String protein) {
	    	protein = protein.trim();
	    	System.out.println("tracking errors xyz " + protein);
		try{
		return
			find.query().where().disjunction()
				.ilike("swiss_prot", protein)
				.ilike("protein", protein)
				.findUnique();
		} catch(Exception e)  {
		System.out.println("Error with Protein Tax query" + e) ;
		}
		return null;
    
    }

    public static List<Proteinstaxonomy> findProteinsTax(String protein) {
		return
			find.query().where().disjunction()
                                .ilike("swiss_prot", protein)
                                .ilike("protein", protein)
                                .findList();
    }
    
}

