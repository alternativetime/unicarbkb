package models;

import io.ebean.*;
import models.composition_protein.CompProtein;
import models.composition_protein.CompSite;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;

@Entity 
@Table(schema="public", name="proteins")
public class Proteins extends Model {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8438300033977154269L;

	@Id
    public Long id;
    
    public String name;
   
    public String swissProt;

    public String description;

    @OneToMany
    public List<Stproteins> stproteins;

    @ManyToOne
    public List<ProteinGlycosylationSites> proteinsites;

    @OneToMany
    public List<models.StructureToSites> stsite;

    @OneToMany
    public List<models.GeneralSites> proteinGeneralSites;

    @OneToMany
    public List<models.DefinedSites> proteinDefinedSites;

    @OneToMany
    public List<Proteinstaxonomy> proteinsTax;

    @OneToMany
    public List<Strproteintax> strproteintax;

    @OneToMany
    public List<CompProtein> compProtein;

    @OneToMany
    public List<CompSite> compSites;

    //@OneToMany
    //public List<Biolsource> biolsource;

    public static Finder<Long,Proteins> find = new Finder<Long,Proteins>( Proteins.class);

    public static List<Proteins> findProteins(String protein) {
        return
           find.query().where().ilike("name", protein).findList();
   }

   public static List<Proteins> findProteinsSwissProt(String protein) {
        return
           find.query().where().ilike("swiss_prot", protein).findList();
   }
   
   public static List<Proteins> findProteinSwissProtMulti(String protein){
	   return
	           find.query().where().ilike("swiss_prot", "%" + protein + "%").findList();
   }
   
   public static List<Proteins> findProteinsName(String protein) {
        return
           //find.query().where().ilike("name", "%" + protein + "%").findList();
	   find.query().where().ilike("name", protein).findList();
   }

   public static List<SqlRow> findProteinsNameRaw(String protein){
	String sql = "SELECT s.structure_id FROM public.proteins p, public.stproteins s where p.name ilike '" + protein + "' and p.swiss_prot is null and p.id = s.proteins_id";

	SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
	List<SqlRow> listSql = sqlQuery.findList();
	return listSql;
   }

   public static List<SqlRow> findProteinsNameSummaryUniprot(String protein) {
	String sql = "SELECT s.structure_id FROM public.proteins p, public.stproteins s where p.swiss_prot ilike '" + protein + "' and p.id = s.proteins_id";
	SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
        List<SqlRow> listSql = sqlQuery.findList();
        return listSql;
   }


    public static HashSet proteinSummary() {

        HashSet proteinUnique = new HashSet();
        List<Proteins> proteins = Proteins.find.all();

        for (Proteins protein : proteins) {
                proteinUnique.add(protein.name);
        }

    return proteinUnique;
    }
    
    /*
     * Find all accession ids
     */
    public static HashSet proteinAccessionSummary() {
    	HashSet proteinAccession = new HashSet();
    	List<Proteins> proteinAcc = Proteins.find.all();

    	for (Proteins acc : proteinAcc) {
    		proteinAccession.add(acc.swissProt);
    	}
    	return proteinAccession;
    }

    public static PagedList<Proteins> proteinpage(int page, int pageSize, String sortBy, String order, String filter) {
        return
            find.query().where().disjunction()
                .ilike("swissProt", "%" + filter + "%")
                .ilike("name", "%" + filter + "%")
                .endJunction()
                //.join("journal")
                    .orderBy(sortBy + " " + order)
                    .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
                //.getPage(page);
    }

     public static PagedList<Proteins> proteinpagerefine(int page, int pageSize, String sortBy, String order, String filter) {

	RawSql rawsql = RawSqlBuilder.parse(filter).create();
	io.ebean.Query<Proteins> query = Ebean.find(Proteins.class);
	query.setRawSql(rawsql); 

	PagedList<Proteins> pagingList = query.setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
	pagingList.getFutureCount();

	PagedList<Proteins> page2 = (PagedList<Proteins>) pagingList;

	return page2;

    }

    public static List proteinList() {
	List<Proteins> proteins = Proteins.find.all();
	return proteins;
    }

    public static List<Proteins> proteinSearch(String filter) {
	return
	   find.query().where()
	        .ilike("name", "%" + filter + "%").findList();
    }
}

