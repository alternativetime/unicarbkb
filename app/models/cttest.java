package models;

import io.ebean.Finder;
import io.ebean.Model;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import stage.main.Sugar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;


/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="ct")

public class cttest extends Model {
	
	@Id
	public Long id;
	
	public String ct;
	
	public static Finder<Long,cttest> find = new Finder<Long,cttest>( cttest.class);
	
	public Map<String, String> digest(String ct, String enzymesInput) throws IOException{
		
		ArrayList<String> enz = new ArrayList<String>();
		String[] x = null;
		List<String> list = null;	
		if(enzymesInput != null){
			Logger.info("some odd " + enzymesInput); // + Arrays.toString(enzymesInput) );
			String [] strings = enzymesInput.split(",");
			list = Arrays.asList(strings);

		} else { 
			enz.add("ABS");
		}
		enz.add("ABS");
		//
		for(String l : list){
			enz.add(l);
		}
		
		//EnzymeMapping e = new EnzymeMapping();
		//e.createDic_link();
		//e.createDic_res();
		
		Sugar s = new Sugar(ct, enz, false, false);
		
		//s.getGlycanInput();
		//SugarTreatment d = new SugarTreatment(ct, e, enz, null, false);
		//Logger.info("output enz treatment" + s.glycanTreatment());
		
		String[] enzymes = StringUtils.substringsBetween(s.glycanTreatment(), "enzyme", "RES");
	
		ArrayList<String> out = s.getGlycanOutput();
		
		String[] enz2 = null;
		List enzList = new ArrayList<String>();
		for(String o : out){
			o.toString();
			enz2 = o.split("enzyme \\:");
			Logger.info("split " + URLEncoder.encode(enz2[0]) );
			enzList.add(URLEncoder.encode(enz2[0]));

		}
		Logger.info("size is " + enzList.size() );
		
		Map<String, String> hashMap= 	new HashMap<String, String>();
		
		int count = 1;
		for (String t : enzymes){
			hashMap.put(t, enzList.get(count).toString());
			count++;
		}	
		
		for (Map.Entry<String,String> entry : hashMap.entrySet()) {
			  String key = entry.getKey();
			  String value = entry.getValue();
		}
		
		return hashMap;
	}

}
