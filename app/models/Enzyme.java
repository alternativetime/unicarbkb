package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="enzyme")
public class Enzyme extends Model {
	
	@Id
	public Long id;
	
	public String jcggdb; //e from excel sheet to be removed
	public String kegg; //p
	public String name; //al
	public String goterm; //col az
	public String cazy;
	public String cazyfamily;
	
	public static Finder<Long,Enzyme> find = new Finder<Long,Enzyme>( Enzyme.class);


	public static List<Enzyme> getEnzymes() {
			List<Enzyme> enzyme = Enzyme.find.all();
		return enzyme;
	}
	
}

