package models;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.*;
import java.util.List;

/**
 * Unicarbdbreference entity managed by Ebean
 */
@Entity 
@Table(schema="public", name="unicarbdbreference")
public class Unicarbdbreference extends Model {

    @Id
    public Long id;

    @play.data.validation.Constraints.Required
    public String first;
   
    public int year;

    public String volume;
    public String pages;
    public String medline;
    public String title;
    public String authors; 

    //@ManyToOne(fetch=FetchType.EAGER)
     @ManyToOne
    //@JoinColumn(name="id", nullable=false)
    //public Journal getJournal() { return journal; }
    public Journal journal;

    @OneToMany
    public List<Lcmucin> lcmucin;


    
     public Unicarbdbreference(String volume, String pages, String medline, String title, String authors, String first, Journal journal) {
        this.first = first;
        this.volume = volume;
	this.pages = pages;
	this.medline = medline;
	this.title = title;
	this.authors = authors;
	this.journal = journal;
}

    
    /**
     * Generic query helper for entity Unicarbdbreference with id Long
     */
    public static Finder<Long,Unicarbdbreference> find = new Finder<Long,Unicarbdbreference>( Unicarbdbreference.class);
    
    /**
     * Return a page of computer
     *
     * @param page Page to display
     * @param pageSize Number of computers per page
     * @param sortBy Unicarbdbreference property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static PagedList<Unicarbdbreference> page(int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.query().fetch("journal").where().disjunction()
		.ilike("title", "%" + filter + "%") 
		.ilike("authors", "%" + filter + "%") 
		.endJunction().setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
                //.getPage(page);
    }


  public static List<Unicarbdbreference> findJournal(Long id) {
       return find.query().fetch("journal")
           .where()
                .eq("id", id)
           .findList();
    }
    
}

