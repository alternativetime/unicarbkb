package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="pubchem")
public class Pubchem extends Model {

    @Id
    public Long id;
    
    public String iupac;
    public String pubchem_id ;
   
    @ManyToOne
    public Structure structure;

    public static Finder<Long,Pubchem> find = new Finder<Long,Pubchem>( Pubchem.class);

    public static List<Pubchem> pubchemId(String pubchem) {
	return find //.fetch("reference")
           .query().where()
                .eq("pubchem_id", pubchem)
           .findList();
    }

    
}

