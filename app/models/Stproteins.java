package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Streference entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="stproteins")
public class Stproteins extends Model {

    @Id
    public Long id;
    @ManyToOne
    public Structure  structure;
    @ManyToOne
    public Proteins proteins; 
      

    public Stproteins(Structure structure, Proteins proteins) {
	this.structure=structure;
	this.proteins=proteins;
    }   
    
    /**
     * Generic query helper for entity with id Long
     */
    public static Finder<Long,Stproteins> find = new Finder<Long,Stproteins>( Stproteins.class);
    


}

