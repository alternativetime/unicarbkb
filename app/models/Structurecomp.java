package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(schema="public", name="structurecomp")
public class Structurecomp extends Model {

	@Id
	public Long id;
	public String glycanst;
	public String compositionId;
		
	public String glycanType;
	
	@ManyToOne
	public Structure structure;

	public static Finder<Long,Structurecomp> find = new Finder<Long,Structurecomp>( Structurecomp.class);

	/*
	 * 
	 * Find matching composition string used in glycosuite
	 */
	public static List<Structurecomp> findStructurecomp(String compositionId, String glycanType){

		if(glycanType != null) {
		return find.query().where().eq("compositionId", compositionId).ieq("glycanType", glycanType).findList();
		}
		else{
		return find.query().where().eq("compositionId", compositionId).findList();

		}
	}

}
