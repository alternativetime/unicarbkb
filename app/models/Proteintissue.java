package models;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="public", name="proteintissue")
public class Proteintissue {
	
	@Id
	public Long id;

	//public String div1;

	//public String div2;

	//public String div3;

	//public String div4;
	
	public String protein;
	
	public String swiss;
	
	@ManyToOne
	public Tissue tissue;
	
	@ManyToOne
	public Proteins proteins;
	
	public static Finder<Long,Proteintissue> find = new Finder<Long,Proteintissue>( Proteintissue.class);

}
