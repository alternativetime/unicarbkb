package models.composition_protein;

import io.ebean.*;
import models.Structure;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 24/11/2015.
 */
@Entity
@Table(schema="composition_protein", name="crcdata")
public class Crcdata extends Model {

    @Id
    public Long id;

    //public Integer structureReportId;
    public Integer charge;
    public Integer hex;
    public Integer hexnac;
    public Integer fuc;
    public Integer neuac;
    public String isoform;
    public String msMs;
    public Double abundanceLim1215;
    public Double abundanceLim1899;
    public Double abundanceLim2405;

    @ManyToOne
    public Structure structure;

    public Integer lcmucinId;


    public static Finder<Long, Crcdata> find = new Finder<Long, Crcdata>( Crcdata.class);

    public static PagedList<Crcdata> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Crcdata> pagedList = Crcdata.find.query().where().setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

    public static List<SqlRow> groupCompositionsCrc() {
        String sql = "SELECT crcdatajenny.abundance_1116_a, \n" +
                "  crcdatajenny.abundance_1116_b, \n" +
                "  crcdatajenny.abundance_620_a, \n" +
                "  crcdatajenny.abundance_620_b, \n" +
                "  crcdatajenny.abundance_837_a, \n" +
                "  crcdatajenny.abundance_837_b, \n" +
                "  crcdatajenny.abundance_480_a, \n" +
                "  crcdatajenny.abundance_480_b, \n" +
                "  crcdata.abundance_lim1215, \n" +
                "  crcdata.abundance_lim1899, \n" +
                "  crcdata.abundance_lim2405, \n" +
                "  crcdatajenny.hex, \n" +
                "  crcdatajenny.hexnac, \n" +
                "  crcdatajenny.fuc, \n" +
                "  crcdatajenny.neuac from composition_protein.crcdatajenny, composition_protein.crcdata " +
                "where crcdata.hex = crcdatajenny.hex " +
                "AND crcdata.hexnac = crcdatajenny.hexnac " +
                "AND crcdata.fuc = crcdatajenny.fuc " +
                "AND crcdata.neuac = crcdatajenny.neuac";

        //awSql rawSql = RawSqlBuilder.parse(sql).columnMapping("l.glycan_sequence_id", "l.glycan_sequence_id").create();

        SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
        List<SqlRow> listSql = sqlQuery.findList();

        return listSql;
    }

    public static List<Crcdata> getMatchingCompositionsAll(int hex, int hexnac, int dhex, int neunac){
        List<Crcdata> crcdatacomp = find.query().fetch("structure")
                .where().eq("hex", hex).eq("hexnac", hexnac).eq("fuc", dhex).eq("neuac", neunac).findList();
        return crcdatacomp;
    }

}
