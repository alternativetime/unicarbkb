package models.composition_protein;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 23/11/2015.
 * This is very temp. until we sort this kind of data out
 */
@Entity
@Table(schema="composition_protein", name="shah")
public class Shah extends Model {

    @Id
    public Long id;


    public Integer hex;
    public Integer hexnac;
    public Integer fucose;
    public Integer neuac;
    public String peptide;
    public String category;
    public String protein;
    public Double lncap;
    public Double pc3;

    public static Finder<Long,Shah> find = new Finder<Long,Shah>( Shah.class);

    public static PagedList<Shah> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Shah> pagedList = Shah.find.query().where().setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

    public static List<Shah> getProteins() {

        return Shah.find.query().select("protein").findList();
    }


}
