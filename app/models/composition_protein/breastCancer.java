package models.composition_protein;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 23/11/2015.
 * Not FT needs to be merged with Shah
 * RDF or not
 * 25210975
 */
@Entity
@Table(schema="composition_protein", name="breastdata")
public class breastCancer extends Model {

    @Id
    public Long id;

    //public Integer structureReportId;
    public Integer charge;
    public Integer hex;
    public Integer hexnac;
    public Integer fuc;
    public Integer neuac;
    public String isoform;
    public String ms_ms;
    public Double abundance_hmec;
    public Double abundance_mcf7;
    public Double abundance_skbr3;
    public Double abundance_mda157;
    public Double abundance_mda231;
    public Double abundance_hs578t;

    public static Finder<Long,breastCancer> find = new Finder<Long,breastCancer>( breastCancer.class);

    public static PagedList<breastCancer> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<breastCancer> pagedList = breastCancer.find.query().where().setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;

    }



}
