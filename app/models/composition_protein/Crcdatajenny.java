package models.composition_protein;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import models.core.GlycanSequence;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 26/11/15.
 */
@Entity
@Table(schema="composition_protein", name="crcdatajenny")
public class Crcdatajenny extends Model {

    @Id
    public Long id;

    public Integer structure_report_id;
    public Integer hex;
    public Integer hexnac;
    public Integer fuc;
    public Integer neuac;

    public Double abundance_1116_a;
    public Double abundance_1116_b;
    public Double abundance_620_a;
    public Double abundance_620_b;
    public Double abundance_837_a;
    public Double abundance_837_b;
    public Double abundance_480_a;
    public Double abundance_480_b;
    public Double abundance_t1;
    public Double abundance_t2;
    public Integer lcmucinId;
    //public Long unicarbdbSequenceId;

    @ManyToOne
    @JoinColumn(name="glycan_sequence_id")
    public GlycanSequence glycanSequence;

    public static Finder<Long,Crcdatajenny> find = new Finder<Long,Crcdatajenny>( Crcdatajenny.class);


    public static PagedList<Crcdatajenny> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Crcdatajenny> pagedList = Crcdatajenny.find.query().where().setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

    public static List<Crcdatajenny> getMatchingCompositionsAll(int hex, int hexnac, int dhex, int neunac){
        List<Crcdatajenny> crcdatacomp = find.query().fetch("glycanSequence")
                .where().eq("hex", hex).eq("hexnac", hexnac).eq("fuc", dhex).eq("neuac", neunac).findList();
        return crcdatacomp;
    }


}