package models.composition_protein;

import io.ebean.Finder;
import io.ebean.Model;
import models.Reference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 30/05/2014.
 */
@Entity
@Table(schema="composition_protein", name="comp_ref")
public class CompRef extends Model  {

    @Id
    public Long id;

    //public String composition;

    @ManyToOne
    public Reference reference;

    @ManyToOne
    public CompositionStructure composition_structure;

    public static Finder<Long,CompRef> find = new Finder<Long,CompRef>( CompRef.class);

    public static List<CompRef> findCompRefs(Long id) {
        return
                find.query().fetch("composition_structure")//.fetch("comp_ref")//.fetch("composition_structure")
                        .where()
                        .eq("reference_id", id)
                        .orderBy("composition_structure.composition")
                        .findList();
    }

}
