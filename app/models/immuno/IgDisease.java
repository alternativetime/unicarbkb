package models.immuno;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 23/11/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="immuno", name="ig_disease")
public class IgDisease extends Model {

    @Id
    public Long id;

    public String diseaseName;

    public static Finder<Long,IgDisease> find = new Finder<Long,IgDisease>( IgDisease.class);

}
