package models.immuno;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 23/11/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="immuno", name="ig_structure")
public class IgStructure extends Model {

    @Id
    public Long id;

    public Integer reported_compound;
    public Double retention_time;
    public Double mass;
    public Integer charge;
    public Double experimental;
    public Double theorectical;
    public Integer hex;
    public Integer hexnac;
    public Integer fuc;
    public Integer neuac;
    public Integer neugc;
    public Integer pentose;
    public Integer hexa;
    public Integer kdn;
    public Integer acetate;
    public Integer sulphate;
    public Integer modifications;
    public String classType;
    public String subclassType;
    public Integer structureId;

    public IgDisease igDisease;
    public IgProtein igProtein;

    public String ct;

    public static Finder<Long,IgStructure> find = new Finder<Long,IgStructure>( IgStructure.class);


}
