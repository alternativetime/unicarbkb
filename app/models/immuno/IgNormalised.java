package models.immuno;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 23/11/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="immuno", name="ig_normalised")
public class IgNormalised extends Model{

    @Id
    public Long id;

    public int compound_number;
    public double rep1;
    public double rep2;
    public double rep3;

    public IgStructure igStructure;

    public static Finder<Long,IgNormalised> find = new Finder<Long,IgNormalised>( IgNormalised.class);



}
