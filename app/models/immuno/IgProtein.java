package models.immuno;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 23/11/15.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="immuno", name="ig_protein")
public class IgProtein extends Model {

    @Id
    public Long id;

    public String proteinName;

    public static Finder<Long,IgProtein> find = new Finder<Long,IgProtein>( IgProtein.class);


}
