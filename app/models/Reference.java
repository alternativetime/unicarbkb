package models;

import io.ebean.EbeanServer;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import models.composition_protein.CompRef;
import play.data.validation.Constraints;

import javax.inject.Inject;
import javax.persistence.*;
import java.util.List;

/**
 * Reference entity managed by Ebean
 */
@Entity
@Table(schema = "public", name = "reference")
public class Reference extends Model {

    @Inject
    public static EbeanServer server;
    /**
     * Generic query helper for entity Reference with id Long
     */
    public static Finder<Long, Reference> find = new Finder<Long, Reference>( Reference.class);
    @Id
    public Long id;
    @Constraints.Required
    public String first;
    public int year;
    public String volume;
    public String pages;
    public String medline;
    public String pmid;
    public String title;
    //public String source;
    public String authors;
    //@ManyToOne(fetch=FetchType.EAGER)
    @ManyToOne
    //@JoinColumn(name="id", nullable=false)
    //public Journal getJournal() { return journal; }
    public Journal journal;
    @OneToMany
    public List<Streference> streference;
    @OneToMany
    public List<Sourceref> sourceref;
    @OneToMany
    public List<Refmethod> refmethod;
    @OneToMany
    public List<Refmethodgp> refmethodgps;
    @OneToMany
    public List<Ftmerge> ftmerge;
    @OneToMany
    public List<Strproteintaxbiolsource> strproteintaxbiolsource;
    @OneToMany
    public List<CompRef> compReferences;
    @OneToMany
    public List<Strproteintaxdisease> strproteintaxdiseases;



    public Reference(String volume, String pages, String medline, String title, String authors, String first, Journal journal) {
        this.first = first;
        this.volume = volume;
        this.pages = pages;
        this.medline = medline;
        this.title = title;
        this.authors = authors;
        this.journal = journal;
    }

    /**
     * Return a page of references
     *  @param page     Page to display
     * @param pageSize Number of computers per page
     * @param sortBy   Reference property used for sorting
     * @param order    Sort order (either or asc or desc)
     * @param filter   Filter applied on the name column
     */
    public static PagedList<Reference> page(int page, int pageSize, String sortBy, String order, String filter) {
        //return
                /*find.query().where().disjunction()
                        .ilike("title", "%" + filter + "%")
                        .ilike("authors", "%" + filter + "%")
                        .endJunction()
                        .orderBy(sortBy + " " + order)
                        .fetch("journal")
                        .find
                        .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
                        //.getPage(page); */
        PagedList<Reference> pagedList = Reference.find.query().where().disjunction()
                       .ilike("title", "%" + filter + "%")
                       .ilike("authors", "%" + filter + "%")
                       .endJunction()
                       .orderBy(sortBy + " " + order)
                    .fetch("journal")

                        .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }


    public static List<Reference> findJournal(Long id) {
        return
                find.query().fetch("journal").fetch("sourceref").fetch("sourceref.biolsource")
                .where()
                .eq("id", id)
                .findList();
    }

    public static List<Reference> findRefMethods(Long id) {
        return
                find.query().fetch("refmethod.method")
                .where()
                .eq("id", id)
                .findList();
    }

    public static List<Reference> findRefMethodsgp(Long id) {
        return
                find.query().fetch("refmethodgps.methodgp")
                .where()
                .eq("id", id)
                .findList();
    }

    public static List<Reference> findSourceref(Long id) {
        return
                find.query().fetch("sourceref").fetch("method")
                .where()
                .eq("reference_id", id)
                .findList();
    }


}

