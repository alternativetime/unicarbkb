package models;

import io.ebean.Finder;
import io.ebean.Model;
import models.disease.DiseaseOnto;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

/**
 * Streference entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="public", name="strproteintaxdisease")
public class Strproteintaxdisease extends Model {

    //@Id
    //public Long id;

    @ManyToOne
    public Structure structure;
    @ManyToOne
    public Proteins proteins;
    @ManyToOne
    public Taxonomy taxonomy;
    @ManyToOne
    public DiseaseOnto diseaseOnto;
    @ManyToOne
    public Reference reference;



    /**
     * Generic query helper for entity with id Long
     */
    public static Finder<Long,Strproteintaxdisease> find = new Finder<Long,Strproteintaxdisease>( Strproteintaxdisease.class);

    public static List<Strproteintaxdisease> getDiseaseRef(Long id){
        return Strproteintaxdisease.find.query().fetch("reference").where().disjunction().eq("diseaseOnto", id).findList();
    }


}



