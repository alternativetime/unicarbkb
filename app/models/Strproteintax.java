package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Streference entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="strproteintax")
public class Strproteintax extends Model {

    @ManyToOne
    public Structure structure;
    @ManyToOne
    public Proteins proteins;
    @ManyToOne
    public Taxonomy taxonomy; 
      
    
    /**
     * Generic query helper for entity with id Long
     */
    public static Finder<Long,Strproteintax> find = new Finder<Long,Strproteintax>( Strproteintax.class);
    


}

