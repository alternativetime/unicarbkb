package models.curated;

import io.ebean.Finder;
import io.ebean.Model;
import models.Tissue;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */

@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel")
public class Importeddataexcel extends Model {

    @Id
    public Long id;

    public String uoxf; 
    public Integer gwpNumber;
    public String composition; 
    public String relativeAbundance; //- used for some reason
    public Double standardDeviation; 
    public String glycanType;
    public String aminoAcidLink;
    public String linkSugar;
    public String coreType;
    public String commonName;
    public String proteinDescription;
    public String uniprot;
    public String aminoAcidPosition;
    public String recombinant;
    public String diseaseOntology;
    public String bloodGroup;
    public String cellLine;
    public String strain;
    public String lifeStage;
    public String special;
    //public Date releaseDate;
    //public Date lastUpdate;
    public Integer pmid;
    public String  firstAuthor;
    public Integer year;
    public String  journal;
    public String  volume;
    public String  pages;
    public String  authors;
    public String title;
    public String analytes;

    @ManyToOne
    public Fluid fluid;

    @ManyToOne
    public Species species;

    @ManyToOne
    public Tissue tissue;


    @OneToMany
    public List<ImportedGlycanSamplePrep> importedGlycanSamplePreps;
    //public GlycanSamplePrep glycanSamplePrep;

    @OneToMany
    public List<ImportedGlycanReleaseMethod> importedGlycanReleaseMethods;
    //public GlycanReleaseMethod glycanReleaseMethod;

    //@ManyToOne
    //public List<ImportedGlycanL>
    //public GlycanLabelling glycanLabelling;

    @OneToMany
    public List<ImportedChemicalMethodsGlycan> importedChemicalMethodsGlycenList;
    //public ChemicalMethodsGlycan chemicalMethodsGlycan;

    @OneToMany
    public List<ImportedGlycosidaseTreatment> importedGlycosidaseTreatments;
    //public Glycosidases glycosidaseTreatmentsGlycan;

    @OneToMany
    public List<ImportChromatography> chromatographyGlycanList;
    //public Chromatography chromatographyGlycan;


    @OneToMany
    public List<ImportedOtherStrategiesGlycan> importedOtherStrategiesGlycanList;
    //public OtherStrategies otherStrategiesGlycan;

    @OneToMany
    public List<ImportedLectinCharacterisationGlycan> importedLectinCharacterisationGlycanList;
    //public List<LectinCharacterisation lectinCharacterisationGlycan;

    @OneToMany
    public List<ImportedIonisation> importedIonisations;
    //public Ionisation ionisation;

    @OneToMany
    public List<ImportedAnalyser> importedAnalyserList;
    //public Analyser analyser;

    @OneToMany
    public List<ImportedFragmentation> importedFragmentationList;

    @OneToMany
    public List<ImportedFragmentation> importedFragmentations;

    public String validatedInferred;

    //@OneToMany
    //public List<ImportedGlycosidaseTreatment> importedGlycosidaseTreatments;
    //public Glycosidases glycosidaseTreatmentsGlycopeptide;

    @OneToMany
    public List<ImportChromatography> chromatographyGlycopeptideList;
    //public Chromatography chromatographyGlycopeptide;

    @OneToMany
    public List<ImportedOtherStrategiesGlycoprotein> importedOtherStrategiesGlycoproteins;
    //public OtherStrategies otherStrategiesGlycopeptide;

    //@OneToMany
    //public List<ImportedLectinCharacterisationGlycan> importedLectinCharacterisationGlycanList;
    //public LectinCharacterisation lectinCharacterisationGlycopeptide;

    @OneToMany
    public List<ImportedGlycopeptideSamplePrep> importedGlycopeptideSamplePreps;
    //public GlycoproteinSamplePrep glycoproteinSamplePrep;

    @ManyToOne
    public Publication publication;

    public String gwp;
    public String glycoct;

    public static Finder<Long,Importeddataexcel> find = new Finder<Long,Importeddataexcel>(Importeddataexcel.class);

}
