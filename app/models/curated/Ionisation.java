package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_ionisation")
public class Ionisation {

    @Id
    public Long id;

    public String description;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    @OneToMany
    public List<ImportedIonisation> importedIonisations;

    public static Finder<Long,Ionisation> find = new Finder<Long,Ionisation>(Ionisation.class);

}
