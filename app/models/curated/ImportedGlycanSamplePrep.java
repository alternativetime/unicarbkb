package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_glycan_sample_prep")
public class ImportedGlycanSamplePrep {

    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Glycansampleprep glycansampleprep;

    public static String createGlycanSamplePrep(Importeddataexcel importeddataexcel, String samplePrep){


        if(samplePrep.contains(",")) {
            String[] preps = samplePrep.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedGlycanSamplePrep importedGlycanSamplePrep = new ImportedGlycanSamplePrep();
                Glycansampleprep glycansampleprep = Glycansampleprep.find.query().where().ilike("description", prep).findUnique();

                if(glycansampleprep == null ){
                    Glycansampleprep glycansampleprep1 = new Glycansampleprep();
                    glycansampleprep1.description = prep.toLowerCase();
                    Ebean.save(glycansampleprep1);

                    importedGlycanSamplePrep.importeddataexcel = importeddataexcel;
                    importedGlycanSamplePrep.glycansampleprep = glycansampleprep1;
                    Ebean.save(importedGlycanSamplePrep);

                } else {

                    importedGlycanSamplePrep.importeddataexcel = importeddataexcel;
                    importedGlycanSamplePrep.glycansampleprep = glycansampleprep;
                    Ebean.save(importedGlycanSamplePrep);
                }

            }

        } else {
            ImportedGlycanSamplePrep importedGlycanSamplePrep = new ImportedGlycanSamplePrep();
            samplePrep.trim();
            Glycansampleprep glycansampleprep = Glycansampleprep.find.query().where().ilike("description", samplePrep).findUnique();

            if(glycansampleprep == null ){
                Glycansampleprep glycansampleprep1 = new Glycansampleprep();
                glycansampleprep1.description = samplePrep.toLowerCase();
                Ebean.save(glycansampleprep1);

                importedGlycanSamplePrep.importeddataexcel = importeddataexcel;
                importedGlycanSamplePrep.glycansampleprep = glycansampleprep1;
                Ebean.save(importedGlycanSamplePrep);

            } else {
                importedGlycanSamplePrep.importeddataexcel = importeddataexcel;
                importedGlycanSamplePrep.glycansampleprep = glycansampleprep;
                Ebean.save(importedGlycanSamplePrep);
            }
        }

        return "done";
    }
}
