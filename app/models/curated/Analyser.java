package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_analyser")
public class Analyser {

    @Id
    public Long id;

    public String description;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    //@ManyToOne
    //public Importeddataexcel importeddataexcel;

    @OneToMany
    public List<ImportedAnalyser> importedAnalysers;

    public static Finder<Long,Analyser> find = new Finder<Long,Analyser>(Analyser.class);
}
