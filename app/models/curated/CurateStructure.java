package models.curated;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;

/**
 * Created by matthew on 16/09/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="curate_structure")
public class CurateStructure extends Model {

    @Id
    public Long id;

    public String glycoct;
    public String composition;

    @ManyToOne
    @JoinColumn(name = "curate_site_id")
    public CurateSite curateSite;

    public static Finder<Long,CurateStructure> find = new Finder<Long,CurateStructure>(CurateStructure.class);

    public static CurateStructure createStructures(Importeddataexcel importeddataexcel, CurateSite curateSite){

        CurateStructure curateStructure= CurateStructure.find.query().where().eq("curate_site_id", curateSite.id).ilike("glycoct", String.valueOf(importeddataexcel.glycoct)).findUnique();

        if (curateStructure== null) {
            CurateStructure curateStructure1 = new CurateStructure();
            if(importeddataexcel.glycoct != null ) {
                curateStructure1.glycoct = importeddataexcel.glycoct;
            }else{
                curateStructure1.glycoct = "";
            }

            if(importeddataexcel.composition != null) {
                curateStructure1.composition = importeddataexcel.composition;
            }else{
                curateStructure1.composition = "";
            }


            curateStructure1.curateSite = curateSite;
            Ebean.save(curateStructure1);

        }
        return curateStructure;

    }

}
