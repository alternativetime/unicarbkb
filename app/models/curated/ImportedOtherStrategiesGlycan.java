package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_other_strategies_glycan")
public class ImportedOtherStrategiesGlycan {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public OtherStrategies otherStrategies;

    public static String createOtherStrategiesGlycan(Importeddataexcel importeddataexcel, String other){


        if(other.contains(",")) {
            String[] preps = other.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedOtherStrategiesGlycan importedOtherStrategiesGlycan = new ImportedOtherStrategiesGlycan();
                OtherStrategies otherStrategies = OtherStrategies.find.query().where().ilike("description", prep).findUnique();

                if(otherStrategies == null ){
                    OtherStrategies otherStrategies1 = new OtherStrategies();
                    otherStrategies1.description = prep;
                    Ebean.save(otherStrategies1);

                    importedOtherStrategiesGlycan.importeddataexcel = importeddataexcel;
                    importedOtherStrategiesGlycan.otherStrategies = otherStrategies1;
                    Ebean.save(importedOtherStrategiesGlycan);
                } else {
                    importedOtherStrategiesGlycan.importeddataexcel = importeddataexcel;
                    importedOtherStrategiesGlycan.otherStrategies = otherStrategies;
                    Ebean.save(importedOtherStrategiesGlycan);
                }

            }

        } else {
            ImportedOtherStrategiesGlycan importedOtherStrategiesGlycan = new ImportedOtherStrategiesGlycan();
            other.trim();
            OtherStrategies otherStrategies = OtherStrategies.find.query().where().ilike("description", other).findUnique();

            if(otherStrategies == null ){
                OtherStrategies otherStrategies1 = new OtherStrategies();
                otherStrategies1.description = other;
                Ebean.save(otherStrategies1);

                importedOtherStrategiesGlycan.importeddataexcel = importeddataexcel;
                importedOtherStrategiesGlycan.otherStrategies = otherStrategies1;
                Ebean.save(importedOtherStrategiesGlycan);
            } else {
                importedOtherStrategiesGlycan.importeddataexcel = importeddataexcel;
                importedOtherStrategiesGlycan.otherStrategies = otherStrategies;
                Ebean.save(importedOtherStrategiesGlycan);
            }
        }

        return "done";
    }
}
