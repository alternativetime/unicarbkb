package models.curated;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_fluid")
public class Fluid {

    @Id
    public Long id;

    public String description;

    public String meshId;
    public String meshUniqueId;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;


}
