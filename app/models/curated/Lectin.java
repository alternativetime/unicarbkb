package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_lectin")
public class Lectin {

    @Id
    public Long id;

    public String description;
    public String name;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    @OneToMany
    public List<ImportedLectinCharacterisationGlycan> importedLectinCharacterisationGlycanList;

    public static Finder<Long,Lectin> find = new Finder<Long,Lectin>(Lectin.class);
}
