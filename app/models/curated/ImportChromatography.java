package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_chromatography_glycan")
public class ImportChromatography {

    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Chromatography chromatography;

    public static String createChrom(Importeddataexcel importeddataexcel, String chrom){


        if(chrom.contains(",")) {
            String[] preps = chrom.split(",");
            for(String prep : preps){
                prep.trim();
                ImportChromatography importChromatography = new ImportChromatography();
                Chromatography chromatography = Chromatography.find.query().where().ilike("description", prep).findUnique();

                if(chromatography == null ) {

                    Chromatography chromatography1 = new Chromatography();
                    chromatography1.description = prep.toLowerCase();
                    Ebean.save(chromatography1);

                    importChromatography.importeddataexcel = importeddataexcel;
                    importChromatography.chromatography = chromatography1;
                    Ebean.save(importChromatography);

                } else {
                    importChromatography.importeddataexcel = importeddataexcel;
                    importChromatography.chromatography = chromatography;
                    Ebean.save(importChromatography);
                }

            }

        } else {
            ImportChromatography importChromatography = new ImportChromatography();
            chrom.trim();
            Chromatography chromatography = Chromatography.find.query().where().ilike("description", chrom).findUnique();

            if(chromatography == null ) {

                Chromatography chromatography1 = new Chromatography();
                chromatography1.description = chrom.toLowerCase();
                Ebean.save(chromatography1);

                importChromatography.importeddataexcel = importeddataexcel;
                importChromatography.chromatography = chromatography1;
                Ebean.save(importChromatography);

            } else {

                importChromatography.importeddataexcel = importeddataexcel;
                importChromatography.chromatography = chromatography;
                Ebean.save(importChromatography);

            }
        }

        return "done";
    }
}
