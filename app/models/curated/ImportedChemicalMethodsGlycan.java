package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_chemical_methods_glycan")
public class ImportedChemicalMethodsGlycan {

    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public ChemicalMethodsGlycan chemicalMethodsGlycan;

    public static String createChemicalMethods(Importeddataexcel importeddataexcel, String chemical){


        if(chemical.contains(",")) {
            String[] preps = chemical.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedChemicalMethodsGlycan importedChemicalMethodsGlycan = new ImportedChemicalMethodsGlycan();
                ChemicalMethodsGlycan chemicalMethodsGlycan = ChemicalMethodsGlycan.find.query().where().ilike("description", prep).findUnique();

                if(chemicalMethodsGlycan == null ){
                    ChemicalMethodsGlycan chemicalMethodsGlycan1 = new ChemicalMethodsGlycan();
                    chemicalMethodsGlycan1.description = prep.toLowerCase();
                    Ebean.save(chemicalMethodsGlycan1);

                    importedChemicalMethodsGlycan.importeddataexcel = importeddataexcel;
                    importedChemicalMethodsGlycan.chemicalMethodsGlycan = chemicalMethodsGlycan1;
                    Ebean.save(importedChemicalMethodsGlycan);

                } else {
                    importedChemicalMethodsGlycan.importeddataexcel = importeddataexcel;
                    importedChemicalMethodsGlycan.chemicalMethodsGlycan = chemicalMethodsGlycan;
                    Ebean.save(importedChemicalMethodsGlycan);
                }

            }

        } else {
            ImportedChemicalMethodsGlycan importedChemicalMethodsGlycan = new ImportedChemicalMethodsGlycan();
            chemical.trim();
            ChemicalMethodsGlycan chemicalMethodsGlycan = ChemicalMethodsGlycan.find.query().where().ilike("description", chemical).findUnique();

            if(chemicalMethodsGlycan == null ){
                ChemicalMethodsGlycan chemicalMethodsGlycan1 = new ChemicalMethodsGlycan();
                chemicalMethodsGlycan1.description = chemical.toLowerCase();
                Ebean.save(chemicalMethodsGlycan1);

                importedChemicalMethodsGlycan.importeddataexcel = importeddataexcel;
                importedChemicalMethodsGlycan.chemicalMethodsGlycan = chemicalMethodsGlycan1;
                Ebean.save(importedChemicalMethodsGlycan);

            } else {
                importedChemicalMethodsGlycan.importeddataexcel = importeddataexcel;
                importedChemicalMethodsGlycan.chemicalMethodsGlycan = chemicalMethodsGlycan;
                Ebean.save(importedChemicalMethodsGlycan);
            }
        }

        return "done";
    }
}
