package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_glycoproteinsampleprep")
public class Glycoproteinsampleprep {

    @Id
    public Long id;

    public String description;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    public static Finder<Long,Glycoproteinsampleprep> find = new Finder<Long,Glycoproteinsampleprep>(Glycoproteinsampleprep.class);
}
