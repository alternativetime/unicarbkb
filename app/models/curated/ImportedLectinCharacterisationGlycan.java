package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_lectin_characterisation_glycan")
public class ImportedLectinCharacterisationGlycan {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Lectin lectin;

    public static String createLectin(Importeddataexcel importeddataexcel, String lectin){


        if(lectin.contains(",")) {
            String[] preps = lectin.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedLectinCharacterisationGlycan importedLectinCharacterisationGlycan = new ImportedLectinCharacterisationGlycan();
                Lectin lectinUnique = Lectin.find.query().where().ilike("description", prep).findUnique();

                if(lectinUnique == null ){

                    Lectin lectin1 = new Lectin();
                    lectin1.description = prep.toLowerCase();
                    lectin1.name = prep.toLowerCase();
                    Ebean.save(lectin1);
                    importedLectinCharacterisationGlycan.importeddataexcel = importeddataexcel;
                    importedLectinCharacterisationGlycan.lectin = lectin1;
                    Ebean.save(importedLectinCharacterisationGlycan);

                } else{
                    importedLectinCharacterisationGlycan.importeddataexcel = importeddataexcel;
                    importedLectinCharacterisationGlycan.lectin = lectinUnique;
                    Ebean.save(importedLectinCharacterisationGlycan);
                }

            }

        } else {
            ImportedLectinCharacterisationGlycan importedLectinCharacterisationGlycan = new ImportedLectinCharacterisationGlycan();
            lectin.trim();
            Lectin lectinUnique = Lectin.find.query().where().ilike("description", lectin).findUnique();

            if(lectinUnique == null ) {

                Lectin lectin1 = new Lectin();
                lectin1.description = lectin.toLowerCase();
                lectin1.name = lectin.toLowerCase();
                Ebean.save(lectin1);
                importedLectinCharacterisationGlycan.importeddataexcel = importeddataexcel;
                importedLectinCharacterisationGlycan.lectin = lectin1;
                Ebean.save(importedLectinCharacterisationGlycan);
            } else {
                importedLectinCharacterisationGlycan.importeddataexcel = importeddataexcel;
                importedLectinCharacterisationGlycan.lectin = lectinUnique;
                Ebean.save(importedLectinCharacterisationGlycan);
            }
        }

        return "done";
    }
}
