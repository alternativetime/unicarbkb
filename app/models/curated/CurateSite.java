package models.curated;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 16/09/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="curate_site")
public class CurateSite extends Model {

    @Id
    public Long id;
    public String site;

    @ManyToOne
    public CurateProtein curateProtein;

    @OneToMany(cascade = CascadeType.ALL)
    public List<models.curated.CurateStructure> curateStructures;

    public static Finder<Long,CurateSite> find = new Finder<Long,CurateSite>(CurateSite.class);

    public static CurateSite createSite(Importeddataexcel importeddataexcel, CurateProtein protein) {

        CurateSite curateSitex = new CurateSite();

        System.out.println("UNIPROT: " + importeddataexcel.aminoAcidPosition + " id: " + importeddataexcel.id);
        CurateSite curateSite= CurateSite.find.query().where().ilike("site", importeddataexcel.aminoAcidPosition).
                eq("curate_protein_id", protein.id).findUnique();

        if(!importeddataexcel.aminoAcidPosition.isEmpty() ) {
            if (curateSite == null) {
                CurateSite curateSite1 = new CurateSite();
                curateSite1.site = importeddataexcel.aminoAcidPosition;
                curateSite1.curateProtein = protein;
                Ebean.save(curateSite1);

                CurateStructure.createStructures(importeddataexcel, curateSite1);

            } else {
                CurateStructure.createStructures(importeddataexcel, curateSite);
            }
            return curateSite;
        }
        return curateSitex;

    }
}
