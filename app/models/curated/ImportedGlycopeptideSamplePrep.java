package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_glycoprotein_sample_prep")
public class ImportedGlycopeptideSamplePrep {

    @Id
    public Long id;

   @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Glycoproteinsampleprep glycoproteinsampleprep;

    public static String createGlycopeptideSamplePrep(Importeddataexcel importeddataexcel, String preparation){


        if(preparation.contains(",")) {
            String[] preps = preparation.split(",");
            for(String prep : preps){
                prep = prep.trim();
                System.out.println("check line: " + prep);
                ImportedGlycopeptideSamplePrep importedGlycopeptideSamplePrep = new ImportedGlycopeptideSamplePrep();
                Glycoproteinsampleprep glycoproteinsampleprep = Glycoproteinsampleprep.find.query().where().ilike("description", prep.toUpperCase()).findUnique();

                if(glycoproteinsampleprep == null ) {
                    Glycoproteinsampleprep glycoproteinsampleprep1 = new Glycoproteinsampleprep();
                    glycoproteinsampleprep1.description = prep.toUpperCase();
                    Ebean.save(glycoproteinsampleprep1);

                    importedGlycopeptideSamplePrep.importeddataexcel = importeddataexcel;
                    importedGlycopeptideSamplePrep.glycoproteinsampleprep = glycoproteinsampleprep1;
                    Ebean.save(importedGlycopeptideSamplePrep);
                } else {
                    importedGlycopeptideSamplePrep.importeddataexcel = importeddataexcel;
                    importedGlycopeptideSamplePrep.glycoproteinsampleprep = glycoproteinsampleprep;
                    Ebean.save(importedGlycopeptideSamplePrep);
                }

            }

        } else {
            ImportedGlycopeptideSamplePrep importedGlycopeptideSamplePrep = new ImportedGlycopeptideSamplePrep();
            preparation = preparation.trim();
            Glycoproteinsampleprep glycoproteinsampleprep = Glycoproteinsampleprep.find.query().where().ilike("description", preparation.toUpperCase()).findUnique();

            if(glycoproteinsampleprep == null ) {
                Glycoproteinsampleprep glycoproteinsampleprep1 = new Glycoproteinsampleprep();
                glycoproteinsampleprep1.description = preparation;

                importedGlycopeptideSamplePrep.importeddataexcel = importeddataexcel;
                importedGlycopeptideSamplePrep.glycoproteinsampleprep = glycoproteinsampleprep1;
                Ebean.save(importedGlycopeptideSamplePrep);
            } else {
                importedGlycopeptideSamplePrep.importeddataexcel = importeddataexcel;
                importedGlycopeptideSamplePrep.glycoproteinsampleprep = glycoproteinsampleprep;
                Ebean.save(importedGlycopeptideSamplePrep);
            }
        }

        return "done";
    }
}
