package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_other_strategies_glycopeptide")
public class ImportedOtherStrategiesGlycoprotein {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public OtherStrategies otherstrategies;

    public static String createOtherGlycoproteins(Importeddataexcel importeddataexcel, String other){


        if(other.contains(",")) {
            String[] preps = other.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedOtherStrategiesGlycoprotein importedOtherStrategiesGlycoprotein = new ImportedOtherStrategiesGlycoprotein();
                OtherStrategies otherStrategies = OtherStrategies.find.query().where().ilike("description", prep).findUnique();

                if(otherStrategies == null ) {
                    OtherStrategies otherStrategies1 = new OtherStrategies();
                    otherStrategies1.description = prep;
                    Ebean.save(otherStrategies1);

                    importedOtherStrategiesGlycoprotein.importeddataexcel = importeddataexcel;
                    importedOtherStrategiesGlycoprotein.otherstrategies = otherStrategies1;
                    Ebean.save(importedOtherStrategiesGlycoprotein);
                } else {
                    importedOtherStrategiesGlycoprotein.importeddataexcel = importeddataexcel;
                    importedOtherStrategiesGlycoprotein.otherstrategies = otherStrategies;
                    Ebean.save(importedOtherStrategiesGlycoprotein);
                }

            }

        } else {
            ImportedOtherStrategiesGlycoprotein importedOtherStrategiesGlycoprotein = new ImportedOtherStrategiesGlycoprotein();
            other.trim();
            OtherStrategies otherStrategies = OtherStrategies.find.query().where().ilike("description", other).findUnique();

            if(otherStrategies == null ) {
                OtherStrategies otherStrategies1 = new OtherStrategies();
                otherStrategies1.description = other;
                Ebean.save(otherStrategies1);

                importedOtherStrategiesGlycoprotein.importeddataexcel = importeddataexcel;
                importedOtherStrategiesGlycoprotein.otherstrategies = otherStrategies1;
                Ebean.save(importedOtherStrategiesGlycoprotein);
            } else {
                importedOtherStrategiesGlycoprotein.importeddataexcel = importeddataexcel;
                importedOtherStrategiesGlycoprotein.otherstrategies = otherStrategies;
                Ebean.save(importedOtherStrategiesGlycoprotein);
            }
        }

        return "done";
    }
}
