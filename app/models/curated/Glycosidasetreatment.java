package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_glycosidasetreatment")
public class Glycosidasetreatment {

    @Id
    public Long id;

    public String description;
    public String name;

    @OneToMany
    public List<ImportedGlycosidaseTreatment> importedGlycosidaseTreatmentList;

    public static Finder<Long,Glycosidasetreatment> find = new Finder<Long,Glycosidasetreatment>(Glycosidasetreatment.class);
}
