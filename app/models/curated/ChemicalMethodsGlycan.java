package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_chemicalmethods")
public class ChemicalMethodsGlycan {

    @Id
    public Long id;

    public String description;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    @OneToMany
    public List<ImportedChemicalMethodsGlycan> importedChemicalMethodsGlycen;

    public static Finder<Long,ChemicalMethodsGlycan> find = new Finder<Long,ChemicalMethodsGlycan>(ChemicalMethodsGlycan.class);
}
