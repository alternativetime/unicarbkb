package models.curated;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_species")
public class Species {

    @Id
    public Long id;

    public String species;
    public String commonName;
    public String ncbiTaxonomyId;

    //@OneToMany
    //public List<ImportedDataCurated> importedDataCuratedList;

    public static Finder<Long,Species> find = new Finder<Long,Species>(Species.class);
}
