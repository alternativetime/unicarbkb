package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_ionisation")
public class ImportedIonisation {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Ionisation ionisation;

    public static String createChemicalMethods(Importeddataexcel importeddataexcel, String ion){


        if(ion.contains(",")) {
            String[] preps = ion.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedIonisation importedIonisation = new ImportedIonisation();
                Ionisation ionisation = Ionisation.find.query().where().ilike("description", prep).findUnique();

                if(ionisation == null ) {
                    Ionisation ionisation1 = new Ionisation();
                    ionisation1.description = prep;
                    Ebean.save(ionisation1);

                    importedIonisation.importeddataexcel = importeddataexcel;
                    importedIonisation.ionisation = ionisation1;
                    Ebean.save(importedIonisation);
                } else {
                    importedIonisation.importeddataexcel = importeddataexcel;
                    importedIonisation.ionisation = ionisation;
                    Ebean.save(importedIonisation);
                }

            }

        } else {
            ImportedIonisation importedIonisation = new ImportedIonisation();
            ion.trim();
            Ionisation ionisation = Ionisation.find.query().where().ilike("description", ion).findUnique();

            if(ionisation == null ) {
                Ionisation ionisation1 = new Ionisation();
                ionisation1.description = ion;
                Ebean.save(ionisation1);

                importedIonisation.importeddataexcel = importeddataexcel;
                importedIonisation.ionisation = ionisation1;
                Ebean.save(importedIonisation);
            } else {
                importedIonisation.importeddataexcel = importeddataexcel;
                importedIonisation.ionisation = ionisation;
                Ebean.save(importedIonisation);
            }

        }

        return "done";
    }


}
