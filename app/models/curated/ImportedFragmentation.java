package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_fragmentation")
public class ImportedFragmentation {

    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Fragmentation fragmentation;

    public static String createFragmentation(Importeddataexcel importeddataexcel, String fragmentation){


        if(fragmentation.contains(",")) {
            String[] preps = fragmentation.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedFragmentation importedFragmentation = new ImportedFragmentation();
                Fragmentation fragmentation1 = Fragmentation.find.query().where().ilike("description", prep).findUnique();

                if(fragmentation1 == null ) {

                    Fragmentation fragmentation2 = new Fragmentation();
                    fragmentation2.description = prep;
                    Ebean.save(fragmentation2);

                    importedFragmentation.importeddataexcel = importeddataexcel;
                    importedFragmentation.fragmentation = fragmentation2;
                    Ebean.save(importedFragmentation);
                } else {
                    importedFragmentation.importeddataexcel = importeddataexcel;
                    importedFragmentation.fragmentation = fragmentation1;
                    Ebean.save(importedFragmentation);
                }

            }

        } else {
            ImportedFragmentation importedFragmentation = new ImportedFragmentation();
            fragmentation.trim();
            Fragmentation fragmentation1 = Fragmentation.find.query().where().ilike("description", fragmentation).findUnique();

            if(fragmentation1 == null ) {

                Fragmentation fragmentation2 = new Fragmentation();
                fragmentation2.description = fragmentation;
                Ebean.save(fragmentation2);

                importedFragmentation.importeddataexcel = importeddataexcel;
                importedFragmentation.fragmentation = fragmentation2;
                Ebean.save(importedFragmentation);
            } else {
                importedFragmentation.importeddataexcel = importeddataexcel;
                importedFragmentation.fragmentation = fragmentation1;
                Ebean.save(importedFragmentation);
            }
        }

        return "done";
    }

}
