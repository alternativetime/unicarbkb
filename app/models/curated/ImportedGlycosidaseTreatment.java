package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_glycosidase_treatments_glycan")
public class ImportedGlycosidaseTreatment {

    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Glycosidasetreatment glycosidasetreatment;

    public static String createGlycosidaseTreatment(Importeddataexcel importeddataexcel, String exo){


        if(exo.contains(",")) {
            String[] preps = exo.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedGlycosidaseTreatment importedGlycosidaseTreatment = new ImportedGlycosidaseTreatment();
                Glycosidasetreatment glycosidasetreatment = Glycosidasetreatment.find.query().where().ilike("description", prep).findUnique();

                if(glycosidasetreatment == null ) {
                    Glycosidasetreatment glycosidasetreatment1 = new Glycosidasetreatment();
                    glycosidasetreatment1.description = prep.toLowerCase();
                    glycosidasetreatment1.name = prep.toLowerCase();
                    Ebean.save(glycosidasetreatment1);

                    importedGlycosidaseTreatment.importeddataexcel = importeddataexcel;
                    importedGlycosidaseTreatment.glycosidasetreatment = glycosidasetreatment1;
                    Ebean.save(importedGlycosidaseTreatment);
                } else {
                    importedGlycosidaseTreatment.importeddataexcel = importeddataexcel;
                    importedGlycosidaseTreatment.glycosidasetreatment = glycosidasetreatment;
                    Ebean.save(importedGlycosidaseTreatment);
                }

            }

        } else {
            ImportedGlycosidaseTreatment importedGlycosidaseTreatment = new ImportedGlycosidaseTreatment();
            exo.trim();
            Glycosidasetreatment glycosidasetreatment = Glycosidasetreatment.find.query().where().ilike("description", exo).findUnique();

            if(glycosidasetreatment == null ) {
                Glycosidasetreatment glycosidasetreatment1 = new Glycosidasetreatment();
                glycosidasetreatment1.description = exo.toLowerCase();
                Ebean.save(glycosidasetreatment1);

                importedGlycosidaseTreatment.importeddataexcel = importeddataexcel;
                importedGlycosidaseTreatment.glycosidasetreatment = glycosidasetreatment1;
                Ebean.save(importedGlycosidaseTreatment);
            } else {
                importedGlycosidaseTreatment.importeddataexcel = importeddataexcel;
                importedGlycosidaseTreatment.glycosidasetreatment = glycosidasetreatment;
                Ebean.save(importedGlycosidaseTreatment);
            }
        }

        return "done";
    }
}
