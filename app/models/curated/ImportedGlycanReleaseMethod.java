package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_glycan_release_method")
public class ImportedGlycanReleaseMethod {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Glycanreleasemethod glycanreleasemethod;

    public static String createGlycanRelease(Importeddataexcel importeddataexcel, String release){


        if(release.contains(",")) {
            String[] preps = release.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedGlycanReleaseMethod importedGlycanReleaseMethod = new ImportedGlycanReleaseMethod();
                Glycanreleasemethod glycanreleasemethod = Glycanreleasemethod.find.query().where().ilike("description", prep).findUnique();

                if(glycanreleasemethod == null ){
                    Glycanreleasemethod glycanreleasemethod1 = new Glycanreleasemethod();
                    glycanreleasemethod1.description = prep.toLowerCase();
                    Ebean.save(glycanreleasemethod1);

                    importedGlycanReleaseMethod.importeddataexcel = importeddataexcel;
                    importedGlycanReleaseMethod.glycanreleasemethod = glycanreleasemethod1;
                    Ebean.save(importedGlycanReleaseMethod);

                } else {
                    importedGlycanReleaseMethod.importeddataexcel = importeddataexcel;
                    importedGlycanReleaseMethod.glycanreleasemethod = glycanreleasemethod;
                    Ebean.save(importedGlycanReleaseMethod);
                }

            }

        } else {
            ImportedGlycanReleaseMethod importedGlycanReleaseMethod = new ImportedGlycanReleaseMethod();
            release.trim();
            Glycanreleasemethod glycanreleasemethod = Glycanreleasemethod.find.query().where().ilike("description", release).findUnique();

            if(glycanreleasemethod == null ){
                Glycanreleasemethod glycanreleasemethod1 = new Glycanreleasemethod();
                glycanreleasemethod1.description = release.toLowerCase();
                Ebean.save(glycanreleasemethod1);

                importedGlycanReleaseMethod.importeddataexcel = importeddataexcel;
                importedGlycanReleaseMethod.glycanreleasemethod = glycanreleasemethod1;
                Ebean.save(importedGlycanReleaseMethod);

            } else {
                importedGlycanReleaseMethod.importeddataexcel = importeddataexcel;
                importedGlycanReleaseMethod.glycanreleasemethod = glycanreleasemethod;
                Ebean.save(importedGlycanReleaseMethod);
            }
        }

        return "done";
    }
}
