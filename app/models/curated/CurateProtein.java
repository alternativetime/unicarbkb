package models.curated;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 16/09/16.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="curate_protein")
public class CurateProtein extends Model {

    @Id
    public Long id;

    public String uniprot;
    public String description;

    @OneToMany
    public List<CurateSite> curateSiteList;

    @ManyToOne
    public Publication publication;

    public static Finder<Long,CurateProtein> find = new Finder<Long,CurateProtein>(CurateProtein.class);

    public static CurateProtein createProtein(Importeddataexcel importeddataexcel) {

        CurateProtein curateProteinx = new CurateProtein();

        System.out.println("UNIPROT: " + importeddataexcel.uniprot + " id: " + importeddataexcel.id);

        if(!importeddataexcel.uniprot.isEmpty() || importeddataexcel.uniprot != null ) {
            CurateProtein curateProtein = CurateProtein.find.query().where().ilike("uniprot", importeddataexcel.uniprot).eq("publication_id", importeddataexcel.publication.id).findUnique();

            if (curateProtein == null) {
                CurateProtein curateProtein1 = new CurateProtein();
                curateProtein1.uniprot = importeddataexcel.uniprot;
                curateProtein1.publication = importeddataexcel.publication;
                curateProtein1.description = importeddataexcel.proteinDescription;
                Ebean.save(curateProtein1);

                CurateSite.createSite(importeddataexcel, curateProtein1);

            } else {
                CurateSite.createSite(importeddataexcel, curateProtein);
            }
            return curateProtein;
        }
        return curateProteinx;
    }
}
