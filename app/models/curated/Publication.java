package models.curated;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.PagedList;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.util.List;

/**
 * Created by matthew on 3/08/16.
 */

@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_publication")
public class Publication {

    @Id
    public Long id;

    public Integer pmid;
    public String firstAuthor;
    public Integer year;
    public String journal;
    public String volume;
    public String pages;
    public String authors;
    public String title;
    public String doi;

    public String experimentDetails;
    public String experimentInstrumentConditions;

    @OneToMany
    public List<Importeddataexcel> importeddataexcels;

    @OneToMany(cascade = CascadeType.ALL)
    public List<CurateProtein> curateProteins;

    public static Finder<Long,Publication> find = new Finder<Long,Publication>(Publication.class);

    public void setPmid(Integer pmid) {
        this.pmid = pmid;
    }

    public void setFirstAuthor(String firstAuthor) {
        this.firstAuthor = firstAuthor;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public void setExperimentDetails(String experimentDetails) {
        this.experimentDetails = experimentDetails;
    }

    public void setExperimentInstrumentConditions(String experimentInstrumentConditions) {
        this.experimentInstrumentConditions = experimentInstrumentConditions;
    }

    public void setImporteddataexcels(List<Importeddataexcel> importeddataexcels) {
        this.importeddataexcels = importeddataexcels;
    }

    public static Publication createPublication(CSVRecord record){

        System.out.println("YEAR: " + record.get("year"));

        Publication publication = new Publication();
        publication.authors = record.get("authors");
        //publication.setAuthors = record.get("authors");
        publication.firstAuthor = record.get("firstAuthor");
        publication.year = Integer.valueOf(record.get("year"));
        publication.volume = record.get("volume");
        publication.pages = record.get("pages");
        publication.title = record.get("title");
        publication.pmid = Integer.valueOf(record.get("pmid"));
        publication.journal = record.get("journal");
        publication.doi = "tbc";

        publication.experimentDetails = "tbc";
        publication.experimentInstrumentConditions = "tbc";

        Ebean.save(publication);
        return publication;
    }

    public static PagedList<Publication> page(int page, int pageSize, String sortBy, String order, String filter) {
        PagedList<Publication> pagedList = Publication.find.query().where().disjunction()
                .ilike("title", "%" + filter + "%")
                .ilike("authors", "%" + filter + "%")
                .endJunction()
                .orderBy(sortBy + " " + order)
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
