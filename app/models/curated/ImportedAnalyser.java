package models.curated;

import io.ebean.Ebean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by matthew on 3/08/2016.
 */
@SuppressWarnings("serial")
@Entity
@Table(schema="curated", name="datacollection_importeddataexcel_analyser")
public class ImportedAnalyser {
    @Id
    public Long id;

    @ManyToOne
    public Importeddataexcel importeddataexcel;

    @ManyToOne
    public Analyser analyser;

    public static String createAnalayser(Importeddataexcel importeddataexcel, String analyser){


        if(analyser.contains(",")) {
            String[] preps = analyser.split(",");
            for(String prep : preps){
                prep.trim();
                ImportedAnalyser importedAnalyser = new ImportedAnalyser();
                Analyser analyser1 = Analyser.find.query().where().ilike("description", prep).findUnique();

                if(analyser1 == null ) {
                    Analyser analyser2 = new Analyser();
                    analyser2.description = prep;
                    Ebean.save(analyser2);

                    importedAnalyser.importeddataexcel = importeddataexcel;
                    importedAnalyser.analyser = analyser2;
                    Ebean.save(importedAnalyser);
                } else {
                    importedAnalyser.importeddataexcel = importeddataexcel;
                    importedAnalyser.analyser = analyser1;
                    Ebean.save(importedAnalyser);
                }

            }

        } else {
            ImportedAnalyser importedAnalyser = new ImportedAnalyser();
            analyser.trim();
            Analyser analyser1 = Analyser.find.query().where().ilike("description", analyser).findUnique();

            if(analyser1 == null ) {
                Analyser analyser2 = new Analyser();
                analyser2.description = analyser;
                Ebean.save(analyser2);

                importedAnalyser.importeddataexcel = importeddataexcel;
                importedAnalyser.analyser = analyser2;
                Ebean.save(importedAnalyser);
            } else {
                importedAnalyser.importeddataexcel = importeddataexcel;
                importedAnalyser.analyser = analyser1;
                Ebean.save(importedAnalyser);
            }
        }

        return "done";
    }

}
