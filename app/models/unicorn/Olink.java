package models.unicorn;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//import io.ebean.*;

/**
 * Created by matthew on 19/06/15.
 */

@SuppressWarnings("serial")
@Entity
//@EntityConcurrencyMode(ConcurrencyMode.NONE)
@Table(schema="unicorn", name="olink")
public class Olink {

    @Id
    public Long id;
    public String iupac;
    public String glycoct;
    public Integer mapkey ;

    public static Finder<Long,Olink> find = new Finder<Long,Olink>( Olink.class);
}
