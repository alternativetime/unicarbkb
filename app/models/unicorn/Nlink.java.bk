package models.unicorn;


import io.ebean.*;
import io.ebean.*;
import io.ebean.*;
import io.ebean.*;
import io.ebean.*;
import com.hp.hpl.jena.query.*;
import org.apache.commons.lang.StringUtils;
import org.expasy.glycanrdf.datastructure.Glycan;
import org.expasy.glycanrdf.io.glycoCT.GlycoCTReader;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import play.Logger;
import io.ebean.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
@Table(schema="unicorn", name="nlink")
public class Nlink extends Model {

    public static Finder<Long,Nlink> find = new Finder<Long,Nlink>( Nlink.class);
    @Id
    public Long id;
    public String iupac;
    public String glycoct;
    public Integer mapkey ;
    public Integer gal;
    public Integer fuc;
    public Integer glcnac;
    public Integer galnac;
    public Integer kdn;
    public Integer man;
    public Double calcmass;
    public Double averageMass;
    public Double monoMass;

    public static Boolean countMonoNlink() {
        final double galmass = 162.0528;
        final double fucmass = 146.1430;
        final double glcmass = 162.142;
        final double glcnacmass = 203.1950;
        final double galnacmass = 203.1950;
        final double kdnmass = 291.2579;
        final double manmass = 162.142;

        final double mgalmass = 162.0528;
        final double mfucmass = 146.0579;
        final double mglcmass = 162.0528;
        final double mglcnacmass = 203.0794;
        final double mgalnacmass = 203.0794;
        final double mkdnmass = 291.0954;
        final double mmanmass = 162.0528;

        List<Nlink> nlinks = Nlink.find.all();
        for(Nlink n : nlinks) {

            Nlink nupdate = Nlink.find.byId(n.id);

            String str = StringUtils.lowerCase(nupdate.iupac);
            int cman = 0;
            int cgal = 0;
            int cfuc = 0;
            int cglcnac = 0;
            int ckdn = 0;
            int cgalnac = 0;

            cgalnac = StringUtils.countMatches(str, "galnac");
            str = str.replaceAll("galnac", "xxx");
            cman = StringUtils.countMatches(str, "man");
            cgal = StringUtils.countMatches(str, "gal");
            cfuc = StringUtils.countMatches(str, "fuc");
            cglcnac = StringUtils.countMatches(str, "glcnac");
            ckdn = StringUtils.countMatches(str, "neuac");

            n.man = cman;
            n.fuc = cfuc;
            n.glcnac = cglcnac;
            n.kdn = ckdn;
            n.gal = cgal;
            n.galnac = cgalnac;

            double naveragemass = (cgalnac * galnacmass) + (cman * manmass) + (cfuc * fucmass) + (cglcnac * glcnacmass) + ( ckdn * kdnmass) + ( cgal * galmass);
            n.averageMass = (naveragemass+18.01528);



            double nmonoemass = (cgalnac * mgalnacmass) + (cman * mmanmass) + (cfuc * mfucmass) + (cglcnac * mglcnacmass) + ( ckdn * mkdnmass) + ( cgal * mgalmass);
            n.monoMass = (nmonoemass+18.0105546);

            n.update();
            n.save();

        }
        return true;
    }

    public  static List<Nlink> getCompositionMatch(int gal, int fuc, int glcnac, int galnac, int kdn, int man) {

        Logger.info("TEST COMPOSITION SEARCH");
        List<Nlink> nlinks = Ebean.find(Nlink.class).where().conjunction()
                .add(Expr.le("gal", gal)).add(Expr.ge("gal", gal))
                .add(Expr.le("fuc", fuc)).add(Expr.ge("fuc", fuc))
                .add(Expr.le("man", man)).add(Expr.ge("glc", man))
                .add(Expr.le("glcnac", glcnac)).add(Expr.ge("glcnac", glcnac))
                .add(Expr.le("galnac", galnac)).add(Expr.ge("galnac", galnac))
                .add(Expr.le("kdn", kdn)).add(Expr.ge("kdn", kdn))
                .findList();
        return nlinks;
    }

    public static Junction makeQueryComp(int comp1, int comp2, String selection, String field){

        Junction<Nlink> junction;
        junction = Ebean.find(Nlink.class).where().conjunction();


        switch (selection) {
            case "gt":
                junction.add(Expr.gt(field, comp1));
                break;
            case "lt":
                junction.add(Expr.lt(field, comp1));
                break;
            default:
                if (comp1>0 && comp2>0)
                    junction.add(Expr.between(field, comp1, comp2));
                else
                    junction.add(Expr.eq(field, comp1));
                break;
        }

        return junction;
    }

    public static List<Nlink> getCompositionMatchUnicorn(int count, int gal, int fuc, int man, int glcnac, int galnac, int kdn, int gal2, int fuc2, int man2, int glcnac2, int galnac2, int kdn2, String gal3, String fuc3, String man3, String glcnac3, String galnac3, String kdn3) {

        Junction<Nlink> hmg;
        hmg = Ebean.find(Nlink.class).where().conjunction();
        if (gal > 0) {
            hmg.add(makeQueryComp(gal, gal2, gal3, "gal"));
        }
        if (fuc > 0) {
            hmg.add(makeQueryComp(fuc, fuc2, fuc3, "fuc"));
        }
        if (glcnac > 0) {
            hmg.add(makeQueryComp(glcnac, glcnac2, glcnac3, "glcnac"));
        }
        if (galnac > 0) {
            hmg.add(makeQueryComp(galnac, galnac2, galnac3, "galnac"));
        }
        if (kdn > 0) {
            hmg.add(makeQueryComp(kdn, kdn2, kdn3, "kdn"));
        }
        if (man > 0) {
            hmg.add(makeQueryComp(man, man2, man3, "man"));
        }
        Logger.info("check composition query");

        if(count > 100) {
            List<Nlink> glycanCompositions = Ebean.find(Nlink.class).where().conjunction()
                    .add(hmg)
                    .setMaxRows(100)
                    .findList();
            return glycanCompositions;
        }
        else {
            List<Nlink> glycanCompositions = Ebean.find(Nlink.class).where().conjunction()
                    .add(hmg)
                    .findList();
            return glycanCompositions;
        }
    }


    public static int getCompositionUnicornMatchCount(int gal, int fuc, int man, int glcnac, int galnac, int kdn, int gal2, int fuc2, int man2, int glcnac2, int galnac2, int kdn2, String gal3, String fuc3, String man3, String glcnac3, String galnac3, String kdn3) {

        Junction<Nlink> hmg;
        hmg = Ebean.find(Nlink.class).where().conjunction();
        if (gal > 0) {
            hmg.add(makeQueryComp(gal, gal2, gal3, "gal"));
        }
        if (fuc > 0) {
            hmg.add(makeQueryComp(fuc, fuc2, fuc3, "fuc"));
        }
        if (glcnac > 0) {
            hmg.add(makeQueryComp(glcnac, glcnac2, glcnac3, "glcnac"));
        }
        if (galnac > 0) {
            hmg.add(makeQueryComp(galnac, galnac2, galnac3, "galnac"));
        }
        if (kdn > 0) {
            hmg.add(makeQueryComp(kdn, kdn2, kdn3, "kdn"));
        }
        if (man > 0) {
            hmg.add(makeQueryComp(man, man2, man3, "man"));
        }
        Logger.info("check composition query");

        int count = Ebean.find(Nlink.class).where().conjunction()
                .add(hmg)
                .findCount()();

        return count;
    }

    public static int iupacUnicornSearchCount(String iupac) {
        int countIupac = Ebean.find(Nlink.class).where().conjunction().add(Expr.contains("iupac", iupac)).findCount()();
        return countIupac;
    }

    public static List<Nlink> iupacUnicornSearch(String iupac, int count) {
        List<Nlink> iupacPartial = null;

        if(count > 100) {
            iupacPartial = Ebean.find(Nlink.class).where().conjunction().add(Expr.contains("iupac", iupac)).setMaxRows(100).findList();
        }else{
            iupacPartial = Ebean.find(Nlink.class).where().conjunction().add(Expr.contains("iupac", iupac)).findList();
        }


        return iupacPartial;
    }

    public static List<Nlink> massUnicornSearch(Double mass){
        return Nlink.find.query().where().between("averageMass", mass - 0.5, mass + 0.5).orderBy("averageMass").findList();
    }




    public static List<String> searchNRDF(Long id) {

        Nlink nlink = Nlink.find.byId(id);
        String glycoct = nlink.glycoct;


        GlycoCTReader reader = new GlycoCTReader();
        Glycan glycanCT = reader.read(glycoct, "1");

        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String q = queryGenerator.generateQueryString(glycanCT);
        q = q.replaceAll("mzjava.expasy.org", "rdf.unicarbkb.org");

        System.out.println("query: " + q);
        String b = " LIMIT 20";
        String a = new StringBuilder()
                .append(q)
                .append(b)
                .toString();

        Query query = QueryFactory.create(b);
        //ARQ.getContext().setTrue(ARQ.useSAX);
        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://103.29.112.169:443/unicorn/query", query); //check IP

        //          Long time = qexec.getTimeout1();

        com.hp.hpl.jena.query.ResultSet results = qexec.execSelect();
        //List<String> results2 = qexec.execSelect().getResultVars();
        List<String> hits = new ArrayList<>();
        int count = 0;
        while (results.hasNext()) {
            QuerySolution soln = results.nextSolution();
            System.out.println(soln);
            count++;
            String hitsFound = soln.toString();
            hits.add(hitsFound.replaceAll("<http://rdf.unicarbkb.org/structureConnection/", ""));
        }
        qexec.close();
        return hits;

    }
}


