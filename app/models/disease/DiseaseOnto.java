package models.disease;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import models.Strproteintaxdisease;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by matthew on 19/01/2016.
 */
@Entity
@Table(schema="disease", name="disease_onto")
public class DiseaseOnto extends Model {

    @Id
    public Long id;
    public String diseaseName;
    public int doid;
    public int doidSecond;
    public String meshId;

    @OneToMany
    public List<Strproteintaxdisease> strproteintaxdiseaseList;

    public static Finder<Long,DiseaseOnto> find = new Finder<Long,DiseaseOnto>( DiseaseOnto.class);

    public static PagedList<DiseaseOnto> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<DiseaseOnto> pagedList = DiseaseOnto.find.query().where()
                .ilike("diseaseName", "%" + filter + "%")
                .orderBy("diseaseName").setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;

    }

    public static List<DiseaseOnto> getDiseaseInfo(int doid){
        return DiseaseOnto.find.query().fetch("strproteintaxdiseaseList").where().disjunction().eq("doid", doid).eq("doidSecond", doid).findList();

    }

}
