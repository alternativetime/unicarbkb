package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Streference entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="streference")
public class Streference extends Model {

    @Id
    public Long id;
    @ManyToOne
    public Structure  structure;
    @ManyToOne
    public Reference reference;
      

    public Streference(Reference reference, Structure structure) {
	this.structure=structure;
	this.reference=reference;
    }   
    
    /**
     * Generic query helper for entity with id Long
     */
    public static Finder<Long,Streference> find = new Finder<Long,Streference>( Streference.class);
    


}

