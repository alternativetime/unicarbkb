package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Streference entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="strproteintaxbiolsource")
public class Strproteintaxbiolsource extends Model {

    @Id
    public Long id;

    @ManyToOne
    public Structure structure;
    @ManyToOne
    public Proteins proteins;
    @ManyToOne
    public Taxonomy taxonomy;
    @ManyToOne
    public Sourceref sourceref; 
    @ManyToOne
    public Reference reference;
      
    
    /**
     * Generic query helper for entity with id Long
     */
    public static Finder<Long,Strproteintaxbiolsource> find = new Finder<Long,Strproteintaxbiolsource>( Strproteintaxbiolsource.class);
    


}

