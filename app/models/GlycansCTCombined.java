package models;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matthew on 18/06/15.
 */
@Entity
@Table(schema="public", name="glycans_ct_combine")
public class GlycansCTCombined extends Model {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "glycansct_gen")
    @SequenceGenerator(name = "glycansct_gen", sequenceName = "public.glycans_ct2_id_seq", allocationSize=1)
    public Long id;

    @Lob
    public String ct;

    @Lob
    public String ctagain;

    public String database;
    public Long databaseId;

    public static Finder<Long,GlycansCTCombined> find = new Finder<Long,GlycansCTCombined>( GlycansCTCombined.class);

    public static List<GlycansCTCombined> matchCT(String ct){

        List<GlycansCTCombined> glycansCTCombineded =  Ebean.find(GlycansCTCombined.class).where().eq("ctagain", ct).eq("database", "kb").findList();
        List<GlycansCTCombined> glycansCTCombinededHMG =  Ebean.find(GlycansCTCombined.class).where().eq("ctagain", ct.trim()).eq("database", "hmg").findList();
        List<GlycansCTCombined> glycansCTCombinededTCD =  Ebean.find(GlycansCTCombined.class).where().eq("ctagain", ct.trim()).eq("database", "tcd").findList();

        List<GlycansCTCombined> bothDbs = new ArrayList<>();
        if(glycansCTCombineded!=null){bothDbs.addAll(glycansCTCombineded);}
        if(glycansCTCombinededHMG!=null){bothDbs.addAll(glycansCTCombinededHMG);}
        if(glycansCTCombinededTCD!=null){bothDbs.addAll(glycansCTCombinededTCD);}
        //String sql = "select t0.id as c0, t0.ct as c1, t0.database as c2, t0.database_id as c3 from public.glycans_ct_combine t0 where t0.ct like :status";

        //SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
       // sqlQuery.setParameter("name", "Acme%");
        //sqlQuery.setParameter("status", ct);

        // execute the query returning a List of MapBean objects
        //List<SqlRow> list = sqlQuery.findList();


        //Logger.info("size check: " + bothDbs.size());
        return  bothDbs;
    }
}
