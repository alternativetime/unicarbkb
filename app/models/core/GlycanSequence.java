/*
 * Copyright (c) 2014. Copyright (c) 2014. Matthew Campbell <matthew.campbell@mq.edu.au>
 *
 * The program can not be distributed to others with out the consent of the copyright holder, until such a time that the copyright holder has released the code for public use.
 */

package models.core;

import io.ebean.Finder;
import io.ebean.Model;
import models.composition_protein.Crcdatajenny;
import play.data.format.Formats;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(schema = "core", name = "glycan_sequence")
public class GlycanSequence extends Model {

    @Id
    @Column(name="glycan_sequence_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "glycan_sequence_gen")
    @SequenceGenerator(name = "glycan_sequence_gen", sequenceName = "core.glycan_sequence_glycan_sequence_id_seq", allocationSize=1)
    public Long glycanSequenceId;



    /**
     * Numeric count of the number of Residues in this glycan sequence.
     */
    private int residueCount;

    /**
     * Monoisotopic mass
     */
    private BigDecimal massMonoisotopic;

    /**
     * Average mass
     */
    private BigDecimal massAverage;

    /**
     * The date/time on which this sequence was entered into Eurocarb.
     */
    @Formats.DateTime(pattern="dd/MM/yyyy")
    private Date dateEntered = new Date();

    /**
     * Unsure - needs clarification?
     */
    @Formats.DateTime(pattern="dd/MM/yyyy")
    private Date dateContributed = new Date() ;



    /** Contains the actual sequence of the glycan represented by this {@link GlycanSequence} */
    // private SugarSequence sequence;

    /**
     * sequence in iupac format.  @see SequenceFormat#Iupac
     */
    private String sequenceIupac;

    /**
     * sequence in glycoct format.  @see SequenceFormat#Glycoct
     */
    public String sequenceCt;

    /**
     * sequence in stalliano (GWS) format.  @see SequenceFormat#GWS
     */
    public String sequenceGWS;

    /**
     * sequence in glycam format
     */
    private String sequenceGlycam;


    public String glytoucanId;

    @OneToMany
    public List<Crcdatajenny> crcdatajennies;


    public static Finder<Long,GlycanSequence> find = new Finder<Long,GlycanSequence>( GlycanSequence.class);

}