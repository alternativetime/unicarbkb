package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="disease", schema="core")
public class Disease extends Model {

    @Id
    public Long id;
    
    public String disease_name;
    public String mesh_id;
    public String description;
   
    public static Finder<Long,Disease> find = new Finder<Long,Disease>( Disease.class);
    
}

