package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity 
@Table(name="tissue_taxonomy", schema="core")
public class TissueTaxonomy extends Model {

    @Id
    public Long id;
    
    public String tissue_taxon;
    public String mesh_id;
    public String description;
   
    public static Finder<Long,TissueTaxonomy> find = new Finder<Long,TissueTaxonomy>( TissueTaxonomy.class);
    
}

