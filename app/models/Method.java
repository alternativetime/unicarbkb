package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Method entity managed by Ebean
 */
@Entity 
@Table(schema="public", name="method")
public class Method extends Model {

    @Id
    public Long id;
    public String description;
    public Long confidence;
    
    public static Finder<Long,Method> find = new Finder<Long,Method>( Method.class);
    
    public static List<Method> findmethod(Long id) {
		return find
		   .query().where()
			.eq("id", id)
		   .findList();
	   }
    
    
}
