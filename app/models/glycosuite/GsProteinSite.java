package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


/**
 * StructureToSites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="gs_protein_site", schema="glycosuite")
public class GsProteinSite extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid_position;
    //public String note;
    //public int structure_id;
    //public String glycan_type;
    //public int source_id; //make this a relationship

    @OneToMany
    public List<GsProteinSiteStructure> gsProteinSite;

    
    public static Finder<Long,GsProteinSite> find = new Finder<Long,GsProteinSite>( GsProteinSite.class);


    public static List<GsProteinSite> ProteinRetrieval(String accession) {
	return
	  find.query().where().ilike("swiss_prot", accession).findList();

    }

    public static String ProteinRetrievalName(String accession) {
	List<GsProteinSite> gsProteinSite = GsProteinSite.ProteinRetrieval(accession);
	String proteinName = "";
	for(GsProteinSite g : gsProteinSite) {
		proteinName = g.protein_name;
	}
    return proteinName;
    }


}
