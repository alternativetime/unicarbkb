package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;


/**
 * StructureToSiteDefined entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="structure_to_site_defined", schema="glycosuite")
public class StructureToSiteDefined extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid_position;
    //public String note;
    public int structure_id;
    //public String glycan_type;
    //public int source_id; //make this a relationship

    @ManyToOne
    public DefinedSites definedSites; 

    
    public static Finder<Long,StructureToSiteDefined> find = new Finder<Long,StructureToSiteDefined>( StructureToSiteDefined.class);

    public static List<StructureToSiteDefined> findStructuresDefined(String protein, String site) {
        return
           find.query().where().disjunction()
		.ilike("swiss_prot", protein)
		.ilike("protein_name", protein)
		.endJunction()
		.ilike("amino_acid_position", "%" + site + "%")
		.findList();
   }

}
