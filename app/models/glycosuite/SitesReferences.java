package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;


/**
 * Sites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="sites_references", schema="glycosuite")
public class SitesReferences extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String glyco_aa;
    public String glyco_aa_site;

    
    public static Finder<Long,SitesReferences> find = new Finder<Long,SitesReferences>( SitesReferences.class);

    public static List<SitesReferences> findSites(String protein) {
        return
           find.query().where().ilike("swiss_prot", "%" + protein + "%").findList();
    }
}
