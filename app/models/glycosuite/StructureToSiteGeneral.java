package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * StructureToSiteDefined entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="structure_to_site_general", schema="glycosuite")
public class StructureToSiteGeneral extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    //public String amino_acid_position;
    //public String glyco_aa;
    //public String glyco_aa_site;
    //public String note;
    public int structure_id;
    //public String glycan_type;
    //public int source_id; //make this a relationship

    @ManyToOne
    public GeneralSites generalSites; 

    
    public static Finder<Long,StructureToSiteGeneral> find = new Finder<Long,StructureToSiteGeneral>( StructureToSiteGeneral.class);

}
