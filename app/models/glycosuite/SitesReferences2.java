package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;


/**
 * Sites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="sites_references2", schema="glycosuite")
public class SitesReferences2 extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String description;

    public static Finder<Long,SitesReferences2> find = new Finder<Long,SitesReferences2>( SitesReferences2.class);

    public static List<SitesReferences2> findSitesReferences(String protein) {
        return
           find.query().where().ilike("swiss_prot", protein).findList();
    }
}
