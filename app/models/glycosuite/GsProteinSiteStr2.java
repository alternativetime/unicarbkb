package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;


/**
 * StructureToSites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="gs_protein_site_str2", schema="glycosuite")
public class GsProteinSiteStr2 extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid;
    public String glyco_aa_site;
    //public String note;
    //public int structure_id;
    //public String glycan_type;
    //public int source_id; //make this a relationship

	@ManyToOne
	public GsProteinStr2 gsProteinStr2;
	
	@OneToMany
	public List<GsProteinSiteStructureAssociation> gsProteinSiteStructureAssociation;
    
    public static Finder<Long,GsProteinSiteStr2> find = new Finder<Long,GsProteinSiteStr2>( GsProteinSiteStr2.class);


    


}
