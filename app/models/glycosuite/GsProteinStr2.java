package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


/**
 * StructureToSites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="gs_protein_str2", schema="glycosuite")
public class GsProteinStr2 extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid;
    public String glyco_aa_site;
    //public String note;
    public int structure_id;
    //public String glycan_type;
    public int source_id; //make this a relationship

    @OneToMany
    public List<GsProteinSiteStr2> gsProteinSiteStr2;

    
    public static Finder<Long,GsProteinStr2> find = new Finder<Long,GsProteinStr2>( GsProteinStr2.class);


    public static List<GsProteinStr2> ProteinRetrieval(String accession) {
	return
	  find.query().where().ilike("swiss_prot", "%" + accession + "%").findList();

    }


}
