package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;


/**
 * Sites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="defined_sites", schema="glycosuite")
public class DefinedSites extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid_position;

    @OneToMany
    public List<StructureToSiteDefined> strSiteDefined;

    @ManyToOne
    public Proteins proteins; 

    
    public static Finder<Long,DefinedSites> find = new Finder<Long,DefinedSites>( DefinedSites.class);

    public static List<DefinedSites> findProteinsDefined(String protein) {
        return
           find.query().where().ilike("swiss_prot", "%" + protein + "%").findList();
    }
    
    public static List<DefinedSites> findProteinsDefinedName(String protein) {
        return
           find.query().where().ilike("protein_name", protein).findList();
    }


}
