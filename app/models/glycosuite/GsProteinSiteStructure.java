package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * StructureToSites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="gs_protein_site_structure", schema="glycosuite")
public class GsProteinSiteStructure extends Model {

    @Id
    public Long id;
    public String swiss_prot;
    public String amino_acid_position;
    //public String note;
    public int structure_id;
    public String glycan_type;
    //public int source_id; //make this a relationship

    @ManyToOne
    public GsProteinSite gsProteinSite;
    
    public static Finder<Long,GsProteinSiteStructure> find = new Finder<Long,GsProteinSiteStructure>( GsProteinSiteStructure.class);

}
