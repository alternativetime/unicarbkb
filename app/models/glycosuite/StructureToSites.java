package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;


/**
 * StructureToSites entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(name="structure_to_site", schema="glycosuite")
public class StructureToSites extends Model {

    @Id
    public Long id;
    public String protein_name;
    public String swiss_prot;
    public String amino_acid_position;
    public String note;
    public int structure_id;
    public String glycan_type;
    public int source_id; //make this a relationship

    @ManyToOne
    public Proteins proteins;

    
    public static Finder<Long,StructureToSites> find = new Finder<Long,StructureToSites>( StructureToSites.class);


    public static List<StructureToSites> ProteinRetrieval(String accession) {
	return
	  find.query().where().ilike("swiss_prot", accession).findList();

    }


}
