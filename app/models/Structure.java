package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Finder;
import io.ebean.Model;
import models.composition_protein.Crcdata;
import models.ionmob.GlycanMob;
import models.rdf.UniCarbkbEpitopes;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="public", name="structure")
public class Structure extends Model {

	@Id
	public Long id;

	public String compositionId;
	public String glycanst;
	public String aminolink;
	public String core;
	public String type;
	public String antigenic;
	public String lectin;
	public String endoglycosidase;
	public String link;	 

	@OneToMany
	@JsonIgnore
	public List<UniCarbkbEpitopes> uniCarbkbEpitopes;

	@OneToMany
    @JsonIgnore
	public List<Streference> references;

	@OneToMany
    @JsonIgnore
	public List<Stproteins> stproteins; 

	@OneToMany
    @JsonIgnore
	public List<Strtaxonomy> strtaxonomy;

	@OneToMany
    @JsonIgnore
	public List<Stsource> stsource;

	@OneToMany
    @JsonIgnore
	public List<Pubchem> pubchem;

	@OneToMany
    @JsonIgnore
	public List<Composition> strcomposition;

	@OneToMany
    @JsonIgnore
	public List<Structurecomp> structurecomp;

	@OneToMany
    @JsonIgnore
	public List<Images> images;

	@OneToMany
    @JsonIgnore
	public List<Translation> translation;

	@OneToMany
    @JsonIgnore
	public List<Strproteintax> strproteintax;

	@OneToMany
    @JsonIgnore
	public List<Strproteintaxbiolsource> strproteintaxbiolsource;

	@OneToMany
	@JsonIgnore
	public List<Crcdata> crcdata;

	@OneToMany
	@JsonIgnore
	public List<GlycanMob> glycanMobs;

	public Structure(String glycanst, String aminolink, String core, String type, String antigenic, String lectin, String endoglycosidase, String link, String compositionId) {
		this.compositionId = compositionId;
		this.glycanst = glycanst;
		this.aminolink = aminolink;
		this.core = core;
		this.type = type;
		this.antigenic = antigenic;
		this.lectin = lectin;
		this.endoglycosidase = endoglycosidase;
		this.link = link; 
	}

	public static List<Structure> findStructureRef(Long id) {
		return find.query().fetch("references").fetch("references.reference").fetch("stproteins").fetch("stproteins.proteins").fetch("strtaxonomy").fetch("stsource")
				.where()
				.eq("structure_id", id)
				.orderBy("references.reference.year desc")
				.findList();
	}

	/**
	 * Generic query helper for entity Reference with id Long
	 */
	public static Finder<Long,Structure> find = new Finder<Long,Structure>( Structure.class);

	public static String buildComposition(String[] input) {
		String build = "";
		for(String composition : input) {
			if (composition.length() < 1 ) {
				build += "0" ;
			}
			build += composition;	
		}

		return build;   
	}

	public static List<Structure> findComposition(String composition) {
		//return find.query().where().ilike("composition_id", "%" + composition).findList();
		return find.query().where().ilike("composition_id",  composition).findList();
	}


	public static String getJSON(Long id) throws IOException {

		Structure structure = Structure.find.byId(id);
		String iupac = structure.glycanst;
		List<String> components = new ArrayList<String>();
		ArrayList<Object[]> ob = new ArrayList<>();
		String html = "";
		
		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader("/tmp/json/" + iupac + ".json"));

			JSONObject jsonObject = (JSONObject) obj;

			String name = (String) jsonObject.get("str");

			JSONArray msg = (JSONArray) jsonObject.get("parts");

			for(Object c : msg){

				JSONParser parserString = new JSONParser();
				ContainerFactory containerFactory = new ContainerFactory(){
					public List creatArrayContainer() {
						return new LinkedList();
					}

					public Map createObjectContainer() {
						return new LinkedHashMap();
					}

				};
				
				try{
				    Map json = (Map)parser.parse(c.toString(), 	containerFactory);
				    Iterator iter = json.entrySet().iterator();
				    System.out.println("==iterate result==");
				  
				   
				    while(iter.hasNext()){
				     
				      Map.Entry entry = (Map.Entry)iter.next();
				      System.out.println(entry.getKey() + "=>" + entry.getValue());
				     
				      if(entry.getKey().equals("enz")){
				    	  String searchEnz = entry.getValue().toString();
				    	  long lEnz = Long.parseLong(searchEnz);
				    	  
				    	  Enzyme enzyme = Enzyme.find.byId( lEnz );

				    	  html += "<tr><td>" +enzyme.name + "</td><td>" + enzyme.jcggdb + "</td><td>" + enzyme.kegg + "</td><td>" + enzyme.goterm + "</td><td>" + enzyme.cazyfamily + "</td>";

				      }
				      else{
				           html  += "<td>" + entry.getValue().toString() + "</td>";
				      }
				    }
				   
				  
				   // System.out.println("==toJSONString()==");
				   // System.out.println(JSONValue.toJSONString(json));
				     
				 	html  += "</tr>";
				 	Logger.info("html " + html);
				  }
				  catch(ParseException pe){
				    System.out.println(pe);
				  }

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		
		return html;
	}



}

