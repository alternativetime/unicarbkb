package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity 
@Table(schema="public", name="stsource")
public class Stsource extends Model {

	@Id
	public Long id;

	public String div1;
	   
    public String div2;

    public String div3;

    public String div4;

	@ManyToOne
	public Structure structure; 

	@ManyToOne
	public Tissue tissue;

	public static Finder<Long,Stsource> find = new Finder<Long,Stsource>( Stsource.class);
	
	public static List<Stsource> findStsource(String tissue1, String tissue2, String tissue3, String tissue4) {
        return
           find.query().where().disjunction()
		.ilike("div1", tissue1)
		.ilike("div2", tissue2)
		.ilike("div3", tissue3)
		.ilike("div4", tissue4)
		.findList();
   }

}

