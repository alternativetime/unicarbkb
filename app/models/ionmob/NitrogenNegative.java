package models.ionmob;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;

/**
 * Created by matthew on 03/08/2014.
 */

@SuppressWarnings("serial")
@Entity
@Table(schema="ionmob", name="nitrogen_negative")
public class NitrogenNegative extends Model {

    @Id
    public Long id;

    @Column(columnDefinition = "double default 0")
    public Double h = 0.0;
    public Double cl;
    public Double p;

    @ManyToOne
    public GlycanMob glycanMob;

    public static Finder<Long,NitrogenNegative> find = new Finder<Long,NitrogenNegative>( NitrogenNegative.class);


}
