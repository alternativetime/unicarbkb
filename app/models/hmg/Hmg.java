package models.hmg;

import io.ebean.*;
import models.Reference;
import models.Streference;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.*;
import org.eurocarbdb.application.glycanbuilder.GlycoCTCondensedParser;
import org.eurocarbdb.application.glycanbuilder.IonCloud;
import org.eurocarbdb.application.glycanbuilder.MassOptions;
import org.expasy.glycanrdf.datastructure.Glycan;
import org.expasy.glycanrdf.io.glycoCT.GlycoCTReader;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import org.expasy.sugarconverter.parser.IupacParser;
import play.Logger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matthew on 29/05/15.
 */

@SuppressWarnings("serial")
@Entity
//@EntityConcurrencyMode(ConcurrencyMode.NONE)
@Table(schema="unicorn", name="hmg")
public class Hmg extends Model{

    @Id
    public Long id;

    public String iupac;
    public String glycoct;
    public Integer mapkey ;

    public void setAverageMass(Double averageMass) {
        this.averageMass = averageMass;
    }

    public void setMonoMass(Double monoMass) {
        this.monoMass = monoMass;
    }

    public Integer gal;
    public Integer fuc;
    public Integer glc;
    public Integer glcnac;
    public Integer galnac;
    public Integer kdn;

    public Double calcmass;
    public Double averageMass;
    public Double monoMass;



    public static Finder<Long,Hmg> find = new Finder<Long,Hmg>( Hmg.class);

    public static Boolean countMono() {
        final double galmass = 162.0528;
        final double fucmass = 146.1430;
        final double glcmass = 162.142;
        final double glcnacmass = 203.1950;
        final double galnacmass = 203.1950;
        final double kdnmass = 291.2579;
        final double manmass = 162.142;

        final double mgalmass = 162.0528;
        final double mfucmass = 146.0579;
        final double mglcmass = 162.0528;
        final double mglcnacmass = 203.0794;
        final double mgalnacmass = 203.0794;
        final double mkdnmass = 291.0954;
        final double mmanmass = 162.0528;

        //int cman = 0;
        int cgal = 0;
        int cfuc = 0;
        int cglcnac = 0;
        int ckdn = 0;
        int cgalnac = 0;
        int cglc = 0;


        List<Hmg> hmgs = Hmg.find.all();
        for(Hmg n : hmgs) {

        //Hmg n = Hmg.find.byId(33L);
            //Hmg nupdate = Hmg.find.byId(n.id);

            String str = StringUtils.lowerCase(n.iupac);


            //cman = StringUtils.countMatches(str, "man");
            cgalnac = StringUtils.countMatches(str, "galnac");
            cglcnac = StringUtils.countMatches(str, "glcnac");
            str = str.replaceAll("glcnac", "xxx");
            str = str.replaceAll("galnac", "xxx");

            cgal = StringUtils.countMatches(str, "gal");
            cfuc = StringUtils.countMatches(str, "fuc");

            ckdn = StringUtils.countMatches(str, "neu5ac");


            cglc = StringUtils.countMatches(str, "glc");

            /*nupdate.galnac = cgalnac;
            nupdate.fuc = cfuc;
            nupdate.glcnac = cglcnac;
            nupdate.kdn = ckdn;
            nupdate.gal = cgal;
            nupdate.glc = cglc;*/

            Logger.info("What is going on: " + n.id + " count " + cglc);

            double naveragemass = (cglc * glcmass) + (cgalnac * galnacmass) + (cfuc * fucmass) + (cglcnac * glcnacmass) + ( ckdn * kdnmass) + ( cgal * galmass);
           // nupdate.averageMass = naveragemass;

            double nmonoemass = ((cglc * mglcmass) + (cgalnac * mgalnacmass) + (cfuc * mfucmass) + (cglcnac * mglcnacmass) + ( ckdn * mkdnmass) + ( cgal * mgalmass));
           // nupdate.monoMass = nmonoemass;

            Logger.info("mass check: " + nmonoemass);
        Logger.info("values " + cglc + "x" + mglcmass + " " + cgalnac + "x" + mgalnacmass + " " +  cfuc + "x" + mfucmass + " " + cglcnac + "x" + mglcnacmass + " " + ckdn + "x" + mkdnmass + " " + cgal + "x" + mgalmass);
            n.setAverageMass(naveragemass + 18.01528 );
            n.setMonoMass(nmonoemass + 18.0105546);

            //nupdate.calcmass = nupdate.calcmass;

            n.update();
            n.save();



        }
        return true;
    }


    public  static List<Hmg> getCompositionMatch(int gal, int fuc, int glc, int glcnac, int galnac, int kdn) {

        Logger.info("TEST COMPOSITION SEARCH");
        List<Hmg> hmg = Ebean.find(Hmg.class).where().conjunction()
                .add(Expr.le("gal", gal)).add(Expr.ge("gal", gal))
                .add(Expr.le("fuc", fuc)).add(Expr.ge("fuc", fuc))
                .add(Expr.le("glc", glc)).add(Expr.ge("glc", glc))
                .add(Expr.le("glcnac", glcnac)).add(Expr.ge("glcnac", glcnac))
                .add(Expr.le("galnac", galnac)).add(Expr.ge("galnac", galnac))
                .add(Expr.le("kdn", kdn)).add(Expr.ge("kdn", kdn))
                .findList();
        return hmg;
    }

    public static Junction makeQueryComp(int comp1, int comp2, String selection, String field){

        Junction<Hmg> junction;
        junction = Ebean.find(Hmg.class).where().conjunction();


        switch (selection) {
            case "gt":
                junction.add(Expr.gt(field, comp1));
                break;
            case "lt":
                junction.add(Expr.lt(field, comp1));
                break;
            default:
                if (comp1>0 && comp2>0)
                    junction.add(Expr.between(field, comp1, comp2));
                else
                    junction.add(Expr.eq(field, comp1));
                break;
        }

        return junction;
    }

    public static List<Hmg> getCompositionMatch2(int count, int gal, int fuc, int glc, int glcnac, int galnac, int kdn, int gal2, int fuc2, int glc2, int glcnac2, int galnac2, int kdn2, String gal3, String fuc3, String glc3, String glcnac3, String galnac3, String kdn3) {

        Junction<Hmg> hmg;
        hmg = Ebean.find(Hmg.class).where().conjunction();
        if (gal > 0) {
            hmg.add(makeQueryComp(gal, gal2, gal3, "gal"));
        }
        if (glc > 0) {
            hmg.add(makeQueryComp(glc, glc2, glc3, "glc"));
        }
        if (glcnac > 0) {
            hmg.add(makeQueryComp(glcnac, glcnac2, glcnac3, "glcnac"));
        }
        if (galnac > 0) {
            hmg.add(makeQueryComp(galnac, galnac2, galnac3, "galnac"));
        }
        if (kdn > 0) {
            hmg.add(makeQueryComp(kdn, kdn2, kdn3, "kdn"));
        }
        if (fuc > 0) {
            hmg.add(makeQueryComp(fuc, fuc2, fuc3, "fuc"));
        }
        Logger.info("check composition query");

        if(count > 100) {
            List<Hmg> glycanCompositions = Ebean.find(Hmg.class).where().conjunction()
                    .add(hmg)
                    .setMaxRows(100)
                    .findList();
            return glycanCompositions;
        }
        else {
            List<Hmg> glycanCompositions = Ebean.find(Hmg.class).where().conjunction()
                    .add(hmg)
                    .findList();
            return glycanCompositions;
        }
    }

    public static int getCompositionMatch2Count(int gal, int fuc, int glc, int glcnac, int galnac, int kdn, int gal2, int fuc2, int glc2, int glcnac2, int galnac2, int kdn2, String gal3, String fuc3, String glc3, String glcnac3, String galnac3, String kdn3) {

        Junction<Hmg> hmg;
        hmg = Ebean.find(Hmg.class).where().conjunction();
        if (gal > 0) {
            hmg.add(makeQueryComp(gal, gal2, gal3, "gal"));
        }
        if (fuc > 0) {
            hmg.add(makeQueryComp(fuc, fuc2, fuc3, "fuc"));
        }
        if (glcnac > 0) {
            hmg.add(makeQueryComp(glcnac, glcnac2, glcnac3, "glcnac"));
        }
        if (galnac > 0) {
            hmg.add(makeQueryComp(galnac, galnac2, galnac3, "galnac"));
        }
        if (kdn > 0) {
            hmg.add(makeQueryComp(kdn, kdn2, kdn3, "kdn"));
        }
        if (glc > 0) {
            hmg.add(makeQueryComp(glc, glc2, glc3, "glc"));
        }
        Logger.info("check composition query");

        int count = Ebean.find(Hmg.class).where().conjunction()
                .add(hmg)
                .findCount();

        return count;
    }



    public static List<Streference> getMilkStructures() {
        List<Streference> streferences = Reference.find.byId(2890L).streference;
        return streferences;
    }

    public static String convertIUPAC(String iupac) {
        org.expasy.sugarconverter.parser.IupacParser p = new IupacParser(iupac.trim());

        try {
            p.getCtTree(p.parse());
        } catch (Exception ex) {
            System.err.println("Problem parsing the sequence");

        }
        return p.getCtSequence();

    }


    public static List<String> searchRDF(Long id) {

        /*String structure = Structure.find.byId(id).glycanst;

        structure = StringUtils.replace(structure, "freeEnd--??", "freeEnd--?a");


        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);

        Sugar sugar = null;
        try {
            sugar = glycan.toSugar();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //String glycoct = glycan.toGlycoCTCondensed();
        Hmg hmg = Hmg.find.byId(id);
        String glycoct = hmg.glycoct;


        GlycoCTReader reader = new GlycoCTReader();
        Glycan glycanCT = reader.read(glycoct, "1");

        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String q = queryGenerator.generateQueryString(glycanCT);
        //System.out.println("gws1: " + structure );
        //System.out.println("gws: " +  glycan.toGlycoCT() );
        q = q.replaceAll("mzjava.expasy.org", "rdf.unicarbkb.org");

        System.out.println("query: " + q);

        //q = q.concat(" LIMIT 50");
        String b = " LIMIT 20";
        String a = new StringBuilder()
                .append(q)
                .append(b)
                .toString();

        org.apache.jena.query.Query query = QueryFactory.create(a);
        //ARQ.getContext().setTrue(ARQ.useSAX);
        //137.92.56.159
        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://103.29.112.169:443/hmg/query", query); //check IP

        //          Long time = qexec.getTimeout1();

        ResultSet results = qexec.execSelect();
        //List<String> results2 = qexec.execSelect().getResultVars();
        List<String> hits = new ArrayList<>();
        int count = 0;
        while (results.hasNext()) {
            QuerySolution soln = results.nextSolution();
            System.out.println(soln);
            count++;
            String hitsFound = soln.toString();
            hits.add(hitsFound.replaceAll("<http://rdf.unicarbkb.org/structureConnection/", ""));
        }
        qexec.close();

        System.out.println("size: " + count);


        return hits;

    }

    public static List<Hmg> massSearch(Double mass){
        return Hmg.find.query().where().between("calcmass", mass - 0.5, mass + 0.5).orderBy("calcmass").findList();
    }

    public static List<Hmg> iupacSearch(String iupac, int count) {
        List<Hmg> iupacPartial = null;

        if(count > 100) {
            iupacPartial = Ebean.find(Hmg.class).where().conjunction().add(Expr.icontains("iupac", iupac)).setMaxRows(100).findList();
        }else{
            iupacPartial = Ebean.find(Hmg.class).where().conjunction().add(Expr.icontains("iupac", iupac)).findList();
        }


        return iupacPartial;
    }

    public static int iupacSearchCount(String iupac) {
        int countIupac = Ebean.find(Hmg.class).where().conjunction().add(Expr.icontains("iupac", iupac)).findCount();
        return countIupac;
    }

    public void setCalcmass(double calcmass) {
        this.calcmass = calcmass;
    }

    public static Boolean refreshMass() throws Exception {
        int c = 10000;
        List<Hmg> hmgs = Hmg.find.query().where().add(Expr.eq("calcmass", null)).setMaxRows(c).findList();

        //Ebean.find(Hmg.class).set

        for(Hmg h : hmgs){

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);
        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";

        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

       // String redEnd = "redEnd";
        //masso.setReducingEndTypeString(redEnd);

            IupacParser p = new IupacParser(h.iupac.trim());
            //p.setGlycanType("O-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)
            try {
                p.getCtTree(p.parse());
                //MassOptions masso = new MassOptions();
                org.eurocarbdb.application.glycanbuilder.Glycan glycan = parser.readGlycan(p.getCtSequence(), masso);


                //org.eurocarbdb.application.glycanbuilder.Glycan glycan = parser.fromGlycoCTCondensed(h.glycoct, masso);
            double mass = 0.0;
            mass = glycan.withNoReducingEndModification().computeMass();
                Logger.info("mass----> " + mass);
            Hmg hmgstructure = Hmg.find.byId(h.id);
            h.setCalcmass(mass);
            h.save();
            h.update();
            } catch (Exception ex) {
                System.err.println("Problem parsing the sequence");
                ex.printStackTrace();
            }
        }
        return true;
    }



}


