package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Structure entity managed by Ebean
 */
@SuppressWarnings("serial")
@Entity 
@Table(schema="connected", name="glycobase")
public class Glycobase extends Model {

    @Id
    public Long id;
    
    public String gws;
    public String glycoct;
   
//    @OneToMany
//    public List<Eurocarb> eurocarb;


    public static Finder<Long,Glycobase> find = new Finder<Long,Glycobase>( Glycobase.class);

    public static List<Glycobase> searchStructureGlycoBase(String glycoct) {
        return
            find.query().where()
                .like("glycoct", glycoct)
                //.join("strtaxonomy")
                .findList();
    }


    
}

