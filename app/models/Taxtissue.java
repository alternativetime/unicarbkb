package models;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="public", name="taxtissue")
public class Taxtissue {
	
	@Id
	public Long id;

	public String species;
	
	//public String common;
	
	//public String div1;
	   
    //public String div2;

    //public String div3;

    //public String div4;
    
    @ManyToOne
    public Tissue tissue;
    
    @ManyToOne
    public Taxonomy taxonomy;
    
    
    public static Finder<Long,Taxtissue> find = new Finder<Long,Taxtissue>( Taxtissue.class);

}
