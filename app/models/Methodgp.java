package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Method entity managed by Ebean
 */
@Entity
@Table(schema="public", name="methodgp")
public class Methodgp extends Model {

    @Id
    public Long id;
    public String description;
    public Long confidence;

    //@OneToMany
    //List<Refmethodgp> refmethodgps;



    public static Finder<Long,Methodgp> find = new Finder<Long,Methodgp>( Methodgp.class);

    public static List<Methodgp> findmethodgp(Long id) {
        return find
                .query().where()
                .eq("id", id)
                .findList();
    }


}