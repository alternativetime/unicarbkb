package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="co115")
public class Co115 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double Co115_1;
    public Double Co115_2;
    public Double Co115_3;

    public static Finder<Long,Co115> find = new Finder<Long,Co115>( Co115.class);


    public static PagedList<Co115> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Co115> pagedList = Co115.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
