package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Rko")
public class Rko extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double RKO_VUmc_1;
    public Double RKO_VUmc_2;
    public Double RKO_VUmc_3;

    public static Finder<Long,Rko> find = new Finder<Long,Rko>( Rko.class);


    public static PagedList<Rko> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Rko> pagedList = Rko.find.query().where().setFirstRow(page * pageSize)
                .setMaxRows(pageSize)
                .findPagedList();

        return pagedList;
    }
}
