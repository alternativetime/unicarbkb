package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="dld")
public class Dld extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double DLD1;
    public Double DLD2;
    public Double DLD1B;

    public static Finder<Long,Dld> find = new Finder<Long,Dld>( Dld.class);


    public static PagedList<Dld> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Dld> pagedList = Dld.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
