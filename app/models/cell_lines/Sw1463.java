package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="sw1463")
public class Sw1463 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW1463_1;
    public Double SW1463_2;
    public Double SW1463_1B;

    public static Finder<Long,Sw1463> find = new Finder<Long,Sw1463>( Sw1463.class);


    public static PagedList<Sw1463> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw1463> pagedList = Sw1463.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
