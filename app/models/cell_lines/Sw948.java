package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw948")
public class Sw948 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW948_VUmc_1;
    public Double SW948_VUmc_2;
    public Double SW948_VUmc_3;

    public static Finder<Long,Sw948> find = new Finder<Long,Sw948>( Sw948.class);


    public static PagedList<Sw948> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw948> pagedList = Sw948.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
