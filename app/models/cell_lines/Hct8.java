package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="hct8")
public class Hct8 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double HCT8_1;
    public Double HCT8_2;
    public Double HCT8_3;

    public static Finder<Long,Hct8> find = new Finder<Long,Hct8>( Hct8.class);

    public static PagedList<Hct8> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Hct8> pagedList = Hct8.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
