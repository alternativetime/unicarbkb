package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 13/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="hct116")
public class hct116 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;
    public Double HCT116_1;
    public Double HCT116_2;
    public Double HCT116_3;
    public Double HCT116_1B;
    public Double HCT116_2B;
    public Double HCT116_3B;
    public Double HCT116_VUmc_1;
    public Double HCT116_VUmc_2;
    public Double HCT116_VUmc_3;

    public static Finder<Long,hct116> find = new Finder<Long,hct116>( hct116.class);


    public static PagedList<hct116> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<hct116> pagedList = hct116.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;

    }
}
