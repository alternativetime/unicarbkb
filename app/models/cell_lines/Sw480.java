package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw480")
public class Sw480 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW480_1;
    public Double SW480_2;
    public Double SW480_3;
    public Double SW480_1B;
    public Double SW480_2B;
    public Double SW480_3B;
    public Double SW480_VUmc_1;
    public Double SW480_VUmc_2;
    public Double SW480_VUmc_3;

    public static Finder<Long,Sw480> find = new Finder<Long,Sw480>( Sw480.class);


    public static PagedList<Sw480> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw480> pagedList = Sw480.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
