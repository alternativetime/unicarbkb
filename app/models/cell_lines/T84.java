package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="t84")
public class T84 extends Model {

        @Id
        public Long id;

        public String structureName;
        public Double mass;

        public Double T84_1;
        public Double T84_2;
        public Double T84_3;

        public static Finder<Long,T84> find = new Finder<Long,T84>( T84.class);


        public static PagedList<T84> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<T84> pagedList = T84.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
