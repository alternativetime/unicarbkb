package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="colo320")
public class Colo320 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double Colo320_VUmc_1;
    public Double Colo320_VUmc_2;
    public Double Colo320_VUmc_3;

    public static Finder<Long,Colo320> find = new Finder<Long,Colo320>( Colo320.class);


    public static PagedList<Colo320> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Colo320> pagedList = Colo320.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
