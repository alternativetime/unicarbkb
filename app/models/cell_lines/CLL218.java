package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="cll218")
public class CLL218  extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double CLL218_1B;
    public Double CLL218_2;

    public static Finder<Long,CLL218> find = new Finder<Long,CLL218>( CLL218.class);


    public static PagedList<CLL218> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<CLL218> pagedList = CLL218.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
