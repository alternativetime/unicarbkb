package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="ccaco2")
public class CaCo2 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double Caco2_1;
    public Double Caco2_2;
    public Double Caco2_3;
    public Double Caco2_1B;
    public Double Caco2_2B;
    public Double Caco2_3B;




    public static Finder<Long,CaCo2> find = new Finder<Long,CaCo2>( CaCo2.class);


    public static PagedList<CaCo2> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<CaCo2> pagedList = CaCo2.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
