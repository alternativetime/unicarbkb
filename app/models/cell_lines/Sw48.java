package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw48")
public class Sw48 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW48_1;
    public Double SW48_2;
    public Double SW48_3B;

    public static Finder<Long,Sw48> find = new Finder<Long,Sw48>( Sw48.class);


    public static PagedList<Sw48> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw48> pagedList = Sw48.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
