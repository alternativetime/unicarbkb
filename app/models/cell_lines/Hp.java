package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Hp")
public class Hp extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double HP_1;
    public Double HP_2;
    public Double HP_3;

    public static Finder<Long,Hp> find = new Finder<Long,Hp>( Hp.class);


    public static PagedList<Hp> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Hp> pagedList = Hp.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
