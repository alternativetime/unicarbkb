package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Ls180")
public class Ls180 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double LS180_1;
    public Double LS180_2;
    public Double LS180_2B;

    public static Finder<Long,Ls180> find = new Finder<Long,Ls180>( Ls180.class);


    public static PagedList<Ls180> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Ls180> pagedList = Ls180.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
