package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw1116")
public class Sw1116 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW1116_1;
    public Double SW1116_2;
    public Double SW1116_3;

    public static Finder<Long, Sw1116> find = new Finder<Long, Sw1116>( Sw1116.class);


    public static PagedList<Sw1116> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw1116> pagedList = Sw1116.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
