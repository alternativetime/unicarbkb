package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw620")
public class Sw620 extends Model {

        @Id
        public Long id;

        public String structureName;
        public Double mass;

        public Double SW620_1;
        public Double SW620_2;
        public Double SW620_3;

        public static Finder<Long,Sw620> find = new Finder<Long,Sw620>( Sw620.class);


        public static PagedList<Sw620> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw620> pagedList = Sw620.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
