package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Sw1398")
public class Sw1398 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double SW1398_VUmc_1;
    public Double SW1398_VUmc_2B;
    public Double SW1398_VUmc_3;

    public static Finder<Long,Sw1398> find = new Finder<Long,Sw1398>( Sw1398.class);


    public static PagedList<Sw1398> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Sw1398> pagedList = Sw1398.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
