package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Lovo")
public class Lovo extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double LOVO_1;
    public Double LOVO_2;
    public Double LOVO_3;

    public static Finder<Long,Lovo> find = new Finder<Long,Lovo>( Lovo.class);


    public static PagedList<Lovo> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Lovo> pagedList = Lovo.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
