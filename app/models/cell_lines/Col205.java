package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="colo205")
public class Col205 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double Colo205_VUmc_1;
    public Double Colo205_VUmc_2;
    public Double Colo205_VUmc_3;

    public static Finder<Long,Col205> find = new Finder<Long,Col205>( Col205.class);


    public static PagedList<Col205> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Col205> pagedList = Col205.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
