package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Ls174t")
public class Ls174t extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double LS174T_VUmc_1;
    public Double LS174T_VUmc_2;
    public Double LS174T_VUmc_3;

    public static Finder<Long,Ls174t> find = new Finder<Long,Ls174t>( Ls174t.class);


    public static PagedList<Ls174t> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Ls174t> pagedList = Ls174t.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
