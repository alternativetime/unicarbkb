package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="c10")
public class c10 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double C10_1;
    public Double C10_2;
    public Double C10_1B;

    public static Finder<Long,c10> find = new Finder<Long,c10>( c10.class);


    public static PagedList<c10> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<c10> pagedList = c10.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
