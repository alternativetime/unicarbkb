package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Ht29")
public class Ht29 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double HT29_1;
    public Double HT29_2;
    public Double HT29_3;
    public Double HT29_1B;
    public Double HT29_2B;
    public Double HT29_3B;
    public Double HT29_VUmc_1;
    public Double HT29_VUmc_2B;
    public Double HT29_VUmc_3;

    public static Finder<Long,Ht29> find = new Finder<Long,Ht29>( Ht29.class);


    public static PagedList<Ht29> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Ht29> pagedList = Ht29.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
