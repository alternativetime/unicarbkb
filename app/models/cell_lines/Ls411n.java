package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Ls411n")
public class Ls411n extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double LS411N_1;
    public Double LS411N_2;
    public Double LS411N_2B;

    public static Finder<Long,Ls411n> find = new Finder<Long,Ls411n>( Ls411n.class);


    public static PagedList<Ls411n> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Ls411n> pagedList = Ls411n.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
