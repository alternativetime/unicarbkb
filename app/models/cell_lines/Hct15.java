package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="hct15")
public class Hct15 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double HCT15_VUmc_1;
    public Double HCT15_VUmc_2;
    public Double HCT15_VUmc_3;

    public static Finder<Long,Hct15> find = new Finder<Long,Hct15>( Hct15.class);


    public static PagedList<Hct15> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Hct15> pagedList = Hct15.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }
}
