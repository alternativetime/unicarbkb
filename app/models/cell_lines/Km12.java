package models.cell_lines;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by matthew on 12/01/2016.
 */
@Entity
@Table(schema="cell_lines", name="Km12")
public class Km12 extends Model {

    @Id
    public Long id;

    public String structureName;
    public Double mass;

    public Double KM12_VUmc_1;
    public Double KM12_VUmc_2;
    public Double KM12_VUmc_3;

    public static Finder<Long,Km12> find = new Finder<Long,Km12>( Km12.class);


    public static PagedList<Km12> page(int page, int pageSize, String sortBy, String order, String filter) {

        PagedList<Km12> pagedList = Km12.find.query().where()
                .setFirstRow(page * pageSize).setMaxRows(pageSize).findPagedList();
        return pagedList;
    }

}
