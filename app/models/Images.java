package models;

import io.ebean.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Images {

	@Id
	public Long id;
	
	@ManyToOne
	public Structure structure;

	public static Finder<Long,Images> find = new Finder<Long,Images>( Images.class);


}
