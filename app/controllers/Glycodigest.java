package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Translation;
import models.cttest;
import play.Logger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.glycodigest;
import views.html.glycodigestHome;
import views.html.glycodigesttest;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;



public class Glycodigest extends Controller {

	public Result glycodigestHome() {
		return ok( glycodigestHome.render() );
	}

	public Result glycodigest(Long id) {
		String enzymes = "";
		Map<String, String> hashMap = new HashMap<String, String>();
	
		Translation translation = new Translation();
		Translation ctt = translation.translationCT(id);

		cttest ctt2 = new cttest();
		//ctt = ct.find.byId(id);
		
		try{
			hashMap = ctt2.digest(ctt.ct, enzymes);
		}
		catch(IOException e){
			e.printStackTrace();
			
		}
		
		return ok( 
				glycodigest.render(hashMap, id, ctt.ct)
				); 
	};
	
	public Result glycodigesttest(Long id, String s){
		Map<String, String> hashMap = new HashMap<String, String>();
		
		String x = request().queryString().get("digest").toString() ;
		String uri =  request().uri();


	Set<Map.Entry<String,String[]>> entries = request().queryString().entrySet();
        for (Map.Entry<String,String[]> entry : entries) {
	        final String key = entry.getKey();
                final String value = Arrays.toString(entry.getValue());
        Logger.debug(key + " " + value);
						            }
	//Logger.debug(request().getQueryString("digest"));
	String test = request().getQueryString("digest");

		Logger.info("---------#  " + request().queryString().toString() );
		
		Translation translation = new Translation();
		Translation ctt = translation.translationCT(id);
		cttest ctt2 = new cttest();

		try {
			hashMap = ctt2.digest(ctt.ct, test);
		}
		catch(IOException e) {
			e.printStackTrace();
		}

		//check contents of hashmap
	
		for (Map.Entry<String, String> entry : hashMap.entrySet()) {
		    //System.out.println("contents of digesttes map " + entry.getKey()+" : "+entry.getValue());
		    String value = entry.getValue().toString();
		    if(value.startsWith("RES")){
			    Logger.info("need to use this string " + java.net.URLDecoder.decode(value));
			    Long idCheck = Translation.checkDigestStructure(java.net.URLDecoder.decode(value));
			    Logger.info("hopefully: " + idCheck);
		    } 
		}	    
		
		//

		return ok( 
				glycodigesttest.render(hashMap, id, ctt.ct)
				); 
	}
	@BodyParser.Of(BodyParser.Json.class)
	public Result glycodigestStructureDB(String ct){

		Long id = -1L;
		id = Translation.checkDigestStructure(ct);
			
		JsonNode json = request().body().asJson();
		  ObjectNode result = Json.newObject();
		  if(id > 0 ) {
			  result.put("status", "OK");
		 	  result.put("message", id );
		  } else {
			  result.put("status", "Bad");
		  }
		  Logger.info("created json " + result);
		  return ok(result);
	}
}
