package controllers;

import io.ebean.SqlRow;
import models.Biolsource;
import models.Ftmerge;
import models.Proteinstaxonomy;
import models.StructureToSiteGeneral;
import models.composition_protein.CompSite;
import play.mvc.Controller;
import play.mvc.Result;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntry;
import uk.ac.ebi.kraken.interfaces.uniprot.comments.*;
import uk.ac.ebi.uniprot.dataservice.client.Client;
import uk.ac.ebi.uniprot.dataservice.client.QueryResult;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtService;
import uk.ac.ebi.uniprot.dataservice.query.Query;
import views.html.proteinsite;

import java.util.*;

import static models.Proteinstaxonomy.findProteinTax;


public class UniprotConnection extends Controller {

    private abstract static class SearchExecutor {
        private static final int NO_LIMIT = Integer.MAX_VALUE;

        private final UniProtService uniProtService;

        public SearchExecutor(UniProtService uniProtService) {
            this.uniProtService = uniProtService;
        }

        Map<String, List<String>> executeSearch(Query query) throws ServiceException {
            return executeSearch(query, NO_LIMIT);
        }

        Map<String, List<String>> executeSearch(Query query, int limit) throws ServiceException {
            QueryResult<UniProtEntry> searchResult = uniProtService.getEntries(query);

            Map<String, List<String>> entries = new HashMap<>();

            while (searchResult.hasNext() && limit > 0) {
                UniProtEntry entry = searchResult.next();
                String accession = entry.getPrimaryUniProtAccession().getValue();

                entries.put(accession, extractValues(entry));
                limit--;
            }

            return entries;
        }

        abstract List<String> extractValues(UniProtEntry entry);
    }


    public static String FunctionCommentsEntryRetrievalSequence(String accession) throws ServiceException {

        String ptmComment = "";

        uk.ac.ebi.uniprot.dataservice.client.ServiceFactory serviceFactoryInstance = Client.getServiceFactoryInstance();
        UniProtService uniProtService = serviceFactoryInstance.getUniProtQueryService();

        // start the service
        uniProtService.start();
        UniProtEntry entry = uniProtService.getEntry(accession);
        try {
            //try this
            Collection<Comment> test = entry.getComments(CommentType.PTM);
            //entry.getFeatures(FeatureType.CARBOHYD);
            //entry.getProteinDescription().getSection();
            //entry.getProteinDescription().getSection().getNames();
            //Collection<Comment> test = entry.getComments();

            //end

            ptmComment = String.valueOf(entry.getComments(CommentType.FUNCTION).toString());

            List<Comment> comments = entry.getComments(CommentType.FUNCTION);

            List<FunctionComment> functions = entry.getComments( CommentType.FUNCTION );
            for ( FunctionComment function : functions ) {
                    List<CommentText> text = function.getTexts();
                    for(CommentText t : text) {
                        ptmComment = t.getValue();
                    }

            }

        }
            catch (Exception e) {
                e.printStackTrace();
            } finally {
                uniProtService.stop();
                System.out.println("service now stopped.");
            }

        return ptmComment;
    }


    public static List<uk.ac.ebi.kraken.interfaces.uniprot.comments.TextOnlyComment> PTMCommentsEntryRetrievalSequence(String accession) throws ServiceException {

        String ptmComment = "";

        uk.ac.ebi.uniprot.dataservice.client.ServiceFactory serviceFactoryInstance = Client.getServiceFactoryInstance();
        UniProtService uniProtService = serviceFactoryInstance.getUniProtQueryService();


        uniProtService.start();
        UniProtEntry entry = uniProtService.getEntry(accession);
        List<CommentText> ctext = null;
        List<TextOnlyComment> x = null;
        try {


            List<Comment> comments = entry.getComments(CommentType.PTM);

            for (Comment c : comments) {
                System.out.println("checkingfuck: " + c);
            }


            x = entry.getComments(CommentType.PTM);
            for (TextOnlyComment text : x) {
                System.out.println("checkingshit: " + text.getTexts());
                ctext = text.getTexts();
                for (CommentText t : ctext) {
                    System.out.println("checkingshitshit: " + t.getValue());

                }
            }

            List<FunctionComment> functions = entry.getComments(CommentType.PTM);
            for (FunctionComment function : functions) {
                List<CommentText> text = function.getTexts();
                for (CommentText t : text) {
                    System.out.println("checking: " + t.getValue());
                    ptmComment = t.getValue();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            uniProtService.stop();
            System.out.println("service now stopped.");
        }

        return x;
    }

    public static String EntryRetrievalSequence(String accession) {

        String sequenceCat = "";


        uk.ac.ebi.uniprot.dataservice.client.ServiceFactory serviceFactoryInstance = Client.getServiceFactoryInstance();
        UniProtService uniProtService = serviceFactoryInstance.getUniProtQueryService();
        String sequence = null;
        try {
            // start the service
            uniProtService.start();

            // the accession we're interested in
            UniProtEntry entry = uniProtService.getEntry(accession);
            if (entry == null) {
                System.out.println("Entry " + accession + " could not be retrieved");
            } else {
                System.out.println("Retrieved UniProtEntry object");
                System.out.println(entry.getUniProtId().getValue());
                sequence = entry.getSequence().getValue().toString();
            }

            // use the service directly to fetch the UniProtEntry
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // always remember to stop the service
            uniProtService.stop();
            System.out.println("service now stopped.");
        }

        //sequence = attribute.toString();

        String[] thisCombo2 = new String[0];
        if (sequence != null) {
            thisCombo2 = sequence.split("(?<=\\G..........)");
        }
        String asString = Arrays.toString(thisCombo2);
        int i = 0;
        for (String s : thisCombo2) {
            if (i == 50) {
                sequenceCat += s;
                //sequenceCat += "\n";
                i = 0;
            } else {
                sequenceCat += s;
                i = i + 10;
            }
        }

        return sequenceCat;
    }


    public Result proteinsite() {
        String proteinName = "";
        String swissProtName = "";
        String proteinNameFull = "";
        String site = "";
        String type = "";
        String sequenceRetrieval = "";
        String ptmComment = "";
        String protein = "";
        List<Proteinstaxonomy> proteinstaxList = null;

        ArrayList<SqlRow> biolRefs = new ArrayList<SqlRow>();
        List<models.StructureToSiteDefined> definedStructures = null;
        List<models.GeneralSites> generalStructures = null;
        //ArrayList<Long> structuresShow = new ArrayList();
        HashSet<Long> structuresShow = new HashSet();
        List<SqlRow> listSql = null;
        ArrayList<SqlRow> listSqlArray = new ArrayList<SqlRow>();
        ArrayList<Biolsource> biolSourceProtein = new ArrayList<Biolsource>();
        List<Biolsource> biolSourceProteins = Biolsource.findBiolSourceIds(protein);
        HashSet taxsources = new HashSet();
        HashSet multiCAR = new HashSet();

        String proteinFromTax = "";

        Proteinstaxonomy proteinstax = findProteinTax(protein);
        if (proteinstax == null) {
            //this means we have a multi car to protein link in glycobase
            proteinstaxList = Proteinstaxonomy.findProteinsTax(protein);
            for (Proteinstaxonomy p : proteinstaxList) {
                multiCAR.add(p.protein);
            }
        }

        if (request().queryString().size() > 0) {
            Map<String, String[]> params = request().queryString();
            String[] searchTerms = null;
            List<Ftmerge> fts = null;

            String key = null;

            for (Map.Entry<String, String[]> entry : params.entrySet()) {
                key = entry.getKey();
                searchTerms = entry.getValue();

                //from swissProt redirect
                if (key.equals("feature")) {
                    for (String ft : searchTerms) {

                        fts = Ftmerge.findFt(ft);
                    }

                    for (Ftmerge f : fts) {
                        protein = f.swiss_prot;
                        site = f.amino_acid;
                    }
                }

                if (key.equals("position")) {
                    for (String p : searchTerms) {
                        site = p;
                        System.out.println(" -------> output is " + site);
                    }
                }
                if (key.equals("protein")) {
                    for (String p : searchTerms) {
                        protein = p;
                        System.out.println("output is 2 " + protein);
                    }
                }

                if (key.equals("type")) {
                    for (String t : searchTerms) {
                        type = t;
                    }
                }

            }

            if (type.equals("defined")) {
                definedStructures = models.StructureToSiteDefined.findStructuresDefined(protein, site);
                int x;
                for (models.StructureToSiteDefined s : definedStructures) {
                    x = s.structure_id;
                    Long value = Long.valueOf(x);
                    //this check should not be required but for safety
                    if (!structuresShow.contains(value)) {
                        structuresShow.add(value);
                    }
                }
            } else if (type.equals("general")) {
                int gvalue;
                int x;
                generalStructures = models.GeneralSites.findStructuresGeneral(protein, site);
                for (models.GeneralSites str : generalStructures) {
                    List<models.StructureToSiteGeneral> general = str.strSiteGeneral;
                    for (StructureToSiteGeneral g : general) {
                        if (g.structure_id > 0) {
                            gvalue = g.structure_id;
                            Long value = Long.valueOf(gvalue);
                            //this check should not be required but for safety
                            if (!structuresShow.contains(value)) {
                                structuresShow.add(value);
                            }
                        }
                    }
                }
                for (Long s : structuresShow) {
                    System.out.println("check values" + s);
                }
            }

            for (Biolsource biol : biolSourceProteins) {
                swissProtName = biol.swiss_prot;
                proteinName = biol.protein;
            }

            //why am i getting multiple tax for one swiss ID
            //taxsourcesUnique.addAll(taxsources);

            biolRefs = Biolsource.findBiolsourceRefs(protein);

            //TODO
            // sequenceRetrieval = UniprotConnection.EntryRetrievalSequence(protein);

        }

        proteinstax = findProteinTax(protein);
        if (proteinstax == null) {
            //this means we have a multi car to protein link in glycobase
            proteinstaxList = Proteinstaxonomy.findProteinsTax(protein);
            for (Proteinstaxonomy p : proteinstaxList) {
                multiCAR.add(p.protein);
                proteinFromTax = p.protein;
            }
        }

        List<CompSite> compSite = CompSite.compSite(protein);


        return ok(
                proteinsite.render(ptmComment, sequenceRetrieval, protein, biolRefs, site, structuresShow, proteinstax, proteinstaxList, multiCAR, proteinFromTax, compSite)
        );
    }

    //this needs some tidying
    public Result swissprotFT(String ft) throws ServiceException {
        //redirect("/proteinsite");
        //return redirect(controllers.UniprotConnection.proteinsite());
        String proteinName = "";
        String swissProtName = "";
        String proteinNameFull = "";
        String site = "";
        String type = "";
        String sequenceRetrieval = "";
        String ptmComment = "";
        String protein = "";
        List<TextOnlyComment> comment = null;
        List<Proteinstaxonomy> proteinstaxList = null;

        ArrayList<SqlRow> biolRefs = new ArrayList<SqlRow>();
        List<models.StructureToSiteDefined> definedStructures = null;
        List<models.GeneralSites> generalStructures = null;
        //ArrayList<Long> structuresShow = new ArrayList();
        HashSet<Long> structuresShow = new HashSet();
        List<SqlRow> listSql = null;
        ArrayList<SqlRow> listSqlArray = new ArrayList<SqlRow>();
        ArrayList<Biolsource> biolSourceProtein = new ArrayList<Biolsource>();
        List<Biolsource> biolSourceProteins = Biolsource.findBiolSourceIds(protein);
        HashSet taxsources = new HashSet();
        ArrayList taxsourcesUnique = new ArrayList();
        HashSet multiCAR = new HashSet();

        String proteinFromTax = "";

        List<Ftmerge> fts;

        fts = Ftmerge.findFt(ft);


        for (Ftmerge f : fts) {
            protein = f.swiss_prot.trim();
            site = f.amino_acid.trim();

        }

        Proteinstaxonomy proteinstax = findProteinTax(protein);
        if (proteinstax == null) {
            //this means we have a multi car to protein link in glycobase
            proteinstaxList = Proteinstaxonomy.findProteinsTax(protein);
            for (Proteinstaxonomy p : proteinstaxList) {
                multiCAR.add(p.protein);
            }
        }

        type = "defined";

        if (type.equals("defined")) {
            definedStructures = models.StructureToSiteDefined.findStructuresDefined(protein.trim(), site);
            for (models.StructureToSiteDefined s : definedStructures) {
                Long value = Long.valueOf(s.structure_id);
                //this check should not be required but for safety
                if (!structuresShow.contains(value)) {
                    structuresShow.add(value);
                }
            }
        } else if (type.equals("general")) {
            generalStructures = models.GeneralSites.findStructuresGeneral(protein, site);
            for (models.GeneralSites str : generalStructures) {
                List<StructureToSiteGeneral> general = str.strSiteGeneral;
                for (StructureToSiteGeneral g : general) {
                    if (g.structure_id > 0) {
                        Long value = Long.valueOf(g.structure_id);
                        //this check should not be required but for safety
                        if (!structuresShow.contains(value)) {
                            structuresShow.add(value);
                        }
                    }
                }
            }
        }


        List<CompSite> compSite = CompSite.compSite(protein);


        biolRefs = Biolsource.findBiolsourceRefs(protein);

        sequenceRetrieval = UniprotConnection.EntryRetrievalSequence(protein);
        ptmComment = UniprotConnection.FunctionCommentsEntryRetrievalSequence(protein);
        comment = UniprotConnection.PTMCommentsEntryRetrievalSequence(protein);

        proteinstax = findProteinTax(protein);
        if (proteinstax == null) {
            //this means we have a multi car to protein link in glycobase
            proteinstaxList = Proteinstaxonomy.findProteinsTax(protein);
            for (Proteinstaxonomy p : proteinstaxList) {
                multiCAR.add(p.protein);
                proteinFromTax = p.protein;
            }
        }


        return ok(
                proteinsite.render(ptmComment, sequenceRetrieval, protein, biolRefs, site, structuresShow, proteinstax, proteinstaxList, multiCAR, proteinFromTax, compSite)
        );

    }


    private Result redirect(Result proteinsite) {
        // TODO Auto-generated method stub
        return null;
    }


}