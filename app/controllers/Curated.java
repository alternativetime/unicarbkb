package controllers;

import models.curated.CurateProtein;
import models.curated.CurateSite;
import models.curated.Importeddataexcel;
import models.curated.Publication;
import models.unicorn.Nlink;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.OGBI.SugarImporterOgbi;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.application.glycanbuilder.*;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.curated.curatedPublications;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by matthew on 16/09/16.
 */
public class Curated extends Controller {


        public Result showCuratedPublications ( int page, String sortBy, String order, String filter, String protein){
            return ok(
                    curatedPublications.render(
                            Publication.page(page, 10, sortBy, order, filter),
                            sortBy, order, filter
                    )
            );
        }

    public Result showCurationPublication(Long id) {
        Publication publication = Publication.find.byId(id);
            return ok(views.html.curated.curatedPublication.render(publication));
        }


    public Result splitImports() {

        List<Importeddataexcel> importeddataexcels = Importeddataexcel.find.all();

        for(Importeddataexcel imported : importeddataexcels){
            if(imported.pmid == 19343721){
                System.out.println("requires checking");
            } else {
                CurateProtein.createProtein(imported);
            }

        }
        return ok("hello");

    }

    public Result curateSiteStructures(Long id){
        CurateSite curateSite = CurateSite.find.byId(id);
        return ok(views.html.curated.curatedSiteStructures.render(curateSite));

    }

    public Result showImageCurated(String ct) throws Exception {


        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);


        String style = "cfg";
        String format = "png";



        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(ct.trim(), masso);

        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
        }

        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

        //return byteArrayOutputStream.toByteArray();

    }

    public Result showImageCuratedUoxf(String uoxf) throws Exception {

        String format = "png";


        String t_strXML = null;
        try {
            SugarImporterOgbi t_objOGBI = new SugarImporterOgbi();
            Sugar t_objSugar = t_objOGBI.parse(uoxf);
            SugarExporterGlycoCTCondensed t_objExporter = new SugarExporterGlycoCTCondensed();
            t_objExporter.start(t_objSugar);
            t_strXML = t_objExporter.getHashCode();

            System.out.println("translated: " + t_strXML);
        } catch (SugarImporterException e) {
            e.printStackTrace();
        } catch (GlycoVisitorException e) {
            e.printStackTrace();

        }

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfgl"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String style = "cfg";

        String notation = "iupac";


        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);


        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        Glycan glycan = parser.fromGlycoCTCondensed(t_strXML.trim(), masso);

        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 1);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png")) {
                File outputfile = new File("/tmp/lg-iupac/lg-iupac-" + uoxf + ".png");
                ImageIO.write(img, "png", byteArrayOutputStream);
                ImageIO.write(img, "png", outputfile);
            } else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }

        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

    }
}
