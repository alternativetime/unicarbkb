package controllers;

import com.google.inject.Inject;
import models.rdf.RdfEpitopes;
import models.rdf.UniCarbkbEpitopes;
import org.apache.jena.query.*;
import org.eurocarbdb.application.glycanbuilder.*;
import org.expasy.glycanrdf.io.glycoCT.GlycoCTReader;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;



/**
 * Created by matthew on 11/11/14.
 */
public class GlycoRDF extends Controller {

    @Inject
    private FormFactory formFactory;


    public Result SubstructureSPARQLQuery() {

        return ok(views.html.rdf.substructureRDFQuery.render());
    }

    public Result SubstructureSPARQLQueryResultEpitope(Long id) {
        LinkedHashMap<String, String> resMap = new LinkedHashMap<>();
        final long start = System.currentTimeMillis();
        List<String> rdfResults = new ArrayList<>();
        RdfEpitopes rdfEpitopes = RdfEpitopes.find.byId(id);
        String structure = rdfEpitopes.glycoctRings;
        structure = structure.replaceAll(" ", "\n\r");
        Logger.info("ct: " + structure);
        structure = structure.replaceAll("\\s+", "\n");

        //System.out.println(" id " + rs.getString(2).trim() + " query ct " + ct );

        GlycoCTReader reader = new GlycoCTReader();
        org.expasy.glycanrdf.datastructure.Glycan glycan = reader.read(structure, "1");


        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String q = queryGenerator.generateQueryString(glycan);
        System.out.println("query: " + q);

        Query query = QueryFactory.create(q);
        ARQ.getContext().setTrue(ARQ.useSAX);
        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://localhost:3030/test/query", query); //check IP


        ResultSet results = qexec.execSelect();
        //Iterating over the SPARQL Query results
        int count = 0;
        while (results.hasNext()) {

            QuerySolution soln = results.nextSolution();
            //System.out.println(soln);
            String[] splitct = soln.getResource("structureConnection").toString().split("GC");
            Logger.info(splitct[1]);
            rdfResults.add(splitct[1]);
            count++;
        }
        qexec.close();
        final long end = System.currentTimeMillis();
        NumberFormat formatter = new DecimalFormat("#0.00000");

        resMap.put(String.valueOf(count), formatter.format((end - start) / 1000d));
        Logger.info(formatter.format((end - start) / 1000d));

        return ok(views.html.rdf.substructureRDFResult.render(rdfResults));

    }

    public Result SubstructureSPARQLQueryResult() {
        List<String> rdfResults = new ArrayList<>();
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        Logger.info("form posted " + dynamicForm.data().values());
        //prepareQuery(dynamicForm.data().values().toString());


        Logger.info("TESTING " +  dynamicForm.data().values().toString().replaceAll("\\[", "").replaceAll("\\]", "") ); //dynamicForm.data().values().toString().replaceAll("\\[", "").replaceAll("\\]", ""));
        String queryRDF = prepareQuery(dynamicForm.data().values().toString().replaceAll("\\[", "").replaceAll("\\]", ""));


        /*String sparqlQueryString2 = "prefix glycan:  <http://rdf.unicarbkb.org.org/glycan/>\n" +
                "prefix predicate:  <http://rdf.unicarbkb.org.org/predicate/>\n" +
                "prefix component:  <http://rdf.unicarbkb.org.org/component/>\n" +
                "prefix structureConnection:  <http://rdf.unicarbkb.org.org/structureConnection/>\n" +
                "SELECT DISTINCT ?structureConnection\n" +
                "WHERE { \n" +
                "?structureConnection predicate:has_components ?component0 . \n" +
                "{\n" +
                "SELECT * WHERE { \n" +
                "?component0 predicate:is_a_Gal ?component0 . \n" +
                "?component1 predicate:is_a_NAcetyl ?component1 . \n" +
                "?component0 predicate:is_connected ?component1 . \n" +
                "?component0 predicate:is_SubstituentLinkage ?component1 . \n" +
                "?component0 predicate:has_linkedCarbon_2 ?component1 . \n" +
                "?component2 predicate:is_a_Gal ?component2 . \n" +
                "?component0 predicate:is_connected ?component2 . \n" +
                "?component0 predicate:is_GlycosidicLinkage ?component2 . \n" +
                "?component0 predicate:has_anomerConnection_beta ?component2 . \n" +
                "?component0 predicate:has_linkedCarbon_3 ?component2 . \n" +
                "?component0 predicate:has_anomerCarbon_1 ?component2 . \n" +
                "?component3 predicate:is_a_Gal ?component3 . \n" +
                "?component2 predicate:is_connected ?component3 . \n" +
                "?component2 predicate:is_GlycosidicLinkage ?component3 . \n" +
                "?component2 predicate:has_anomerConnection_alpha ?component3 . \n" +
                "?component2 predicate:has_linkedCarbon_4 ?component3 . \n" +
                "?component2 predicate:has_anomerCarbon_1 ?component3 . \n" +
                "?component4 predicate:is_a_Gal ?component4 . \n" +
                "?component3 predicate:is_connected ?component4 . \n" +
                "?component3 predicate:is_GlycosidicLinkage ?component4 . \n" +
                "?component3 predicate:has_anomerConnection_alpha ?component4 . \n" +
                "?component3 predicate:has_linkedCarbon_3 ?component4 . \n" +
                "?component3 predicate:has_anomerCarbon_1 ?component4 . \n" +
                "?component5 predicate:is_a_Fuc ?component5 . \n" +
                "?component4 predicate:is_connected ?component5 . \n" +
                "?component4 predicate:is_GlycosidicLinkage ?component5 . \n" +
                "?component4 predicate:has_anomerConnection_alpha ?component5 . \n" +
                "?component4 predicate:has_linkedCarbon_2 ?component5 . \n" +
                "?component4 predicate:has_anomerCarbon_1 ?component5 . \n" +
                "}\n" +
                "}\n" +
                "}"; */

        Logger.info("QUERY " + queryRDF); //.replaceAll("\\.org", "\\.org\\.org") );

        Query query = QueryFactory.create(queryRDF); //  .replaceAll("\\.org", "\\.org\\.org")  );
        ARQ.getContext().setTrue(ARQ.useSAX);
        //Executing SPARQL Query and pointing to the DBpedia SPARQL Endpoint
        //QueryExecution qexec = QueryExecutionFactory.sparqlService("http://103.29.112.169:3030/unicarkb/query", query);
        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://localhost:3030/test/query", query); //check IP

        //Retrieving the SPARQL Query results
        ResultSet results = qexec.execSelect();


        //Iterating over the SPARQL Query results
        //rdfResults = new ArrayList<>();
        while (results.hasNext()) {
            QuerySolution soln = results.nextSolution();

            //Logger.info("TESTING 2 " + results.nextSolution() ) ;
            //Printing DBpedia entries' abstract.
            //System.out.println(soln.get("structureConnection")  );  //get("?abstract"));
            //rdfResults.add(soln.get("structureConnection").toString());
            rdfResults.add( soln.get("structureConnection").toString() );
        }
        qexec.close();

        return ok(views.html.rdf.substructureRDFResult.render(rdfResults));
    }

    public static String prepareQuery(String glycoCT) {

       /*String glycoCT =    "RES\n" +
               "1b:a-dgal-HEX-1:5\n" +
               "2s:n-acetyl\n" +
               "3b:b-dgal-HEX-1:5\n" +
               "4b:a-dgal-HEX-1:5\n" +
               "5b:a-dgal-HEX-1:5\n" +
               "6b:a-lgal-HEX-1:5|6:d\n" +
               "LIN\n" +
               "1:1d(2+1)2n\n" +
               "2:1o(3+1)3d\n" +
               "3:3o(4+1)4d\n" +
               "4:4o(3+1)5d\n" +
               "5:5o(2+1)6d";*/

        GlycoCTReader reader = new GlycoCTReader();
        org.expasy.glycanrdf.datastructure.Glycan glycan = reader.read(glycoCT, "1");
        //org.expasy.mzjava.glycomics.mol.Glycan glycan = reader.read(glycoCT, "1");

        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String query = queryGenerator.generateQueryString(glycan);
        System.out.println("query: " + query);
        return query;
    }

    public Result ShowEpitopes(int page, String sortBy, String order, String filter, String glytoucan) {
        return ok(
                views.html.rdf.showEpitopes.render(
                        RdfEpitopes.page(page, 20, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    /*
    * This actions shows structures with listed epitopes
    * The above actions lists all epitopes listed but necessary linked with any structure entry
     */
    public Result ShowUnicarbEpitopes(){

        //List<UniCarbkbEpitopes> uniCarbkbEpitopes = UniCarbkbEpitopes.find.findList();



        return ok(
                views.html.rdf.showUnicarbEpitopes.render(
                        UniCarbkbEpitopes.getEpitopesGroup()
                )
        );

    }



    public Result ImageGenerator(Long id, String notation) throws Exception {


        String structure = RdfEpitopes.find.byId(id).glycoctRings;
        structure = structure.replaceAll(" ", "\n\r");
        Logger.info("ct: " + structure);


        if(notation == null) {  notation = "cfgl";};
        String style = "compact";
        String format = "png";
        // parse sequence and create Sugar object


        //BuilderWorkspace workspace=new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());
        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        //Get a reference to the renderer
        GlycanRenderer renderer=workspace.getGlycanRenderer();

        //Get an instance of the GlycoCT Parser
        GlycoCTCondensedParser parser=new GlycoCTCondensedParser(false);


        List<Glycan> glycanList=new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "-2H";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan=parser.fromGlycoCTCondensed(structure.trim(), masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfgl"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("gs"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 1);

            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }
        //return byteArrayOutputStream.toByteArray();

    }

}


/*
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX wurcs: <http://www.glycoinfo.org/glyco/owl/wurcs#>
SELECT DISTINCT ?glycan ?c
# FROM <http://rdf.glycoinfo.org/wurcs/0.5.0>
# FROM <http://rdf.glycoinfo.org/wurcs/0.5.0/ms>
WHERE {
  ?glycan a 	glycan:glycosequence ;
	glycan:in_carbohydrate_format  glycan:carbohydrate_format_glycoct ;
	glycan:has_sequence
		?c filter(contains(?c, "RES\n1b:b-dglc-HEX-1")) .

  }
  ORDER BY ?glycan
limit 10
 */