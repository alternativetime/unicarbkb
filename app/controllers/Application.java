package controllers;

import com.google.inject.Inject;
import io.ebean.Ebean;
import io.ebean.SqlRow;
import models.*;
import models.composition_protein.CompRef;
import models.composition_protein.CompSite;
import models.composition_protein.CompTax;
import models.connected.PubMedRecord;
import models.connected.PubmedCatcher;
import models.rdf.RdfEpitopes;
import models.rdf.UniCarbkbEpitopes;
import models.unicorn.Olink;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;
import play.Logger;
import play.cache.Cached;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;
import views.html.*;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.*;


public class Application extends Controller{

    @Inject
    private FormFactory formFactory;

    //public Result changelog() { return ok (changes.render());}

    //public Result swaggerdoc() { return ok(swagger.render()) ; }

    public Result redirectDocs() {

        //return ok(swagger.render()) ;
        return redirect("/assets/lib/swagger-ui/index.html?url=/swagger.json");
    }



    public Result news() {
        return ok(
                news.render()
        );
    }

    /**
     * This result directly redirect to application home.
     */
    public Result GO_HOME = redirect(
            routes.Application.list2(0, "name", "asc", "", "")
    );

    public Result stref(Long id) {
        Streference display = Streference.find.byId(id);
        return ok(
                stref.ref().render(display)
        );
    }

    ;

    public Result enzymes() {

        List<Enzyme> enzyme = Enzyme.getEnzymes();
        return ok(
                enzymes.render(enzyme)
        );
    }

    ;

    public Result proteinsummary(String protein, String other) throws ServiceException {

        try {
            protein = URLDecoder.decode(protein, "UTF-8");
        } catch (Exception e) {

        }

        List<io.ebean.SqlRow> listSql = null;
        List<io.ebean.SqlRow> listSqlArray = null; // = new List<io.ebean.SqlRow>();
        List<io.ebean.SqlRow> biolSourceProteinsRaw = null;

        String proteinName = "";
        String swissProtName = null;
        String typeProteinEntry = "";
        ArrayList<Biolsource> biolSourceProtein = new ArrayList<Biolsource>();
        String accession = "";
        List<Proteins> proteins = null;
        List<String> uniprotDetails = new ArrayList<String>();
        List<SitesReferences> description = new ArrayList<SitesReferences>();
        List<GsProteinStr2> gsProteinSite = null;
        String sequenceRetrieval = "";
        String ptmComment = "";
        List<uk.ac.ebi.kraken.interfaces.uniprot.comments.TextOnlyComment> comment = null;
        List<Proteins> proteinMultiple = new ArrayList<Proteins>();
        List<GeneralSites> generalSites = null;
        List<DefinedSites> definedSites = null;

        List<Biolsource> biolSourceProteins = null;
        String warning = "";

        if (protein.matches("[A-Z][0-9].*")) {
            biolSourceProteins = Biolsource.findBiolSourceIdsUniProt(protein);
            if (biolSourceProteins.isEmpty()) {
                biolSourceProteins = Biolsource.findBiolSourceIdsUniProtMultiple(protein);
                warning = "GlycoSuiteDB records match multiple IDs containing " + protein;
            }
        } else {
            //biolSourceProteins = Biolsource.findBiolSourceIdsName(protein); //this is ok unless have proteins multiple times with no swiss acc number
            biolSourceProteinsRaw = Biolsource.findBiolSourceIdsRaw(protein);
        }

        List<Reference> referencesFound = new ArrayList<Reference>();
        HashSet referencesU = new HashSet();
        if (biolSourceProteinsRaw == null) {
            for (Biolsource biol : biolSourceProteins) {
                proteinName = biol.protein;
                swissProtName = biol.swiss_prot;
                Biolsource objectBiolSource = Ebean.find(Biolsource.class, biol.id);
                biolSourceProtein.add(objectBiolSource);
                listSqlArray = Sourceref.findReferenceSourceAnnotated(biol.id);
                for (io.ebean.SqlRow r : listSqlArray) {
                    Reference reference = Ebean.find(Reference.class, r.get("id").toString());
                    referencesFound.add(reference);
                }
                referencesU.addAll(referencesFound);
            }
        } else {
            for (SqlRow raw : biolSourceProteinsRaw) {
                String searchId = raw.get("id").toString();
                proteinName = raw.get("protein").toString();
                long l = Long.parseLong(searchId);
                listSqlArray = Sourceref.findReferenceSource(l, other);
                for (io.ebean.SqlRow r : listSqlArray) {
                    Reference reference = Ebean.find(Reference.class, r.get("id").toString());
                    referencesFound.add(reference);
                }
                referencesU.addAll(referencesFound);
            }
        }

        proteins = Proteins.findProteins(protein);
        if (proteins.isEmpty()) {
            proteins = Proteins.findProteinSwissProtMulti(protein);
        }

        //problems with legacy feature of and in the swiss prot names
        if (protein.matches("[A-Z][0-9].*")) {

            sequenceRetrieval = UniprotConnection.EntryRetrievalSequence(protein);
            ptmComment = UniprotConnection.FunctionCommentsEntryRetrievalSequence(protein);
            comment = UniprotConnection.PTMCommentsEntryRetrievalSequence(protein);

            generalSites = GeneralSites.findProteinsGeneral(protein);
            definedSites = DefinedSites.findProteinsDefined(protein);

            String[] splitProtein = protein.split("\\s*[and]+\\s*");
            System.out.println("this needs checking ----------------");
            proteinMultiple = Proteins.findProteinsSwissProt(protein);
            if (proteinMultiple.isEmpty()) {
                proteinMultiple = Proteins.findProteinSwissProtMulti(protein);
            }

            if (!proteinMultiple.isEmpty()) {
                //for(String partProtein : splitProtein) {
                //TODO
                // uniprotDetails = UniprotConnection.EntryRetrievalExample(protein);
                //sequenceRetrieval = UniprotConnection.EntryRetrievalSequence(protein);
            }

            //List<GsProteinSite> gsProteinSite = GsProteinSite.ProteinRetrieval(protein);
            gsProteinSite = GsProteinStr2.ProteinRetrieval(protein);
            //List<SitesReferences2> description = SitesReferences2.findSitesReferences(protein);
            description = SitesReferences.findSites(protein);

        }

        if (!protein.matches("[A-Z][0-9].*")) {
            //possible problem identified with species refinement
            generalSites = GeneralSites.findProteinsGeneralName(protein);
            definedSites = DefinedSites.findProteinsDefinedName(protein);

            proteinMultiple = Proteins.findProteinsName(protein);
            //typeProteinEntry = "not swiss prot";
        }

        if (protein.contains("and") || protein.contains("AND")) {
            typeProteinEntry = "not swiss prot";
        }

        if (swissProtName == null) {
            typeProteinEntry = "not swiss prot";
        }

        if (typeProteinEntry.isEmpty()) {
            protein = swissProtName;
        }

        System.out.println("Protein typeEntry " + typeProteinEntry + " or swiss " + swissProtName + " name " + protein);
        List<io.ebean.SqlRow> proteinTax = null;
        if (other.equals("annotated")) {
            proteinTax = Proteinsource.findProteinSourceAnnotated(protein);
        } else {
            proteinTax = Proteinsource.findProteinSource(protein, other);

        }




        HashSet<String> uniqueStructures = new HashSet<String>();

        for (Proteins p : proteinMultiple) {
            System.out.println("swiis_prot -----------" + p.swissProt);
            List<SqlRow> stproteins;
            if (!other.equals("annotated")) {
                stproteins = Proteins.findProteinsNameRaw(p.name);
            } else {
                stproteins = Proteins.findProteinsNameSummaryUniprot(p.swissProt); //(protein);
                Logger.info("---------- check query ---------");
            }
            for (SqlRow s : stproteins) {
                uniqueStructures.add(String.valueOf(s.getString("structure_id").toString()));
            }

        }

        HashSet<String> test = new HashSet();
        for (String s : uniqueStructures) {
            //test.add(Long.parseLong(s));
            test.add(s);
        }

        ArrayList<String> t2 = new ArrayList<String>();
        t2.addAll(test);

        List<CompSite> compSite= CompSite.compSite(protein);

        return ok(
                proteinsummary.render(warning, proteinName, protein, biolSourceProtein, proteins, uniprotDetails, gsProteinSite, referencesU, description, sequenceRetrieval, /*proteinMultiple, uniqueStructures,*/ t2, generalSites, definedSites, typeProteinEntry, swissProtName, proteinTax, compSite, ptmComment, comment)
        );

    }

    public Result compositions() {

        List<Structurecomp> compositionResult = null;
        String type = "";

        int e = 0;
        String[] list = new String[14];
        String[] l = new String[14];

        if (request().queryString().size() > 0) {
            Map<String, String[]> params = request().queryString();
            String[] searchTerms = null;

            String key = null;
            String out = "";
            String glycanType[] = new String[1];
            String g = "";
            for (Map.Entry<String, String[]> entry : params.entrySet()) {
                key = entry.getKey().toString();
                searchTerms = entry.getValue();

                if (key.equals("glycanType")) {
                    glycanType = searchTerms;
                }

                if (key.equals("comp_Hex")) {
                    list[0] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[0] = "Hexose: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_HexNAc")) {
                    list[1] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[1] = " HexNAc: " + s;
                            }
                        }
                    }

                }

                if (key.equals("comp_Deoxyhexose")) {
                    list[2] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[3] = " dHex: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_Pent")) {
                    list[5] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[5] = " Pentose: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_NeuAc")) {
                    list[3] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[3] = " NeuAc: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_NeuGc")) {
                    list[4] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[4] = " NeuGc: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_KDN")) {
                    list[8] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[8] = " KDN: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_HexA")) {
                    list[10] = Structure.buildComposition(searchTerms);

                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[10] = " HexA: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_Phos")) {
                    list[7] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[7] = " Phos: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_Sulph")) {
                    list[6] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[6] = " Sulph: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_methyl")) {
                    list[11] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[11] = " Methyl: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_acetyl")) {
                    list[12] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[12] = " Acetyl: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_other")) {
                    list[13] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[13] = " Other: " + s;
                            }
                        }
                    }
                }

                if (key.equals("comp_KDO")) {
                    list[9] = Structure.buildComposition(searchTerms);
                    if (searchTerms != null) {
                        for (String s : searchTerms) {
                            if (!s.isEmpty()) {
                                l[9] = " KDO: " + s;
                            }
                        }
                    }
                }

            }

            StringBuilder builder = new StringBuilder();
            String r = "";
            for (String s : list) {
                builder.append(s);
                r += s;
            }

            StringBuilder glycanT = new StringBuilder();
            for (String s : glycanType) {
                glycanT.append(s);
                g += s;
            }

            r = r.replace("null", "0");

            compositionResult = Structurecomp.findStructurecomp(r, g);

            return ok(
                    compositions.render(compositionResult, l)
            );
        }
        return ok(compositions.render(compositionResult, l));
    }

    public Result structureQuery() {
        return ok(structureQuery.render());
    }

    public Result query() {

        List<String> taxonomy = Taxonomy.findSpecies();
        HashSet taxUnique = Taxonomy.findSpeciesUnique();

        HashSet sourceUnique = Tissue.sourceSummary();
        HashSet tissueUnique = GlycobaseSource.tissueSummary();
        sourceUnique.addAll(tissueUnique);

        HashSet proteinUnique = Proteins.proteinSummary();
        //pull in the glycobase associated proteins
        HashSet pertubationUnique = GlycobaseSource.perturbationSummary();
        proteinUnique.addAll(pertubationUnique);

        HashSet proteinAccession = Proteins.proteinAccessionSummary();


        List<Tissue> foundTissue = null;
        List<GlycobaseSource> glycobasesource = null;
        List<GlycobaseSource> glycobaseFindPerturbation = null;

        List<Biolsource> biolsource = null;
        List<SqlRow> listSql = null;
        List<SqlRow> glycobaseSql = null;
        ArrayList<SqlRow> glycobaseSqlArray = new ArrayList<SqlRow>();

        List<SqlRow> glycobaseSqlTissue = null;
        ArrayList<SqlRow> glycobaseSqlArrayTissue = new ArrayList<SqlRow>();

        ArrayList<SqlRow> listSqlArray = new ArrayList<SqlRow>();
        Taxonomy taxonomyId = null;
        Proteins proteinId = null;
        Tissue tissueId = null;
        ArrayList<Taxonomy> taxonomyList = new ArrayList<Taxonomy>();
        ArrayList<Proteins> proteinList = new ArrayList<Proteins>();
        ArrayList<Tissue> tissueList = new ArrayList<Tissue>();
        List<String> listSql22 = new ArrayList<String>();
        List<List<String>> listSql2 = new ArrayList<List<String>>();
        String output = "";
        String outputtissue = "";
        String outputprotein = "";
        List<String> outputlist = new ArrayList<String>();
        ArrayList<Proteins> accSearch = new ArrayList<Proteins>();
        List<String> outputtissuelist = new ArrayList<String>();
        List<String> outputproteinlist = new ArrayList<String>();
        int countGlycobase = 0;
        int countTissueGlycobase = 0;
        int countProteinGlycobase = 0;

        if (request().queryString().size() > 0) {
            String glycobasePerturbationFind = "";
            Map<String, String[]> params = request().queryString();
            String[] searchTerms = null;
            String key = null;
            for (Map.Entry<String, String[]> entry : params.entrySet()) {
                key = entry.getKey();
                searchTerms = entry.getValue();
            }


            if (key.equals("swiss")) {
                for (String querySwiss : searchTerms) {
                    List<Proteins> proteinsTerm = Proteins.findProteinsSwissProt(querySwiss);
                    accSearch.addAll(proteinsTerm);
                }

            }

            if (key.equals("taxonomy")) {
                for (String queryTaxonomy : searchTerms) {

                    String glycobasePerturbation = queryTaxonomy;
                    output = GlycobaseSource.findGlycobaseTaxonomy(glycobasePerturbation);
                    outputlist.add(output);

                    List<Taxonomy> foundTaxonomy = Taxonomy.findSpeciesTemp(queryTaxonomy);
                    Long taxId = null;
                    for (Taxonomy tax : foundTaxonomy) {
                        taxId = tax.id;
                        taxonomyId = Taxonomy.find.byId(taxId);
                        taxonomyList.add(taxonomyId);
                        String taxon = taxonomyId.species;
                        biolsource = Biolsource.findTaxonomyProtein(taxon);
                        listSql = Biolsource.findTaxonomyProteinSQL(taxon);
                        listSql2 = Biolsource.findTaxonomyProteinString(taxon);
                    }
                }
            }

            if (key.equals("protein")) {
                for (String queryProtein : searchTerms) {

                    String glycobasePerturbation = queryProtein;

                    //glycobaseFindPerturbation = GlycobaseSource.findPerturbation(glycobasePerturbation);
                    outputprotein = GlycobaseSource.findPerturbation(glycobasePerturbation);
                    outputproteinlist.add(outputprotein);

                    List<Proteins> foundProteins = Proteins.findProteins(queryProtein);
                    Long protId = null;
                    for (Proteins protein : foundProteins) {
                        protId = protein.id;
                        //}
                        //if (protId > 0 && protId != null ) {
                        proteinId = Proteins.find.byId(protId);
                        proteinList.add(proteinId);
                    }
                }
            }

            if (key.equals("tissue")) {
                for (String queryTissue : searchTerms) {
                    foundTissue = Tissue.findTissue(queryTissue);
                    //glycobaseSqlTissue = GlycobaseSource.findGlycobaseTissue(queryTissue);
                    //glycobaseSqlArrayTissue.addAll(glycobaseSqlTissue);

                    outputtissue = GlycobaseSource.findGlycobaseTissue(queryTissue);
                    outputtissuelist.add(outputtissue);


                    Long tissId = null;
                    for (Tissue source : foundTissue) {
                        tissId = source.id;
                    }
                    if (tissId > 0) {
                        tissueId = Tissue.find.byId(tissId);
                        tissueList.add(tissueId);
                    }
                }
            }

            for (String xyz : outputlist) {
                if (xyz.length() > 10) {
                    countGlycobase++;
                }
            }

            for (String xyz : outputtissuelist) {
                if (xyz.length() > 10) {
                    countTissueGlycobase++;
                }
            }

            for (String xyz : outputproteinlist) {
                if (xyz.length() > 10) {
                    countProteinGlycobase++;
                }
            }


            return ok(query.render(taxonomy, taxonomyList, biolsource, listSql2, sourceUnique, proteinUnique, proteinList, tissueList, foundTissue, glycobaseFindPerturbation, /*glycobaseSqlArray, glycobaseSqlArrayTissue,*/ outputlist, countGlycobase, outputtissuelist, countTissueGlycobase, outputproteinlist, countProteinGlycobase, proteinAccession, accSearch));
        }

        return ok(query.render(taxonomy, taxonomyList, biolsource, listSql2, sourceUnique, proteinUnique, proteinList, tissueList, foundTissue, glycobaseFindPerturbation, /* glycobaseSqlArray, glycobaseSqlArrayTissue,*/ outputlist, countGlycobase, outputtissuelist, countTissueGlycobase, outputproteinlist, countProteinGlycobase, proteinAccession, accSearch));
    }


    public Result structureDetails(Long id) {

        List<Structure> strDisplay = Structure.findStructureRef(id);
        Structure strInfo = Structure.find.byId(id);
        ArrayList proteinNames = new ArrayList();
        ArrayList proteinIds = new ArrayList();
        ArrayList proteinItems = new ArrayList();
        HashSet proteinNamesUnique = new HashSet();
        ArrayList taxNames = new ArrayList();
        //ArrayList taxItems = new ArrayList();
        HashSet taxItems = new HashSet();
        ArrayList sourceNames = new ArrayList();
        //ArrayList sourceItems = new ArrayList();
        HashSet sourceItems = new HashSet();
        HashSet sourceNamesUnique = new HashSet();
        ArrayList uniprot = new ArrayList();
        HashSet uniprotUnique = new HashSet();
        ArrayList taxIds = new ArrayList();
        HashSet taxIdsUnique = new HashSet();
        HashSet taxDivsUnique = new HashSet();

        List<String[]> rowList = new ArrayList<String[]>();
        Map<String, Integer> m = new HashMap<String, Integer>();
        String compositionId = "";
        String type = "";
        String pubchemId = "";

        if (strDisplay != null) {
            for (Structure entries : strDisplay) {
                List<Stproteins> stToProtein = entries.stproteins;
                List<Strtaxonomy> stToTax = entries.strtaxonomy;
                List<Stsource> stToSource = entries.stsource;
                List<Pubchem> pubchem = entries.pubchem;
                type = entries.type;

                compositionId = entries.compositionId;

                if (!pubchem.isEmpty()) {
                    for (Pubchem c : pubchem) {
                        pubchemId = c.pubchem_id;
                    }
                }

                if (!stToProtein.isEmpty()) {
                    for (Stproteins stProteinEntry : stToProtein) {

                        if (stProteinEntry.proteins != null) {
                            proteinNames.add(stProteinEntry.proteins.name);
                            proteinItems.add(stProteinEntry.proteins);
                            proteinNamesUnique.add(stProteinEntry.proteins);
                        }
                        if (stProteinEntry.proteins == null) {

                        }
                    }
                }

                if (!stToTax.isEmpty()) {
                    for (Strtaxonomy stTaxEntry : stToTax) {
                        taxNames.add(stTaxEntry.taxonomy.species);
                        taxItems.add(stTaxEntry.taxonomy);
                    }
                }

                if (!stToSource.isEmpty()) {
                    for (Stsource stSourceEntry : stToSource) {
                        String div = "<a href=\"../taxonomy/" + stSourceEntry.tissue.id + "\"> > " + stSourceEntry.tissue.div1 + " > " + stSourceEntry.tissue.div2 + " > " + stSourceEntry.tissue.div3 + " > " + stSourceEntry.tissue.div4 + "</a> <br />";
                        sourceNamesUnique.add(div);
                        sourceNames.add(stSourceEntry.tissue.id);
                        sourceItems.add(stSourceEntry.tissue);
                    }
                }
            }
        }

       List<Composition> compositions = new ArrayList<Composition>();
        if(compositionId.matches("to be set")) {
            compositions = Composition.findCompositionDetails(compositionId.trim());
        }


		/*Should make this an ajax call */
        String reader = "";
        try {
            reader = Structure.getJSON(id);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        try rdf glytoucan call
         */

        //ResultSet glytoucanRDF = GlytoucanRDF.glytoucanCTSearch(strInfo);

        return ok(
                structureDetails.render(strInfo, type, compositions, strDisplay, id, proteinNames, proteinNamesUnique, sourceNames, sourceItems, rowList, uniprot, taxItems, taxNames, pubchemId, reader)
        );
    }

    ;

    public Result refdisplay(Long id) {
        //Reference displayReference = null;
        Reference displayReferencce = Reference.find.byId(id);
        List<Reference> t = Reference.findJournal(id);
        List<Method> m = Method.findmethod(id);
        List<Reference> u = Reference.findRefMethods(id);
        List<Reference> refmethodgp = Reference.findRefMethodsgp(id);
        List<CompRef> compRefs = CompRef.findCompRefs(id);
        String abstractPMID = "";

/*HERE*/

        int pmid = Integer.parseInt(displayReferencce.pmid);
        PubmedCatcher fetcher = new PubmedCatcher();
        List<Integer> pubmedIDs = null;


        try {
            pubmedIDs.add(pmid);

        } catch (Exception e) { e.printStackTrace();}

        List<PubMedRecord> records;

        try {
            records = fetcher.getPubMedRecordForIDs(pmid);
            abstractPMID = records.toString().replaceAll("null", "").replaceAll("\t", "").replaceAll("\\[", "").replaceAll("\\]", "");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

		/*
		for (Reference r : t ) {
			List<Sourceref> biol = r.sourceref;		
		} */

        ArrayList taxsources = new ArrayList();
        ArrayList proteinsources = new ArrayList();
        ArrayList protsources = new ArrayList();
        HashSet hs = new HashSet();
        HashSet<Biolsource> proteinHs = new HashSet<Biolsource>();
        HashSet swissHs = new HashSet();

        for (Reference taxfind : u) {
            List<Sourceref> source = taxfind.sourceref;
            for (Sourceref tax : source) {
                hs.add(tax.biolsource.taxonomy);
                proteinHs.add(tax.biolsource); //.protein);
                swissHs.add(tax.biolsource.swiss_prot);
            }
        }

        taxsources.addAll(hs);
        proteinsources.addAll(proteinHs);
        protsources.addAll(swissHs);

        return ok(
                refdisplay.render("View selected reference", t, u, taxsources, proteinsources, protsources, refmethodgp, compRefs, abstractPMID)
        );
    }

    ;


    @Cached(key = "index", duration = 86400)
    public Result index() {
        //return GO_HOME;
        //Cache.set("format", "gs", 0);
        session("notation", "cfgl");
        return ok(index.render());
    }

    public Result about() {
        return ok(about.render());
    }

    public Result builder() {
        return ok(builder.render());
    }

    public Result builderDigest() {
        return ok(builderDigest.render());
    }

    /*public Result login() {
        return ok(login.render(form(Login.class)));
    }*/

    public Result workflows() {
        return ok(workflows.render());
    }

    @Security.Authenticated(Secured.class)
    public Result massspec() {
        System.out.println("message " + request().username());
        return ok(mass_spec.render());
    }



	/*public Result saySearch(String structure) {
	try{
	String result = URLDecoder.decode(structure, "UTF-8");
	System.out.println("string test; " + result);
	} catch (UnsupportedEncodingException e) {
        e.printStackTrace();
        }

        controllers.Search.searchStructure();
	//return ok (saysearch.render() );
	return ok ( saySearch.render() );
	}*/

    public Result ms() {
        List<SqlRow> results = Lcmucin.groupLcGlycans();
        return ok(ms.render());
    }

    /**
     * Display the taxon search results.
     *
     * @param taxon Search taxon string
     */
    public Result taxonDetails(Long id) {
        Taxonomy taxonomy = Taxonomy.find.byId(id);
        List<CompTax> compTax = CompTax.findCompTax(id);
        String taxon = taxonomy.species;
        List<Biolsource> biolsource = Biolsource.findTaxonomyProtein(taxon);
        List<io.ebean.SqlRow> listSql = Biolsource.findTaxonomyProteinSQL(taxon);

        //return TODO;
        return ok(
                taxonDetails.render("Taxonomy Description", taxonomy, biolsource, listSql, compTax)
        );
    }

    public Result taxonsearch(String findTaxon) {
        List<Taxonomy> foundTaxonomy = Taxonomy.findSpeciesTemp(findTaxon);
        Long taxId = null;
        for (Taxonomy tax : foundTaxonomy) {
            taxId = tax.id;
        }
        ;
        if (taxId > 0) {
            Taxonomy taxonomy = Taxonomy.find.byId(taxId);

            String taxon = taxonomy.species;
            List<Biolsource> biolsource = Biolsource.findTaxonomyProtein(taxon);
            List<SqlRow> listSql = Biolsource.findTaxonomyProteinSQL(taxon);

            List<CompTax> compTax = CompTax.findCompTax(taxId);

            //return TODO;
            return ok(
                    taxonDetails.render("Taxonomy Description", taxonomy, biolsource, listSql, compTax));
        } else {
            return TODO;
        }
    }


    /**
     * Display the paginated list of computers.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied
     */
    public Result list2(int page, String sortBy, String order, String filter, String protein) {
        return ok(
                list.render(
                        Reference.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    /**
     * Display the paginated list of unicarbreferences.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied on computer names
     */
    public Result unicarb(int page, String sortBy, String order, String filter) {
        return ok(
                unicarb.render(
                        Unicarbdbreference.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result findAllSpecies() {
        List<String> speciesCollection = Taxonomy.findSpecies();

        //AString target = "[ ";
        String target = "";
        for (String species : speciesCollection) {
            //target += "\"" + species + "\", ";
            target += "replacethis" + species + "replacethis, ";
        }
        String endTarget = ""; // " ]";
        String finalTarget = target.concat(endTarget).toString();
        return ok(
                species.render("Display Species", speciesCollection, finalTarget)
        );
    }


    public Result tissueSummary(Long id) {

        Tissue tissueFind = Ebean.find(Tissue.class, id);

        String databaseReference = tissueFind.div1; // + tissueFind.div2 + tissueFind.div3 + tissueFind.div4;

        if(StringUtils.isNotBlank(tissueFind.div2)){
            databaseReference = databaseReference + tissueFind.div2;
        }
        if(StringUtils.isNotBlank(tissueFind.div3)){
            databaseReference = databaseReference + tissueFind.div3;
        }
        if(StringUtils.isNotBlank(tissueFind.div4)){
            databaseReference = databaseReference + tissueFind.div4;
        }


        ArrayList taxNames = new ArrayList();
        ArrayList taxItems = new ArrayList();
        ArrayList taxIds = new ArrayList();
        ArrayList proteinNames = new ArrayList();
        ArrayList proteinIds = new ArrayList();
        ArrayList proteinItems = new ArrayList();
        ArrayList sourceNames = new ArrayList();
        ArrayList sourceItems = new ArrayList();
        ArrayList structureTest = new ArrayList();
        HashSet structureTesta = new HashSet();
        HashSet structureTax = new HashSet();
        //Set<String> ordertax = new TreeSet<String>(); //order tax
        TreeMap ordertax = new TreeMap();


        List<io.ebean.SqlRow> listSql2 = Tissue.findTissueStructures(id);
        for (io.ebean.SqlRow l : listSql2) {

            structureTest.add(l.getLong("structure_id"));
            for (Object ll : structureTest) {
                structureTesta.add(ll.toString());
            }
        }

        for (Object a : structureTesta) {
            String aa = a.toString();
            Long look = Long.valueOf(aa);
            Structure objectStr = Ebean.find(Structure.class, look);

            List<Strtaxonomy> tax = objectStr.strtaxonomy;
            for (Strtaxonomy t : tax) {
                structureTax.add(t.species);
                //ordertax.add(t.species.toString());
                ordertax.put(t.species, t.species);

            }

        }


        return ok(
                tissuesummary.render(tissueFind, structureTesta, ordertax, "Tissue Summary", databaseReference, taxNames, taxItems, proteinNames, proteinItems, sourceNames, sourceItems)
        );
    }


    public Result format(String notation) {
        session("notation", notation);
        String refererUrl = request().getHeader("referer");
        return redirect(refererUrl);
    }

    /*public Result GetStructure(String ct ) throws Exception {
    }*/


    public Result refreshCTTable() throws Exception {

        List<Olink> olinks = Olink.find.all();
        for(Olink o : olinks){
            GlycansCTCombined glycansCTCombined = new GlycansCTCombined();
            glycansCTCombined.ct = o.glycoct;
            glycansCTCombined.database = "tcd";
            glycansCTCombined.databaseId = o.id;
            glycansCTCombined.save();
        }

        /*List<Nlink> nlinks = Nlink.find.all();
        for(Nlink n : nlinks){
            GlycansCTCombined glycansCTCombined = new GlycansCTCombined();
            glycansCTCombined.ct = n.glycoct;
            glycansCTCombined.database = "unicorn";
            glycansCTCombined.databaseId = n.id;
            glycansCTCombined.save();
        }*/

        /*List<Hmg> hmgs = Hmg.find.all();
        for(Hmg h : hmgs){

            GlycansCTCombined glycansCTCombined = new GlycansCTCombined();
            glycansCTCombined.ct = h.glycoct;
            glycansCTCombined.database = "hmg";
            glycansCTCombined.databaseId = h.id;
            glycansCTCombined.save();
        }*/

        /*List<Structure> structures = Structure.find.all();
        //MassOptions massOptions = new MassOptions();
        //massOptions.setDerivatization(MassOptions.NO_DERIVATIZATION);
        String notation = "cfgl";
        for(Structure structure : structures) {
            if(structure.glycanst.contains("--") && !structure.glycanst.contains("freeEnd--??1L-Fuc")) {
                Logger.info("this structure id: " + structure.id);

                BuilderWorkspace workspace;
                workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


                workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

                // GlycanRenderer renderer = workspace.getGlycanRenderer();

                //Get an instance of the GlycoCT Parser
                GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);


                String style = "extended";
                String format = "png";


                if (structure.glycanst.contains("--") && structure.id != 8105  && structure.id != 8081 && structure.id != 8047 && structure.id != 8067 && structure.id != 7651 && structure.id != 8058 && structure.id != 7815 &&  !structure.glycanst.contains("000")) {
                    String str = structure.glycanst;

                    if (str.contains("FreenEnd")) {
                        str = str.replaceAll("FreenEnd", "FreeEnd");
                        Structure structureReplace = Structure.find.byId(structure.id);
                        structureReplace.glycanst = str;
                        structureReplace.update();

                    }

                    if (str.contains("FreeEnd?b1D")) {
                        continue;
                        /*str = str.replaceAll("FreeEnd?b1D", "FreeEnd--?b1D");
                        Structure structureReplace = Structure.find.byId(structure.id);
                        structureReplace.glycanst = str;
                        structureReplace.update();*/
                    /*}

                    if (notation.matches("cfgl")) {
                        notation = "cfg";
                    }
                    if (notation.matches("uoxf")) {
                        notation = "uoxf";
                    }
                    if (notation.matches("gs")) {
                        notation = "iupac";
                    }

                    // parse sequence and create Sugar object


                    List<Glycan> glycanList = new ArrayList<Glycan>();

                    String op = MassOptions.NO_DERIVATIZATION;

                    String cloud = "-2H";


                    MassOptions masso = new MassOptions();
                    IonCloud ic = new IonCloud(cloud);
                    //ic.add("-", 1);

                    masso.ION_CLOUD.clear();
                    masso.setDerivatization(op);

                    String redEnd = "redEnd";//"redEnd";
                    masso.setReducingEndTypeString(redEnd);


                    //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
                    //Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(str);


                    if(glycan.toGlycoCTCondensed().toString() != null) {
                        GlycansCTCombined glycansCt = new GlycansCTCombined();
                        glycansCt.ct = glycan.toGlycoCTCondensed().toString();
                        glycansCt.database = "kb";
                        glycansCt.databaseId = structure.id;
                        glycansCt.save();
                        //glycansCt.update();

                    }
                } else{

                    IupacParser p = new IupacParser(structure.glycanst.trim());
                    //p.setGlycanType("O-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)
                    try {
                        p.getCtTree(p.parse());
                        GlycansCTCombined glycansCt = new GlycansCTCombined();
                        glycansCt.ct = p.getCtSequence();
                        glycansCt.database = "kb";
                        glycansCt.databaseId = structure.id;
                        glycansCt.save();
                    } catch (Exception ex) {
                        System.err.println("Problem parsing the sequence");
                        ex.printStackTrace();
                    }
                }
            }
        }*/
    return ok("done");
    }

    public Result searchCT(){
        List<GlycansCTCombined> glycansCTCombined = null;
        return ok(views.html.searchCT.searchCT.render(glycansCTCombined));
    }
    public Result searchCTResults(){
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String ct = requestData.get("glycoct");
        List<GlycansCTCombined> glycansCTCombined = null;
        //List<GlycansCTCombined> glycansCTCombined = new ArrayList<>();
        ct = ct.trim().replaceAll("(\\r|\\n)", "").replaceAll("\r", "");

        List<GlycansCTCombined> glycansCTCombinedList;
        glycansCTCombinedList = GlycansCTCombined.matchCT(ct + " ");

        return ok(views.html.searchCT.searchCT.render(glycansCTCombinedList));
    }

    /*
    * Show structures listed in the rdf eptiope table
    *
     */

    public Result ShowStructuresEpitope(int page, String sortBy, String order, int filter, String id) {
        String hello = "hello";
        RdfEpitopes rdfEpitopes = RdfEpitopes.find.byId(Long.valueOf(filter));


        return ok(
                views.html.epitopes.epitopeStructure.render(
                        UniCarbkbEpitopes.page(page, 5, sortBy, filter),
                        sortBy, order, filter, rdfEpitopes
                )
        );
    }
}

