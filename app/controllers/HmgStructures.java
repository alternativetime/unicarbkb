package controllers;

import com.google.inject.Inject;
import models.hmg.Hmg;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by matthew on 29/05/15.
 */
public class HmgStructures extends Controller {

    @Inject
    private FormFactory formFactory;

    public Result showMilkStructures() throws Exception {
        return ok(views.html.hmg.showMilkPublishedStructures.render(Hmg.getMilkStructures(), "temp"));
    }

    public  CompletionStage< Result> searchHmgRDF(Long id) {

        Hmg hmgstr = Hmg.find.byId(id);
        //int count = Hmg.searchRDF(id);
        //TODO keep track below

        CompletionStage<List<String>> promiseOfInt = CompletableFuture.supplyAsync(
                () -> Hmg.searchRDF(id)
        );
        return promiseOfInt.thenApplyAsync(
                i -> ok(views.html.hmg.hmgStructure.render(hmgstr, promiseOfInt ))
        );

    }

    public Result searchCompositionHmg() {
        String iupac = "";
        double mass = 0.0;
        int gal = 0;
        int fuc = 0;
        int kdn = 0;
        int glc = 0;
        int glcnac = 0;
        int galnac = 0;

        int gal2 = 0;
        int fuc2 = 0;
        int kdn2 = 0;
        int glc2 = 0;
        int glcnac2 = 0;
        int galnac2 = 0;

        String se_gal = "";
        String se_fuc = "";
        String se_kdn = "";
        String se_glc = "";
        String se_glcnac = "";
        String se_galnac = "";

        DynamicForm requestData = formFactory.form().bindFromRequest();

        iupac = requestData.get("IUPAC");
        mass = Double.valueOf(requestData.get("mass"));

        gal = Integer.parseInt(requestData.get("comp_Gal"));
        fuc = Integer.parseInt(requestData.get("comp_Fuc"));
        kdn = Integer.parseInt(requestData.get("comp_Kdn"));
        glc = Integer.parseInt(requestData.get("comp_Glc"));
        glcnac = Integer.parseInt(requestData.get("comp_Glcnac"));
        /* galnac = Integer.parseInt(requestData.get("comp_Galnac")); */

        gal2 = Integer.parseInt(requestData.get("comp_Gal2"));
        fuc2 = Integer.parseInt(requestData.get("comp_Fuc2"));
        kdn2 = Integer.parseInt(requestData.get("comp_Kdn2"));
        glc2 = Integer.parseInt(requestData.get("comp_Glc2"));
        glcnac2 = Integer.parseInt(requestData.get("comp_Glcnac2"));
        /* galnac2 = Integer.parseInt(requestData.get("comp_Galnac2")); */

        se_gal = String.valueOf(requestData.get("sel_gal"));
        se_fuc = String.valueOf(requestData.get("sel_fuc"));
        se_kdn = String.valueOf(requestData.get("sel_kdn"));
        se_glc = String.valueOf(requestData.get("sel_glc"));
        se_glcnac = String.valueOf(requestData.get("sel_glcnac"));
        /* se_galnac = String.valueOf(requestData.get("sel_galnac")); */

        List<Hmg> hmgComp = null;

        int count = 0;
        if (iupac.length()>1 && mass <= 0.0) {
            count = Hmg.iupacSearchCount(iupac);
            hmgComp = Hmg.iupacSearch(iupac, count);
        } else if (iupac.length()<1 && mass <= 0.0  ) {
            count = Hmg.getCompositionMatch2Count(gal, fuc, glc, glcnac, galnac, kdn, gal2, fuc2, glc2, glcnac2, galnac2, kdn2, se_gal, se_fuc, se_glc, se_glcnac, se_galnac, se_kdn);
            hmgComp = Hmg.getCompositionMatch2(count, gal, fuc, glc, glcnac, galnac, kdn, gal2, fuc2, glc2, glcnac2, galnac2, kdn2, se_gal, se_fuc, se_glc, se_glcnac, se_galnac, se_kdn);
        } else if (iupac.length()<1 && mass > 0.0){
            hmgComp = Hmg.massSearch(mass);
        }

        return ok(views.html.hmg.searchHmgCompositions.render(hmgComp, count));
    }
}
