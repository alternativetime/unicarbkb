package controllers;

import models.cell_lines.*;
import models.composition_protein.Crcdata;
import models.composition_protein.Crcdatajenny;
import play.mvc.Result;
import views.html.news;

import java.util.ArrayList;
import java.util.List;

import static play.mvc.Http.Context.Implicit.request;
import static play.mvc.Results.ok;


/**
 * Created by matthew on 13/01/2016.
 */
public class Crc {

    /*public Result showCellLine(Long id) {

        if (id == 1) {
            //List<c10> c10List = c10.find.all();
            return ok(views.html.crc.holst.c10.render(c10.find.all()));
        } else if (id == 2) {
            return ok(views.html.crc.holst.CaCo2.render(CaCo2.find.all()));
        } else if (id == 3) {
            return ok(views.html.crc.holst.CLL218.render(CLL218.find.all()));
        } else if (id == 4) {
            return ok(views.html.crc.holst.Co115.render(Co115.find.all()));
        } else if (id == 5) {
            return ok(views.html.crc.holst.Col205.render(Col205.find.all()));
        } else if (id == 6) {
            return ok(views.html.crc.holst.Colo320.render(Colo320.find.all()));
        } else if (id == 7) {
            return ok(views.html.crc.holst.Dld.render(Dld.find.all()));
        } else if (id == 8) {
            return ok(views.html.crc.holst.Hct8.render(Hct8.find.all()));
        } else if (id == 9) {
            return ok(views.html.crc.holst.Hct15.render(Hct15.find.all()));
        } else if (id == 10) {
            return ok(views.html.crc.holst.hct116.render(hct116.find.all()));
        } else if (id == 11) {
            return ok(views.html.crc.holst.Hp.render(Hp.find.all()));
        } else if (id == 12) {
            return ok(views.html.crc.holst.Ht29.render(Ht29.find.all()));
        } else if (id == 13) {
            return ok(views.html.crc.holst.Km12.render(Km12.find.all()));
        } else if (id == 14) {
            return ok(views.html.crc.holst.Lovo.render(Lovo.find.all()));
        } else if (id == 15) {
            return ok(views.html.crc.holst.Ls174t.render(Ls174t.find.all()));
        } else if (id == 16) {
            return ok(views.html.crc.holst.Ls180.render(Ls180.find.all()));
        } else if (id == 17) {
            return ok(views.html.crc.holst.Ls411n.render(Ls411n.find.all()));
        } else if (id == 18) {
            return ok(views.html.crc.holst.Rko.render(Rko.find.all()));
        } else if (id == 19) {
            return ok(views.html.crc.holst.Sw48.render(Sw48.find.all()));
        } else if (id == 20) {
            return ok(views.html.crc.holst.Sw480.render(Sw480.find.all()));
        } else if (id == 21) {
            return ok(views.html.crc.holst.Sw620.render(Sw620.find.all()));
        } else if (id == 22) {
            return ok(views.html.crc.holst.Sw948.render(Sw948.find.all()));
        } else if (id == 23) {
            return ok(views.html.crc.holst.Sw1116.render(Sw1116.find.all()));
        } else if (id == 24) {
            return ok(views.html.crc.holst.Sw1398.render(Sw1398.find.all()));
        } else if (id == 25) {
            return ok(views.html.crc.holst.Sw1463.render(Sw1463.find.all()));
        } else if (id == 25) {
            return ok(views.html.crc.holst.T84.render(T84.find.all()));
        }
        return ok("nothing");
        //return ok (views.html.crc.holst.render());
    }*/

    public Result showSummaryHolst() {
        return ok(views.html.crc.holst.crcsummary.render());
    }


    public Result showC10(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.c10data.render(
                        c10.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showCaco2(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.caco2data.render(
                        CaCo2.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showCll218(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.CLL218data.render(
                        CLL218.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showC0115(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Co115data.render(
                        Co115.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showColo320(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Colo320data.render(
                        Colo320.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showCol205(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Col205data.render(
                        Col205.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showDld(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Dlddata.render(
                        Dld.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showHct8(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Hct8data.render(
                        Hct8.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showHct15(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Hct15data.render(
                        Hct15.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showHct116(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.hct116data.render(
                        hct116.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showHp(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Hpdata.render(
                        Hp.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }
    public Result showHt29(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Ht29data.render(
                        Ht29.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }
    public Result showKm12(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Km12data.render(
                        Km12.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showLovo(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Lovodata.render(
                        Lovo.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showLs174t(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Ls174tdata.render(
                        Ls174t.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showLs180(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Ls180data.render(
                        Ls180.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showLs411n(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Ls411ndata.render(
                        Ls411n.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showRko(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Rkodata.render(
                        Rko.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw48(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw48data.render(
                        Sw48.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw480(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw480data.render(
                        Sw480.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw620(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw620data.render(
                        Sw620.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw948(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw948data.render(
                        Sw948.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw1116(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw1116data.render(
                        Sw1116.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw1398(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw1398data.render(
                        Sw1398.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSw1463(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.Sw1463data.render(
                        Sw1463.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showT84(int page, String sortBy, String order, String filter){
        return ok(
                views.html.crc.holst.T84data.render(
                        T84.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }


    public Result compositionSearchCrc(){

        if(request().queryString().size() > 0) {
            int hex = 0;
            int hexnac = 0;
            int dhex = 0;
            int neunac = 0;

            hex = Integer.parseInt(request().getQueryString("hex"));
            hexnac = Integer.parseInt(request().getQueryString("hexnac"));
            dhex = Integer.parseInt(request().getQueryString("dhex"));
            neunac = Integer.parseInt(request().getQueryString("neunac"));


            String unicarbkbComposition = "/compositions?glycanType=N-Linked&comp_Hex=" + hex + "&comp_HexNAc=" + hexnac + "&comp_Deoxyhexose=" + dhex + "&comp_NeuAc=" + neunac + "&comp_NeuGc=&comp_Pent=&comp_Sulph=&comp_Phos=&comp_KDN=&comp_KDO=&comp_HexA=&comp_methyl=&comp_acetyl=&comp_other=";


            List<Crcdatajenny> crcdatas = new ArrayList<>();


            crcdatas = Crcdatajenny.getMatchingCompositionsAll(hex, hexnac, dhex, neunac);

            return ok(views.html.cell_lines.crcComposition.render(crcdatas));
        }

        return ok( news.render());

    }

    public Result compositionSearchCrc2(){

        if(request().queryString().size() > 0) {
            int hex = 0;
            int hexnac = 0;
            int dhex = 0;
            int neunac = 0;

            hex = Integer.parseInt(request().getQueryString("hex"));
            hexnac = Integer.parseInt(request().getQueryString("hexnac"));
            dhex = Integer.parseInt(request().getQueryString("dhex"));
            neunac = Integer.parseInt(request().getQueryString("neunac"));

            String unicarbkbComposition = "/compositions?glycanType=N-Linked&comp_Hex=" + hex + "&comp_HexNAc=" + hexnac + "&comp_Deoxyhexose=" + dhex + "&comp_NeuAc=" + neunac + "&comp_NeuGc=&comp_Pent=&comp_Sulph=&comp_Phos=&comp_KDN=&comp_KDO=&comp_HexA=&comp_methyl=&comp_acetyl=&comp_other=";


            List<Crcdata> crcdatas = new ArrayList<>();


            crcdatas = Crcdata.getMatchingCompositionsAll(hex, hexnac, dhex, neunac);

            return ok(views.html.cell_lines.crcComposition2.render(crcdatas));
        }

        return ok( news.render());

    }



}
