package controllers;


import models.Reference;
import models.Strproteintaxdisease;
import models.disease.DiseaseOnto;
import org.apache.jena.query.*;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by matthew on 26/10/15.
 */
public class DiseaseOntology extends Controller {

    public Result showDiseases(int page, String sortBy, String order, String filter){

        System.out.println("filter " + filter);
        List<DiseaseOnto> diseases = DiseaseOnto.find.all();

        return ok(
                views.html.diseaseOntology.showDiseases.render(
                        DiseaseOnto.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter, diseases
                )
        );
    }


    public Result diseaseSearch(long id) {


        String doid = String.valueOf(id);
        String queryString = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
                "\n" +
                "select ?p ?o \n" +
                "from <http://purl.obolibrary.org/obo/merged/DOID>\n" +
                "\n" +
                "WHERE {\n" +
                "   <http://purl.obolibrary.org/obo/DOID_" + doid + "> ?p ?o\n" +
                "}";

        System.out.println("checking query string: " + queryString);
        Query query = QueryFactory.create(queryString);
        QueryExecution qExe = QueryExecutionFactory.sparqlService("http://sparql.hegroup.org/sparql/", query);
        ResultSet results = qExe.execSelect();
        //ResultSetFormatter.out(System.out, results, query) ;


        List<String> dbXref = new ArrayList<>();
        String iao = null;
        List<String> exactSynonym = new ArrayList<>();
        List<String> alternativeId = new ArrayList<>();
        String diseaseLabel = null;

        while(results.hasNext()) {
            QuerySolution querySolution = results.nextSolution();

            if (querySolution.get("p").toString().matches("http://www.w3.org/2000/01/rdf-schema#label")) {
                diseaseLabel = querySolution.get("o").toString();
            }

            if (querySolution.get("p").toString().matches("http://www.geneontology.org/formats/oboInOwl#hasDbXref")) {
                //System.out.println(querySolution.get("p").toString() + "   " + querySolution.get("o").toString());
                dbXref.add(querySolution.get("o").toString());
            }


            if (querySolution.get("p").toString().matches("http://purl.obolibrary.org/obo/IAO_0000115")) {
                //System.out.println(querySolution.get("p").toString() + "   " + querySolution.get("o").toString());
                iao = querySolution.get("o").toString();
            }

            if (querySolution.get("p").toString().matches("http://www.geneontology.org/formats/oboInOwl#hasExactSynonym")) {
                //System.out.println(querySolution.get("p").toString() + "   " + querySolution.get("o").toString());
                exactSynonym.add(querySolution.get("o").toString());
            }

            if (querySolution.get("p").toString().matches("http://www.geneontology.org/formats/oboInOwl#hasAlternativeId")) {
                //System.out.println(querySolution.get("p").toString() + "   " + querySolution.get("o").toString());
                alternativeId.add(querySolution.get("o").toString());
            }
        }

        /*String queryString2 = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
                "\n" +
                "select ?o\n" +
                "from <http://purl.obolibrary.org/obo/merged/DOID>\n" +
                "\n" +
                "WHERE {\n" +
                "   <http://purl.obolibrary.org/obo/DOID_1485> <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?o\n" +
                "}";

        System.out.println("String: " + queryString2);
        Query query2 = QueryFactory.create(queryString2);
        QueryExecution qExe2 = org.apache.jena.query.QueryExecutionFactory.sparqlService("http://sparql.hegroup.org/sparql/", query2);
        ResultSet results2 = qExe2.execSelect();
        //ResultSetFormatter.out(System.out, results2, query2) ;


        while(results2.hasNext()){
            QuerySolution querySolution2 = results2.nextSolution();
            String subclass = querySolution2.get("o").toString();
        }

        String queryString3 = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
                "\n" +
                "select ?o\n" +
                "from <http://purl.obolibrary.org/obo/merged/DOID>\n" +
                "\n" +
                "WHERE {\n" +
                "    <http://purl.obolibrary.org/obo/DOID_0050737>  ?p ?o\n" +
                "}";

        Query query3 = QueryFactory.create(queryString3);
        QueryExecution qExe3 = QueryExecutionFactory.sparqlService("http://sparql.hegroup.org/sparql/", query2);
        ResultSet results3 = qExe3.execSelect();
        //ResultSetFormatter.out(System.out, results3, query3) ;


        while(results3.hasNext()){
            QuerySolution querySolution3 = results3.nextSolution();
            //String subclass = querySolution3.get("o").toString();
        }
*/


        List<DiseaseOnto> diseaseOnto = DiseaseOnto.getDiseaseInfo(Integer.parseInt(doid));
        HashSet<Reference> references = new HashSet<>();
        //HashSet<Strproteintaxdisease> strproteintaxdiseases = new HashSet<>();
        for(DiseaseOnto disease : diseaseOnto){
            List<Strproteintaxdisease> str = disease.strproteintaxdiseaseList;
            for(Strproteintaxdisease ll : str){
                references.add(ll.reference);
            }
        }

        return ok(views.html.diseaseOntology.diseaseDescription.render(references, id, dbXref, iao, exactSynonym, alternativeId, diseaseLabel, diseaseOnto));
    }


}
