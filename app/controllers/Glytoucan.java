package controllers;

import play.mvc.Controller;

/**
 * Created by matthew on 10/09/15.
 */
public class Glytoucan extends Controller {


}

//http://test.ts.glytoucan.org/sparql?default-graph-uri=&query=PREFIX+dc%3A+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%3E%0D%0APREFIX+rdfs%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0D%0APREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+bibo%3A+%3Chttp%3A%2F%2Fpurl.org%2Fontology%2Fbibo%2F%3E%0D%0APREFIX+owl%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2002%2F07%2Fowl%23%3E%0D%0APREFIX+xsd%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema%23%3E%0D%0APREFIX+rdf%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%3E%0D%0APREFIX+faldo%3A+%3Chttp%3A%2F%2Fwww.biohackathon.org%2Fresource%2Ffaldo%2F%3E%0D%0APREFIX+glyco%3A+%3Chttp%3A%2F%2Fpurl.jp%2Fbio%2F12%2Fglyco%2Fglycan%2F%3E%0D%0APREFIX+dcterms%3A+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0APREFIX+uniprot%3A+%3Chttp%3A%2F%2Fpurl.uniprot.org%2Fcore%2F%3E%0D%0A%0D%0ASELECT+DISTINCT++%3Fgseq+%0D%0AWHERE%0D%0A++%7B+%3Fglycan+glycan%3Ahas_glycosequence+%3Fgseq+.%0D%0A+++%7D%0D%0AGROUP+BY+%3Fgseq%0D%0ALIMIT+++1%0D%0A%0D%0A%0D%0A&format=text%2Fhtml&timeout=0&debug=on