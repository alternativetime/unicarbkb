package controllers;

import models.Structure;
import models.Translation;
import models.core.GlycanSequence;
import models.hmg.Hmg;
import models.immuno.IgStructure;
import models.rdf.RdfEpitopes;
import models.unicorn.Nlink;
import models.unicorn.Olink;
import org.eurocarbdb.application.glycanbuilder.*;
import org.expasy.sugarconverter.parser.IupacParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matthew on 27/05/15.
 */
public class Image extends Controller {
    public Result showImage(Long id, String notation, String extended) throws Exception {

        //GlycanWorkspace glycanWorkspace = null;

        //MonosaccharideConversion monosaccharideConverter = null;


        /*String structure = "RES\n" +
                "1b:b-dgal-HEX-1:5\n" +
                "2b:a-lgal-HEX-1:5|6:d\n" +
                "LIN\n" +
                "1:1o(2+1)2d";
*/
        //Get a reference to the renderer
        //BuilderWorkspace workspace = new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        // GlycanRenderer renderer = workspace.getGlycanRenderer();

        //Get an instance of the GlycoCT Parser
        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Structure.find.byId(id).glycanst;

        System.out.println("glycanst: " + structure);


        String style = extended;
        String format = "png";


        if(structure.contains("--")) {


            if(notation.matches("cfgl")) {
                notation = "cfg";
            }
            if(notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if(notation.matches("gs")) {
                notation = "iupac";
            }

            // parse sequence and create Sugar object



            List<Glycan> glycanList = new ArrayList<Glycan>();

            String op = MassOptions.NO_DERIVATIZATION;

            String cloud = "-2H";


            MassOptions masso = new MassOptions();
            IonCloud ic = new IonCloud(cloud);
            //ic.add("-", 1);

            masso.ION_CLOUD.clear();
            masso.setDerivatization(op);

            String redEnd = "redEnd";//"redEnd";
            masso.setReducingEndTypeString(redEnd);


            //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
            //Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
            org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
glycan.withNoReducingEndModification();
            //glycan.computeMass();
                    //parser.readGlycan(structure, masso);


            //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

            // configure the image settings
            if (notation == null || notation.equalsIgnoreCase("cfg"))
                workspace.setNotation(GraphicOptions.NOTATION_CFG);
            else if (notation.equalsIgnoreCase("cfgbw"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
            else if (notation.equalsIgnoreCase("cfg-uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
            else if (notation.equalsIgnoreCase("uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXF);
            else if (notation.equalsIgnoreCase("uoxf-color"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
            else if (notation.equalsIgnoreCase("iupac"))
                workspace.setNotation(GraphicOptions.NOTATION_TEXT);
            else
                throw new IllegalArgumentException("Notation " + notation + " is not supported");

            if (style == null || style.equalsIgnoreCase("compact"))
                workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
            else if (style.equalsIgnoreCase("normal"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
            else if (style.equalsIgnoreCase("extended"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
            else
                throw new IllegalArgumentException("Notation style " + style + " is not supported");


            if (format == null)
                format = "png";
            ByteArrayOutputStream byteArrayOutputStream;
            if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                // create a buffered image of scale 1
                BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 4);


                byteArrayOutputStream = new ByteArrayOutputStream();
                if (format.equalsIgnoreCase("png"))
                    ImageIO.write(img, "png", byteArrayOutputStream);
                else
                    ImageIO.write(img, "jpg", byteArrayOutputStream);
                return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                //return byteArrayOutputStream.toByteArray();
            }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
            else {
                throw new IllegalArgumentException("Image format " + format + " is not supported");
            }

            //return byteArrayOutputStream.toByteArray();
        } else {

            System.out.println("ERROR CHECKING HERE ########## " + structure);

            List<Translation> translation = Structure.find.byId(id).translation;


            System.out.println("CHECK TRANSLATION: " + translation);

            if(notation.matches("cfgl")) {
                notation = "cfg";
            }
            if(notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if(notation.matches("gs")) {
                notation = "iupac";
            }


            for(Translation t : translation){

                System.out.println("IN LOOP TEST");



                //Translation translation = Translation.findIupac(Structure.find.byId(id).glycanst);

                //org.expasy.sugarconverter.parser.IupacParser p = new org.expasy.sugarconverter.parser.IupacParser(t.iupac.trim());

                //p.setGlycanType("O-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)

                try {

                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(t.gws);
                    glycan.withNoReducingEndModification();
                    System.out.println("BROKEN " + glycan);
                    //p.getCtTree(p.parse());
                    MassOptions masso = new MassOptions();
                    //Glycan glycan = parser.readGlycan(p.getCtSequence(), masso);

                    System.out.println("SEQ: " + t.gws);
                    //Glycan glycan = parser.readGlycan(t.gws, masso);


                    if (notation == null || notation.equalsIgnoreCase("cfg"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFG);
                    else if (notation.equalsIgnoreCase("cfgbw"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
                    else if (notation.equalsIgnoreCase("cfg-uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
                    else if (notation.equalsIgnoreCase("uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXF);
                    else if (notation.equalsIgnoreCase("uoxf-color"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
                    else if (notation.equalsIgnoreCase("iupac"))
                        workspace.setNotation(GraphicOptions.NOTATION_TEXT);
                    else
                        throw new IllegalArgumentException("Notation " + notation + " is not supported");

                    if (style == null || style.equalsIgnoreCase("compact"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
                    else if (style.equalsIgnoreCase("normal"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
                    else if (style.equalsIgnoreCase("extended"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
                    else
                        throw new IllegalArgumentException("Notation style " + style + " is not supported");

                    if (format == null)
                        format = "png";
                    ByteArrayOutputStream byteArrayOutputStream;
                    if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                        // create a buffered image of scale 1
                        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 1);


                        byteArrayOutputStream = new ByteArrayOutputStream();
                        if (format.equalsIgnoreCase("png"))
                            ImageIO.write(img, "png", byteArrayOutputStream);
                        else
                            ImageIO.write(img, "jpg", byteArrayOutputStream);
                        if(byteArrayOutputStream.size()>0) {
                            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                        } else {
                            return ok("/images/gs/_@{structureId}.png");
                        }
                    }


                } catch (Exception ex) {
                    System.err.println("Problem parsing the sequence");
                    ex.printStackTrace();
                }
            }
            throw new IllegalArgumentException("Image format is not supported");
        }
    }



    public Result showImageUnicorn(Long id, String notation, String extended) throws Exception {

        //GlycanWorkspace glycanWorkspace = null;

        //MonosaccharideConversion monosaccharideConverter = null;


        /*String structure = "RES\n" +
                "1b:b-dgal-HEX-1:5\n" +
                "2b:a-lgal-HEX-1:5|6:d\n" +
                "LIN\n" +
                "1:1o(2+1)2d";
*/
        //Get a reference to the renderer
        //BuilderWorkspace workspace = new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Nlink.find.byId(id).glycoct;
        String style = extended;
        String format = "png";



            if(notation.matches("cfgl")) {
                notation = "cfg";
            }
            if(notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if(notation.matches("gs")) {
                notation = "iupac";
            }

            // parse sequence and create Sugar object



            List<Glycan> glycanList = new ArrayList<Glycan>();

            String op = MassOptions.NO_DERIVATIZATION;

            String cloud = "";


            MassOptions masso = new MassOptions();
            IonCloud ic = new IonCloud(cloud);
            //ic.add("-", 1);

            masso.ION_CLOUD.clear();
            masso.setDerivatization(op);

            String redEnd = "freeEnd";
            masso.setReducingEndTypeString(redEnd);


            //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
            Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
            //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
           // glycan.setReducingEndType()
            //parser.readGlycan(structure, masso);


            //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

            // configure the image settings
            if (notation == null || notation.equalsIgnoreCase("cfg"))
                workspace.setNotation(GraphicOptions.NOTATION_CFG);
            else if (notation.equalsIgnoreCase("cfgbw"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
            else if (notation.equalsIgnoreCase("cfg-uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
            else if (notation.equalsIgnoreCase("uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXF);
            else if (notation.equalsIgnoreCase("uoxf-color"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
            else if (notation.equalsIgnoreCase("iupac"))
                workspace.setNotation(GraphicOptions.NOTATION_TEXT);
            else
                throw new IllegalArgumentException("Notation " + notation + " is not supported");

            if (style == null || style.equalsIgnoreCase("compact"))
                workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
            else if (style.equalsIgnoreCase("normal"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
            else if (style.equalsIgnoreCase("extended"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
            else
                throw new IllegalArgumentException("Notation style " + style + " is not supported");

            if (format == null)
                format = "png";
            ByteArrayOutputStream byteArrayOutputStream;
            if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                // create a buffered image of scale 1
                BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);


                byteArrayOutputStream = new ByteArrayOutputStream();
                if (format.equalsIgnoreCase("png"))
                    ImageIO.write(img, "png", byteArrayOutputStream);
                else
                    ImageIO.write(img, "jpg", byteArrayOutputStream);
                return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                //return byteArrayOutputStream.toByteArray();
            }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
            else {
                throw new IllegalArgumentException("Image format " + format + " is not supported");
            }

            //return byteArrayOutputStream.toByteArray();

    }

    public Result showImageHmg(Long id, String notation, String extended) throws Exception {

        //GlycanWorkspace glycanWorkspace = null;

        //MonosaccharideConversion monosaccharideConverter = null;


        /*String structure = "RES\n" +
                "1b:b-dgal-HEX-1:5\n" +
                "2b:a-lgal-HEX-1:5|6:d\n" +
                "LIN\n" +
                "1:1o(2+1)2d";
*/
        //Get a reference to the renderer
        //BuilderWorkspace workspace = new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Hmg.find.byId(id).glycoct;
        String style = extended;
        String format = "png";



        if(notation.matches("cfgl")) {
            notation = "cfg";
        }
        if(notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if(notation.matches("gs")) {
            notation = "iupac";
        }

        // parse sequence and create Sugar object



        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
        //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        // glycan.setReducingEndType()
        //parser.readGlycan(structure, masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

        //return byteArrayOutputStream.toByteArray();

    }


    public Result showImageTCD(Long id, String notation, String extended) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Olink.find.byId(id).glycoct;
        String style = extended;
        String format = "png";

        if(notation.matches("cfgl")) {
            notation = "cfg";
        }
        if(notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if(notation.matches("gs")) {
            notation = "iupac";
        }

        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";

        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);

        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);

        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

        //return byteArrayOutputStream.toByteArray();

    }

    public Result showImageIM(Long id, String notation, String extended) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        // GlycanRenderer renderer = workspace.getGlycanRenderer();

        //Get an instance of the GlycoCT Parser
        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Structure.find.byId(id).glycanst;
        String style = "";
        if(notation.isEmpty()) {
            style = "cfgl";
        } else {
            style = extended;
        }
        String format = "png";


        if (structure.contains("--")) {


            if (notation.matches("cfgl")) {
                notation = "cfg";
            }
            if (notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if (notation.matches("gs")) {
                notation = "iupac";
            }

            // parse sequence and create Sugar object


            List<Glycan> glycanList = new ArrayList<Glycan>();

            String op = MassOptions.NO_DERIVATIZATION;

            String cloud = "-2H";


            MassOptions masso = new MassOptions();
            IonCloud ic = new IonCloud(cloud);
            //ic.add("-", 1);

            masso.ION_CLOUD.clear();
            masso.setDerivatization(op);

            String redEnd = "redEnd";//"redEnd";
            masso.setReducingEndTypeString(redEnd);


            //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
            //Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
            org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
            glycan.withNoReducingEndModification();
            //glycan.computeMass();
            //parser.readGlycan(structure, masso);


            //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

            // configure the image settings
            if (notation == null || notation.equalsIgnoreCase("cfg"))
                workspace.setNotation(GraphicOptions.NOTATION_CFG);
            else if (notation.equalsIgnoreCase("cfgbw"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
            else if (notation.equalsIgnoreCase("cfg-uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
            else if (notation.equalsIgnoreCase("uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXF);
            else if (notation.equalsIgnoreCase("uoxf-color"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
            else if (notation.equalsIgnoreCase("iupac"))
                workspace.setNotation(GraphicOptions.NOTATION_TEXT);
            else
                throw new IllegalArgumentException("Notation " + notation + " is not supported");

            if (style == null || style.equalsIgnoreCase("compact"))
                workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
            else if (style.equalsIgnoreCase("normal"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
            else if (style.equalsIgnoreCase("extended"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
            else
                throw new IllegalArgumentException("Notation style " + style + " is not supported");


            if (format == null)
                format = "png";
            ByteArrayOutputStream byteArrayOutputStream;
            if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                // create a buffered image of scale 1
                BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 1);


                byteArrayOutputStream = new ByteArrayOutputStream();
                if (format.equalsIgnoreCase("png"))
                    ImageIO.write(img, "png", byteArrayOutputStream);
                else
                    ImageIO.write(img, "jpg", byteArrayOutputStream);
                return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                //return byteArrayOutputStream.toByteArray();
            }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
            else {
                throw new IllegalArgumentException("Image format " + format + " is not supported");
            }

            //return byteArrayOutputStream.toByteArray();
        } else {

            if (notation.matches("cfgl")) {
                notation = "cfg";
            }
            if (notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if (notation.matches("gs")) {
                notation = "iupac";
            }


                IupacParser p = new IupacParser(structure.trim());
                System.out.println("HELLO WORLD");
                p.setGlycanType("N-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)
                try {
                    p.getCtTree(p.parse());
                    MassOptions masso = new MassOptions();
                    Glycan glycan = parser.readGlycan(p.getCtSequence(), masso);


                    if (notation == null || notation.equalsIgnoreCase("cfg"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFG);
                    else if (notation.equalsIgnoreCase("cfgbw"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
                    else if (notation.equalsIgnoreCase("cfg-uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
                    else if (notation.equalsIgnoreCase("uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXF);
                    else if (notation.equalsIgnoreCase("uoxf-color"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
                    else if (notation.equalsIgnoreCase("iupac"))
                        workspace.setNotation(GraphicOptions.NOTATION_TEXT);
                    else
                        throw new IllegalArgumentException("Notation " + notation + " is not supported");

                    if (style == null || style.equalsIgnoreCase("compact"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
                    else if (style.equalsIgnoreCase("normal"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
                    else if (style.equalsIgnoreCase("extended"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
                    else
                        throw new IllegalArgumentException("Notation style " + style + " is not supported");

                    if (format == null)
                        format = "png";
                    ByteArrayOutputStream byteArrayOutputStream;
                    if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                        // create a buffered image of scale 1
                        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 4);


                        byteArrayOutputStream = new ByteArrayOutputStream();
                        if (format.equalsIgnoreCase("png"))
                            ImageIO.write(img, "png", byteArrayOutputStream);
                        else
                            ImageIO.write(img, "jpg", byteArrayOutputStream);
                        if (byteArrayOutputStream.size() > 0) {
                            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                        } else {
                            return ok("/images/gs/_@{structureId}.png");
                        }
                    }


                } catch (Exception ex) {
                    System.err.println("Problem parsing the sequence");
                    ex.printStackTrace();
                }

            throw new IllegalArgumentException("Image format is not supported");
        }
    }



    public Result showImageImmuno(Long id, String notation, String extended) throws Exception {

        //GlycanWorkspace glycanWorkspace = null;

        //MonosaccharideConversion monosaccharideConverter = null;


        /*String structure = "RES\n" +
                "1b:b-dgal-HEX-1:5\n" +
                "2b:a-lgal-HEX-1:5|6:d\n" +
                "LIN\n" +
                "1:1o(2+1)2d";
*/
        //Get a reference to the renderer
        //BuilderWorkspace workspace = new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = IgStructure.find.byId(id).ct;
        String style = "normal";
        String format = "png";



        if(notation.matches("cfgl")) {
            notation = "cfg-uoxf";
        }
        if(notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if(notation.matches("gs")) {
            notation = "iupac";
        }

        // parse sequence and create Sugar object



        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
        //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        // glycan.setReducingEndType()
        //parser.readGlycan(structure, masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

    }


    public Result showImageUniCarbDB(Long id, String notation, String extended) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg");
        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = GlycanSequence.find.byId(id).sequenceGWS;
        String style = extended;
        String format = "png";

        if (notation.matches("cfgl")) {
            notation = "cfg";
        }
        if (notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if (notation.matches("gs")) {
            notation = "iupac";
        }

        String op = MassOptions.NO_DERIVATIZATION;
        String cloud = "";

        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "redEnd";
        masso.setReducingEndTypeString(redEnd);

        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);

        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {

            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 1);

            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        } else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

    }



    public Result showImageCRC(Long id, String notation, String extended) throws Exception {


        //GlycanWorkspace glycanWorkspace = null;

        //MonosaccharideConversion monosaccharideConverter = null;


        /*String structure = "RES\n" +
                "1b:b-dgal-HEX-1:5\n" +
                "2b:a-lgal-HEX-1:5|6:d\n" +
                "LIN\n" +
                "1:1o(2+1)2d";
*/
        //Get a reference to the renderer
        //BuilderWorkspace workspace = new BuilderWorkspace();
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        // GlycanRenderer renderer = workspace.getGlycanRenderer();

        //Get an instance of the GlycoCT Parser
        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = Structure.find.byId(id).glycanst;


        String style = extended;
        String format = "png";


        if (structure.contains("--")) {


            if (notation.matches("cfgl")) {
                notation = "cfg";
            }
            if (notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if (notation.matches("gs")) {
                notation = "iupac";
            }

            // parse sequence and create Sugar object


            List<Glycan> glycanList = new ArrayList<Glycan>();

            String op = MassOptions.NO_DERIVATIZATION;

            String cloud = "-2H";


            MassOptions masso = new MassOptions();
            IonCloud ic = new IonCloud(cloud);
            //ic.add("-", 1);

            masso.ION_CLOUD.clear();
            masso.setDerivatization(op);

            String redEnd = "redEnd";//"redEnd";
            masso.setReducingEndTypeString(redEnd);


            //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
            //Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
            org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
            glycan.withNoReducingEndModification();
            //glycan.computeMass();
            //parser.readGlycan(structure, masso);


            //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

            // configure the image settings
            if (notation == null || notation.equalsIgnoreCase("cfg"))
                workspace.setNotation(GraphicOptions.NOTATION_CFG);
            else if (notation.equalsIgnoreCase("cfgbw"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
            else if (notation.equalsIgnoreCase("cfg-uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
            else if (notation.equalsIgnoreCase("uoxf"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXF);
            else if (notation.equalsIgnoreCase("uoxf-color"))
                workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
            else if (notation.equalsIgnoreCase("iupac"))
                workspace.setNotation(GraphicOptions.NOTATION_TEXT);
            else
                throw new IllegalArgumentException("Notation " + notation + " is not supported");

            if (style == null || style.equalsIgnoreCase("compact"))
                workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
            else if (style.equalsIgnoreCase("normal"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
            else if (style.equalsIgnoreCase("extended"))
                workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
            else
                throw new IllegalArgumentException("Notation style " + style + " is not supported");


            if (format == null)
                format = "png";
            ByteArrayOutputStream byteArrayOutputStream;
            if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                // create a buffered image of scale 1
                BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


                byteArrayOutputStream = new ByteArrayOutputStream();
                if (format.equalsIgnoreCase("png"))
                    ImageIO.write(img, "png", byteArrayOutputStream);
                else
                    ImageIO.write(img, "jpg", byteArrayOutputStream);
                return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                //return byteArrayOutputStream.toByteArray();
            }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
            else {
                throw new IllegalArgumentException("Image format " + format + " is not supported");
            }

            //return byteArrayOutputStream.toByteArray();
        } else {

            System.out.println("ERROR CHECKING HERE ########## " + structure);

            List<Translation> translation = Structure.find.byId(id).translation;


            System.out.println("CHECK TRANSLATION: " + translation);

            if (notation.matches("cfgl")) {
                notation = "cfg";
            }
            if (notation.matches("uoxf")) {
                notation = "uoxf";
            }
            if (notation.matches("gs")) {
                notation = "iupac";
            }


            for (Translation t : translation) {

                System.out.println("IN LOOP TEST");


                //Translation translation = Translation.findIupac(Structure.find.byId(id).glycanst);

                //org.expasy.sugarconverter.parser.IupacParser p = new org.expasy.sugarconverter.parser.IupacParser(t.iupac.trim());

                //p.setGlycanType("O-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)

                try {

                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(t.gws);
                    glycan.withNoReducingEndModification();
                    System.out.println("BROKEN " + glycan);
                    //p.getCtTree(p.parse());
                    MassOptions masso = new MassOptions();
                    //Glycan glycan = parser.readGlycan(p.getCtSequence(), masso);

                    System.out.println("SEQ: " + t.gws);
                    //Glycan glycan = parser.readGlycan(t.gws, masso);


                    if (notation == null || notation.equalsIgnoreCase("cfg"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFG);
                    else if (notation.equalsIgnoreCase("cfgbw"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
                    else if (notation.equalsIgnoreCase("cfg-uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
                    else if (notation.equalsIgnoreCase("uoxf"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXF);
                    else if (notation.equalsIgnoreCase("uoxf-color"))
                        workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
                    else if (notation.equalsIgnoreCase("iupac"))
                        workspace.setNotation(GraphicOptions.NOTATION_TEXT);
                    else
                        throw new IllegalArgumentException("Notation " + notation + " is not supported");

                    if (style == null || style.equalsIgnoreCase("compact"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
                    else if (style.equalsIgnoreCase("normal"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
                    else if (style.equalsIgnoreCase("extended"))
                        workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
                    else
                        throw new IllegalArgumentException("Notation style " + style + " is not supported");

                    if (format == null)
                        format = "png";
                    ByteArrayOutputStream byteArrayOutputStream;
                    if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
                        // create a buffered image of scale 1
                        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


                        byteArrayOutputStream = new ByteArrayOutputStream();
                        if (format.equalsIgnoreCase("png"))
                            ImageIO.write(img, "png", byteArrayOutputStream);
                        else
                            ImageIO.write(img, "jpg", byteArrayOutputStream);
                        if (byteArrayOutputStream.size() > 0) {
                            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
                        } else {
                            return ok("/images/gs/_@{structureId}.png");
                        }
                    }


                } catch (Exception ex) {
                    System.err.println("Problem parsing the sequence");
                    ex.printStackTrace();
                }
            }
            throw new IllegalArgumentException("Image format is not supported");
        }
    }


    public Result showImageEpitope(Long id, String notation, String extended) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);

        String structure = RdfEpitopes.find.byId(id).glycoctRings;
        System.out.println("check: " + structure);
        String style = "extended";
        String format = "png";

        if(notation.matches("cfgl")) {
            notation = "cfg";
        }
        if(notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if(notation.matches("gs")) {
            notation = "iupac";
        }


        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";

        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);

        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);

        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

        //return byteArrayOutputStream.toByteArray();

    }

}
