package controllers;

import io.ebean.Ebean;
import models.Pubchem;
import models.Structure;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;


public class PubchemLinks extends Controller {

	public Result pubchemExternal(String pubchem) {

		List<Pubchem> pubchemResult = Pubchem.pubchemId(pubchem);
		Structure structure = null;
		Long structureId = 1234566L;
		for(Pubchem c : pubchemResult) {
			structure = Ebean.find(Structure.class, c.structure.id);
			structureId = c.structure.id;
		}
		//return redirect(structureDetails.render(structureId);
		return redirect(routes.Application.structureDetails(structureId) );	
	}
	
}

