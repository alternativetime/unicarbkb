package controllers;

import io.ebean.Ebean;
import io.ebean.SqlRow;
import models.composition_protein.Crcdata;
import models.composition_protein.Crcdatajenny;
import models.composition_protein.Shah;
import models.immuno.IgNormalised;
import models.immuno.IgProtein;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.cell_lines.crc;
import views.html.cell_lines.crc2;
import views.html.cell_lines.prostate;
import views.html.cell_lines.sameCompositionMatch;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by matthew on 13/09/15.
 */
public class Immuno extends Controller {

    public Result showCellLines(){
        return ok(views.html.cell_lines.summaryCellLines.render());
    }

    public Result showImmunoStructure(){

        List<IgProtein> immunoProteins = IgProtein.find.all();
        //List<IgStructure> igStructureList = IgStructure.find.all();
        //return ok(views.html.immuno.summary.render(immunoProteins));
        return ok("temp");
    }

    public Result showNormalised(){
        List<IgNormalised> normalised = IgNormalised.find.all();
        //return ok(views.html.immuno.normalisedData.render(normalised));
        return ok("temp");
    }

    public Result showShah(int page, String sortBy, String order, String filter, String protein) throws UnsupportedEncodingException {

        List<Shah> proteinList =
                Ebean.find(Shah.class)
                        .setDistinct(true)
                        .select("protein")
                        .findList();

        if(StringUtils.isNotEmpty(request().queryString().toString())) {
            Map<String, String[]> params = request().queryString();

            //System.out.println("query string +++ " + request() );
            String filterOption = request().toString();
            filterOption = filterOption.replace("/prostate?", "");
            System.out.println("updated filteroption " + java.net.URLDecoder.decode(filterOption, "UTF-8"));
            String[] searchTerms = null;
            String key = null;
            ArrayList<Shah> proteinSearch = new ArrayList<Shah>();
            HashSet<String> proteins = new HashSet<String>();
            List<Shah> filteredHash = new ArrayList<Shah>();

            for (Map.Entry<String, String[]> entry : params.entrySet()) {
                key = entry.getKey();
                searchTerms = entry.getValue();

                String filterOn = "";
                filterOn = Arrays.toString(params.get(key)).replace("[", "").replace("]", "");
                if (filterOn.length() > 2) {
                    System.out.println("terms=====> " + filterOn);
                    proteins.add(filterOn);
                }
            }

            String queryProtein = "select id, name, swiss_prot, description from public.proteins where ";
            int count = 0;
            if (proteins.size() == 1) {
                for (String proteinlookup : proteins) {
                    String[] split = proteinlookup.split(",");
                    for (String s : split) {

                        List<Shah> filteredProteins = Shah.find.query().where().like("protein", s).findList();
                        System.out.println("check this ===> " + filteredProteins.size());
                        for(Shah shah : filteredProteins){
                            filteredProteins.add(shah);
                            System.out.println("checking " + shah.peptide);
                        }

                    }
                }
            }
        }




            return ok(
                    prostate.render(
                            Shah.page(page, 10, sortBy, order, filter),
                            sortBy, order, filter, proteinList
                    )
            );


    }

    public Result showCRC(int page, String sortBy, String order, String filter){
        return ok(
                crc.render(
                        Crcdatajenny.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    /*
    manveen data
    need to merge and handle this data
     */
    public Result showCRC2(int page, String sortBy, String order, String filter){

        return ok(
                crc2.render(
                        Crcdata.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    public Result showSameCompositionsCRC(){
        List<SqlRow> list = Crcdata.groupCompositionsCrc();
        return ok(sameCompositionMatch.render(list));
    }



}
