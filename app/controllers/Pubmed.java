package controllers;

import io.ebean.Ebean;
import models.Disease;
import models.Method;
import models.TissueTaxonomy;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.pubmed;

import java.util.List;

//import play.db.ebean.*;


public class Pubmed extends Controller {

	public Result getpubmed() {

		List<Method> method = Ebean.find(Method.class).findList();
		List<TissueTaxonomy> tissue = Ebean.find(TissueTaxonomy.class).findList();
                List<Disease> disease = Ebean.find(Disease.class).findList();
		return ok(pubmed.render("pubmed", method, tissue, disease) );	

	}







	
}

