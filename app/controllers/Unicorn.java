package controllers;

import com.google.inject.Inject;
import models.unicorn.Nlink;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


/**
 * Created by matthew on 14/06/15.
 */
public class Unicorn extends Controller {

    @Inject
	private FormFactory formFactory;

    public Result maninUnicorn(){
        Nlink.countMonoNlink();
        return ok("job done");
    }

    public Result showStructures() {
        List<Nlink> nlinks = Nlink.find.query().setMaxRows(10).findList();
        int count = Nlink.find.query().findCount();
        return ok(views.html.unicorn.showUnicornStructures.render(nlinks, count));
    }

    public Result searchCompositionUnicorn(){
        String iupac = "";
        double mass = 0.0;
        int gal = 0;
        int fuc = 0;
        int kdn = 0;
        int man = 0;
        int glcnac = 0;
        int galnac = 0;
        int neuac = 0;
        int neugc = 0;
        int hexa = 0;

        int gal2 = 0;
        int fuc2 = 0;
        int kdn2 = 0;
        int man2 = 0;
        int glcnac2 = 0;
        int galnac2 = 0;
        int neuac2 = 0;
        int neugc2 = 0;
        int hexa2 = 0;

        String se_gal = "";
        String se_fuc = "";
        String se_kdn = "";
        String se_man = "";
        String se_glcnac = "";
        String se_galnac = "";
        String se_neuac = "";
        String se_neugc = "";
        String se_hexa = "";

        DynamicForm requestData = formFactory.form().bindFromRequest();

        iupac = requestData.get("IUPAC");
        mass = Double.valueOf(requestData.get("mass"));

        gal = Integer.parseInt(requestData.get("comp_Gal"));
        fuc = Integer.parseInt(requestData.get("comp_Fuc"));
        kdn = Integer.parseInt(requestData.get("comp_Kdn"));
        man = Integer.parseInt(requestData.get("comp_Man"));
        glcnac = Integer.parseInt(requestData.get("comp_Glcnac"));
        galnac = Integer.parseInt(requestData.get("comp_Galnac"));
        neuac = Integer.parseInt(requestData.get("comp_NeuAc"));
        neugc = Integer.parseInt(requestData.get("comp_NeuGc"));
        hexa = Integer.parseInt(requestData.get("comp_HexA"));

        gal2 = Integer.parseInt(requestData.get("comp_Gal2"));
        fuc2 = Integer.parseInt(requestData.get("comp_Fuc2"));
        kdn2 = Integer.parseInt(requestData.get("comp_Kdn2"));
        man2 = Integer.parseInt(requestData.get("comp_Man"));
        glcnac2 = Integer.parseInt(requestData.get("comp_Glcnac2"));
        galnac2 = Integer.parseInt(requestData.get("comp_Galnac2"));
        neuac2 = Integer.parseInt(requestData.get("comp_NeuAc"));
        neugc2 = Integer.parseInt(requestData.get("comp_NeuGc"));
        hexa2 = Integer.parseInt(requestData.get("comp_HexA"));

        se_gal = String.valueOf(requestData.get("sel_gal"));
        se_fuc = String.valueOf(requestData.get("sel_fuc"));
        se_kdn = String.valueOf(requestData.get("sel_kdn"));
        se_man = String.valueOf(requestData.get("sel_man"));
        se_glcnac = String.valueOf(requestData.get("sel_glcnac"));
        se_galnac = String.valueOf(requestData.get("sel_galnac"));
        se_neuac = String.valueOf(requestData.get("sel_neuac"));
        se_neugc = String.valueOf(requestData.get("sel_neugc"));
        se_hexa = String.valueOf(requestData.get("sel_hexa"));

        List<Nlink> nlinkComp = null;

        int count = 0;
        if (iupac.length()>1 && mass <= 0.0) {
            count = Nlink.iupacUnicornSearchCount(iupac);
            nlinkComp = Nlink.iupacUnicornSearch(iupac, count);

        } else if (iupac.length()<1 && mass <= 0.0  ) {
              count = Nlink.getCompositionUnicornMatchCount(gal, fuc, man, glcnac, galnac, kdn, gal2, fuc2, man2, glcnac2, galnac2, kdn2, se_gal, se_fuc, se_man, se_glcnac, se_galnac, se_kdn);
            nlinkComp = Nlink.getCompositionMatchUnicorn(count, gal, fuc, man, glcnac, galnac, kdn, neuac, neugc, hexa, gal2, fuc2, man2, glcnac2, galnac2, kdn2, neuac2, neugc2, hexa2, se_gal, se_fuc, se_man, se_glcnac, se_galnac, se_kdn, se_neuac, se_neugc, se_hexa);

        } else if (iupac.length()<1 && mass > 0.0) {
            nlinkComp = Nlink.massUnicornSearch(mass);
        }

        return ok(views.html.unicorn.searchUnicornCompositions.render(nlinkComp, count));

    }

    public CompletionStage<Result> searchNlinkRDF(Long id) {

        Nlink nlinkstr = Nlink.find.byId(id);
        //int count = Hmg.searchNRDF(id);
        //TODO keep track below

        CompletionStage<List<String>> promiseOfInt = CompletableFuture.supplyAsync(
                () -> Nlink.searchNRDF(id)
        );
        return promiseOfInt.thenApplyAsync(
                i -> ok(views.html.unicorn.unicornStructure.render(nlinkstr, promiseOfInt))
        );
    }
}
