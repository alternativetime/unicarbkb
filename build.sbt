
name := "UniCarbKB"

version := "1.0"

initialize := {
  val _ = initialize.value
  if (sys.props("java.specification.version") != "1.8")
    sys.error("Java 8 is required for this project.")
}

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)
conflictWarning := ConflictWarning.disable

//scalaVersion := "2.11.7"
scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
    javaCore,
    javaJdbc, filters, ehcache, javaWs,
    "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
    "commons-io" % "commons-io" % "2.5",
    "org.apache.commons" % "commons-lang3" % "3.6",
    "com.fasterxml.jackson.core" % "jackson-core" % "2.6.1",
    //"pl.matisoft" %% "swagger-play24" % "1.4",
    //"io.swagger" % "swagger-play2_2.11" % "1.5.3",
    //"org.webjars" %% "webjars-play" % "2.5.0-4",
    "org.webjars" % "webjars-play_2.11" % "2.6.2",
     "org.webjars" % "bootstrap" % "3.1.1-2",
    //"org.webjars" % "swagger-ui" % "2.2.0",
    //"com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.9.1",
    "com.googlecode.json-simple" % "json-simple" % "1.1.1",
    "org.apache.jena" % "jena-core" % "3.4.0", "org.apache.jena" % "jena-base" % "3.4.0", "org.apache.jena" % "jena-arq"  % "3.4.0",
    "org.codehaus.jackson" % "jackson-jaxrs" % "1.9.13",
    "org.apache.commons" % "commons-csv" % "1.5",
    "org.apache.poi" % "poi" % "3.17",
    "org.apache.poi" % "poi-ooxml" % "3.17",
    "org.apache.poi" % "openxml4j" % "1.0-beta"


)

libraryDependencies += guice
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.0"
//libraryDependencies += "com.typesafe.play" %% "play-iteratees" % "2.6.1"
//libraryDependencies += "com.typesafe.play" %% "play-iteratees-reactive-streams" % "2.6.1"
//dependencyOverrides += "com.typesafe.play" %% "play-guice" % "2.6.5"

libraryDependencies += "com.google.guava" % "guava" % "19.0"


/*
dependencyOverrides += "org.scala-lang" % "scala-compiler" % scalaVersion.value
dependencyOverrides += "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.6.1"
  dependencyOverrides += "org.apache.httpcomponents" % "httpclient" % "4.3.4"
  dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-annotations" % "2.6.0"
  dependencyOverrides += "com.google.guava" % "guava" % "18.0"
  dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.6.1"
  dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.1"
  dependencyOverrides += "junit" % "junit" % "4.11"
  dependencyOverrides += "org.apache.httpcomponents" % "httpcore" % "4.3.2"
  dependencyOverrides += "commons-logging" % "commons-logging" % "1.1.3"
  dependencyOverrides += "org.hamcrest" % "hamcrest-core" % "1.3"
  dependencyOverrides += "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.3"
  dependencyOverrides += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.3"

*/

//dependencyOverrides += "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.3"
//dependencyOverrides += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.3"


//filterScalaLibrary := false // include scala library in output

//dependencyDotFile := file("dependencies.dot") //render dot file to `./dependencies.dot`

//fork in run := false
//routesGenerator := InjectedRoutesGenerator

//
// sbt-web configuration
// https://github.com/sbt/sbt-web
//

// Configure the steps of the asset pipeline (used in stage and dist tasks)
// rjs = RequireJS, uglifies, shrinks to one file, replaces WebJars with CDN
// digest = Adds hash to filename
// gzip = Zips all assets, Asset controller serves them automatically when client accepts them
//pipelineStages := Seq(rjs, digest, gzip)

//RjsKeys.paths += ("jsRoutes" -> ("/jsroutes" -> "empty:"))

val akkaVersion = "2.5.4"
dependencyOverrides ++= Set(
    "com.google.guava" % "guava" % "22.0",
    "org.slf4j" % "slf4j-api" % "1.7.25"
)