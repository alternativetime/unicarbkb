import models.unicorn.Nlink;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.List;

/**
 * Created by s2980369 on 28/4/17.
 */
public class unicorn {

    @Test
    public void updateMass(){
        final double galmass = 162.1424;
        final double fucmass = 146.1430;
        final double glcmass = 162.142;
        final double glcnacmass = 203.1950;
        final double galnacmass = 203.1950;
        final double kdnmass = 291.2579;
        final double manmass = 162.142;

        final double mgalmass = 162.0528;
        final double mfucmass = 146.0579;
        final double mglcmass = 162.0528;
        final double mglcnacmass = 203.0794;
        final double mgalnacmass = 203.0794;
        final double mkdnmass = 291.0954;
        final double mmanmass = 162.0528;

        List<Nlink> nlinks = (List<Nlink>) Nlink.find.query().setMaxRows(20);
        //List<Nlink> nlinks = Nlink.find.all();

        for(Nlink n : nlinks) {

            System.out.println("ID: " + n.id);

            Nlink nupdate = Nlink.find.byId(n.id);

            String str = StringUtils.lowerCase(nupdate.iupac);
            int cman = 0;
            int cgal = 0;
            int cfuc = 0;
            int cglcnac = 0;
            int ckdn = 0;
            int cgalnac = 0;

            cgalnac = StringUtils.countMatches(str, "galnac");
            str = str.replaceAll("galnac", "xxx");
            cman = StringUtils.countMatches(str, "man");
            cgal = StringUtils.countMatches(str, "gal");
            cfuc = StringUtils.countMatches(str, "fuc");
            cglcnac = StringUtils.countMatches(str, "glcnac");
            ckdn = StringUtils.countMatches(str, "neuac");

            n.man = cman;
            n.fuc = cfuc;
            n.glcnac = cglcnac;
            n.kdn = ckdn;
            n.gal = cgal;
            n.galnac = cgalnac;

            double naveragemass = (cgalnac * galnacmass) + (cman * manmass) + (cfuc * fucmass) + (cglcnac * glcnacmass) + ( ckdn * kdnmass) + ( cgal * galmass);
            n.averageMass = (naveragemass+18.01528);



            double nmonoemass = (cgalnac * mgalnacmass) + (cman * mmanmass) + (cfuc * mfucmass) + (cglcnac * mglcnacmass) + ( ckdn * mkdnmass) + ( cgal * mgalmass);
            n.monoMass = (nmonoemass+18.0105546);

            n.update();
            n.save();

            break;

        }
    }

}
